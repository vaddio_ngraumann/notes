Definitions:

Old Update: Update with signing and timestamp prior to Jan 1, 2020
New Update: Update with signing and timestamp after Jan 1, 2020

### Case 1: Old->Old, system time < 2020
Covered today.

### Case 2: Old->Old, system time >= 2020
This may be a common case in the field.
Factory reset or NTP update so that time is before 2020. Perform update.
Alternatively: Vaddio could re-sign the update with new, valid certificate.

### Case 3: Old->New, system time < 2020
Ensure new update certificate is valid after 2010 or so to cover products in the field today.

### Case 4: Old->New, system time >= 2020
This will work as long as the update's certificate has been updated.

### Case 5: New->New, system time < 2020
This will work as long as the update's certificate has been updated.
Unlikely case.

### Case 6: New->New, system time >= 2020
This will work as long as the update's certificate has been updated.

### Case 7: New->Old, system time < 2020
This will work by default (unlikely case since it would require NTP server with a time in the past).

### Case 8: New->Old, system time >= 2020
Unsure. Might need to reset the system time or other magic here. Maybe we can check if the error message indicates the certificate expired? Luckily we will have control over what happens here since the new update will require a release of software anyway.

Could also look into openssl 1.1.0's -no_check-time flag (easily backported to 1.0.2)

Note: CA cert expires 2030. We should re-create it before then (this works as long as the public key and DA are the same, see: https://serverfault.com/questions/306345/certification-authority-root-certificate-expiry-and-renewal)

### How-to's

Password for all private keys is "vaddio"

##### Regenerate root certificate

Make sure to enter fields exactly as shown in the current root cert (Vaddio-Engineering-CA/Vaddio-Engineering-CA.crt), so the DA's match. Otherwise validation will fail:
```
openssl req -text -key Vaddio-Engineering-CA/private/Vaddio-Engineering-CA.pem -new -x509 -extensions v3_ca -out newroot.pem -days 11323
```

Verify the existing update cert works with it:
```
openssl verify -CAfile newroot.pem Vaddio-Engineering-CA/certs/Vaddio-Engineering-Update-Signer.crt
```

As long as this says "OK", you can replace the existing root cert:
mv newroot.pem Vaddio-Engineering-CA/Vaddio-Engineering-CA.crt

Alternative method, copies over everything from the old cert:
```
openssl x509 -x509toreq -in Vaddio-Engineering-CA/Vaddio-Engineering-CA.crt -signkey Vaddio-Engineering-CA/private/Vaddio-Engineering-CA.pem -out newroot.csr
openssl x509 -in newroot.csr -out newroot.crt -signkey Vaddio-Engineering-CA/private/Vaddio-Engineering-CA.pem -req -days 11322
rm -f newroot.csr

openssl x509 -in newroot.crt -text -noout > newroot.pem
cat newroot.crt >> newroot.pem
rm -f newroot.crt
```
Then verify and copy newroot.pem as above.

##### Create new certificate using CA

When prompted, you cannot leave Common Name blank!
```
openssl req -new -key Vaddio-Engineering-CA/private/Vaddio-Engineering-Update-Signer.key -out certreq.pem
openssl ca -policy policy_anything -infiles certreq.pem -out Vaddio-Engineering-CA/certs/Vaddio-Engineering-Update-Signer.crt
```

rm -f certerq.pem

Referneces:
https://jamielinux.com/docs/openssl-certificate-authority/introduction.html
https://serverfault.com/questions/306345/certification-authority-root-certificate-expiry-and-renewal
https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs
https://sbabic.github.io/swupdate/signed_images.html
https://gist.github.com/Soarez/9688998

### Re-sign updates

./tools/build-scripts-ark/vng-containerized-build --downloads-dir ~/workspace/build/downloads --build-dir ~/workspace/build/sumo -m genericx86 -k -f openssl-native

```
bitbake openssl-native -caddto_recipe_sysroot
oe-run-native openssl-native openssl -h

# Extract data from smime
oe-run-native openssl-native openssl smime -verify -no_check_time -CAfile /var/build/poky/meta-vaddio-ark/recipes-vaddio/pki/files/CA/Vaddio-Engineering-CA/Vaddio-Engineering-CA.crt -inform DER -in

# Decrypt data


# Encrypt data

# Recreate smime
oe-run-native openssl-native openssl smime -sign -in vaddio-app-debug-image-zynq-vulcan-update-2.1.0.dat -signer /var/build/poky/meta-vaddio-ark/recipes-vaddio/pki/files/CA/Vaddio-Engineering-CA/certs/Vaddio-Engineering-Update-Signer.crt -inkey /var/build/poky/meta-vaddio-ark/recipes-vaddio/pki/files/CA/Vaddio-Engineering-CA/private/Vaddio-Engineering-Update-Signer-NoPass.key -binary -nodetach -outform DER > vaddio-app-debug-image-zynq-vulcan-update-2.1.0.new.p7m
```
