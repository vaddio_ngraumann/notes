## Setup

    cd ~/workspace/vng
    export REPO_PATH=`pwd`
    exprot METAVADDIO_URI="meta-milestone"
    ./tools/build-scripts/fetch morty

    cd poky
    git checkout morty

    . oe-init-build-env build-morty
    bitbake ...

## Cleanup

- [ ] procps/sysctl.conf: We appear to have some customizations here that need deeper investigation.
- [x] checkroot.sh had RTC set to 3 - we may want this change.
- [ ] kernel gpio customizations?

Updating to latest morty errors:
| Escaping /var/build/build/tmp/work/armv7a-neon-poky-linux-gnueabi/vng-clish/1.0.0-r0-147/built-in/sleep.man
| sed: can't read /var/build/build/tmp/work/armv7a-neon-poky-linux-gnueabi/vng-clish/1.0.0-r0-147/built-in/sleep.xml: No such file or directory
| WARNING: /var/build/build/tmp/work/armv7a-neon-poky-linux-gnueabi/vng-clish/1.0.0-r0-147/temp/run.do_install.21053:1 exit 2 from 'sed -i "/<!--AUTOGEN DETAIL-->/ r $manFile" $xmlFile'

#### Pyro

2017.3 seems to add a long delay to u-boot loading the bitstream, and we never
make it past "Starting kernel...". Pyro seems fine with 2017.2.

Did see this boot error:
[2017-11-30T23:28:54.128667] WARNING: Configuration file format is too old, syslog-ng is running in compatibility mode Please update it to use the syslog-ng 3.8

#### Rocko

Newer u-boot needs gcc 7 which means our old ti staging u-boot won't work.
Created new cam01-sitara board loosely based on beaglebone black and am335x_evm board.
Some issues: needed to pull in DDR config from Beaglebone. Certain io regs changed.
FAT12 didn't work - hung after MLO (SPL) tried to read u-boot image. Turned out this was fixed upstream: https://github.com/rcn-ee/ti-uboot/commit/b352caea752f9e840863ade43da67ff0272e8594

TODO to get Rocko changes into PR:
- [ ] python-cdecimal has build issues - with do_install, it fails for the 32-bit case. Without it, it succeeds for both 32 and 64 bit, but does not install the python module.
- [ ] Middleware does not start up (nginx appears fine).
- [ ] Jenkins fails to build due to fetching old poky it seems.
- [ ] Move volatiles initscripts to new format (/etc/default/volatiles/0x_blah)


## Update Checklist

This document is intended to be a starting point for someone looking to pull in a new version of Yocto, for example, from Pyro (2.3) to Rocko (2.4). These updates can be tricky because there are several types of changes which must be accounted for and dealt with:

- New versions of packages which may require different configuration or patches than older versions. This includes updates to the Linux kernel.
- Updates to the Yocto/Poky recipe metadata which may address build issues, add features, perform more QA checks, etc.

Not all of these changes will be obvious in that they may not cause a build failure or emit a warning or QA message. A good starting point is to read your new release's sections in Yocto's  [migration chapter](http://www.yoctoproject.org/docs/latest/ref-manual/ref-manual.html#migration). You will of course want to read all of the necessary sections between your current release and the target release if you are jumping several versions. However, it is usually easiest to follow this process one version at a time so that the number of errors and changes required is smaller.

In general, the steps to perform are as follows:

1. If you don't already, add an upstream remote for each upstream repository  for Poky and all of the metadata layers. For example, for Poky:
    cd <local poky repository>
    git remote add upstream git://git.yoctoproject.org/poky
    git fetch upstream
2. Repeat step 1 for other layers, such as meta-oe, meta-gplv2, meta-qt5, meta-xilinx, and meta-ti (this may not be an exhaustive list).
3. Ensure that each layer has a branch corresponding to the release ("morty", "pyro", etc). If not, you may need to use either the master branch or a branch from an older release if master is unstable or incompatible.
4. Push the new branches to the Bitbucket forks. For example, for Poky:
    git push origin <branch name>
5. Update the "branch" variable of the"fetch" script (tools/build-scripts/fetch) to fetch the branch name of the release.
6. Follow the migration guide, making any obvious changes or fixes.
7. Try a test build. There will likely be errors and/or QA warning messages which should be addressed. The most common errors include:
    - bbappend file trying to append non-existent recipe. This usually happens when a bbappend is trying to append a specific version, for example, initscripts_1.0.bbappend, but the recipe itself has changed versions. In this example, perhaps initscripts is now initscripts_1.1.bb. In general, it is good practice to use a specific version for a bbappend only if it contains changes specific to that version of the recipe or package. Otherwise, the '%' can be used. See the [Append Files](https://www.yoctoproject.org/docs/latest/bitbake-user-manual/bitbake-user-manual.html#append-bbappend-files) section of the Bitbake User Manual for more information.
    - Patches which must be refreshed to apply to newer versions of packages. For other packages, you can use a tool called Quilt to efficiently create and refresh patches (see [Using Quilt in your Workflow](http://www.yoctoproject.org/docs/2.5/dev-manual/dev-manual.html#using-a-quilt-workflow))

## gdb with gplv3 blacklist

Add this to local.conf:
    INCOMPATIBLE_LICENSE_pn-gdb = ""
    INCOMPATIBLE_LICENSE_pn-readline = ""

Comment out any PREFERRED_VERSION_readline's in distro.conf, etc.
Build gdb. You can add gdbserver to IMAGE_INSTALL for a server only.
