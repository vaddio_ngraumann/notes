# Comments:

1. 4K/30 HDMI Input and 4K/30 Output (loop through of input)

- Should be fine, but HDCP will add development time
    - How desirable is HDCP, could we drop initially and see if people ask for it?
    - Sounds like the original AV Bridge may not have HDCP (no HDMI out)
- FPGA utilization will go up (mainly BRAM) with the scalers needing to handle 4k resolution.

2. Dual USB 3 peripheral ports

- What are the use cases for the ports? Any requirements for UVC framerate etc. on both ports? What about audio?
- FPGA utilization will increase due to scalers and DMA needing to handle 4k.

3. RTMP/RTSP Streaming (H.264 )- Assume we would need to scaler to 1080p for IP stream or could we do low frame rate at 4K ?

- We cannot do H.264 in software alongside DEP unless we limit the resolution to 720p, even then two cores would be fully utilized leaving one for DEP and one for the rest of the system.
- If we purchase an IP core, we could decide what maximum resolution we would like (tradeoff of resource utilization).
- The EV part can do 4k30 both with h.264 and h.265 encoding, but R&D would need to be done to determine whether memory bandwidth with existing LPDDR4 would be adequate.

4. Stereo Mic/Line Input (with AEC) and Stereo Line Output

- Is this stereo balanced or unbalanced? The current hardware only supports stereo balanced.

5. Dante Audio Support (# channels -TBA)

- We can support 4 tx/4 rx flows with 1 core reserved and 8 tx/8 rx with 2 cores, tentatively.

6. Audio Matrix Mixer and Proportional Gain Sharing Mixer

- Anything new here vs. other existing products?

7. POE Powered
8. 1 Rack Unit enclosure

- This would allow the board to be substantially larger. Other things we'd like to add support for?

# Questions:

1. How many potential DEP flows could we handle based upon current ARM utilization w/H.264 encoder and all other functions? (critical feature)

- Zero. We need to buy a core or move to the EV part to support h.264 and DEP at the same time.
- One option would be to choose one or the other, either Dante or H.264 IP streaming.

2. Option to move audio mixing to A17 processor vs adding Sharc/audio daughter card to derivative (time vs cost saving)

- Assuming this means R5 processor?

3. Have we looked at potential of Adaptive Digital AEC on the ARM side or is Sharc option better. (time vs cost saving)

- I think Adaptive Digital had an ARM version? Should we reach out to them?
- Should we plan on adding the Sharc to the board and de-pop as a cost reduction?

4. Can we support 4K USB stream and what would frame rate be? (differentiating feature on market)

- We can support up to 4k30 (4:2:0) over both USB streams. I verified that they can operate simultaneously at that resolution.

5. What is the possibility of creating a USB 2.0 compressed stream using H.264 payload (verse trying to put Cast MJPEG pipeline). (desired feature)

- This would be a large amount of effort as we would need to plumb in support for h.264 in the Linux UVC stack; no support exists for this today.
- May make more sense to move to a slightly larger FPGA and include the Cast core in the fabric for MJPEG over USB 2.0.
