John Bonesio
John@bonesio.net

LFtraining
Penguin2014

## 9/11/17

Probably best book: Linux kernel Development 3rd edition by Robert Love
Linux Weather Forecast: developments coming in the near future

ucLibc needs a compiler configured especially for it.
Linaro has been making ARM support better - emDebian
crosstool-ng: One of the oldest ways to build cross-toolchains.

Kernel config: .config is used to generate files needed to build kernel; .config
is not actually read by make!
When you move to a newer kernel, config will pick default configs for new values which are not specified.

pr_fmt: Also displays name of module.
dmesg -w: Follow like tailf
CONFIG_PRINTK_TIME: Prints date and time in printk's.

devm allocations are managed interfaces. When your module unloads, it's freed
automatically (it is slower and adds extra overhead).

process id: tgid

PAGE_OFFSET: Place where kernel memory starts, usually is at 3G mark on 32-bit
(so userspace as 0-3G and kernel as 3-4G).
Normal/low memory - simply subtract/add page offset to go from memory addresses.
First 16M is DMA zone (some devices only have 24 bits of DMA access regs)
DMA32 range - same but for devices with only 32 bits of DMA addressing
Rest of memory is called high memory

- Physical address: address actually on memory
- Bus address: Device address itself (might be same, or mapped by iommu)
- User virtual addresses - addresses in userspace
- 1:1 mapping at top of memory (e.g. 1G): Kernel logical addresses
- Mapping at top of kernel mem for dyamic pages: Kernel virtual addresses

Use \__get_free_pages() when what you need is an actual page.
Otherwise, use kmalloc, even if what you need is the size of a page.
kmem_cache is nice when you need a special feature like slab poison.
kmam_cache is also nice when you have a lot of weirdly-sized objects to avoid
wasting memory.

## 9/12/17

In open() handler, recommend storing the minor number from the inode in the file struct for convencience.
dentry's are sort of 1st class citizens in the kernel - inodes are 2nd class; they come from the concept of filesystems.

In the task_struct, tgid is the actual pid corresponding to the userspace pid.
Pid is still important but maps to the specific task, not the whole process.

In process context, you can count on 2 things:
1. The current variable points to the process that made the system call.
2. You can count on pointers to userspace being something meaningful, and if
   you sleep, the process that made the system call will sleep.

In interrupt context, the virtual address space is whatever process was running
when the interrupt happened.

copy_to/from_user: Usually best for a couple-100 bytes, for larger sizes need
 to do something like locking pages.

mmap has many different use cases:
1. Shared libraries for applications (e.g. libc code, data, etc) - private setting - Application itself is mmap'd in and is loaded by the page fault handler.
2. Memory-mapped IO
3. Memory-mapped IO to a file (shared name)
4. Sharing memory between
5. Device driver in userspace

Pages for shared libraries use copy-on-write
You can't really share anonymous pages between applications unless they have a parent/child relationship

In remap_pfn_range, you usually change the vm_pgoff member. Not totally sure how this works yet.

## 9/13/17

node-name[@unit-address]: unit-address is probably not required, but strongly encouraged.
Property nodes can have labels, but rarely happens. ([label:] property-name;)
A reference to another node in a tree (&ref) is called a phandle.
When multiple / {}'s are defined, the second overwrites the first in the event of conflicts.
No way to subtract nodes - as far as the instructor knows.
If you have a bus and it doesn't need any special setup, you should use , simple-bus.
Aliases or labels shouldn't affect what things are called in /dev
Chosen is used when you can't pass a kernel command line due to register limitations.
* Important - some people use device trees to disable hardware (by omission or commenting out), but it's better to disable it via other means, since the hardware is still connected to the system.

Interrupts in /proc/interrupts which have names but no numbers are controlled by the kernel, and you can't install a handler for that interrupt.
dev_id in irqaction is an address that's unique to your driver - usually memory allocated with kmalloc.

gettimeofday() is preferred over time() and ftime() since it handles time zone changes, DST, and other corner cases.
setitimer() is recommended over alarm() (they can conflict with each other on some systems).
sleep_interruptible: Means a task can wake from a signal (software interruptible in this case, no relation to hardware interrupts) - an example is kill -9 works for interruptible sleep, but not the other.

kprobes: Good for dumping a bunch of data at a specific point - for example if you hit a weird bug or something and need to know the state of the system. ftrace is better if you want to know how you got to that point.

kset: a list (basically) of kobjects, usually in a host controller

wait_event: The condition only prevents certain race conditions while going to
sleep - it is not a substitute for wake_up!

poll_wait() saves the tasks's state

## 9/14/17

Don't assume that interrupts are disabled in your top half - then if you ever use the realtime extensions, it'll still work.
Default number of tasklets run in IRQ context is 10 - after that the rest are scheduled in a kernel thread.
There is a push by many to reduce the use of tasklets because they run in interrupt context.

DMA interface is basically a helper interface - you have to figure out how to do it on your architecture. It basically helps control cache.
You can DMA directly to user pages that you've mapped with get_user_pages().

If a device has DMA inside of it, dma_supported() will return false! It only returns true if there is a central DMA controller on the system.
Make sure to call dma_map_single() so that the cache is flushed and your device sends out the correct data!
dma_pool_create() is useful for ring buffers.
