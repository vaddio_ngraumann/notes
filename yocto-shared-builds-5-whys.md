Problem statement: Builds cannot be easily shared on Ark, even when hardware is almost the same.

Why?

1. Different PRODUCT and/or MACHINE features needed between the two products.
Why?
	1. Features directly affect which packages end up on the target.
	Why?
		1. Avoided separate image files
		Why?
			1. Reduced image and packagegroup duplication
			2. Fewer Jenkins builds
			3. Reduced test burden

		2. Easier to add features and/or applications to multiple products.
		3. Mechanism preferred by Yocto.

	2. Features are determined at build time rather than on run time.
	Why?
		1. Leverages Yocto's existing infrastructure
		2. Would not be possible to select packages at build time if list was purely runtime
		3. Run time modification of the existing features list has not been explored.

	3. Features treated as absolutely supported rather than capabilities.

2. Bitbake fetching uses PRODUCT and MACHINE for files overrides
Why?
	1. Configuration files need to be different for different products.
	Why?
		1. Most configuration used by applications is static

3. Different sets of packages are required on the target.
Why?
	1. Most applications today cannot handle the absence of hardware at runtime.
	Why?
		1. Poor or lack of error handling
		Why?
			1. Time constraints
			2. How it's been done.

		2. Configuration data for application tends to be static
		Why?
			1. Little need for dynamic data in the past.
			Why?
				1. Builds in the past were always separate.
				Why?
					1. Fewer products meant separate builds was not a big deal.

		3. Hard-coding of machine or product information into decisions.
		Why?
			1. Time constraints
			2. Lack of awareness for dynamic capabilities
			3. Lack of infrastructure?

	2. System startup is static (same applications every time)
	Why?
		1. Boot times
		2. Read-only root filesystem
		3. Historcically, the image has contained the minimum application set for a product.
