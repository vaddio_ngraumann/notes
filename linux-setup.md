## Misc Linux setup

For gnome fallback/compiz:

    gnome-session-fallback compiz-plugins-extra compiz-plugins-extra

Disable overlay scrollbars:

    gsettings set com.canonical.desktop.interface scrollbar-mode normal

Disable auto-mounting of media:

- Install dconf-editor and open it.
- Browse to org.gnome.desktop.media-handling.
- Disable automount and automount-open.

Add user to dialout group:

    usermod -a -G dialout myuser

Enable syntax highlighting in gedit for .bb and .bbappend files:

    mkdir -p ~/.local/share/gtksourceview-3.0/language-specs/
    cp /usr/share/gtksourceview-3.0/language-specs/python.lang /usr/share/gtksourceview-3.0/language-specs/sh.lang ~/.local/share/gtksourceview-3.0/language-specs/
    (Edit each and add .bbclass, .bb, and .bbappend files as needed)

Install support for smb shares:
    sudo apt-get install cifs-utils

Use latest git release:

    sudo add-apt-repository ppa:git-core/ppa -y
    sudo apt-get update
    sudo apt-get install git git-core

Remove ModemManager so it doesn't grab USB serial ports:

    sudo apt-get remove modemmanager

Remove apport error reporting daemon:

    sudo apt-get purge apport

Better theme tweaking tool with gnome fallback: gnome-tweak-tool

Fix issue with NIC "detected hardware unit hang". Disable tx segementation offload by opening /etc/network/interfaces and adding a new line in the wan0 configuration:

    iface wan0 inet dhcp
      offload-tx off

## Docker containers

[Vaddio Docker Image](https://hub.docker.com/r/vaddio/yocto/)

## Audacity

Pulseaudio 4.0 and audacity have some compatibility issues. May need to run audacity with:
    PULSE_LATENCY_MSEC=30 audacity

Seems DMA with 2k transfers for video only and PIO for audio is pretty good. Need to do more testing.

More things to check:
- Do we really need 4 video buffers for v4l? That leaves us with only ~15MB RAM free when idle.
- Occasional sample corruption in video
- Why doesn't DMA play well with 3k transfers?

## tftpboot

I recommend using the tftpd-hpa package for Ubuntu because it is more full-featured. Edit /etc/default/tftpd-hpa and change the username to "nobody".
Add "--create" to the TFTP_OPTIONS if you want to be able to put to files that don't already exist.

## Block ram mounts

It makes looking at SD card images much easier rather than having to write to a card first:

sudo modprobe brd rd_size=524288 max_part=8
sudo chmod a+rw /dev/ram*
pv vaddio-app-debug-image-zynq-boombox.img.zip | funzip | dd of=/dev/ram0 bs=1M
sudo sfdisk -R /dev/ram0; sync
for rd in /dev/ram0p* ; do mkdir -p /media/nickg/`basename $rd`; sudo mount $rd /media/nickg/`basename $rd` ; done

# Ubuntu 18.04

PicoScope fails to start:
https://www.picotech.com/support/viewtopic.php?t=40103

### Common Drives

Connect to server: smb://10.30.12.25/MTK_Common/

Engrstorage:
\\vadfs\engrstorage

VADDIO\ domain login.
