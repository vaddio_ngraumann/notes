## Dbus tips, tricks, and examples

Send all services the ready signal:

    dbus-send --system /com/vaddio/ProductService/object com.vaddio.ProductService.sync_signal string:"ready"

With the helper (pretty-prints):

    /opt/vaddio/bin/dbus-send-helper -o -s CameraService -m put_ccu -p int32 0 -p string '{"one_push":true}'

Send the ready signal without the helper:

    dbus-send --system --print-reply --dest=com.vaddio.CameraService /com/vaddio/CameraService/object com.vaddio.CameraService.ping

Example which takes a couple of arguments:

    dbus-send --system --print-reply --dest=com.vaddio.CameraService /com/vaddio/CameraService/object com.vaddio.CameraService.put_ccu int32:0 string:'{"chroma":90}'


Monitor a signal in realtime:

    dbus-monitor --system member=camera_realtime_signal &

Enable debug on a service:

    /opt/vaddio/bin/dbus-send-helper -o -s CameraService -m vng_debug -p int32 5

Ping a service to get its status:

    /opt/vaddio/bin/dbus-send-helper -o -s CameraService -m ping

Get info about the Valens chip from the ValensService:

    ./dbus-send-helper -o -s ValensService -m get -p string /

Watch for camera complete signals:

    dbus-monitor --system member=camera_complete_signal

Transition to camera input on Orpheus:

    /opt/vaddio/bin/dbus-send-helper -o -s CameraCtlService -m set_input -p int32 1

### pyvideo

Set various stream settings on Cerberus:

```
dbus-send --system --print-reply --dest=com.vaddio.RtStreamServer1 /com/vaddio/RtStreamServer1/video0/rtsp org.freedesktop.DBus.Properties.Set string:com.vaddio.RtStreamServer1 string:mtu_size variant:int32:18000
```

Start the server:
```
dbus-send --system --print-reply --dest=com.vaddio.RtStreamServer1 /com/vaddio/RtStreamServer1/video0/rtsp com.vaddio.RtStreamServer1.start
```

Set client settings:
```
dbus-send --system --print-reply --dest=com.vaddio.RtspClient1 /com/vaddio/RtspClient1/video0 org.freedesktop.DBus.Properties.Set string:com.vaddio.RtspClient1 string:url_full variant:string:rtsp://10.30.203.7/stream
```

Start the client:
```
dbus-send --system --print-reply --dest=com.vaddio.RtspClient1 /com/vaddio/RtspClient1/video0 com.vaddio.RtspClient1.start
```

If Audio service is not enabled, you need to run this for USB to start:
```
devmem 0x4060007c 32 0x00000007
```

Other useful stuff:
Run the service with debug turned on in autostart mode:

```
/etc/init.d/pyvideo-rtsp-client stop
pyvideo-rtstream-server -a -d 2
```

Get settings of the service:
```
dbus-send --system --print-reply --dest=com.vaddio.RtStreamServer1 /com/vaddio/RtStreamServer1 org.freedesktop.DBus.ObjectManager.GetManagedObjects
```

Monitor server or client for changes:
```
dbus-monitor --system "type='signal', sender='com.vaddio.RtStreamServer1'"
dbus-monitor --system "type='signal', sender='com.vaddio.RtspClient1'"
```
