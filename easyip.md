"DMA Cache Control"

DMA controllers in the PL are cache coherent in Zynq with ACP port
– The HP ports are not cache coherent such that cache control is required

For transfers from a device to memory, the cache must be
invalidated after the transfer and before the CPU accesses memory.

```
cd /sys/class/gpio-vaddio
echo 0 > logijpg-enable/value

echo 1 > logijpg-reset/value
echo 0 > logijpg-reset/value
echo 1 > logijpg-enable/value
```

Run 1080p30 rtsp stream:
```
rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 do-timestamp=true io-mode=0 ! videorate drop-only=true ! capsfilter caps=image/jpeg,width=1920,height=1080,framerate=30/1 ! queue ! rtpjpegpay perfect-rtptime=false pt=96 name=pay0 mtu=1400 )"
```

Can do 20fps at 90 quality on Rigel:
```
rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 do-timestamp=true io-mode=0 ! videorate drop-only=true ! capsfilter caps=image/jpeg,width=1920,height=1080,framerate=20/1 ! queue ! rtpjpegpay perfect-rtptime=false pt=96 name=pay0 mtu=1400 )"
```

```
yavta -c100 -f MJPEG -s 1920x1080 -F"/run/frame-#.jpg" /dev/video0 -n4
```

Setting MTU to larger and letting the TCP/IP stack fragment the packets appears to help cpu usage significantly. Trial and error shows slightly less than 19000 is the most we can get without corrupting frames:
```
rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 do-timestamp=true io-mode=2 ! videorate drop-only=true ! capsfilter caps=image/jpeg,width=1920,height=1080,framerate=30/1 ! queue ! rtpjpegpay perfect-rtptime=false pt=96 name=pay0 mtu=18900 )"
```

Reset axi dma:
```
devmem 0x40400030 32 0x00010006
```

Consume rtsp stream in gstreamer (on Ubuntu) and show framerate:
```
gst-launch-1.0 -v rtspsrc location="rtsp://10.30.203.7/stream" name=rtspsrc0 port-range="3000-3005" ! capsfilter caps="application/x-rtp,media=video" ! rtpjpegdepay ! queue ! jpegdec ! queue ! fpsdisplaysink video-sink="xvimagesink" text-overlay=false
```
Can also add "latency=x" to rtspsrc to specify latency in ms. If the latency is too low, you may need to add sync=false to the displaysink, otherwise all of the frames will be dropped.

##### 2/18/19

Trying to figure out why true sg mode still isn't working - need to run one big transaction and only submit one at a time.
Tried adding axi stream reset before or after reset of Xylon core, no luck. Also tried inserting delay of up to 30ms (mdelay) between resets to give everything a chance to sync up. I did notice that resetting the camera block allows everything to produce one valid frame again.
Ran another test where I only submit the first dma transaction (commented out the others in logijpg.c). The first time I run it, I'll get a valid frame, but after that the frame I get is always shiften in approximately 0x2030 bytes. This is only a bit larger than 2 pages, and I've been testing in useptr mode. Coincidence?

##### 2/21/19

Testing yavta into /dev/null with uncompressed input from Glenn:
yavta -c10000 -f MJPEG -s 1920x1080 /dev/video0 -F'/dev/null'

Found sevearl issues in driver, includeing non-atomic operations for pending_descs and submitted_descs. This caused the interrupt coalesce to be set to 4 even though only 3 buffers were queued which made an interupt never happen.

Removing & loading module makes it run completely (1000 frames captured) the first time.

##### 2/22/19

Test ran all night after making the following changes:
- Alloc the dma channel when the first buffer is prepared and free it on streamoff (protected by mutex). This implies something is not quite right in xilinx's terminate_all impementation.

##### 2/25/19

Writing 1080p uncompressed frames to /tmp or /run is not a valid test: requires 235MB/s, and those tmpfs can only deliver ~160MB/s in benchmarking.

Changed imagesize to size / 4 which is 60MB/s (this seems about the max, size / 3 does not keep up). This can write frames easily.

To disable interrupt coalesce in xilinx_dma for testing (in xilinx_dma_start_transfer):
- Set the tail_segment to the head_desc->segements instead of tail_desc
- Write 1 to the pending irq amount instead of the pending amount
- Use list_move_tail for head_desc->node instead of list_splice_tail_init

60fps:
```
rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 do-timestamp=true io-mode=2 ! capsfilter caps=image/jpeg,width=1920,height=1080,framerate=60/1 ! queue ! rtpjpegpay perfect-rtptime=false pt=96 name=pay0 mtu=18900 )"
```

Lowest latency consumption on gstreamer with

##### 2/26/19

Might want to limit maximum number of connections to the rtsp server - too many (2-3) can cause it to die:

https://gitlab.freedesktop.org/gstreamer/gst-rtsp-server/issues/17

DMA sequences:

0 1 2 3
0 1 3 2
0 2 1 3
0 2 3 1
0 3 1 2
0 3 2 1
1 0 2 3
1 0 3 2
1 2 0 3
1 2 3 0
1 3 0 2 Fail
1 3 2 0
2 0 1 3 Fail
2 0 3 1
2 1 0 3
2 1 3 0 Fail
2 3 0 1
2 3 1 0
3 0 1 2
3 0 2 1
3 1 0 2
3 1 2 0
3 2 0 1
3 2 1 0

Glenn has still not seen any failures for the stream to come up on his Centaur.

##### 3/7/19

Lots of updates from Glenn and Cast - streaming working well now. Only issue is saturation is off and lines appear if both image rate and video rate controls are turned off.

Should use at least 8 buffers (SRC_BSFC) to avoid flickering for video rate control.

Needed to perform byte swap on stream between jpeg encoder and axi dma for endianness to be correct in memory (see JPEG Encoder section 7.5.3, Figure 10: "Byte order on the output stream interface")

##### 3/8/19

Imprecise external abort kernel panic appears to come from trying to write the RBC register to disable the core in streamoff() - likely the core shuts down somehow due to the evaluation timeout.

Comparing Images with Imatest. If uncompressed image's colors are backwards, just need to trim off first two bytes:
```
scp root@10.30.21.91:/run/frame-\*.raw .
dd if=frame-000007.raw of=frame-000007.offset.raw bs=1 skip=2
dd if=/dev/zero of=frame-000007.offset.raw seek=4147198 bs=1 count=2
for f in frame-*.raw ; do raw2rgbpnm -f YUYV -s 1920x1080 $f $f.pnm ; done
```

```
gst-launch-1.0 rtspsrc location="rtsp://10.30.203.114/stream" latency=100 ! application/x-rtp,media=video ! rtpjpegdepay ! queue ! jpegdec ! queue ! fpsdisplaysink video-sink="xvimagesink"
```

Can also stream in windows, latency seems even better:
```
cd C:\gstreamer\1.0\x86_64\bin
gst-launch-1.0.exe rtspsrc location="rtsp://10.30.203.7/stream" latency=10 ! application/x-rtp,media=video ! rtpjpegdepay ! queue ! jpegdec ! queue ! fpsdisplaysink
```

Videorate can be used to limit framerate. Probably easier than having the FPGA try to drop frames:

```
v4l2src device=/dev/video0 do-timestamp=true io-mode=2 ! \
videorate drop-only=true average-period=1000000000 ! \
capsfilter caps=image/jpeg,width=1920,height=1080,framerate=20/1 ! \
queue !
```

##### 3/20/19

Working on getting decoder working. Ended up using yavta for initial testing to spit out a static image:
```
yavta -c10 -F'/run/frame-000000.jpg' -f MJPEG -s 1920x1080 /dev/video0 -n3
```

Mdin `src_format` needed to be set to 3 since Joel and Glenn are using Dennis' output core from Plutus.

Gstreamer is much more picky for output devices than capture devices. Frame rates, sizes, and other things need to actually be defined by the driver. Also, colorimetry and pixel aspect ratio appear to not be inferred from the input and must be manually set in a capsfilter with: `image/jpeg,pixel-aspect-ratio=1/1,colorimetry="1:4:7:1"`. The 1:4:7:1 value corresponds to V4L2_COLORSPACE_JPEG, though there doesn't appear to be a good string/constant for that. I found that by setting teh colorspace in teh driver in response to the ENUM_FMT ioctl and looked at the caps set on the sink pad of v4l2sink in the debug log (GST_CAPS:6, GST_PADS:6).

```
nice -n -10 gst-launch-1.0 -v rtspsrc location="rtsp://10.30.203.114/stream" latency=0 ! application/x-rtp,media=video ! rtpjpegdepay ! queue ! image/jpeg,parsed=true,pixel-aspect-ratio=1/1,colorimetry="1:4:7:1",framerate=30/1 ! videorate ! image/jpeg,framerate=30000/1001 ! fpsdisplaysink video-sink="v4l2sink device=/dev/video0 io-mode=2" text-overlay=false sync=false | grep fpsdisplay
 ```

##### 3/21/19

Latency: 200-300ms depending on pipelines etc.
Video rate control (VRC) does potentially increase latency (be careful of frame averaging period). See 5.7.12 in encoder reference for suggested settings for low-latency.
Latency in this mode seems even lower than with VRC off completely.

Tested with "latency=0", "sync=false" on desktop.

##### 3/25/19

How much savings can we gain by running in "Abbreviated Format" mode? Datasheet claims lower latency. Have not been able to get decoder to handle the format yet. Encoded frames are ~10k less than before.

xvimagesink: double-buffer=true seems to perhaps help with choppiness.

```
gst-launch-1.0 rtspsrc location="rtsp://10.30.203.114/stream" latency=50 ! application/x-rtp,media=video ! rtpjpegdepay ! queue ! jpegdec ! queue ! fpsdisplaysink video-sink="xvimagesink double-buffer=true" sync=true
```

##### 4/1/19

USB decoding working on a Centaur with new build from Glenn. Basically the same pipeline as for HDMI output:
```
gst-launch-1.0 -v rtspsrc location="rtsp://10.30.203.7/stream" latency=0 ! application/x-rtp,media=video ! rtpjpegdepay ! queue ! image/jpeg,parsed=true,pixel-aspect-ratio=1/1,colorimetry="1:4:7:1" ! fpsdisplaysink video-sink="v4l2sink device=/dev/video0 io-mode=2" text-overlay=false | grep fpsdisplay
```

##### 4/3/19

Submitted patches upstream. Feedback:

- [ ] Rebase patches onto xilinx master
- [ ] Subject: Use dmaengine: prefix and do not end with "."
- [ ] Eliminate email footer

Patch 1 (callback_residue support):
- [ ] Missing comment description for residue
- [ ] Add patch dmaengine: xilinx_dma: Remove desc_callback_valid check first
- [ ] Below residue calculation should only be for SG.

Patch 2 (log error):
- [ ] Fix indentation (checkpatch --strict)
- [ ] No need to split over multiple lines

Patch 3 (atomic ops):
- [ ] Want to understand this race condition. Start transfer and Append_queue is already protected by spinlock so in which
scenario we see the race condition?
- [ ] We can avoid use of temp variable and instead use...
- [ ] Above is probably not required as mem is allocated using kzalloc.

Patch 4 (allow both idle and halted states):
- [ ] Mention that is only applicable for axidma.
- [ ] For CDMA XILINX_DMA_DMASR_HALTED BIT(0) is reserved. No need of this change.

Patch 5 (allow disable of coalesce):
- [ ] Is this applicable to all IP's?
- [ ] xlnx,enable-irq-coalesce: Looks better. The default is enabled.
- [ ] Missing structure comment for enable_coalsece.
- [ ] Above should go in a helper function as it will be need for other IP's as well.

UDP send/receive buffer sizes may need tweaking? So far doesn't seem to help, but keep it in back pocket:
```
sysctl -w net.core.rmem_max
sysctl -w net.core.wmem_max
```
Default is 163840, can go much larger

##### 4/4/19

- [x] Run for a while with CONFIG_DEBUG_LIST=y to ensure list corruption issue is resolved
- [x] Silence decoder warning on first frame (which usually is incomplete or stale)
- [x] Setting max-size-bytes paramter on queue buffers so we don't eventually run out of CMA memory. Looks like 8MB is probably the sweet spot - supports 4 streams, ~250ms of video @1080p60 (500kB frames). CmaFree sitting at 28724/65536kB with four 1080p30 streams going. On decode side, not really an issue since there is only one stream: 59144/65536kB
- [x] JPEG encoder quality parameter
- [ ] Work on AXI DMA patch fixups

To set compression quality:
`v4l2src... extra-controls="c,compression_quality=3`
        or
`v4l2-ctl --set-ctrl=compression_quality=3`

Defined:
1: 50 quality
2: 90 quality
3: 90 quality with VRC

Restart markers? Mainly useful for framentation recovery, however with Cast's multi-pipe implementation, they will be required (see 5.2. Optimal Use of Multiple Pipes and 7.7.5.1 in the decoder manual).
Will need to determine optimal restart marker number once we have the multi-pipe implementation.

##### 4/10/19

puma-cerberus stream encode:
```
nice -n -10 rtsp-server -p 554 -u "/stream" "( v4l2src device=/dev/video0 do-timestamp=true io-mode=2 ! queue max-size-bytes=8388608 ! capsfilter caps=image/jpeg,width=1920,height=1080,framerate=60/1 ! rtpjpegpay perfect-rtptime=false pt=26 name=pay0 mtu=65000 )"
```

puma-orpheus stream decode:
```
nice -n -10 gst-launch-1.0 rtspsrc location=rtsp://10.30.203.7/stream name=rtspsrc0 port-range="3000-3005" latency=10 ! application/x-rtp,media=video ! rtpjpegdepay ! image/jpeg,parsed=true,pixel-aspect-ratio=1/1,colorimetry="1:4:7:1" ! queue max-size-bytes=8388608 ! v4l2sink device=/dev/video0 io-mode=2 sync=true
```

##### 4/16/19

wip audio pipeline on PC:
```
gst-launch-1.0 -v rtspsrc location="rtsp://10.30.203.114/stream" latency=100 name=rtspsrc0 ! application/x-rtp,media=video ! rtpjpegdepay ! fakesink rtspsrc0. ! applcation/x-rtp,media=audio,clock-rate=48000,encoding-name=L16,channels=2,payload=96 ! rtpL16depay ! audio/x-raw,format=S16BE,layout=interleaved,rate=48000,channels=2 ! autoaudiosink
```

##### 4/17/19

Getting dma patches tested for xilinx. Notes below:
Be sure to set up dtsi's as indicated on the wiki page
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842337/DMA+Drivers+-+Soft+IPs

Axidmatest:
- AXI data fifo was important between mm2s and s2mm port

Cdmatest:
No need for loopback, just connect s2mm to axi mem intercon. The loopback is handled by the test itself (since cdma is memory->memory).

Vdmatest:
Be sure to set Fsync options to None to run the test.
These threads were helpful:
https://forums.xilinx.com/t5/Embedded-Processor-System-Design/Running-the-vdmatest-c-driver-in-Linux/td-p/587348
https://forums.xilinx.com/t5/Embedded-Linux/What-vdma-options-should-be-set-for-vdmatest/td-p/754474

Trying to increase network throughput, per https://opensourceforu.com/2016/10/network-performance-monitoring/

Coalesce on teh decoder side does not seem to make a difference one way or the other; probably due to frames being received one at a time.

##### 4/18/19

Bidirectional streaming benchmarking (fuzzy cube wall, streaming preset '3', VBR):
- Decode, 2x 1080p30 streams:
    - Load average: 2.42
    - % idle: 34%
    - gst-launch: ~40%
    - second gst-launch (fakesink), 30%
    - ksoftirqd: 30%

- Encode, 2x 1080p30 streams:
    - Load average: 0.88
    - % idle: 64%
    - rtsp-server: 41-48%

Linux network performance parameters: https://github.com/leandromoreira/linux-network-performance-parameters

##### 5/24/19

`next_frm_check: 0x43c80068`
Defaults to 0x00000800
Increase to something like 0x000ff000 (needs to be tuned)

##### 8/28/19

Use `tc` to emulate traffic conditions. Build `iproute2` package and install `iproute2-tc` and `libmnl0`. Also need to enable `CONFIG_NET_SCHED` and `CONFIG_NET_SCH_NETEM` in kernel.

https://www.corvil.com/kb/how-can-i-simulate-delayed-and-dropped-packets-in-linux

##### 11/8/19

Experimenting with Concat to display still image on frame stop:
```
gst-launch-1.0 concat name=c ! image/jpeg,parsed=true,pixel-aspect-ratio=1/1,colorimetry="1:4:7:1" ! queue max-size-bytes=8388608 ! v4l2sink device=/dev/video0 io-mode=2 sync=true   rtspsrc location="rtsp://10.30.203.21/stream" name=rtspsrc0 port-range="3000-3005" protocols=0x00000004 latency=10 ! capsfilter caps=application/x-rtp,media=video ! rtpjpegdepay ! c.  multifilesrc loop=true location=/run/image-000000.jpg index=0 do-timestamp=true ! image/jpeg,framerate=1/1 ! c.
```

### DMA Resources

https://lwn.net/Articles/605348/
https://lwn.net/Articles/486301/
https://www.slideshare.net/kerneltlv/continguous-memory-allocator-in-the-linux-kernel
https://events.static.linuxfound.org/images/stories/pdf/lceu2012_nazarwicz.pdf
https://elinux.org/images/3/32/Pinchart--mastering_the_dma_and_iommu_apis.pdf
https://forums.xilinx.com/t5/Embedded-Processor-System-Design/Axi-DMA-correct-parameters/m-p/639865/highlight/true#M15508

### Luxul switch settings

Try these settings from Dan:
- spanning tree to STP
- CIST ports - disable

No noticeable change in streaming behavior.

### Other References

Interesting article about 4:2:0 subsampling: https://calendar.perfplanet.com/2015/why-arent-your-images-using-chroma-subsampling/
https://developer.ridgerun.com/wiki/index.php?title=Streaming_RAW_Video_with_GStreamer
https://developer.ridgerun.com/wiki/index.php?title=Why_RidgeRun_Loves_GStreamer

### Misc Notes

- TCP streaming does not work in VLC with a large MTU size set for some reason.
- To generate JPEG's compatible with the Cast Decoder with GIMP, ensure Subsampling is set to 4:2:2 horizontal and Use Restart markers is enabled and set to MCU rows=1.
