## ZLK38000 dev board

Note: Diagram shows pins labeled looking down from the top of the board. Do not
trust the white "pin 1" dot on the bottom of the board!

![zlk38000-rpi-header.png](images/zlk38000-rpi-header.png)

### I2C connection

Using I2C0, choice 0

19 MOSI GND (I2C address 0x45)
21 MISO SDA (I2C data)          JE1 pin 3 (MIO11)
23 SCLK GND (sel. I2C mode)
24 CS0  SCL (I2C clock)         JE1 pin 2 (MIO10)
25 GND  GND                     JE1 pin 5

### SPI connection

Using SPI1, choice 1

19 MOSI JE1 pin 2 (MIO10)
21 MISO JE1 pin 3 (MIO11)
23 SCLK JE1 pin 4 (MIO12)
24 CS0  JE1 pin 1 (MIO13)
25 GND  JE1 pin 5

### Commands

modprobe hbi
hbi_test -d 0 -r 0x022 2
(ensure reads 38063 or 0x94af)

Should do the same:
echo 0022 2 > /proc/hbi/dev_00/read_reg
cat /proc/hbi/dev_00/read_reg

Set bit 0 of register 0x400 to hear test tone.

## sdk

Can set DEBUG_LEVEL=0x0f for lots of debug info
