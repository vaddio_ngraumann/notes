### LPDDR4

To achieve maximum performance, address copy mode is suggested.

SN102 passed once memory was set up correctly in XSA. Has termination resistors removed.

u-boot memory test overnight w/Glenn's updated settings

SN00108
Booted with cma=0,
```
memtester 850M
(...)
Loop 55:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok   
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok
  ```

##### 3/5/20
SN00108: 32439 iterations
SN00109: 32819 iterations

##### 3/6/20
Switched to "alt" memory test in u-boot, more thorough.

SN00108:
Testing 00000000 ... 39000000:
Iteration:   6319

SN00109:
Testing 00000000 ... 39000000:
Iteration:   7270

##### 3/13/20

SN00108 Ran 164 iterations of memtester in Linux over 3 days.

### EMMC

Verified in FSBL using f_mkfs and f_open/f_write/fclose from Xilinx FATFS. Read back data using f_read and verified it was the same. Have not done any performance testing.

### I2C

##### I2C0

Remember to run at 100kHz for security chip.

```
# i2cdetect -y -r 0
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: 20 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: 30 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: 50 51 52 53 54 55 56 57 -- -- -- -- -- -- -- --
60: 60 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

PMIC

Input voltage:
```
# i2cget -y 0 0x30 0x88 w
0x01e3
# 0x01e3*25e-3
12.075000000000001V
```

Output voltage (set VOUT_SCALE_LOOP for our feedback resistors first):
```
i2cset -y 0 0x30 0x29 0x2bf w
i2cget -y 0 0x30 0x8b w
0x02aa
0x02aa*1.25e-3
0.8525V
```

Output current:
```
i2cget -y 0 0x30 0x8C w
0x002c
0x002c*62.5e-3
2.8125A
```

Note: Used `stress -c 4 -i 2 -m 1` to load system, then ran with USB and HDMI in/outs active. Maximum current reported: 4.375A. Voltage stayed constant at 1.79V.

##### I2C1

```
# i2cdetect -y -r 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

Fan5702 (0x36): Verified all 4 channels, varying brightness
Turn on White LED, blue, green, then red LEDs:
```
i2cset -y 1 0x36 0x10 0x02
i2cset -y 1 0x36 0x10 0x04
i2cset -y 1 0x36 0x10 0x08
i2cset -y 1 0x36 0x10 0x10
```

Can be disabled with switch.

##### Daughterboard

```
# i2cdetect -y -r 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: 20 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

Found issue where LEDs don't light up due to two being in series, forward voltage is too high. Jeff modded one to remove one LED, now all channels work.

Bluetooth HCI works with updated FPGA image.

##### I2C2

Needed to manually add interrupt parent and interrupts to dts since pl.dtsi does not populate when they are on EMIO.

```
# i2cdetect -y -r 2
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- UU
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- UU
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- 38 39 -- -- 3c -- 3e 3f
40: -- -- -- -- -- -- -- 47 -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

##### I2C3 (motors)

Using motors from Remus. Enable motors:
```
# echo 1 > pan-motor-enable/value
# echo 1 > tilt-motor-enable/value
```
Note: Motors will not appear via i2cdetect, don't be alarmed. Will be at 0x28 and 0x29.

Read 4-byte version register:
```
# i2ctransfer -y 3 w1@0x28 0x1b r4
0x05 0x05 0x00 0x5c
```
Reads 5.5.92 as expected.

Install `pymotor` package from VNG. Then:
```
python
from pymotor.motor_overview import MotorOverview
pan = MotorOverview(3, 0x28, 'pan')
pan.wakeup_for_calibration()
pan.get_calibration_state()
1
pan.get_firmware_version()
'5.5.92'
pan.go_to_position_in_time(180, 2000)

tilt = MotorOverview(3, 0x29, 'tilt')
tilt.go_to_position_in_time(25, 2000)
```

### HDMI

##### HDMI Input

Note: Works better with Continuous clock mode on Toshiba chip. Nevermind, fixed with incorrect Toshiba settings from spreadsheet.


```
v4l2-ctl --set-edid pad=0,type=hdmi-4k-300mhz -d /dev/v4l-subdev1
v4l2-ctl --set-dv-bt-timings index=12 -d /dev/v4l-subdev1
yavta -c10 -f YUYV -s 1920x1080 -F/run/image-#.yuyv /dev/video1


Device /dev/video1 opened.
Device `vcap_hdmi output 0' on `platform:vcap_hdmi:0' is a video output (without mplanes) device.
Video format set: YUYV (56595559) 1920x1080 field none, 1 planes:
 * Stride 3840, buffer size 4147200
Video format: YUYV (56595559) 1920x1080 field none, 1 planes:
 * Stride 3840, buffer size 4147200
8 buffers requested.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 0/0 mapped at address 0x7f889e5000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 1/0 mapped at address 0x7f885f0000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 2/0 mapped at address 0x7f881fb000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 3/0 mapped at address 0x7f87e06000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 4/0 mapped at address 0x7f87a11000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 5/0 mapped at address 0x7f8761c000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 6/0 mapped at address 0x7f87227000.
length: 1 offset: 3385251056 timestamp type/source: mono/EoF
Buffer 7/0 mapped at address 0x7f86e32000.
0 (0) [-] none 0 0 B 129.806347 129.806408 31.543 fps ts mono/EoF
1 (1) [-] none 1 0 B 129.823036 129.854041 59.920 fps ts mono/EoF
2 (2) [-] none 2 0 B 129.839717 129.899690 59.948 fps ts mono/EoF
3 (3) [-] none 3 0 B 129.856398 129.945709 59.948 fps ts mono/EoF
4 (4) [-] none 4 0 B 129.873081 129.991522 59.941 fps ts mono/EoF
5 (5) [-] none 5 0 B 129.889763 130.037668 59.945 fps ts mono/EoF
6 (6) [-] none 6 0 B 129.906447 130.083334 59.938 fps ts mono/EoF
7 (7) [-] none 7 0 B 129.923134 130.129878 59.927 fps ts mono/EoF
8 (0) [-] none 8 0 B 129.939812 130.175242 59.959 fps ts mono/EoF
9 (1) [-] none 9 0 B 129.956495 130.221997 59.941 fps ts mono/EoF
Captured 10 frames in 0.447353 seconds (22.353709 fps, 0.000000 B/s).
8 buffers released.
```

Show detected format:
```
root@vaddio-generic-04-91-62-C8-BF-D4:~# media-ctl -p
Media controller API version 4.19.0

Media device information
------------------------
driver          xilinx-video
model           Xilinx Video Composite Device
serial          
bus info        
hw revision     0x0
driver version  4.19.0

Device topology
- entity 1: vcap_hdmi output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
        pad0: Sink
                <- "a0130000.csiss":0 [ENABLED]

- entity 5: tc358840 2-001f (2 pads, 1 link)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
        pad0: Source
                [fmt:UYVY8_1X16/1920x1080 field:none colorspace:rec709 xfer:709 ycbcr:601 quantization:lim-range]
                [dv.caps:BT.656/1120 min:160x120@25000000 max:3840x2160@300000000 stds:CEA-861,DMT,CVT,GTF caps:progressive,reduced-blanking,custom]
                [dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds: flags:]
                [dv.current:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861,DMT flags:can-reduce-fps,CE-video,has-cea861-vic]
                -> "a0120000.csiss":1 [ENABLED]
        pad1: Source
                [dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds: flags:]                [dv.current:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861,DMT flags:can-reduce-fps,CE-video,has-cea861-vic]
(...)
```

Similar test at 4k30:
```
v4l2-ctl --set-edid pad=0,type=hdmi-4k-300mhz -d /dev/v4l-subdev0
v4l2-ctl --set-dv-bt-timings index=89 -d /dev/v4l-subdev0
media-ctl -v -V '"a0120000.csiss":0 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0120000.csiss":1 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0130000.csiss":0 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0130000.csiss":1 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
yavta -c10 -f YUYV -s 3840x2160 -F/run/image-#.yuyv /dev/video0
```

HDCP: Ensure `enable-hdcp = <1>` is present in the dts. Then enable it:
v4l2-ctl -d /dev/v4l-subdev1 -c hdcp_enable=1

And HDCP source will show:
v4l2-ctl -d /dev/v4l-subdev1 -l | grep hdcp_present
        hdcp_present 0x00981905 (bool)   : default=0 value=1 flags=read-only

Verified w/Roku when HDCP is disabled, Roku will not send video and instead shows error screen.

CEC:    
```
root@vaddio-generic-04-91-62-C8-BF-D4:~# cec-ctl -d /dev/cec1 --tv -S
Driver Info:
        Driver Name                : tc358840
        Adapter Name               : 2-001f
        Capabilities               : 0x0000002e
                Logical Addresses
                Transmit
                Passthrough
                Monitor All
        Driver version             : 4.19.0
        Available Logical Addresses: 4
        Physical Address           : 0.0.0.0
        Logical Address Mask       : 0x0001
        CEC Version                : 2.0
        Vendor ID                  : 0x000c03 (HDMI)
        OSD Name                   : TV
        Logical Addresses          : 1 (Allow RC Passthrough)

          Logical Address          : 0 (TV)
            Primary Device Type    : TV
            Logical Address Type   : TV
            All Device Types       : TV
            RC TV Profile          : None
            Device Features        :
                None

        System Information for device 4 (Playback Device 1) from device 0 (TV):
                CEC Version                : 1.4
                Physical Address           : 1.0.0.0
                Primary Device Type        : Playback
                Vendor ID                  : 0x8ac72e
                OSD Name                   : Roku
                Power Status               : On

        Topology:

        0.0.0.0: TV
            1.0.0.0: Playback Device 1                   
```

Can monitor CEC input events, when Roku remote is pressed activity happens:
```
# cec-ctl -d /dev/cec1 --tv -W
(...)
Received from Playback Device 1 to TV (4 to 0): CEC_MSG_IMAGE_VIEW_ON (0x04)
Received from Playback Device 1 to all (4 to 15): CEC_MSG_ACTIVE_SOURCE (0x82):
        phys-addr: 1.0.0.0
```

##### Camera Block Input

Note: Also tested in Continuous mode.

Connected 8530 block from Remus Prime to camera input. Set up EDID and formats same as HDMI in.

TODO: Sync input/output *change on next rev*

##### HDMI Output

Trouble getting 74.25MHz clock - clocking wizard driver configuring wrong value when set_rate called by vtc driver.

720p in on HDMI to HDMI out:
```
media-ctl -v -V '"a0120000.csiss":0 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"a0130000.csiss":1 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"a0120000.csiss":1 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"a0130000.csiss":1 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"a0130000.csiss":0 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media1
v4l2-ctl --set-edid pad=0,type=hdmi-4k-300mhz -d /dev/v4l-subdev1
v4l2-ctl -d /dev/v4l-subdev1 --set-dv-bt-timings index=7
media-ctl -v -V '"adv7511 2-0039":0 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]'
v4l2-ctl -d /dev/v4l-subdev0 --set-dv-bt-timings index=7
v4l2-ctl -d /dev/v4l-subdev0 -c transmit_mode=1
gst-launch-1.0 v4l2src io-mode=4 device=/dev/video1 ! video/x-raw,width=1280,height=720 ! v4l2sink device=/dev/video0 io-mode=5
```

With scaler (1080p60 input 720p60 output)

```
v4l2-ctl --set-edid pad=0,type=hdmi-4k-300mhz -d /dev/v4l-subdev1
v4l2-ctl -d /dev/v4l-subdev1 --set-dv-bt-timings index=12
media-ctl -v -V '"a0040000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]' -d /dev/media3
media-ctl -v -V '"a0040000.v_proc_ss":1 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media3
v4l2-ctl -d /dev/v4l-subdev0 --set-dv-bt-timings index=7
v4l2-ctl -d /dev/v4l-subdev0 -c transmit_mode=1
media-ctl -v -V '"adv7511 2-0039":0 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]'
gst-launch-1.0 v4l2src io-mode=4 device=/dev/video1 ! video/x-raw,width=1920,height=1080,framerate=60/1 ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,width=1280,height=720 ! v4l2sink device=/dev/video0 io-mode=5
```

Audio: Verified in I2S section.

CEC:
List devices:
```
# cec-ctl -d /dev/cec2 --playback -S
Driver Info:
        Driver Name                : adv7511
        Adapter Name               : 2-0039
        Capabilities               : 0x0000000e
                Logical Addresses
                Transmit
                Passthrough
        Driver version             : 4.19.0
        Available Logical Addresses: 3
        Physical Address           : 3.0.0.0
        Logical Address Mask       : 0x0800
        CEC Version                : 2.0
        Vendor ID                  : 0x000c03 (HDMI)
        OSD Name                   : Playback
        Logical Addresses          : 1 (Allow RC Passthrough)

          Logical Address          : 11 (Playback Device 3)
            Primary Device Type    : Playback
            Logical Address Type   : Playback
            All Device Types       : Playback
            RC TV Profile          : None
            Device Features        :
                None

        System Information for device 4 (Playback Device 1) from device 11 (Playback Device 3):
                CEC Version                : 1.3a
                Physical Address           : 1.2.0.0
                Primary Device Type        : Playback
                Vendor ID                  : 0x0000f0 (Samsung)
                OSD Name                   : BD Player
                Power Status               : Standby
        System Information for device 5 (Audio System) from device 11 (Playback Device 3):
                CEC Version                : 1.4
                Physical Address           : 1.0.0.0
                Primary Device Type        : Audio System
                Vendor ID                  : 0x00a0de (Yamaha)
                OSD Name                   : HTR-4064
                Power Status               : On
        System Information for device 8 (Playback Device 2) from device 11 (Playback Device 3):
                CEC Version                : 1.4
                Physical Address           : 1.1.0.0
                Primary Device Type        : Playback
                Vendor ID                  : 0x8ac72e
                OSD Name                   : Roku
                Power Status               : On
        System Information for device 14 (Specific) from device 11 (Playback Device 3):
                CEC Version                : 1.4
                Physical Address           : 0.0.0.0
                Primary Device Type        : TV
                Vendor ID                  : Tx, OK, Rx, Timeout
                OSD Name                   : Tx, OK, Rx, Timeout
                Menu Language              : eng
                Power Status               : On

        Topology:

        0.0.0.0: Specific
            1.0.0.0: Audio System
                1.1.0.0: Playback Device 2
                1.2.0.0: Playback Device 1
```

Turned on TV with CEC:
```
cec-ctl -d /dev/cec2 --to 0 --image-view-on
```

Change source to this device:
```
# cec-ctl -d /dev/cec2 --to 0 --active-source phys-addr=3.0.0.0
Driver Info:
        Driver Name                : adv7511
        Adapter Name               : 2-0039
        Capabilities               : 0x0000000e
                Logical Addresses
                Transmit
                Passthrough
        Driver version             : 4.19.0
        Available Logical Addresses: 3
        Physical Address           : 3.0.0.0
        Logical Address Mask       : 0x0800
        CEC Version                : 2.0
        Vendor ID                  : 0x000c03 (HDMI)
        OSD Name                   : Playback
        Logical Addresses          : 1 (Allow RC Passthrough)

          Logical Address          : 11 (Playback Device 3)
            Primary Device Type    : Playback
            Logical Address Type   : Playback
            All Device Types       : Playback
            RC TV Profile          : None
            Device Features        :
                None


Transmit from Playback Device 3 to TV (11 to 0):
CEC_MSG_IMAGE_VIEW_ON (0x04)
        Sequence: 204 Tx Timestamp: 493.622s
        Tx, Not Acknowledged (4), Max Retries
```

### I2S

Note: Logii2s version 2.3 used which supports Zynq MPSoC. Interface with 32-bit AXI_HPM width (be sure to update fsbl which configures this).

##### i2s0: Receiver from TC358840 (note: goes through samplerate converter in FPGA)

arecord -D "hw:0,0" -f dat -d 5 /tmp/record.wav

##### i2s1: Transmitter to ADV7513

aplay -f dat -D "hw:0,1" test.wav

Note: HDMI video must be going (see above), instead of gstreamer, can use yavta with yuyv image:
yavta -c -f YUYV -s 1280x720 -Fimage-000000.yuyv /dev/video0

MCLK Ratio: 128xfs

v4l2-dbg -d /dev/v4l-subdev0 -s 0x0a 0x00
v4l2-dbg -d /dev/v4l-subdev0 -s 0x0b 0x2e
v4l2-dbg -d /dev/v4l-subdev0 -s 0x0c 0x84

##### i2s2: Receiver from codec

Validated with AIC image that provides test waveforms on L and R channels.

##### i2s3: Transmitter to codec
##### i2s4: Receiver from DSP (A)

Record while talking into microphones:

arecord -D "hw:0,4" -f dat -V stereo  /run/recording.wav

Then play back and hear recorded audio.

##### i2s5: Receiver from DSP (B)

Used same TW image as Boombox from Steve Z (0.20) which looped I2SA back to I2SB. Played tone on i2s6 and recorded back on i2s5, verified it was the same.

##### i2s6: Transmitter to DSP (A)

Verified via loopback test of i2s5.

##### i2s7: Transmitter to DSP (B)
(disabled since currently unused)

### UARTs

##### UART0

Debug serial console works.

##### UART1

Camera serial. Read block ID (inspiration from `pypackage/vng_util/vng_util/identify_camera.py`)

```
python
import serial
s = serial.Serial('/dev/ttyPS1', baudrate=9600, timeout=3.0)
s.write(serial.to_bytes([0x81, 0x09, 0x00, 0x02, 0xFF]))
resp = bytearray(s.read(16))
[hex(n) for n in resp]
hex((resp[4] << 8) + resp[5])
```

Read 0x709 which corresponds to the 8530 Sony block attached.

##### UART2

Timberwolf. Connect via minicom, 115200 baud:

```
.> ?
Host Command Context

Commands:
 ? [command]
 rd <address> [length]
 wr <address> <word0> [word1] ...
 sload
 ver


Contexts:
 . [command]
```

### Codec (AIC)

LDO_SELECT should be pulled to 1.8, not 3.3V.

https://www.xilinx.com/support/answers/69670.html

### Timberwolf

Echo cancellation:
TODO

Reset: Validated.

Interrupt line:
Same test sequence as Boombox: Reset TW using reset line. Then read value and ensure is asserted. Next connect to TW with minicom (`minicom -D /dev/ttyS3 -b 115200`) and read status register 0000 with `rd 0000`, it will return 0001 but also clear itself and the interrupt line will de-assert. Reading again should read back 0000.


### Rev bits GPIO expander

Enable pull downs:
```
i2cset -y 0 0x20 0x44 0x00
i2cset -y 0 0x20 0x43 0xff
```

Read values:
```
i2cget -y 0 0x20 0x00
0x00
```

(If you set 0x44 to pull-ups with 0xff instead, register 0x00 should now read 0xff).

Manually pulled up a couple of pins and verified the values changed to 1.

### Early boot:

Remove to allow FSBL debug (per wiki page):
-Os -flto -ffat-lto-objects

##### JTAG boot with other boot mode

Used Boot Mode alternate register to set JTAG boot mode, then enter while 1 loop:
```
#define BOOT_MODE_REG 0xff5e0200

u32 BootModeRegister;
u32 RegValue;
/*
* Read bootmode register
*/
BootModeRegister = XFsbl_In32(BOOT_MODE_REG);
BootModeRegister &= ~0x0000f000;
BootModeRegister |= 0x100;

XFsbl_Out32(BOOT_MODE_REG, BootModeRegister);
dsb();
isb();

/* Soft reset the system */
RegValue = XFsbl_In32(CRL_APB_RESET_CTRL);
XFsbl_Out32(CRL_APB_RESET_CTRL,
        RegValue|CRL_APB_RESET_CTRL_SOFT_RESET_MASK);

/* wait here until reset happens */
while(1) {

}

```

Use USB DFU to load this.
Then you can load a new fsbl elf from the Vitis debugger, ensure settings match the following screenshots:
eagle-boot-1.png
eagle-boot-2.png

Make sure psu_peripherals_init_data is not commented out or uart won't work!

See also: https://www.zachpfeffer.com/single-post/Change-the-Boot-Mode-of-the-Xilinx-Zynq-UltraScale-MPSoC-from-XSCT

##### debug

-Os -flto -ffat-lto-objects

##### JTAG load u-boot

https://www.xilinx.com/support/answers/68657.html

### USB0

Verified with USB DFU.

Need to verify USB 3.0, host mode.

```
dd if=/dev/zero of=/run/storage bs=1M count=256
cd /sys/kernel/config/usb_gadget/
mkdir g1
cd g1
echo 0x1234 > idVendor
echo 0x5678 > idProduct
mkdir strings/0x409
echo "Nobody" > strings/0x409/manufacturer
echo "Mass Storage Gadget" > strings/0x409/product
mkdir configs/c.1
mkdir functions/mass_storage.usb0
echo /run/storage > functions/mass_storage.usb0/lun.0/file
ln -s functions/mass_storage.usb0/ configs/c.1/
echo fe300000.dwc3 > UDC
```

USBC_INT_N:
```
cd /sys/class/gpio-vaddio/usbc-interrupt
cat value
# 1
i2cget -y 2 0x47 0x09
# 0x90s
i2cset -y 2 0x47 0x09 0x90
# 0x80
cat value
# 0
# (unplug/plug in cable)
i2cget -y 2 0x47 0x09
# 0x90
cat value
# 1
```

USB0_OVP_FLAG_N:
Shorted USB0_VBUS to GND via C304. Flag goes high and device un-enumerates. Once short is removed, flag goes low and device re-enumerates.

### USB1

Verified with mass storage gadget in Linux.

##### SuperSpeed 3.0

Ensure AC coupling caps are correct for functionality (only on Tx, size 0.1uF). Power sequencing might need slight changes as well per Jeff. PS_MGTRAVCC should be 0.85V, not 0.9V.

Ran badblocks test overnight on mass-storage gadget, usb1:

```
# i=0; while badblocks -w -s /dev/sdf ; do i=$((i+1)) ; echo "Iteration $i" ; done
(...)
Iteration 2242
Testing with pattern 0xaa: done
Reading and comparing: done
Testing with pattern 0x55: done
Reading and comparing: done
Testing with pattern 0xff: done
Reading and comparing: done
Testing with pattern 0x00: done
Reading and comparing: done
```

### TODO

- [x] Reboot does not work (disabling watchdog driver seems to fix this)
This does: `echo pm_system_shutdown 1 2 > /sys/kernel/debug/zynqmp-firmware/pm`
with `ZYNQMP_FIRMWARE_DEBUG=y`
- [x] Series resistor missing on ADV7513_CEC_CLK - mentioned to Jeff, fix in next rev
- [x] AIC
- [x] Audio IO
- [ ] MOTOR_DGS current input (seems not to work on this rev, Jeff says low priority)
- [ ] IO_INT_N from daughterboard

### Emissions

Video:
- Camera block -> MJPEG Out (1080p)
- Camera block -> Scaler -> HDMI Out (720p)
- HDMI in (720p) -> Scaler -> USB Out

Audio:
- TW mics + AIC i2s in -> HDMI out
- HDMI in -> USB Out
- Line out: test tones from AIC (looped to line ins)

Bluetooth:
- Boot with power on, scanning

### Latent issues

Rarely, HDMI output does not work after board has been sitting for a while with HDMI and USB plugged in. Things to try:
- Soft reset ADV7513
- Toggle PD pin on ADV7513
- Hotplug USB, HDMI
