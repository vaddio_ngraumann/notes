# Introduction

This guide will describe how to update our layers to new versions of Yocto. It is only applicable to the Ark layer(s); existing dora and morty-based products using the meta-vaddio, meta-vaddio-util, and meta-milestone layers have no upgrade path at this time.
It's important to understand which Yocto version the layer is currently on and which version you're upgrading to. For example, at the time of writing the Ark layer is using the Yocto Rocko release (2.4.x). Eventually it will move to Sumo (2.5.x) and beyond.

# One-time setup
First, you'll need to have the poky repository as well as other repositories cloned inside of it. The easiest way to accomplish this is to run the fetch script in your workspace: `./tools/build-scripts-ark/fetch`

Next, we need to add upstream repositories for each repository. The upstream repositories are where the community is actively developing changes and where we'll pull in changes from. Here is a list of current upstreams for each layer:

- poky: git://git.yoctoproject.org/poky
- meta-oe: git://git.openembedded.org/meta-openembedded
- meta-gplv2: git://git.yoctoproject.org/meta-gplv2
- meta-qt5: https://github.com/meta-qt5/meta-qt5.git
- meta-ti: git://git.yoctoproject.org/meta-ti
- meta-xilinx: https://github.com/Xilinx/meta-xilinx.git

Starting with the poky directory, change directory into it and add the appropriate upstream:

    cd poky
    git remote add upstream git://git.yoctoproject.org/poky

Repeat this process for each meta directory in the list. All of the meta- directories are storied inside the poky directory.

# Upgrade Process

We are going to perform the upgrade in two stages: non-bsp layers and then bsp layers. The reason for this is non-bsp layers generally reveal if there are any mismatched bbappends, filesystem issues, etc. BSP layer updates tend to be more involved because they often contain version updates for u-boot and the kernel. It is usually, although not always, possible to build new non-bsp layers with old bsp layers, but sometimes you'll have to go through the pain of upgrading everything at once.

## Non-BSP Layers

The non-bsp layers are everything that's not adding vendor support for specific chips. As of writing, this is all layers except meta-ti and meta-xilinx (poky, meta-oe, meta-gplv2, meta-qt5).

Fetch the latest changes. You'll need to do this every time you want to upgrade:

    cd <repo>
    git fetch upstream
    git checkout <yocto branch>
    git pull upstream <yocto branch>
    git push --tags origin <yocto branch>

Notice that the steps specify a yocto branch. This is the name of the Yocto release, such as "rocko". Note that you usually shouldn't update master, because some of our older products build directly off of master from these forks. Luckily, bitbucket has some checks set up to avoid allowing people to blindly push to the old branches.

Next, update the revision listed for each layer in `tools/build-scripts-ark/fetch`. Generally use a release tag when possible (yocto-2.4.0, xilinx-v2018.1, etc). When tags aren't used by the upstream repositories (meta-oe, meta-gplv2 (which is changing soon), meta-qt5, and meta-xilinx), use a sha from the appropriate branch instead. It's best to avoid specifying a branch name here.

## BSP Layers

Now we need to upgrade the BSP layers. I recommend upgrading one at time to keep your sanity. First perform the fetch, pull, and push as described in Non-BSP Layers. Then, try a build. You'll find that you'll likely need to upgrade the kernel and u-boot recipes, usually indicated by the u-boot bbappend no longer applying:

    ERROR: No recipes available for:   /var/build/poky/meta-vaddio-ark/recipes-bsp/u-boot/u-boot-ti-staging_2017.01.bbappend

Or possibly a kernel build failure.

### Upgrading u-boot

First, clone the relevant u-boot repository: [u-boot-ti-staging](https://bitbucket.org/vaddio/u-boot-ti-staging) or [u-boot-xlnx](https://bitbucket.org/vaddio/u-boot-xlnx). It's best to just clone both, since you'll likely need to upgrade both.

Each u-boot fork has branches prefixed with vaddio- that are based off the relevant development branch used by the new recipe in the appropriate meta- layer, for example vaddio-ti-u-boot-2017.01 is based off of the ti-u-boot-2017.01 branch because, in the current version, u-boot-ti-staging_2017.01.bb is using that. In this example we'll continue upgrading from TI's 2017.01 u-boot to the version used by the ti2018.00 tag in meta-ti, because the new u-boot-ti-staging_2018.01.bb points to that tag. You'll need to rename the bbappend in our layer (likely meta-vaddio-ark) to match the the new bb file in the appropriate meta layer.

1. Determine the new u-boot branch being used. Usually you can find this out by inspecting the relevant u-boot recipe in meta-ti or meta-xilinx. In our example, we look at poky/meta-ti/recipes-bsp/u-boot/u-boot-ti-staging_2018.01.bb and find:

    BRANCH = "ti-u-boot-2018.01"
    SRCREV = "ed7323e9f60153ff25c053b8fdba16770ccc8fab"

If you only have a SRCREV, you may need to use `git log` to view that revision in the u-boot repo and see if there's a corresponding tag or branch for it. Regardless, that's the revision where you'll usually want to base the new vaddio- branch in the next step.

1. Check out that commit in your local clone, and then create a new vaddio- branch on that branch/tag. Push that branch to the origin repository (our Bitbucket fork):

    git checkout ed7323e9f60153ff25c053b8fdba16770ccc8fab
    git checkout -b vaddio-ti-u-boot-2018.01
    git push -u origin vaddio-ti-u-boot-2018.01

1. Check out the last supported u-boot version being used in Vaddio's meta layer. In our case we'll inspect meta-vaddio-ark/recipes-bsp/u-boot/u-boot-ti-staging_2017.01.bbappend and see:

    BRANCH = "vaddio-ti-u-boot-2017.01"
    SRCREV = "ac94c0d1e21457122a4b833666a87d8eb173db60"

So we run:

    git checkout ac94c0d1e21457122a4b833666a87d8eb173db60

1. Create a development branch at the current commit which you will use to create a pull request. This should be named with the standard ticket prefix, e.g. VNG-xxxx, and generally should have the branch we're rebasing for traceability:

    git checkout -b VNG-1234-rebase-from-vaddio-ti-u-boot-2017.01

1. Determine the branching off point where the vaddio- branch branched off of the base branch. This can be done visually with gitk or git log --graph, or programatically with:

    git merge-base HEAD origin/ti-u-boot-2017.01

or for a tag:

    git merge-base HEAD xilinx-v2017.3

1. Rebase the new development branch onto the new vaddio- branch we just created using rebase --onto to specify the branching off point (merge base). In our example:

    git rebase --onto vaddio-ti-u-boot-2018.01 c68ed086bd00054e28c46e033385f79104c3f84c

Don't be surprised if you come across merge conflicts. Do your best to resolve them. Then once the rebase is complete, push the branch:

    git push -u origin VNG-1234-ti-u-boot-2018.01-upgrade

1. Before we go any farther, we need to create a PR to merge our rebased branch into the new vaddio- branch we created earlier. Since it's only a rebase and has no functional changes, this PR doesn't need any reviewers and is just for traceability. For an example, see [this PR](https://bitbucket.org/vaddio/u-boot-ti-staging/pull-requests/4/). Make sure to point the PR at the new vaddio- branch and not master! Once it's created and looks good to you, approve and merge it.

1. Finally, we need to update the recipe to point to the new u-boot revision. We likely also need to rename it to match the new .bb recipe in meta-ti or meta-xilinx. In our example:

    cd meta-vaddio-ark
    git mv meta-vaddio-ark/recipes-bsp/u-boot/u-boot-ti-staging_2017.01.bbappend meta-vaddio-ark/recipes-bsp/u-boot/u-boot-ti-staging_2018.01.bbappend

And update BRANCH and SRCREV in that recipe. Also update the PREFERRED_VERSION_u-boot definitions in any machine configs or inc files that may specify it (for example, the am335x-based products specify it in `meta-vaddio-ark/conf/machine/include/am335x-vaddio-common.inc`.

Then try a build. With luck, things will work, but you may encounter issues you'll need to resolve. For example, in our upgrade to 2018.01, it was discovered that the Kconfigs for TI's OMAP2+ chips, like the am335x, were completely refactored, and some manual changes needed to be made to the defconfig, otherwise the proper config options for the am335x were not selected and the build failed. At first, the author attempted to run `make am335x_vaddio_common_defconfig` followed by `makeoldconfig` and make `savedefconfig`, but it still refused to cooperate. So instead, the author looked at the differences between the common am335x vaddio defconfig and the beaglebone defconfig, then created a new am335x common config on the 2018 branch copied from the beaglebone config and applied the set of config changes necessary (such as changing the CONFIG_TARGET_AM335X_* to CONFIG_TARGET_AM335X_VADDIO_COMMON).

Any follow-up in the u-boot repo at this point will require a new branch and PR. You'll likely want to incorporate that PR into the PR to update the kernel as well (see next section) to avoid any issues caused by mismatches in the build system.

### Upgrading the kernel

If you're very lucky, the patches may all apply fine and the kernel will build. If not, you'll need to regenerate the patches that don't apply. The easiest way to do this is to follow a similar procedure to rebasing the changes in u-boot:

- Clone the kernel into its own repository
- Checkout the branch corresponding to the current (old) version
- Create a new branch
- Apply each patch from the `meta-vaddio-ark` kernel layer using `git am` or falling back to `git apply` if there is any patch that fails with `git am` due to it being in a legacy format.
- Once you've applied and commited all patches, rebase the branch onto the new branch/tag, resolving conflicts as you go.
- Use `git format-patch -N <new tag>` to regenerate the patches, generating from everything after the new branch/tag.

Next, we need to update any defconfigs for the kernel. First, make sure all vaddio- machine features inactive in order to prevent any config fragments from leaking in. Do this by removing or commenting them out in the machine conf. The best way to do this is to run the containerize script, then enter a devshell for the kernel with:

    bitbake virtual/kernel -c devshell

Next, copy in the existing config and update it with:

    cp $KBUILD_OUTPUT/../defconfig $KBUILD_OUTPUT/.config
    make O=$KBUILD_OUTPUT olddefconfig

Finally, copy the updated .config back to the ark layer, for example for the TI kernel defconfig:

    cp $KBUILD_OUTPUT/.config /var/build/meta-vaddio-ark/recipes-kernel/linux/linux-ti-staging/am335x-vaddio-common/defconfig

Visually inspect the new config for changes and turn things off as needed - for example, new drivers usually don't need to be turned on. You can alternatively run `oldconfig` instead of `olddefconfig` above to interactively select these options one by one, which can be tedious, but does force you to look through everything. You can also run `make menuconfig` and type`/` to initiate a search to look up the items you're not sure of one at a time. When you have something selected, type `?` to get more information on it.

### Other upgrades

Other architectures, specifically ZynqMP (UltraScale) may require additional upgrades. For example, the UltraScale may require an FSBL and PMUFW upgrade, at least early on as the firmware is changing rapidly. These will eventually be provided by the FPGA projects, but initially we have been using the pre-built images that ship with PetaLinux to run on the dev kits (zcu102, zcu104, UltraZed, etc).

##### Find backported recipes that are present in the current version

```
find ./meta-vaddio-ark -name \*.bb -print0 | xargs -0 -n1 /usr/bin/basename | sort > bbs-ark.txt
find ./poky -name \*.bb -print0 | xargs -0 -n1 /usr/bin/basename | sort > bbs-poky.txt
comm -12 bbs-ark.txt bbs-poky.txt
```
Be sure to diff to the poky version to see whether or not the recipe is still needed. For example, in Sumo `ttf-ubuntu-font-family_0.80.bb` appears to be a duplicate but is actually different because we added `ttf-ubuntu-font-family_0.83.bb` recipe and shared inc file.
