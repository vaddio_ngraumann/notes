## Notes

Zynq Ark machines have both uImage and zImage in /boot

Important squashfs configs:
    CONFIG_SQUASHFS=y
    CONFIG_SQUASHFS_DECOMP_MULTI_PERCPU=y
    CONFIG_SQUASHFS_FILE_DIRECT=y

Disadvantages:
- u-boot does not like squashfs
- cannot remount as rw

Options:

1. squashfs with overlayfs on each individual dir, such as /lib, /usr, etc.
2. squashfs with initramfs, squashfs on each partition. BOOT.BIN, kernel and device tree live in /boot. Full overlay for development mode.
3. squashfs with initramfs, single (or two) ext4 partitions with squashfs file, kernel, devicetree, etc. squshfs mounted in loopback. Full overlay in development mode.

Questions:
1. How to indicate that a filesystem should have an overlay for debug? Presence of certain file?
2. Filesystem layout

Resources:
[Linux kernel overlayfs doc](https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt)
[Embedded Linux: Using Compressed File Systems (LWN)](https://lwn.net/Articles/219827/)
[Filesystem Considerations for Embedded Devices](https://events.static.linuxfound.org/sites/events/files/slides/fs-for-embedded-full_0.pdf)
[A Comparative Analysis BetweenEmbedded Linux Flash File Systems](http://ewh.ieee.org/r4/se_michigan/cs/20131019/A%20Comparative%20Analaysis%20Between%20Embedded%20Linux%20File%20Systems_old_format.pdf)
[FLIR - Embedded Systems Best Practices](http://www.ices.kth.se/upload/events/90/7d55438046eb495980db2ea66e8ab0dc.pdf)

## Benchmarks (non-gst build)

Baseline b27e18f69f5f320ef057755c14afd9338abf4a9a

Uncompressed (106.8M used):
19.834007
18.473995
18.310927
19.667230
free -m after boot: 218M

squashfs gzip (42.3M used):
22.210806
19.774044
20.640810
19.681868
free -m after boot: 205M

squashfs gzip (DECOMP_MULTI) (42.3M used):
22.887318
20.960815
19.850795
20.637233
free -m after boot: 205M

squashfs gzip (DECOMP_SINGLE) (42.3M used):
24.089475
19.757890
19.757345
19.677283
free -m after boot: 205M

squashfs lzo (46.1M used):
22.007442
18.567229
18.257782
18.494059
free -m after boot: 199M

squashfs lz4 (57.8M used):
19.389189
18.560813
18.569369
18.334142
free -m after boot: 198M

squashfs xz (35.3M used):
27.854142
27.847636
27.680787
27.760809
free -m after boot: 208M

squashfs zstd (40.1M used):
22.894172
21.177250
19.920840
19.927230
free -m after boot: 205M

## Benchmarks (gst build)

Baseline 85a948624a376459e4f57b8a0074a1ac4ff074f8

Uncompressed (131.0M used):
20.475638
18.540840
19.420985
18.307258
free -m after boot: 195M

squashfs gzip (52.4M used):
21.299029
19.771180
19.526782
19.650887
free -m after boot: 173M

## Initramfs benchmarks (gst build)

squashfs with no overlay:
20.005978
19.337283

free -m after boot: 173M

squashfs with debug overlay:
21.605645
21.139639
19.937171
19.840814
free -m after boot: 170M (but only because ~3M additional cached)

| Metric | Overlay individual dirs | Initramfs with full overlay |
| :------------- | :------------- | :------------- |
| Initial dev effort | Low | High |
| Maintenance effort | Lower | Higher |
| Boot time | ~20s | ~20s |
| Update flexibility | Low | Medium |
| Future expandibility | Low | High |
| Learning curve | Medium | High |

## Centaur boot times

Nightly, f42986bb6ccd8e9739f9954405eca1d90e2a90ff (128.4M used):
[   17.910847] macb e000b000.ethernet eth0: link up (1000/Full)
[   17.897696] macb e000b000.ethernet eth0: link up (1000/Full)
[   17.755754] macb e000b000.ethernet eth0: link up (1000/Full)
              total        used        free      shared  buff/cache   available
Mem:         381552      124824      199268         312       57460      225724

After, 28b7421df1dc67cde29530efde8e8a44bff0cbbd (usage is of total rootpart):

gzip (52.8M used):
[   20.389937] macb e000b000.ethernet eth0: link up (1000/Full)
[   20.397216] macb e000b000.ethernet eth0: link up (1000/Full)
[   20.467480] macb e000b000.ethernet eth0: link up (1000/Full)
              total        used        free      shared  buff/cache   available
Mem:         381548      127212      142916         312      111420      221852

lzo (57.7M used):
[   20.558657] macb e000b000.ethernet eth0: link up (1000/Full)
[   20.867329] macb e000b000.ethernet eth0: link up (1000/Full)
[   19.500819] macb e000b000.ethernet eth0: link up (1000/Full)
              total        used        free      shared  buff/cache   available
Mem:         381548      128564      126704         316      126280      220260

lzo (256k block size) (57.2M used):
[   21.260850] macb e000b000.ethernet eth0: link up (1000/Full)
[   20.067259] macb e000b000.ethernet eth0: link up (1000/Full)
[   20.147345] macb e000b000.ethernet eth0: link up (1000/Full)
              total        used        free      shared  buff/cache   available
Mem:         381548      129820      122916         316      128812      219044

lz4 (72.7M used):
[   19.680944] macb e000b000.ethernet eth0: link up (1000/Full)
[   19.820857] macb e000b000.ethernet eth0: link up (1000/Full)
[   19.670848] macb e000b000.ethernet eth0: link up (1000/Full)
              total        used        free      shared  buff/cache   available
Mem:         381548      128924      110536         316      142088      219684
