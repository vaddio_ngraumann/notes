### 4/6/22

Eagle block change input issue:
VNG-12754: works
VNG-12884: does not work
VNG-11641:
nightly: does not work

### 3/29/22

```
pipeline_create outpipe interpipesrc listen-to=/com/vaddio/VideoServer1/input/capture0 is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=YUY2,width=1280,height=720,pixel-aspect-ratio=1/1 ! fakesink
pipeline_play outpipe
```

| Timer Delay (ns) | 720p Jitter (ms) with 4 streams |
| :--------------- | :------------------------------ |
|  100 | 0.4-1.0 |
|  200 | 1.2-1.4 |
|  400 | 0.7-0.9 |
|  800 | 0.7-1.2 |
| 1400 | 0.4-0.9 |
| 2000 | 0.4-0.8 |
| 4000 | 0.4-0.6 |

Using yavta to count number of frames over 5% apart (ie, <57fps and >= 63fps):

```
yavta -t1/60 -c600 -f YUYV -s 1280x720 -F/dev/null /dev/video2 | grep fps | grep -v -E "(5[789])|(6[012])\.[[:digit:]]{3} fps" | tee >(cat) | wc -l
```

| Delay (ns) | Frames outside range (3x) | Average |
| :--------- | :------------------------ | :------ |
|  200 | 52, 62, 22 | 45 |
|  400 | 59, 41, 26 | 42 |
|  800 | 53, 38, 32 | 41 |
| 1400 | 43, 16, 57 | 39 |
| 2000 | 39, 52, 45 | 45 |

### 3/16/22

Turns out you really don't need more than one scaler set up in the multi-scaler since they run sequentially anyway. You just have to reset the core each time to program a new resolution. This saves a little further:

3x scalers, 1 samples per clock: LUT 27926 (59.1%), BRAM 67.5 (45.0%)
1x scalers, 1 samples per clock: LUT 26495 (56.1%), BRAM 60.5 (40.3%)

### 3/8/22

multi-scaler LUT/BRAM usage on Eagle-USB:

2 samples per clock, 8 taps, 1080p, YUYV (YUYV8) and NV12 (Y_UV8_420)
2 outputs: 10057, 39
3 outputs: 10239, 42

```
gst-launch-1.0 \
v4l2src device=/dev/video0 io-mode=4 pixel-aspect-ratio=1/1 ! \
video/x-raw,format=YUY2, width=1920, height=1080, framerate=60/1 ! \
interpipesink name=vidcap sync=false async=false \
interpipesrc listen-to=vidcap is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! \
videorate skip-to-first=true drop-only=true ! \
video/x-raw,framerate=30/1 !  \
v4l2convert output-io-mode=5 capture-io-mode=4 ! \
video/x-raw, format=NV12, width=1920, height=1080, pixel-aspect-ratio=1/1 ! \
queue leaky=2 ! \
x264enc speed-preset=ultrafast pass=quant quantizer=25 bitrate=2048 key-int-max=30 ! \
video/x-h264, mapping=/stream, profile=main ! \
queue max-size-time=20000000 ! \
rtspsink service=554 mtu=1400 \
interpipesrc listen-to=vidcap is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! \
v4l2convert output-io-mode=5 capture-io-mode=4 ! \
video/x-raw, format=YUY2, width=1280, height=720, pixel-aspect-ratio=1/1 ! \
queue max-size-buffers=1 leaky=2 ! \
kmssink qos=false plane-id=28 async=false sync=false force-modesetting=true \
modeset-caps=video/x-raw,format=BGRx,width=1280,height=720,framerate=60/1,interlace-mode=progressive \
interpipesrc listen-to=vidcap is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! \
videorate skip-to-first=true drop-only=true ! \
video/x-raw,framerate=60/1 !  \
v4l2convert output-io-mode=5 capture-io-mode=4 ! \
video/x-raw, format=YUY2, width=1280, height=720, pixel-aspect-ratio=1/1 ! \
queue leaky=2 max-size-buffers=1 ! \
v4l2sink device=/dev/video4 io-mode=5 async=false sync=false
```

```
dhcp $kernel_addr_r 10.0.0.6:fitImage && ext4write mmc 0:$rootpart $kernel_addr_r /boot/fitImage $filesize && boot
```

### 3/3/22

1-1 short-term priorities:

AV Brdige Nano stuff:
- Max USB framerate on kestrel
- USB MJPEG on Kestrel

Longer term:
- python3 non-ark
- Maybe Eagle multi-scaler

### 2/16/22

DEP presentation notes:

Desire from Rob:
conference:
4 of anyone's mics (4 rx flows)
tx: 1 for AEC

lecture capture:
8 of our mics (or 4 of anyone's)
tx: 1 again

Lots of people ask about using the shure mx90
Potential new product to replace the pro IO box (Kestrel platform)
Have more parts available for Kestrel than Zynq or Eagle

### 2/14/22

Dante DEP stuff:

- DEP is very sensitive to scheduling. Even setting numDepCores to 1 instead of 0 drastically improves the latency histogram in Dante Controller.
- CPU usage is proportional to number of flows (not channels) which is important. A flow is unidirectional between two devices (unless multicast is enabled) and can contain up to 4 channels. So to test 2 tx and 2 rx flows you would need two other devices plus yours.
- On the Device View page of Dante Controller go to View->View Flow Information (or Ctrl+F) to see number of current flows.

CPU usage per flow on Eagle-USB with numDepCores set to 1. Averaged with top -d3. Note that DepApe is the container process and DepLoopabck is the loopbafck test app.

| Tx Flows | Rx Flows | DepApe CPU % | DepLoopback % |
| :------- | :------- | :----------- | :------------ |
|    0     |     0    |      4.3     |      2.3      |
|    0     |     1    |      7.0     |      2.3      |
|    0     |     2    |      8.3     |      2.3      |
|    1     |     0    |      7.0     |      2.3      |
|    1     |     1    |      9.6     |      2.3      |
|    1     |     2    |     11.0     |      2.3      |
|    2     |     0    |      9.0     |      2.3      |
|    2     |     1    |     12.0     |      2.3      |
|    2     |     2    |     13.3     |      2.3      |

### 2/9/22

DEP Stuff:

- Might need true hardware RNG (maybe this one: https://www.xilinx.com/products/intellectual-property/1-onj5tx.html)
- Might need to disable CPU freq scaling
- How important is PTP? (seems like we should just use it)
- Recommend dedicating up to 3 cores for Dante for most deterministic operation (see "DEP Performance" section)
- There is also a validating your configuration section (run fully-subscribed device for an extended period, make sure each core is not overlaoded in top, ensure that there are no late packets via histogram in Dante Controller)
	- They note that you can safely extrapolate as the CPU usage increases linearly
- DHCP requires "link-local addressing"

Tips for CPU


### 2/4/22

Trying to program CYBLE from host via openocd. "program" does not work - reset halt appears not to work correctly when the target is unprogrammed. However, we can use a few manual commands to get where we need:

```
FIRMWARE=/media/firmware/device-fw/cyble-222005-00-b0-bootloader_0.1.elf

echo cypress_ble_reset > /sys/bus/platform/drivers/gpio-output/unbind

openocd -f ~/cyble.cfg \
	-c init -c reset -c halt \
	-c "flash write_image $FIRMWARE 0 elf" \
	-c "verify_image $FIRMWARE 0 elf" \
	-c "reset run" -c exit

echo cypress_ble_reset > /sys/bus/platform/drivers/gpio-output/bind
```

openocd config:
```
# Only needed if you want to telnet to the server:
bindto 0.0.0.0

interface sysfsgpio

transport select swd

sysfsgpio_swd_nums 996 997
sysfsgpio_srst_num 966
reset_config srst_only srst_push_pull

source [find target/psoc4.cfg]
```

### 2/3/22

Talk with Nick about dante
BLE
Ping Matt about eptz

### 1/24/22

Trying to address issue where MJPEG streaming doesn't start. Two failure modes:

1. Interrupts are still going along just fine for scaler and encoder
2. No interrupts for either.

For (1), I think I solved it. Was able to reproduce by adding a sleep(1) at the beginning of usb_video_record's stop_stream. Exposed a race in the kernel when stopping streaming where the stream ep sometimes wouldn't be reset.

For (2), it appears this actually has been present for a long time even on Eagle-USB with  YUYV format. Tested: Nightly, 1.1.1. Just ran uvc-test over and over to capture 60 frames with yavta, and eventually it hangs up. Restarting the output pipeline fixes the issue.

Was not able to reproduce on 1.0.0 nor 1.1.0.

Tested right before interpipe: 0e2e939528: Pass
Interpipe merge: ca3037c9b1: Fail

Root cause: rarely, the initial buffer being sent by `interpipesrc` actually uses the previous timetsamp of the pipeline rather than restarting with `restart-ts`. However, subsequent buffers use the restarted TS, at which point `videorate` discards them all because they're behind the last buffer.

### 1/12/22

Jenkins slaves with idle timeouts updated:

- [ ] lnx-blade-0
- [ ] lnx-blade-1
- [ ] lnx-blade-2
- [x] lnx-blade-3
- [ ] lnx-blade-4
- [ ] lnx-blade-5
- [ ] lnx-blade-6
- [ ] lnx-build-slave-1
- [ ] lnx-builder-0
- [ ] lnx-vad-srv-02-r9

### 1/11/22

Steps to switch Jenkins master:

1. Run Jenkins backup config job
2. Set Jenkins to shut down
3. Rsync jobs
4. Re-provision lnx-jenkins1b
5. Switch haproxy
6. Verify Linux slaves, kick off nightlies
7. Reconnect windows slaves and kick off FPGA build(s)
8. Test buildy
9. Re-provision lnx-jenkins1a

### 1/6/22

Reviews by 1/22

### 12/16/21

TODO for jenkins: add the following to /etc/sudoers for Jenkins reboot job:

```
jenkins ALL=NOPASSWD: /usr/sbin/reboot
```

### 12/15/21

Ran into a checksum error when attempting to `git svn fetch`. Turns out rewriting the history with git lfs confuses git svn ("Encountered x file(s) that should have been pointers, but weren't"). I worked around this by adding the old hardware repo (the one cloned from git svn with no changes) as a remote, fetching, and cherry-picking the new commits. Any files that come up as conflicts either can be deleted if the commit deleted them (git rm -f), or if modified need to be:

- Added with git add <path>
- Reset to the cherry-pick with git restore --theirs <path>

### 12/13/21

Setting up Jenkins master on lnx-jenkins1a.milestone.local.

1. Create jenkins user and jenkins group with default home dir:
```
sudo useradd -U -m jenkins
```
2. Copy over ssh keys from old jenkins master (/var/lib/jenkins/.ssh). They can also be found on the protected build group One Drive page: https://grpleg.sharepoint.com/:f:/r/sites/vaddioengineering2/Documents%20partages/General/Internal%20editable/Build%20team?csf=1&web=1&e=8CdnFk
3. Mount a volume for Jenkins with at least 500G of space at `/mnt/jenkins`
4. Clone docker-jenkins repo to /mnt/jenkins
```
sudo su jenkins
cd /mnt/jenkins
git@bitbucket.org:vaddio/docker-jenkins.git
# right now, you need a special branch, but after it is through PR master will work
git checkout nickg/phusion-jenkins-plugins
```
5. Install some dependencies:
```
sudo apt install make docker.io docker-compose
```
6. Enable docker on boot:
```
sudo systemctl enable docker.service
```
7. Let the Jenkins user run docker:
```
sudo usermod -a -G docker jenkins
```
8. Create an .env file with the appropriate jenkins user and group ID's:
```
sudo su jenkins
cd /mnt/jenkins/docker-jenkins
echo JENKINS_UID=`id -u` > .env
echo JENKINS_GID=`id -g` >> .env
```
9. Go to docker-jenkins and build the image
```
cd /mnt/jenkins/docker-jenkins
make build
```
10. Modify docker-compose.yml to listen on all interfaces (for testing):
vim docker-compose.yml
(remove `127.0.0.1:` from the 8080 entry in the ports list)
11. Seed the config with a recent copy of jenkins-config:
```
cd /mnt/jenkins
git clone git@bitbucket.org:vaddio/jenkins-config.git
git clone git@bitbucket.org:vaddio/docker-jenkins.git
cp --no-target-directory -r jenkins-config/files docker-jenkins/jenkins_home/
```
12. Copy jenkins secrets and extract to jenkins_home:
```
cd /mnt/jenkins/docker-jenkins/jenkins_home
tar xvf ~/jenkins-secrets.tar.gz
```
13. Optional: Update the `jenkins_home/jobs` directory with a recent backup of
 the jenkins jobs:

```
rsync --rsync-path 'sudo -u jenkins rsync' -avP --delete ./jobs support@lnx-jenkins1a.milestone.local:/mnt/jenkins/
```

See also: configuring an overlayfs on the jobs directory below.
14. Now you can run Jenkins:
```
make start
```

Note that the container will automatically restart due to the `restart:
always` policy. If this is not what you want, either modify the restart policy
to `unless-stopped` or `no` in `docker-compose.yml`, or run `make stop`
followed by `make rm` to remove the container after use.

Other notes:

- If you want to clear out the home directory and start fresh, such as from a
newer backup, simply remove `docker-jenkins/jenkins_home` and recreate it, then follow steps 10-12 again.

Configuring an overlayfs on the jobs directory for testing:

For development, you can mount an overlayfs over the top to avoid accidental
deletion (you will need to run these as the support user for sudo access):
```
sudo mkdir -p /mnt/jenkins/upper /mnt/jenkins/work
sudo chown jenkins:jenkins /mnt/jenkins/upper /mnt/jenkins/work
sudo mount -t overlay overlay -o metacopy=on,lowerdir=/mnt/jenkins/jobs,upperdir=/mnt/jenkins/upper,workdir=/mnt/jenkins/work /mnt/jenkins/docker-jenkins/jenkins_home/jobs
```

To clean this up when you are done, stop Jenkins and run:
```
sudo umount  /mnt/jenkins/docker-jenkins/jenkins_home/jobs
sudo rm -rf /mnt/jenkins/upper /mnt/jenkins/work
```

### 12/9/21

Jenkins master migration:

what is the purpose of srv-01/02/03?
	Is evertyhing set up via ansible, or was anything manual?
Which server for Jenkins master?
DNS?
	haproxy on vm
Then Nick needs to test builds:
Windows slaves

### 12/2/21

Latest todos:

1. Git training for hardware group
2. HW SVN server migration
3. Jenkins master migration
4. MS teams MJPEG stuff (VNG-12661, VNG-12746)

Nate security install handoff:

bitbake-mirror: Nate has a different host that's running a web server. nate suggests hosting on vad-srv-01/02/03
jenkins: Dockerized jenkins needs to run as user 1000. Nate created a user on srv-01 and srv-03.
Regular dockerized jenkins functions right now
docker-jenkins repo (nate has a version of vaddio-iac that points at this)
user can't be an unnamed user since it needs outputs
lnx-srv-02 has a 3-vm vagrant env (vm for builder, jenkins, and web host)

http://jenkins.vad02.milestone.local
user: jenkinsadmin
pass: jenkinspassword

DNS wildcard is useful because we use haproxy on the hosts that were added - you can run dns underneath those (I think?)

### 12/1/21

Converting hardware SVN to git. More or less following Atlassian SVN guide: https://www.atlassian.com/git/tutorials/migrating-convert

SVN questions:
Trying to clean out repo to start fresh.

What are other files we can ignore?
Step files?

Authors file (assumed no author is jeff since he's been committing like that for a while):

```
(no author) = Jeff Forshee <jeff.forshee@legrand.com>
bruceh = Bruce Honnigford <bruce.honnigford@legrand.com>
erick = Eric Koester <eric.koester@legrand.com>
jefff = Jeff Forshee <jeff.forshee@legrand.com>
josiahq = Josiah Quant <josiah.quant@legrand.com>
williamg = William Graham <william.graham@legrand.com>
```

1. Clone: `git svn clone --stdlayout --authors-file=authors.txt http://lnx-svn.vaddio.com/hardware/`
2. Run Atlassian's clean-git script (this didn't do much since we don't have many branches nor tags):
```
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar clean-git
java -Dfile.encoding=utf-8 -jar ~/svn-migration-scripts.jar clean-git --force
```
3. Run `git prune && git gc` to pack the repository to a more reasonable size.
4. Clone the repo to a new spot just to avoid accidentally nuking the copy you just downloaded.
5. Ask git lfs for a list of files it suggests tracking: `git lfs migrate info --everything --above="1MB"`
6. Interactive rebase `7bf9e4be` commit to remove large Centaur firmware files which were removed later.

Get the top 100 largest files, then create a list of extensions
```
git lfs migrate info --everything --top 100 > top-100.txt
cat top-100.txt | cut -d' ' -f1 | sort | uniq > extensions.txt
# Hand-modify extension list as needed, then generate:
sed 's/$/ filter=lfs diff=lfs merge=lfs -text/' extensions.txt > ../new-gitattributes
```

Convert to lfs (thanks https://github.com/git-lfs/git-lfs/issues/3543) (seems to not include some objects?)
```
git filter-branch --index-filter "cp /full/path/to/new-gitattributes ./.gitattributes && git add .gitattributes" --tag-name-filter cat --prune-empty -- --all
git lfs migrate import --everything --fixup
```

Or:
```
exts=`cut -d' ' -f1 new-gitattributes | paste -sd ','`
git lfs migrate import --include="$exts" --everything
```

old:
7. Convert to git lfs (needs git lfs >= 2.13): `git lfs migrate import --everything --above="1MB"`

8. Remove the origin remote (so there is no ref to a now completely different history), prune objects from the reflog and expire again:
```
git remote remove origin
git reflog expire --expire=now --all
git gc --prune=now --aggressive
```

Latest:
```
exts=`cut -d' ' -f1 gitattributes-hardware | sed 's/\*\.//' | paste -sd ','`
git clone -n hardware hardware-2
pushd hardware-2 && git remote remove origin && popd
# Remove ~ files which caused issues with lfs:
java -jar bfg-1.14.0.jar --delete-files "*~*" --no-blob-protection --private ./hardware-2
# Convert to lfs, remove gitattributes generated all of the place
java -jar bfg-1.14.0.jar --convert-to-git-lfs "*.{$exts}" --no-blob-protection --private ./hardware-2
java -jar bfg-1.14.0.jar --delete-files "*.gitattributes" --no-blob-protection --private ./hardware-2
# Then, interactive rebase and add gitattributes as new second commit
git rebase -i `git rev-list HEAD | tail -n 1`
cp ../gitattributes-hardware .gitattributes
git add .gitattributes && git commit -m "Add gitattributes for git lfs"
git rebase --continue
git reflog expire --expire=now --all && git gc --prune=now --aggressive
```

```
java -jar bfg-1.14.0.jar --convert-to-git-lfs "*.{[pP][dD][fF],[sS][tT][eE][pP],[zZ][iI][pP]}" --no-blob-protection --private ./hardware-2 && java -jar bfg-1.14.0.jar --delete-files "*.gitattributes" --no-blob-protection --private ./hardware-2
# Then, interactive rebase and add gitattributes as new second commit
git rebase -i `git rev-list HEAD | tail -n 1`
cp ../gitattributes-hardware .gitattributes
git add .gitattributes && git commit -m "Add gitattributes for git lfs"
git rebase --continue
git reflog expire --expire=now --all && git gc --prune=now --aggressive
```

Add SVN later:
```
git svn init --prefix=legacy-svn/ --stdlayout http://lnx-svn.vaddio.com/hardware
git config svn.authorsFile /home/nickg/workspace/hardware-svn-authors.txt
mkdir .git/refs/remotes/legacy-svn
echo bf3128dbfa07f1ae82a8dfd21ef7cc22d60715d8 > .git/refs/remotes/legacy-svn/trunk
git svn fetch
git svn rebase
```

authorsFile = /home/nickg/workspace/hardware-svn-authors.txt

### 12/1/21

Figured out the flicker mystery. There is a certain size of JPEG that causes two packets coming over USB to be appended together (zero-length packet issue). I figured this out because I checked for the end-of-JPEG sequence in wireshark immediately followed by a UVC packet header (ff d9 02). I already had images saved from when this happened (the images were size 524284, aka 512k - 4), so I just truncated the image to the correct size and played it with multifilesrc. A normal jpeg came through the other side just fine, but this one caused havoc and varying sizes:

```
# eagle-usb:
gst-launch-1.0 multifilesrc location=000280-trunc.jpg ! image/jpeg,width=1920,height=1080,framerate=30/1,colorimetry=bt709 ! queue ! v4l2sink device=/dev/video6 io-mode=2

# host:
yavta -t1/30 -c -f MJPEG -s 1920x1080 -F/dev/null /dev/video0

# output (would expect to see the size of the JPEG)
8 (0) [-] none 0 524284 B 5128.084702 5128.087563 0.000 fps ts mono/SoE
9 (1) [-] none 1 262124 B 5128.087615 5128.088974 343.289 fps ts mono/SoE
10 (2) [-] none 2 261986 B 5128.089021 5128.089608 711.238 fps ts mono/SoE
11 (6) [-] none 8 261934 B 5128.092697 5128.093255 272.035 fps ts mono/SoE
12 (7) [-] none 9 524163 B 5128.093300 5128.094470 1658.375 fps ts mono/SoE
13 (2) [-] none 13 262026 B 5128.096906 5128.097547 277.316 fps ts mono/SoE
14 (3) [-] none 15 261934 B 5128.097522 5128.098126 1623.377 fps ts mono/SoE
15 (4) [-] none 16 262142 B 5128.098172 5128.098926 1538.462 fps ts mono/SoE
```

The size of 000280-trunc.jpg is 326652 bytes.

### 11/30/21

Nate G. devops knowledge share:

Bitbake-mirror, jenkins, and linux-svn are not yet upgraded. These need new configuration etc.
All blades are upgraded.

WIP:

Not sure what's going on with JPEG flicker (VNG-12746). Used identity element with trace message in `gst_identity_update_last_message_for_buffer` but the buffer sizes are correct when the flickering occurs. This means it must be lower level. If I increase `max_payload_size` to something like 1MB, the problem also goes away.

### 11/23/21

Build slaves left to migrate:

jenkins, bitbake mirror, linux svn
build slave 3, 6, 7, 2

### 11/19/21

Nate Vada knowledge share:

- Who updates Vada? Not sure on whose responsibility it is. Sometimes it's when the ECO goes out, sometimes it's later. Steve B. and Miles N. should know when. Dave S. needs to know too for when to update the mfg. test.
- vad-srv02 is the NFS server and Jenkins runner
- vad-srv03 is Dave Paddin's VM virtual

Main repo is "Vada". "dfu-archive" and "dfu-zynqmp-archive" store the boot loader files.
"vad_dfu" isn't really used but is mostly for developers.

Gitlab: Dockerized python and docker yocto, but not much else is done on there today
Configuration would be there to deploy the runner to all of the build agents.

### 11/9/21

Questions for Integra:

- Do you provide consulting as well as contracting?
- Do you have any experience with firmware update over USB, either via USB DFU, Microsoft's CFU, or other means?

### 11/1/21

| Config | Min. DEP latency | Min. dtest latency | DepApe CPU % |
| :----- | :--------------- | :----------------- | :----------- |
|    2x2 |                  |                    |              |
|    4x4 |                  |                    |              |
|    8x8 |                  |                    |              |
|  16x16 |                  |                    |              |

### 10/27/21

Windows update CFU stuff:

Added the example HID report from Host/CFUFirmwareSimulation/sys/DmfInterface.c to a new HID function. Built the example application in Tools, but I needed to change a few things like VID, PID, USAGEPAGE, and VERSION_FEATURE_USAGE, otherwise it couldn't find the device.

Looks like f_hid needs support for get_report (feature), also generically known as "Feature Reports", which we don't currently have. set_report(feature) was added a while back for the Skype for Business mute stuff.

### 10/26/21

Arbitrary format/quality for Cast JPEG encoder:

- Header and quantisation tables should be stored as bytes, must be byte-swapped when written
- Everything else can be stored as 32-bit words and written directly
- For VRC: The "base" quantization table is the same as the regular quantisation table, just written to a special spot.

### 10/14/21

Next tasks:
- [x] Finish Eagle DisplayPort testing
- [x] Teams stuff (as of 10/15/21)
- [ ] Small tickets (Remove Eagle etc)
- [ ] R core/ DSP R&D

Zynq ultrascale DP out: Only shows 1080p60 resolutions with Hobbit. For some reason drm_dp_max_link_rate is returning 2.7 instead of 5.4 from the DPCD (zynqmp_dp.c:zynqmp_dp_connector_detect). I hard-coded it to 5.4, and 4k seems to work.

Testing primary plane with Petalinux:

```
/etc/init.d/xserver-nodm stop
modetest -D "fd4a0000.zynqmp-display" -s 42@40:3840x2160-30@BG24
```

Test overlay plane. Note that the overlay appears below the primary, which is confusing, so I set the alpha to 0 so it shows up:

```
modetest -D "fd4a0000.zynqmp-display" -w 39:alpha:0 -s 42@40:3840x2160-30@BG24 -P 38@40:3840x2160@YUYV
```

YUV444 output:

```
modetest -D "fd4a0000.zynqmp-display" -w 40:output_color:1 -s 42@40:3840x2160-30@BG24
```

Tested sound as well:

```
aplay --buffer-size=100000 -f dat -D hw:0,0 test2.wav
```

### 10/5/21

Tasks for Nick S:

- Improve population of I2C bus number for vng-updater on Ark (VNG-8588)
- Add build-time check for kernel config merge issues (VNG-10462)
- Disable 4k SPI flash sectors on Ark-based Zynq products and validate (VNG-12142)
- Build script cleanup: Run artifact creation scripts in container (VNG-11436), improve error handling (VNG-11723)
- Improving dts/dtsi directory structure and build scheme on Ark (VNG-11812)
- Remove zynqmp-eagle machine and eagle product (VNG-12599)
- Setting up Yocto mirrors for mostly no-network builds (VNG-9420 and others, Ask Nate G. about this)

### 9/30/21

```
(gdb) p 'malloc.c'::mp_
$2 = {trim_threshold = 22773760, top_pad = 131072, mmap_threshold = 11386880, arena_test = 8,
  arena_max = 0, n_mmaps = 0, n_mmaps_max = 65536, max_n_mmaps = 10, no_dyn_threshold = 0,
  mmapped_mem = 0, max_mmapped_mem = 44048384, sbrk_base = 0x555556a000 "", tcache_bins = 64,
  tcache_max_bytes = 1032, tcache_count = 7, tcache_unsorted_limit = 0}
```

MALLOC_TRIM_THRESHOLD_=1000000 and MALLOC_MMAP_THRESHOLD_=1000000 over lunch on eagle-usb: before 77.5-78m RES with encoder on. Increased to 89.1m after lunch.

Worked better with trim threshold at 128k and mmap threshold slightly higher around 512k (see malloc.c for details and inspriation).

Can use this to watch the nubmer of heap pages:

```
watch -n1 sed -n '/heap/,/lib/p' /proc/`pidof gstd`/maps \| wc -l

```
### 9/29/21

Disable internal transmit delay on AM572x ethernet in u-boot:

```
mw 0x4a002534 0x06000000 1 && md 0x4a002534 1
```

```
Thread 215 "queue48:src" hit Breakpoint 4, _sysmem_new_block (flags=(unknown: 0), maxsize=140738, align=0,
    offset=0, size=140738) at ../../gstreamer-1.16.0/gst/gstallocator.c:407
407       align |= gst_memory_alignment;
(gdb) bt
#0  _sysmem_new_block (flags=(unknown: 0), maxsize=140738, align=0, offset=0, size=140738)
    at ../../gstreamer-1.16.0/gst/gstallocator.c:407
#1  0x0000007fbf561198 in gst_buffer_new_allocate (allocator=0x0 [GstAllocator], size=140738,
    params=<optimized out>) at ../../gstreamer-1.16.0/gst/gstbuffer.c:881
#2  0x0000007faf787ad4 in ?? () from /usr/lib/gstreamer-1.0/libgstx264.so
#3  0x0000007faf78b970 in ?? () from /usr/lib/gstreamer-1.0/libgstx264.so
#4  0x0000007fbd7e7bbc in gst_video_encoder_chain (pad=<optimized out>,
    parent=0x7f8000b720 [GstObject|x264enc15], buf=<optimized out>)
    at ../../../../gst-plugins-base-1.16.0/gst-libs/gst/video/gstvideoencoder.c:1558
#5  0x0000007fbf59f73c in gst_pad_chain_data_unchecked (data=0x7f800125a0, type=<optimized out>,
    pad=0x7fb0182d40 [GstPad|sink]) at ../../gstreamer-1.16.0/gst/gstpad.c:4326
#6  gst_pad_push_data (pad=0x7fb0173a10 [GstPad|src], type=type@entry=4112, data=data@entry=0x7f800125a0)
    at ../../gstreamer-1.16.0/gst/gstpad.c:4582
#7  0x0000007fbf5a7ca4 in gst_pad_push (pad=<optimized out>, buffer=buffer@entry=0x7f800125a0)
    at ../../gstreamer-1.16.0/gst/gstpad.c:4701
#8  0x0000007fbd1a1f68 in gst_queue_push_one (queue=0x7fb015a4a0 [GstQueue|queue48])
    at ../../../gstreamer-1.16.0/plugins/elements/gstqueue.c:1384
#9  gst_queue_loop (pad=<optimized out>) at ../../../gstreamer-1.16.0/plugins/elements/gstqueue.c:1537
#10 0x0000007fbf5d86d0 in gst_task_func (task=0x7f80012950 [GstTask|queue48:src])
    at ../../gstreamer-1.16.0/gst/gsttask.c:328
#11 0x0000007fbf4035a8 in ?? () from /usr/lib/libglib-2.0.so.0
#12 0x0000007fbf402b14 in ?? () from /usr/lib/libglib-2.0.so.0
#13 0x0000007fbf3646fc in start_thread (arg=0x7f8fffd5a6) at pthread_create.c:486
#14 0x0000007fbf2bcfac in thread_start () at ../sysdeps/unix/sysv/linux/aarch64/clone.S:78
```

### 9/24/21

Try breaking on gst_dmabuf_allocator_init. May need to add a definition for gst_dmabuf_allocator_finalize and break within that.


```
while true; do echo disable; dbus-send --system --print-reply --dest=com.vaddio.VideoServer1 /com/vaddio/VideoServer1/output/rtstream0 com.vaddio.VideoServer1.OutputIface1.enable boolean:false; sleep 3; echo enable; dbus-send --system --print-reply --dest=com.vaddio.VideoServer1 /com/vaddio/VideoServer1/output/rtstream0 com.vaddio.VideoServer1.OutputIface1.enable boolean:true; sleep 7; done
```

master: 100 -> 114m -> 123.8m after 5 enables.
up 17 min: 240m RES.

Starting at 17 minutes, running client in a loop. At 20m, up to 296MB res.

new build: after 10 enables, 58m RES (1033m VIRT).

At 20 minutes: 86.7m RES, 1033m VIRT.

Restarted test: VIRT: 1609m, RES: 30m

### 9/22/21

Tomorrow: Try searching for left) and see the number jump from 3 left to 7 left when the encoder starts, then never back down.

x264 memory leak: I confirmed that the v4l2 fd memory is being leaked. I did this by doing an FPGA build with the DMA having NV12 enabled so I could remove the v4l2convert which was noisying up the logs quite a bit. Then run gstd with `GST_DEBUG=*fdmemory*:7`. gdb shows the ref count as 2 for the buffers:

```
0:00:21.300115057  5764   0x7fb0158450 DEBUG               fdmemory gstfdmemory.c:290:gst_fd_allocator_alloc: 0x7f98007040: fd: 16 size 3110400
0:00:21.300172747  5764   0x7fb0158450 DEBUG               fdmemory gstfdmemory.c:74:gst_fd_mem_free: 0x7fb0131e00: freed
[Thread 0x7fad7fa180 (LWP 6429) exited]
[Thread 0x7fae7fc180 (LWP 6427) exited]
[Thread 0x7fadffb180 (LWP 6428) exited]
[Thread 0x7fa7fff180 (LWP 6431) exited]
[Thread 0x7fa67fc180 (LWP 6426) exited]
[Thread 0x7fa6ffd180 (LWP 6433) exited]
[Thread 0x7fa77fe180 (LWP 6432) exited]
[Thread 0x7facff9180 (LWP 6430) exited]
[Thread 0x7faffff180 (LWP 6425) exited]
[Thread 0x7faeffd180 (LWP 6424) exited]
[Thread 0x7faf7fe180 (LWP 6224) exited]
^C
Thread 1 "gstd" received signal SIGINT, Interrupt.
0x0000007fbf2b4218 in __GI___poll (fds=0x55556700d0, nfds=2, timeout=<optimized out>) at ../sysdeps/unix/sysv/linux/poll.c:41
41        return SYSCALL_CANCEL (ppoll, fds, nfds, timeout_ts_p, NULL, 0);
(gdb) set $ptr = 0x7f98007040
(gdb) p *(GstFdMemory *)$ptr
$1 = {mem = {mini_object = {type = 0x5555587130 [GstMemory], refcount = 2, lockstate = 65536, flags = 1, copy = 0x7fbf5993b8 <_gst_memory_copy>, dispose = 0x0, free = 0x7fbf598c20 <_gst_memory_free>, priv_uint = 3,
      priv_pointer = 0x7f9a2dbdb0}, allocator = 0x7fb013c670 [GstDmaBufAllocator|dmabufallocator2], parent = 0x0, maxsize = 3110400, align = 0, offset = 0, size = 3110400}, flags = GST_FD_MEMORY_FLAG_DONT_CLOSE, fd = 16, data = 0x0,
  mmapping_flags = 0, mmap_count = 0, lock = {p = 0x0, i = {0, 0}}}
```

Thinking of value add features from Darrin:

- AI functions on motorized ptz cameras?
- snapshot of different video streams?
- 4k30 usb streaming?

### 9/21/21

Looking at x264 leak again. Tried using libgobject-list to list objects alive when gstd shuts down, but it seems like things are shutting down appropriately. Things still to check:

- Mini objects (like buffers) do not show up in this list
- SysMem GstAllocator might be hiding something

### 9/20/21

Containerize arago builds:

```
# Outside container:
export CROSS_DIR="/home/nickg/workspace/arm-2009q1"
export DOWNLOADS_DIR="/home/nickg/workspace/arago-downloads"
export EXTRA_CONTAINER_ARGS="\
    -v ${CROSS_DIR?}:/opt/arm-build-compiler \
    -v ${DOWNLOADS_DIR?}:/var/lib/jenkins/arago/downloads \
"

# In container:

apt-get update && apt-get install gdb libncurses5:i386 texi2html cvs subversion ruby yui-compressor ruby-thor ruby-compass

ln -s vaddio-git/vng-custom .

sudo -E /sbin/setuser builduser /bin/bash

export VNG_REPO="git@bitbucket.org:vaddio/vaddio_ngraumann-vng.git"
export VAD_SCM_REPO=$(echo "$VNG_REPO" | sed 's!:!/!')
export VAD_SCM_OBJ=1db5c6f48abcbbcacced0ec9ae9f9d6971726fb4

export PATH=/opt/arm-2009q1/bin:$PATH
export MACHINE="vaddio-av-bridge"
export COMMIT="$VAD_SCM_OBJ"

/var/build/vaddio-git/tools/build-scripts/vng-build --machine ${MACHINE} --build-dir /var/build --branch ${COMMIT} --filesystem-image ${MACHINE}-debug
```

Issues:

- Needed to apt-get install more dependencies for Arago
- Had to patch bison to remove gets() call
- HOME needs to be defined (/var/build is fine) for Thorfile
- Missing unistd.h in libvaddioutils-native
- Had to cherry-pick some fixes to the artifact creation scripts (VNG-9228, VNG-11419)
- Need to resolve: Seems to error out with permission denied first time creating artifacts. Subsequent works fine.

Other notes:
- Download and cross compiler need to be mounted with Docker.
- No sstate cache needed nor used
- Need to remove downloads directory every time otherwise Bitbucket fetcher fails. This is probably why the make symlinks task is important.

### 9/16/21

1-1:

Roadmap achievement Milestones: Eagle-USB 1.0.0 - 1.1.0, Kestrel 1.0.0

Over lunch Eagle-USB:
gstd RES before: 186832
gstd RES after:  230044

Kestrel:
gstd RES before: 19568
gstd RES after:  25600

TODO:
- [x] Try requesting USB on Eagle-USB over and over and ensure no leaks - does not appear to be an issue on Eagle-USB.
- [x] Try HPC port (did not resolve CPU usage at all)
- [x] Try plutus decoding new h.264 settings from Eagle. Did not work with fastdecode nor zerolatency.


Eagle metrics after updates (static, gstd cpu%, motion, gstd cpu%):

Before:
1080p/best:   3700, 285, 8700, 345
1080p/better: 2100, 230, 3600, 265
1080p/good:   1150, 145, 1600, 160
720p/best:    2200, 140, 3100, 158
720p/better:  1500, 136, 2200, 155
720p/good:     700,  88,  950,  97

After  (static, media-server cpu%, motion, gstd cpu%):

1080p/best:   2500, 280, 5500, 325
1080p/better:  800, 210, 2000, 250
1080p/good:    500, 125, 1100, 150
720p/best:     750, 120, 2000, 145
720p/better:   350, 110, 1250, 140
720p/good:     300,  65,  600, 75

Remus Prime similar image (static, motion):

1080p/best:   3500, 11000 (can spike as high as 17000)
1080p/better: 1800, 5500
1080p/good:   1100, 3500
720p/best:    5000, 12000
720p/better:   900, 3600
720p/good:     450, 1600

```
setenv bootargs 'dyndbg=\\"file drivers/of/device.c +p\\"'
```

### 9/15/21

Ran videotestsrc direclty into x264enc at 1080p30 (it keeps up!) and have not seen the leak. Based on this and other messages, it seems like the issue is some sort of allocation issue with v4l2src. Tried manually disabling copy threshold in v4l2 buffer pool, did not help.

With:
```
x264enc speed-preset=ultrafast tune=zerolatency pass=quant quantizer=23 bitrate=2048 key-int-max=8 rc-lookahead=5 sync-lookahead=5
```
RES before lunch: 34296
RES after lunch: 35388

with rc-lookahead and sync-lookahead=0:
RES before: 122256
RES after 5m: 104380

From x264.c:
```
- zerolatency:
  --bframes 0 --force-cfr --no-mbtree --sync-lookahead 0 --sliced-threads --rc-lookahead 0
```

with b-adapt=false:
RES before: 105176
RES after: ~105000

See: https://titanwolf.org/Network/Articles/Article?AID=3f0a6814-5cc8-46b4-a4fa-afc679741754#gsc.tab=0

With rc-lookahead=8, sync-lookahead=0:

RES before: 67764
RES after: 164868

with rc-lookahead=4, sync-lookahead=0:

RES before: 67948
RES after:

So it seems like sliced-threads is the key here. Somehow, CPU usage is lower, and the memory leak issue isn't there. Quality might suffer, but that's probably OK.

### 9/9/21

- Also can reproduce with x264enc running into fakesink (this means rtspsink is not the culprit!)
Seems like heap may not be the issue. See `malloc_stats` before and after the leak starts which are roughly the same, within a few MB even though virtual memory use went up ~50MB. This makes me think it's mmap or something else that's growing.

In gdb:
```
break __mmap64 if $x1 > 8392704
b __GI___libc_free if $x0 == 0x7f24000000
b __GI___libc_malloc if $x0 > 8392704
```
Useful, but not sure it's helpful: https://codearcana.com/posts/2016/07/11/arena-leak-in-glibc.html

### 9/8/21

Interpipe debug of x264:

```
gstd-client
pipeline_create vcap_pipe v4l2src device=/dev/video0 io-mode=4 pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2, width=1920, height=1080, framerate=30/1, colorimetry=bt709 ! interpipesink name=capture sync=false async=false
pipeline_play vcap_pipe

pipeline_create rtstream_pipe interpipesrc listen-to=capture is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue leaky=2 ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12,width=1920,height=1080,pixel-aspect-ratio=1/1 ! videorate skip-to-first=true max-rate=30 drop-only=true ! video/x-raw,framerate=30/1 ! x264enc speed-preset=ultrafast pass=quant quantizer=23 bitrate=2048 key-int-max=8 ! video/x-h264, mapping=/stream, profile=main ! queue ! rtspsink service=554
pipeline_play rtstream_pipe

pipeline_create rtstream_pipe interpipesrc listen-to=capture is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue leaky=2 ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12,width=1920,height=1080,pixel-aspect-ratio=1/1 ! videorate skip-to-first=true max-rate=30 drop-only=true ! video/x-raw,framerate=30/1 ! x264enc speed-preset=ultrafast pass=quant quantizer=23 bitrate=2048 key-int-max=8 ! video/x-h264, mapping=/stream, profile=main ! queue ! fakesink
pipeline_play rtstream_pipe

pipeline_create hdmi_pipe interpipesrc listen-to=capture is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue leaky=2 ! kmssink sync=false async=false
pipeline_play hdmi_pipe

pipeline_delete rtstream_pipe
pipeline_delete vcap_pipe
```

```
src/gz aarch64 http://10.0.0.6:8000/aarch64
src/gz all http://10.0.0.6:8000/all
src/gz zynqmp-eagle-usb http://10.0.0.6:8000/zynqmp_eagle_usb

option cache_dir /mnt/cache
```

Install gdb plus recommendations:
```
opkg install gdb libc6-dbg libc6-thread-db
```

Install other debug packages, but skip recommendations:
```
opkg install --no-install-recommends gstreamer1.0-dbg gstd-dbg gstreamer1.0-plugins-base-dbg gstreamer1.0-plugins-good-dbg gstreamer1.0-plugins-bad-dbg gstreamer1.0-plugins-ugly-dbg libx264-dbg glib-2.0-dbg
```

### 9/7/21

Used `dmalloc` to debug memory allocations in lib x264. Seems to be cleared of any wrongdoing. Next planning to use gstd to debug with valgrind in gstreamer.

Good read: "How to Become a GStreamer Developer": http://www.ideasonboard.org/blog/20160805-gst-debugging.html

### 8/25/21

Ethos Framebuffer Read -> MJPEG:

```
media-ctl -v -V '"dummy_v4l2.dummy_v4l2@0":0 [fmt:UYVY8_1X16/1920x1080]'
gst-launch-1.0 v4l2src device=/dev/video1 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! v4l2sink device=/dev/video3 io-mode=5
```

```
rtsp-server -p 554 -u /stream v4l2src device=/dev/video0 do-timestamp=true io-mode=2 ! queue max-size-bytes=8388608 ! rtpjpegpay perfect-rtptime=false pt=26 name=pay0 mtu=65000
```

### 8/20/21

Can we use YUYV framebuffer on Eagle (This would allow EPTZ with no RGB)?

- Backported 5.10 fixes and hard-coded YUYV DRM format. Seems to work. However setting YUYV for xlnx,vformat in dts causes it to not show up again.

I thought I could manually twiddle the bits into YUV format by specifying a fb_setcolreg reg implementation (and commenting out fb_setcmap). This, along with requesting the YUYV format in xlnx_fb_gem_fbdev_fb_create, got me close to what I needed, but the colors aren't quite right for images other than grayscale.

Then I thought maybe NV16 format would be better, since it seems like the fbdev subsystem is okay with planar formats. I built a new FPGA image with NV16 (Y_UV8) enabled in the FB write, but it seems like the Xilinx stuff isn't thrilled about trying to set up multiple planes. Modetest works (`modetest -M xlnx -s 31@29:1920x1080@NV16`), but not the framebuffer console.

### 8/19/21

1-1:

- [x] Fix for NULL ptr dereference on Eagle-USB (quick) - waiting for Jenkins
- [x] CAST core integration support - in progress
- [ ] Eagle scaler resource reduction depending on FPGA group
- [ ] A2E decoding of Eagle, then Kestrel and Helios
- [ ] Eagle TW removal (remoteproc)
- [ ] Eagle/Kestrel GPU

### 8/18/21

In gstd-client:
```
pipeline_create capture0_pipeline input-selector name=selector ! textoverlay name="overlay" font-desc="UbuntuMono-R 12" silent=true valignment=top ! videorate skip-to-first=true ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1 ! interpipesink name=capture0 sync=false async=false v4l2src device=/dev/video3 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,framerate=30/1 ! selector. v4l2src device=/dev/video6 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,framerate=15/1 ! selector.

pipeline_play capture0_pipeline

pipeline_create rtstream0_pipeline rtspsink name=sink service=554 mtu=1400 interpipesrc listen-to=capture0 stream-sync=passthrough-ts format=GST_FORMAT_TIME ! queue leaky=2 ! videorate skip-to-first=true max-rate=30 drop-only=true ! video/x-raw,framerate=30/1 ! v4l2video2convert output-io-mode=5 capture-io-mode=5 ! video/x-raw,format=NV12, width=1920, height=1080 ! queue ! ducatih264enc inter-interval=1 intra-interval=8 max-bitrate=20000 profile=main bitrate=2048 rate-preset=storage ! h264parse ! video/x-h264,stream-format=avc,alignment=au,parsed=true,mapping=/stream ! sink.

pipeline_play rtstream0_pipeline
```

Minimal on Kestrel:

```
pipeline_create capture0_pipeline v4l2src device=/dev/video3 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1 ! queue !  interpipesink name=capture0 sync=false async=false caps=video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1

pipeline_play capture0_pipeline

pipeline_create hdmi0_pipeline interpipesrc listen-to=capture0 stream-sync=restart-ts format=time is-live=true ! queue name=videosink max-size-buffers=1 leaky=2 ! errorignore ignore-notlinked=true convert-to=ok ! kmssink plane-id=38 async=false sync=false qos=false

pipeline_play hdmi0_pipeline

pipeline_create rtstream0_pipeline rtspsink name=sink service=554 mtu=1400 interpipesrc listen-to=capture0 stream-sync=restart-ts format=time is-live=true ! queue leaky=2 ! video/x-raw,framerate=30/1 ! v4l2video2convert output-io-mode=5 capture-io-mode=5 qos=false ! video/x-raw,format=NV12, width=1920, height=1080 ! queue ! ducatih264enc inter-interval=1 intra-interval=8 max-bitrate=20000 profile=main bitrate=2048 rate-preset=storage ! h264parse ! video/x-h264,stream-format=avc,alignment=au,parsed=true,mapping=/stream ! sink.

pipeline_play rtstream0_pipeline
```

Needed to add patch for lost frames in v4l2src. Key seemed to be qos=false on kmssink, restart-ts for h264.

Trying to fix Kestrel:
```
pipeline_create capture0_pipeline input-selector name=selector ! textoverlay name="overlay" font-desc="UbuntuMono-R 12" silent=true valignment=top ! videorate skip-to-first=true ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1 ! interpipesink name=capture0 sync=false async=false v4l2src device=/dev/video3 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,framerate=30/1 ! selector. v4l2src device=/dev/video6 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,framerate=15/1 ! selector.

pipeline_play capture0_pipeline

pipeline_create rtstream0_pipeline rtspsink name=sink service=554 mtu=1400 interpipesrc listen-to=capture0 stream-sync=restart-ts format=time is-live=true ! queue leaky=2 ! v4l2video2convert output-io-mode=5 capture-io-mode=5 ! video/x-raw,format=NV12, width=1920, height=1080,framerate=30/1 ! queue ! videorate skip-to-first=true max-rate=30 drop-only=true ! video/x-raw,framerate=30/1 ! ducatih264enc inter-interval=1 intra-interval=8 max-bitrate=20000 profile=main bitrate=2048 rate-preset=storage ! h264parse ! video/x-h264,stream-format=avc,alignment=au,parsed=true,mapping=/stream ! sink.

pipeline_play rtstream0_pipeline
```

Also had to fix videorate issue where allocation was failing because it was removing the allocation parameters in `gst_video_rate_propose_allocation`. Seems to only be an issue when max == 0 which is why it probably isn't hit normally (ducati specifies this).

Eagle-USB:

```
pipeline_create capture0_pipeline v4l2src device=/dev/video0 io-mode=4 pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1 ! interpipesink name=capture0 sync=false async=false caps=video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1

pipeline_play capture0_pipeline

pipeline_create hdmi0_pipeline interpipesrc listen-to=capture0 stream-sync=passthrough-ts format=bytes is-live=true do-timestamp=true ! kmssink plane-id=28 async=false sync=false qos=false

pipeline_play hdmi0_pipeline

pipeline_delete rtstream0_pipeline
pipeline_create rtstream0_pipeline rtspsink name=sink service=554 mtu=1400 interpipesrc listen-to=capture0 stream-sync=restart-ts format=time is-live=true ! queue leaky=2 ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12, width=1920, height=1080,framerate=30/1 ! queue ! videorate skip-to-first=true max-rate=30 drop-only=true ! video/x-raw,framerate=30/1 ! x264enc speed-preset=ultrafast pass=quant quantizer=25 bitrate=2048 key-int-max=8 ! video/x-h264, mapping=/stream, profile=main ! queue ! sink.

pipeline_play rtstream0_pipeline

pipeline_create usb0_pipeline interpipesrc listen-to=capture0 stream-sync=restart-ts format=time is-live=true do-timestamp=true ! queue leaky=2 max-size-buffers=1 ! v4l2video2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=YUY2,width=1920,height=1080,pixel-aspect-ratio=1/1 ! queue ! videorate skip-to-first=true ! video/x-raw,framerate=60/1 ! v4l2sink device=/dev/video5 io-mode=5 async=false sync=false qos=false

pipeline_play usb0_pipeline
```

Would not recommend videorate before the videoconvert; it seems like this causes the caps negotiation to be more finnicky. Also, this could cause us to be converting at 60fps in some cases which is unnessary.

Kestrel Mute:

```
pipeline_delete capture0_pipeline
pipeline_create capture0_pipeline v4l2src device=/dev/video3 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1 ! queue !  interpipesink name=capture0 sync=false async=false caps=video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1

pipeline_play capture0_pipeline

pipeline_create hdmi0_pipeline interpipesrc listen-to=capture0 stream-sync=restart-ts format=time is-live=true name=interpipesrc ! queue name=videosink max-size-buffers=1 leaky=2 ! errorignore ignore-notlinked=true convert-to=ok ! kmssink plane-id=38 async=false sync=false qos=false

pipeline_play hdmi0_pipeline

pipeline_create mute0_pipeline v4l2src device=/dev/video6 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=15/1 ! queue ! videorate skip-to-first=true ! video/x-raw,framerate=30/1 ! interpipesink name=mute0 sync=false async=false caps=video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1

# switch to mute:
pipeline_play mute0_pipeline
element_set hdmi0_pipeline interpipesrc listen-to mute0

# and back:
element_set hdmi0_pipeline interpipesrc listen-to capture0
pipeline_stop mute0_pipeline
```

```
pipeline_delete capture0_pipeline
pipeline_create capture0_pipeline v4l2src device=/dev/video6 io-mode=4 do-timestamp=true pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=15/1 ! queue ! videorate skip-to-first=true ! video/x-raw,framerate=30/1 ! interpipesink name=capture0 sync=false async=false caps=video/x-raw,format=YUY2,width=1920,height=1080,framerate=30/1
pipeline_play capture0_pipeline
```

### 8/14/21

Questions for tomorrow:


SD cards:
- Do we have a rep at Sony who we can send the SD card to? (Email from Darrin was from Herb Ruterschmidt at TSI: herb@tsi-1.com).

Photos (Plutus):
- Plutus A2E reserved starts at 0x18000000 instead of 0x1c000000, but A2E only uses ~54MB. Does QT use the extra memory?
- Assuming MW will handle pulling in FPGA v1/v2 service?
- Name of "vad_plutus_pl" ?

### 8/13/21

Phobos (Plutus) on Ark:

Found out that h264parse was discarding data for some reason. However, it works fine without h264parse:

```
0:00:00.602276388 11003   0x51fbb0 DEBUG              h264parse gsth264parse.c:1247:gst_h264_parse_handle_frame:<h264parse0> Dropping filler data 1
0:00:00.602372742 11003   0x51fbb0 DEBUG              h264parse gsth264parse.c:1405:gst_h264_parse_handle_frame:<h264parse0> Dropped data
0:00:00.602552535 11003   0x51fbb0 DEBUG              h264parse gsth264parse.c:186:gst_h264_parse_reset_frame:<h264parse0> reset frame
```

Also works with h264parse and alignment sent to "au". Not sure which is better here. (per [this post](https://xilinx.gitlab.avnet.com/petalinux/gstreamer-reference/v2019.1/elements/video/omxh264dec/), seems like NAL has lower latency than AU).

So, we can encode the stream with:

```
/usr/bin/gst-launch-1.0 rtspsrc location="rtsp://10.0.0.15/vaddio-conferenceshot-av-stream" name=rtspsrc0 port-range="3000-3005" latency=0 ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! capsfilter caps=video/x-h264,stream-format=byte-stream,alignment=nal ! filesink location="/dev/h264s" buffer-mode=2 blocksize=800000 async=false
```

Per [this thread](https://marc.info/?l=gstreamer-devel&m=140246120312040&w=2), h264parse is generally optional with `rtph264depay` since the depayloader is capable of decoding the correct format.

Need to set output of A2E/input of Xilinx v_scaler to 1080p:

```
devmem 0x4060000c 32 0x04380780
devmem 0x43c20114 32 0x04380780
```

### 8/12/21

Eagle/A2E: Get video size is invalid when using presets ultrafast-faster. fast and medium do not give error. Medium fails with data stream error. Main profile also fails, but baseline and constrained do not.

```
/usr/bin/gst-launch-1.0 rtspsrc location="rtsp://10.0.0.20/vaddio-conferenceshot-av-stream" name=rtspsrc0 port-range="3000-3005" latency=0 rtspsrc0. ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! capsfilter caps=video/x-h264,stream-format=byte-stream,alignment=nal ! h264parse ! capsfilter caps=video/x-h264,stream-format=byte-stream,alignment=nal ! filesink location="/dev/h264s" buffer-mode=2 blocksize=800000 async=false rtspsrc0. ! capsfilter caps="application/x-rtp,media=audio" ! rtpmp4adepay ! aacparse ! faad ! capsfilter caps=audio/x-raw,format=S16LE,rate=48000,channels=2 ! alsasink device=default sync=false
```

VNG-12517 Eagle-USB HDMI production issue where HDMI did not come up on 15 of 80 units with the 1.1.0 release using an Inogeni.

These tests performed with an Omega:

10 power cycles: Hotplug OK

12 Power cycles after factory reset:
clish -c 'system factory-reset on'; sync

Completely rewriting mmc and booting up fresh: 3

### 6/15/21

Troubleshooting memory leaks on Eagle when restarting the software H.264 pipeline.

In `meta-vaddio-ark/recipes-multimedia/gstreamer/gstreamer1.0_%.bbappend`, add `PACKAGECONFIG += "gst-tracer-hooks unwind"`. (couldn't get dw to work for source/line numbers, needs some pkg-config stuff in elfutils that seemed to backport ok but caused issues when running).

```
export GST_DEBUG="GST_TRACER:7,1"
export GST_TRACERS="leaks"
export GST_TRACERS="leaks(stack-traces-flags=full,filters="GstRTSPThread",check-refs=true)"
export GST_LEAKS_TRACER_SIG=1
```

Audio only:
```
gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 ! video/x-raw,width=1920,height=1080,format=YUY2,framerate=30/1 ! queue ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,width=1920,height=1080,format=NV12 ! queue ! x264enc speed-preset=ultrafast pass=quant quantizer=24 bitrate=2048 key-int-max=8 ! video/x-h264,mapping=/stream,profile=main ! queue ! rtspsink service=554
```

Audio and video:
```
gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 ! video/x-raw,width=1920,height=1080,format=YUY2,framerate=30/1 ! queue ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,width=1920,height=1080,format=NV12 ! queue ! x264enc speed-preset=ultrafast pass=quant quantizer=24 bitrate=2048 key-int-max=8 ! video/x-h264,mapping=/stream,profile=main ! queue ! rtspsink service=554 name=sink alsasrc device="hw:logii2s,4" ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! queue ! faac ! audio/mpeg,mpegversion=4,rate=48000,channels=2,mapping=/stream ! sink.
```

### 6/10/21

Next items from 1-1:

- [x] Archive goals
- [x] Confluence page for eagle-usb readback: https://vaddio.atlassian.net/wiki/spaces/VNG/pages/2464284673/Eagle+eMMC+Readback
- [ ] videorate fix for interpipe
- [ ] x264 memory leak for Eagle-USB
- [ ] improvements for media server (on hold as of 8/9, Steve working on refactoring)
    passing as dictionary
    decouple type vs name

### 6/9/21

Kestrel distroboot

- [x] Move heartbeat LED from red to green to align with Eagle
- [ ] Figure out fix for updating to a pre-distroboot build
- [x] Create tickets for FAN LED driver and factory reset button names
- [ ] ask in review about only erased emmc (fix now or follow up ticket)

### 6/2/21

EasyIP license
Architecture direction/strategic vision
General IT stuff - dedicated eng?
Covid PTO?

### 6/1/21

USB 2.0 Extender testing

Omega (FX3):
540p30: Fail (Image jumps around a bunch)
360p60: Fail (Image jumps around a bunch)
360p30: Pass
180p30: Pass

Omega:
      Endpoint Descriptor:
        bLength                 7
        bDescriptorType         5
        bEndpointAddress     0x83  EP 3 IN
        bmAttributes            2
          Transfer Type            Bulk
          Synch Type               None
          Usage Type               Data
        wMaxPacketSize     0x0200  1x 512 bytes
        bInterval               1

Eagle:
      Endpoint Descriptor:
        bLength                 7
        bDescriptorType         5
        bEndpointAddress     0x82  EP 2 IN
        bmAttributes            2
          Transfer Type            Bulk
          Synch Type               None
          Usage Type               Data
        wMaxPacketSize     0x0200  1x 512 bytes
        bInterval               1

### 5/27/21

Todo from 1-1:

- [x] fix for easyip streaming drop issue with TCP (VNG-12362)
- [x] kestrel dfu poc
- [x] copy of video capture for ethos media server setup
- [x] add helpers for subdev operations to simplify

```
setenv script_size_r 0x10000
setenv imageaddr 0x80100000
setenv initrd_high 0xbdf00000
run bootcmd_dfu_usb

dhcp $kernel_addr_r 10.0.0.6:u-boot.img && fatwrite mmc 1:1 $kernel_addr_r u-boot.img $filesize

Legacy boot:
run findftd; run envboot
```

### 5/19/21

```
dhcp $kernel_addr_r 10.0.0.6:fitImage && ext4write mmc 0:$rootpart $kernel_addr_r /boot/fitImage $filesize && boot
```

10.30.221.
left is for eng (probably odd)
right is for corp
Desktop: 10.30.221.51

### 5/12/21

Can we get RGB from the VIP? Yes! Need to modify vip, enable skip hsync mode (and remove hskip/vskip). Change the line unsetting the actvid_hsync flag to add it instead:

```
config0 |= VIP_USE_ACTVID_HSYNC_ONLY;
```

Also need to add RGBA/BGRA formats in VIP. Then the DRM output knows about alpha:

```
gst-launch-1.0 v4l2src device=/dev/video3 io-mode=4 pixel-aspect-r
atio=1/1 ! video/x-raw,width=1920,height=1080,format=BGRA,framerate=30/1 ! queue ! kmssink sync=false plane-id=38
```

Next step, can we use v4l2video10convert with alpha blending? Unfortunately so far it seems like no. Sending the BGRA into kmssink shows a transparent video, but if I add support for V4L2_PIX_FMT_ABGR32 in omap_wb_m2m.c and DRM_FORMAT_ARGB8888 in omap_wb.c, I can send it BGRA, but converting to YUY2 still doesn't do any blending.

### 5/11/21

Vivid Pros:
- Simple
- Low effort
- Cut to black or color/pattern seems pretty quick

Vivid Cons:
- Less flexible
- No alpha support
- Probably won't help if we need an EasyIP decoder based of Kestrel
- Videorate is required on the input for text overlay; not sure how this will affect things when not muted
- Vivid does take some CPU all the time when running with input-selector (1-2% at 2fps)

DRM Pros:
- More full-featured: Alpha, overlays (with different application), etc possible
- Smoother since it is hardware-accelerated

DRM Cons:
- More effort
- Video interruption when HDMI is connected or disconnected (could be solved by using LCD writeback, but would need kmssink supporting DRM atomic API)
- Any resolution other than 1080p out the HDMI would mean USB and IP need to be re-upscaled from 720p
- Cannot run multiple kmssinks; would need our own custom application or a window manager
- More DDR memory throughput usage

For vivid vs writeback, here are the todo items remaning:

Vivid:
- Productionize source setting on video capture (3 days?)

DRM Writeback:
- Handle HDMI hotplug case. Still unknown: How to gracefully switch between emulated and real EDID? (maybe 1 week)
- Add full fading support to the kmssink via a gstreamer property (2-3 days)
- Pyvideo-media-server needs the ability to restart the video capture on the writeback (3-5 days?)

```
[core]
require-input=false

[shell]
background-image=none
background-color=0xff000090
locking=false
animation=none
panel-position=none
startup-animation=none
close-animation=none

[screensaver]
# Uncomment path to disable screensaver
path=@libexecdir@/weston-screensaver

[output]
name=HDMI-A-1
mode=1920x1080@60
```

### 5/4/21

For vivid demo: CPU usage of vivid is 10-15% for 1080p30 on Kestrel. However, we can reduce to 1 or 2 fps and just use videorate to duplicate the buffers. I also tried imagefreeze, but its CPU usage is near 100%. Turns out imagefreeze was at 100% because it was producing buffers as fast as it could; setting sync=true on the sink fixed it. Turns out newer versions (1.16+) have an is-live property that sends out the buffers at the frame rate you'd expect. Things are much better there.

The `colorspace` option needs to be set to 0 (SMPTE170) on vivid for the colorimetry to negotiate properly with the input selector in gstreamer.

I also tried using a videotestsrc, but ran into issues because the downstream elements expect to be able to import a dmabuf. I had some success running it through a v4l2video2convert first, but this sort of defeats the purpose of using videotestsrc over vivid since it has to write back out to memory to make a contiguous buffer anyway. Further research could be done about how to have videotestsrc create a dmabuf-import-able buffer.

### 5/3/21

- [ ] Audit powerup timing conditions
- [ ] Try starting up without Middleware

### 4/28/21

For PM:
- [ ] vivid demo (cut to/from black with mux)
- [ ] drm writeback disruption demo (dont' worry about fade, just show what happens on hotplug)

Todo for Kestrel DRM writeback:

- Smooth fading in/out from pyvideo, either from GstController or in driver.
- Handle HDMI unconnected/hotplug case
- Handle HDMI 720p case

More notes:

- Setting the plane alpha in kmssink during each show_frame works great. No blocking and is smooth.
- At first I thought I could use GstController's interpolation control source, but that won't work with the plane-properties since it's not a simple int/float (it's boxed).
- I tried setting the plane properies in a loop in pyvideo, and this worked when the framerate was 60, but when it's 50 or 30 it's horrifically slow, even if I put it in its own thread.

Inspired by the threading issue, I also tried interpipe to see if kmssinks were interfering on the "fake lcd" writeback branch, but no luck. Interpipe launch lines (patched kmssink to show fd of first pipeline in INFO and manually specified for second):

```
gstd-client
pipeline_create hdmi_wb v4l2src device=/dev/video3 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! kmssink async=false sync=false connector-id=55 plane-id=43
pipeline_play hdmi_wb
pipeline_create hdmi_out v4l2src device=/dev/video11 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! kmssink async=false sync=false connector-id=57 plane-id=38 drm-fd=13 force-modesetting=true modeset-caps=video/x-raw,width=1920,height=1080,format=BGRx,framerate=60/1
pipeline_play hdmi_out
```

### 4/20/21

Eagle-USB Reprogramming steps:

1. Apply power and hold down the IP/MAC/Factory reset button
2. Once the purple light turns on, release the button within 2 seconds.
3. Wait until the purple light turns off.
4. Within 2 seconds, again press and hold the IP/MAC/Factory reset button
5. Continue to hold the button until the light turns yellow and flashes. Then, you may release the button as the device performs the update process.
6. Eventually, the light will either turn green indicating success or red indicating failure.

### 4/14/21

Eagle-USB TFTP recovery:

Network:
1. Locate a hard-wired ethernet port available on your PC. Alternativley, a USB to Ethernet adapter may be used.
2. Connect the port to the Data port of a PoE injector.
3. Configure the port's network interface with a static IP: 169.254.1.1 and subnet mask: 255.255.0.0.

tftpd64 setup:
1. Open TFTPD. If prompted, allow in Windows firewall (both public and private).
2. Go to Settings->Global. Uncheck all but TFTP Server and DHCP Server. A pop-up will tell you that the application must be restarted, but just click OK for now.
3. Go to the DHCP Tab: Set IP pool start address to 169.254.1.5, sie of pool 5, def. router 169.254.1.1, Mask 255.255.0.0, uncheck ping address, check bind DHCP and select 169.254.1.1 address.
4. Restart TFTPD.

### 4/5/21

Jeremy 1-1

### 4/1/21

Eagle-USB audio distortion. Try:

- [ ] Run with GST_DEBUG
- [ ] Run without middleware
- [ ] Related to queues?

I saw it happen after running around an hour. IP audio only was distorted. Was present after running a dump pipeline, but I tried restarting and doing a dump pipeline and had no issues.

Tried adding queue to video path of rtstream, no luck.

Might need to add a queue after alsasrc (or increase buffer-time a lot). This avoids notices about buffers being dropped at warning level when initially configuring.

- Tried `adder`, but caps negitation was impossible to get working, even with `caps` property.
- Tried messing with discont-wait on `audiomixer`, but that didn't seem to help: `udiomixer name=audiosink latency=15000000 discont-wait=15000000`
- Seems like timeouts become more and more frequent starging around 35-40 minutes in. Run with `GST_DEBUG="*audioaggregator*:5,2"`
```
0:54:03.881676380 23423   0x7f140035e0 DEBUG        audioaggregator gstaudioaggregator.c:1792:gst_audio_aggregator_aggregate:<audiosink:sink_2> Timeout, missing 120 frames (0:00:00.002500000)
```

Interestingly, setting `do-timestamp=true` has removed nearly all of the timeout messages. Only two in 1.5 hours (after initial startup ones, of course), ironically both at 16 minutes:

```
0:16:37.458499356  1464   0x7f841e4d40 DEBUG        audioaggregator gstaudioaggregator.c:1792:gst_audio_aggregator_aggregate:<audiosink:sink_2> Timeout, missing 120 frames (0:00:00.002500000)
0:16:37.471811196  1464   0x7f841e4d40 DEBUG        audioaggregator gstaudioaggregator.c:1792:gst_audio_aggregator_aggregate:<audiosink:sink_0> Timeout, missing 180 frames (0:00:00.003750000)
0:16:37.474392696  1464   0x7f841e4d40 DEBUG        audioaggregator gstaudioaggregator.c:1517:gst_audio_aggregator_fill_buffer:<audiosink:sink_0> Buffer resynced: Pad offset 47839680, current audio aggregator offset 47839680
```

do-timestamp ran for 6 hours until the sink_1 (i2s input) started giving sync messages above. However, audio sounded fine. However once I changed format on the rtstream output, all audio on the system died.

Next test: Still with do-timestamp, but added queue right before tee in audio_capture.py.

Still failed, just took longer.

Next, added leaky=2 to queue. Ran just fine, but queue filled up. Kestrel showed timeout messages, eagle was fine, but after changing format on rtstream eagle audio no longer worked.

Next trying leaky=2 with max-size-time=12500000. Also set latency on audiomixer to match.

Trying slave-method settings on alsasrc. So far no luck.

Seems that the logii2s node is already providing the pipeline clock. Run with GST_DEBUG=5 and search for "pipeline obtained clock" to see the global clock provider.

```
export GST_DEBUG="*audioaggregator*:5,2"
export GST_DEBUG_FILE=/tmp/gst.log
export GST_DEBUG_DUMP_DOT_DIR=/tmp
```

period_size=75, buffer_size=2400: latency is good, 160% pyvideo (720p)
period_size=300, buffer_size=2400: latency is good, 160% pyvideo (720p)
period_size=600, buffer_size=2400: latency is good, 160% pyvideo (720p)

Had best luck using alsa dmix to mix together multiple outputs. Used loopback soundcard to loop device back.

### 3/30/21

```
gst-launch-1.0 v4l2src device=/dev/video3 io-mode=4 ! video/x-raw,format=YUY2, width=1920, height=1080, framerate=30/1 ! tee name=videot ! queue ! kmssink connector-id=55 name=hdmisink videot. ! queue ! kmssink connector-id=57 name=lcdsink
```

### 3/18/21

HID stuff:
https://blog.noser.com/first-steps-with-an-usb-hid-report/

### 3/15/21

Sync is fine:
```
gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 ! video/x-raw,framerate=30/1,width=1920,height=1080,format=YUY2 ! tee name=videot ! queue ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12 ! queue ! x264enc speed-preset=ultrafast pass=cbr quantizer=25 bitrate=2048 key-int-max=8 ! video/x-h264,mapping=stream,profile=main ! queue ! rtspsink name=sink service=554 mtu=1500  alsasrc device="hw:logii2s,4" ! capsfilter name=acaps caps=audio/x-raw,format=S16LE,rate=48000,channels=2 ! queue ! faac ! capsfilter caps=audio/mpeg,mpegversion=4,rate=48000,channels=2,mapping=/stream ! queue ! sink. videot. ! queue leaky=2 max-size-buffers=1 ! kmssink sync=false async=false
```

Sync is off:
```
gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 ! video/x-raw,framerate=30/1,width=1920,height=1080,format=YUY2 ! tee name=videot ! queue ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12 ! queue ! x264enc speed-preset=ultrafast pass=cbr quantizer=25 bitrate=2048 key-int-max=8 ! video/x-h264,mapping=stream,profile=main ! queue ! rtspsink name=sink service=554 mtu=1500  alsasrc device="hw:logii2s,4" ! capsfilter name=acaps caps=audio/x-raw,format=S16LE,rate=48000,channels=2 ! tee name=audiot ! queue ! faac ! capsfilter caps=audio/mpeg,mpegversion=4,rate=48000,channels=2,mapping=/stream ! queue ! sink. videot. ! queue leaky=2 max-size-buffers=1 ! kmssink sync=false async=false audiot. ! queue ! fakesink
```


### 3/5/21

Kestrel Kernel cleanup.
- Need to run `tune2fs -O ^huge_file` for each /dev/mmcblk0p5...8.
- Might need to mount -o remount,ro each followed by e2fsck -f first, but be sure to remount,rw back after.

```
dhcp $kernel_addr_r 10.0.0.6:fitImage && ext4write mmc 0:$rootpart $kernel_addr_r /boot/fitImage $filesize
boot
```
### 2/25/21

For EDID, try backporting:
29a6026624cde0a378ac4ebd2f697ee6d941adf9
04757d0e37897cdfa59050157b9083d661bd099e
(These actually only affect the override/firmware EDID)

```
GST_DEBUG="*kms*:4" gst-launch-1.0 v4l2src device=/dev/video0 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! kmssink plane-id=28 async=false sync=false force-modesetting=true modeset-caps=video/x-raw,format=BGRx,width=1920,height=1080,framerate=25/1 connector-properties=s,hdmi-colorspace=2
```

### 2/17/21

Kestrel video fade/overlay:

- Can we use DRM writeback? Possibly. Number of issues:
        - Capture mode requires HDMI to be enabled and must match its format
        - Seems to support ENUMINPUT, but only one shows up. Could we use this to pull directly from a plane?
        - M2M mode - having trouble with alpha blending here
- What about RGB input with alpha?
        - Had problems getting RGB to consistently start (CSC in VIP)
        - vpe and v4l2convert appear to only accept YUV 4:2:x formats
- 2D graphics accelerator?
        - No Gstreamer support at this time, would need to use Vivante APIs directly in an app or make our own.
- 3D graphics accelerator?
        - No gstreamer support I can find on forums or SDK docs
        - TI has some misc. examples
- OpenCL/OpenCV.
        - Maybe. Lots of kernels available. Requires C66x support.

### 2/16/21

- [x] EPTZ core changes, if any - wait on Roger to see if we need debug
- [x] 1080p30 ticket into review
- [x] Look at way of doing fade to black/overlay on Kestrel
- [ ] Eagle EasyIP, Dante
- [ ] eptz

### 2/9/21

Kestrel DFU needs USB 2.0 or back 3.0 port, otherwise will not enumerate.
Kestrel gpios: (bank - 1) * 32 + offs. Ie, GPIO2_19 = gpio 51.

DFU to arbitrary memory:
```
setenv dfu_alt_info image ram $kernel_addr_r 0x3df68000
dfu 0 ram 0
```

### 1/26/21

try zeroing files one by one
- video.json:

power pull testing

### 1/25/21

Eagle-USB tickets list:

```
"Expected Release" = "Eagle-USB 1.0.0" AND assignee = "Nicholas Graumann" AND status != "Closed" AND status != "Pending Test"
```

### 1/22/21

Looking back at the old Xilinx MIPI SR (10480191), they suggested increasing CLK-POST and CLK-PRE in steps of 8*UI to see if that helped the issue. I am going to try this on the tempermental board and see if it helps with the CRC errors.

Ran 10 minutes with no errors with TclkPrepareCnt and TclkPostCnt increased by 1 each (to 0x06 and 0x0F respectively).

TclkPrepareCnt set back to 0x05 and TclkPostCnt decreased to absolute minimum 0x0D: CRC Errors basically immediately, followed by line buffer full.

Set TclkPrepareCnt to one less than min (0x04) and TclkPostCnt increased to safe 0x0F:
Set TclkPrepareCnt to 0x03 and TclkPostCnt at 0x0F: Ran 10 minutes
Set TclkPrepareCnt to 0x02 and TclkPostCnt at 0x0F: Ran 10 minutes
Set TclkPrepareCnt to 0x01 and TclkPostCnt at 0x0F: Ran 10 minutes

Next, I set the CSI line rate back to 456MHz (912mbps) and tested with the TclkPostCnt set to the minimum (to 0x0D) which immediately failed. 0x0E was marginal, as before, and bumping up to 0x10 ran for 10 minutes.

### 1/20/21

- [x] Trying changing PPI_DPHY_DCNTRL for CSI1 (0x440) to delay the clock per Jeff:
    - 0x0000 is the default and fails eventually (~2-3min)
    - 0x1000 failed after ~40s, massive video corruption
    - 0x2000 failed after ~90s
    - 0x3000 failed after ~90s
    - 0x4000 failed really fast, big green bar on right half of video
    - 0x5000
    - 0x6000 failed after ~180s
    - 0x7000 failed after ~90s
    - 0x8000 failed in an average amount of time (~2min)
    - 0x9000 failed after ~3min
    - 0xA000 fails almost immediately
    - 0xB000 errors after ~90s
    - 0xC000 took a little longer to fail (~4min)
    - 0xD000 failed after ~30s
    - 0xE000 video froze after ~90s
    - 0xF000 fails pretty fast and has lots of video artifacts

- [x] tc355840: Call set_csi only on enable or enable/disable: both behaved identically

### 1/19/21

To try:

- [x] Bad HDMI right build - fails quickly
- [x] HDMI input build from Glenn - works fine on two units
- [ ] Update HS_SETTLE with just MIPI CSI3
- [ ] Traffic patterns of 1080p vs 2160?
- [x] Setting DPHY_CAP values on CSI1 only
    - 0000 failed but much better, only frame sync errors.
    - 0155 mostly frame syncs with very few CRC errors.
    - 02aa is the default.
    - 03ff seems to be the same


Observations:
- CSI1 1080p test also fails on good board. What does this mean?
- Removing DPHY registers causes CSI0 to fail while adding them in means CSI1 is the only one to fail.

- Increasing lineinitcnt may help.

### 1/15/21

MIPI issues, Tried the following:

- Running both CSI interface at original 912Mbps rate
- Adding 500ms delay after start stream of both Xilinx CSI cores
- Using 1080p input (but both cores still present)
- Setting both MIPI cores to use shared logic in core vs. sharing.
- Both resets separate, no shared logic
- Both resets separate, no shared logic, 500ms delay for csi start
- Both resets separate, no shared logic, 500ms delay for csi start, start up CSI 3 then 2
- Single core, 912Mbps, 1080p, with gpio reset, using CSI3 lanes
- Single core, 912Mbps, 720p, with gpio reset, using first 2 CSI3 lanes

Testing:
- Single core, 272Mbps, 720p, with gpio reset, using 4x CSI3 lanes

Works:
- Single core, 912Mbps, 1080p, with gpio reset - still tested with CSI_TX_BOTH even though splitter disabled (I think this mirrors the video on both interfaces)
- Single core, 912Mbps, 1080p, with gpio reset (not touched in Linux)
- Single core, 912Mbps, 1080p, with gpio reset - passed overnight

### 1/14/21

Latest todos:

- [x] pyvideo-media-server volume control
        Check out render-delay
- [x] Eagle block issue - filed JIRA
- [ ] Centaur PoE switch issue
- [ ] dbus-next problem (VNG-11805)

Potential Eagle issues:
- [ ] MIPI/HDMI in
- [x] I2C0 - filed JIRA, long cables seem to be culprit

### 1/12/21

Vaddio checklist:

- [x] Return HDMI analyzer
- [ ] Centaur for Steve Z. (deferred, got switch instead)
- [x] Eagle for testing
- [x] USB-C cable
- [x] Badge
- [x] Kestrel for lens change
- [x] Grab Raspberry Pi

### 1/11/21

Motor failures notes:

Primarily Remus Prime HDMI only
Motors jitter and not homing
low ohmage fault on the i2c bus
sda, scl or rarely both will be 300-1.5k on zynq side, good unit 2.7k

30% failure rate
zynqs have same dates
same pcbs

not seen on Rigel

### 1/6/21

Eagle variants:

| Build      | SN's | PCB Rev | Qty | Zynq | Product       | Notes                     |
| :--------- | :--- | :------ | :-- | :--- | :------------ | :------------------------ |
| Apr. 2020  | ???  | C       | 10? | 4EG  | Eagle (Arch). | Full population run. Only build with SN sticker on top. |
| Sep. 2020  | ???  | D       | 6   | 2EG  | Eagle-USB     | Still has HDMI in/motor circuits poulated |
| Jan. 2021  | ???  | E       | 20? | 2EG  | Eagle-USB     |  |
| Feb. 2021? | ???  | E       | ??? | 3EG  | Eagle EasyIP  |  |

### 1/5/21

DRM TODO:

- [x] FPGA update to enable BGRX for framebuffer
- [x] Console text output issue -  resolved by diabling CONFIG_VT
- [x] Decide between adding PL encoder or just modifying PL disp
- [x] vpss clock generator support
- [x] vpss interrupt support, or use FB interrupt for vblank
- [x] Scaling/plane set support
- [x] 1080p USB streaming negotiation issue? - colorimetry on input fixed
- [x] ADV7511 sysfs notify hotplug
- [ ] Enable audio on ADV7511 driver
- [ ] Refacator hdmi_video_output.py

### 12/28/20

Interrupts are coming in very fast for FB write... (144fps).
Removing the vpss scaler allows things to run and the framerate is correct.
However, had to adjust axi stream to video out:
- Master mode, no scaler, 2048 deep fifo, 128 hysteresis: works
- Slave mode, no scaler, 2048 fifo, 128 hysteresis: works
- Slave mode, no scaler, 1024 fifo, 128 hysteresis: works
- Slave mode, no scaler, 32 fifo, 12 hysteresis: works

Stack trace form drm_atomic_helper wait for vblanks took too long; seems to only be with GST_DEBUG="*kms*:5"

Turns out the blank interrupt is not happening every time. The DMA is on auto-restart, so a vblank interrupt only happens when the plane is updated and a new descriptor is queued. I think the best way to address this is to port over a change from the older xilinx driver that uses the VTC's vblank interrupt.

Also had trouble getting the xilinx pl display driver to load when connected via "ports" in the dts to an adv7511. Turns out the component matching in xlnx_drv.c is adding a "component" match for the 7511.

### 12/18/20

Trying to add new "PL encoder" driver to go along with "PL display" driver for adv7511.

Running into issues with `modetest -M xlnx` showing the crtc as 0 for encoders, connectors, and planes. Do we need a new connector like xlnx_dsi, or can we safely ignore it like the mixer?

### 12/14/20

Jeremy meeting:

- Camera blocks for Eagles?
- Standoffs, other hardware

Goals for 2110 or other streaming platform (even point-to-point):
- Get rid of HDBT
- 4k

- [ ] Ask Steve and Nate about prototype hardware - standoffs
- [ ] Jeremy camera blocks

### 12/11/20

Implemented UAC status interrupt for sending volume/mute status back to the host. Found out after the fact that Windows USB audio 1.0 driver likely does not support this. See:

https://www.xcore.com/viewtopic.php?t=6663
https://www.xcore.com/viewtopic.php?f=3&t=7181&p=37279&hilit=volume+control+status+interrupt#p37279

Seems like HID is the recommended way to do this.

### 12/2/20

```
Dec 02 15:54:09 nick-desktop kernel: usb 4-4.1.1: Warning! Unlikely big volume range (=12288), cval->res is probably wrong.
Dec 02 15:54:09 nick-desktop kernel: usb 4-4.1.1: [6] FU [Volume control & mute Playback Volume] ch = 1, val = -10752/1536/1
Dec 02 15:54:09 nick-desktop kernel: usb 4-4.1.1: Warning! Unlikely big volume range (=12288), cval->res is probably wrong.
Dec 02 15:54:09 nick-desktop kernel: usb 4-4.1.1: [5] FU [Volume control & mute Capture Volume] ch = 1, val = -10752/1536/1```

### 11/30/20

Proposed:
```
def set_format(self, resolution, format):
```

Examples:
```
# HDMI:
set_format("1920x1080p60", "YCbCr")
set_format("1920x1080p59.94", "auto")
set_format("1920x1080i60", "auto")
set_format("1280x720p60", "RGB")
set_format("auto", "auto")


# RTSP/RTMP:
set_format("1920x1080p30", "auto")


# USB:
set_format("1280x720p30", "NV12")
set_format("640x360p60", "YUY2")
```

### 11/23/20

Today:

```
def set_format(self, width, height, fps, fcc):
```

Proposed a (one mega set_format method):

```
@ds.method(VIDEO_IFACE, in_signature="uuuubs")
def set_format(self, width, height, fps_n, fps_d, interlace, fcc):

# Does dbus have a "none" equivalent? Empty?
```

Proposed b (separate methods):

```
# HDMI:
@ds.method(VIDEO_IFACE, in_signature="uuuubb")
def set_format_hdmi(self, width, height, fps_n, fps_d, interlace, ycbcr):

# RTStream:
@ds.method(VIDEO_IFACE, in_signature="uuuu")
def set_format_stream(self, width, height, fps_n, fps_d):

# USB:
@ds.method(VIDEO_IFACE, in_signature="uuus")
def set_format_usb(self, width, height, fps_n, fps_d, fcc):
```


Questions:
- Overloading dbus methods versus defining new ones?
- Floating point arguments?

### 11/20/20

Eagle MJPEG troubleshooting.

Clear traps:
devmem 0xa0010000 32 0x27C00000

read 0x44 and 0x48
capture ILA setup

First error: Timed out only. No ILA trigger.
[ 1775.881236] cast_jpeg a0240000.MJPEG_Encoder_Subsys: DMA transfer of buffer 0 timed out    
[ 1775.889334] cast_jpeg a0240000.MJPEG_Encoder_Subsys: DMA transfer of buffer 4 timed out    
[ 1775.897429] cast_jpeg a0240000.MJPEG_Encoder_Subsys: DMA transfer of buffer 3 timed out    
[ 1775.905558] cast_jpeg a0240000.MJPEG_Encoder_Subsys: DMA transfer of buffer 2 timed out

root@vaddio-device-04-91-62-C8-A4-81:~# devmem 0xa0010044
0x08006E6B
root@vaddio-device-04-91-62-C8-A4-81:~# devmem 0xa0010048
0x00000018

Kestrel HDMI stuff

```
dbus-send --system --print-reply --dest=com.vaddio.MediaServer1 /com/vaddio/MediaServer1/video/output/hdmi0 com.vaddio.MediaServer1.OutputIface1.enable boolean:true

while dbus-send --system --print-reply --dest=com.vaddio.MediaServer1 /com/vaddio/MediaServer1/video/output/hdmi0 com.vaddio.MediaServer1.OutputIface1.commit ; do sleep 2 ; done
```


### 11/19/20

(See Interpipes.md)

### 11/17/20

Increase CMA past 200MB on Kestrel requires changing VMSPLIT from 3G/1G to
2G/2G. I believe this is because CMA can only reside in lowmem. Not an issue on
Eagle since highmem/lowmem is a 32-bit thing.

Kestrel HDMI output changing kills gstreamer. Turns out this happens when, in
`gstv4l2object.c`, a buffer copy happens whne the number of buffers drops below
the copy threshold. Disabling the copy allowed things to run overnight. In the
morning, still had 100188kB of 319488kB CMA free with h.264+1080p USB+HDMI.

### 10/27/20

Running Wayland/Weston on Kestrel:

```
weston-launch --tty /dev/tty1 -u root -- --backend=drm-backend.so
```

Install `weston-examples` for `weston-debug`.

```
weston-launch --tty /dev/tty1 -u root -- --debug --backend=drm-backend.so &
weston-debug drm-backend
```

/etc/weston.ini:
```
[core]
require-input=false
idle-time=0

[shell]
locking=false
animation=none
panel-position=top
startup-animation=none
close-animation=none
background-color=0xff808080

[screensaver]
# Uncomment path to disable screensaver
# path=@libexecdir@/weston-screensaver

# [output]
# name=HDMI-A-1
# mode=1280x720
```

### 10/21/20

EPTZ PR1 todo:

- [x] Better name for EPTZ_PAN_STATUS_*_MASK
- [x] default_formats still needed?
- [x] Create macros to handle shifting
- [x] Reset crop if either source or sink pad changes
- [x] use v4l2_info when available
- [x] log_status, g_register, and s_register
- [x] VADDIO_VIDEO -> VIDEO_VADDIO
- [x] Move devicetree bindings docs to vng
- [x] Ensure shebangs in all scripts
- [x] Location for eptz.py test script? (ticket)
- [x] Place camera scripts in board bringup repo
- [x] Improve documentation around vaddio-video-eptz machine feature
- [x] Split up text blocks referencing v4l2 controls
- [x] Elaborate on the definiton of pan and zoom and compare/contrast with our current products
- [x] trailing space in vaddio-machine-features.inc
- [x] TODO comment and follow up ticket for enumerating subdev information
- [x] MAX_CROP_FACTOR -> MAX_ZOOM_FACTOR
- [x] Better definitions for multiple bits
- [x] Links in md aren't working

## Eagle/Kestrel USB

After running uvc-test.sh for ~15 min, pyvideo sometimes locks up with:

```
MediaManager @ /com/vaddio/MediaServer1: Error: gst-stream-error-quark: Internal data stream error. (1): ../../../../gstreamer-1.16.0/libs/gst/base/gstbasesrc.c(3072): gst_base_src_loop (): /GstPipeline:server_pipe1/GstBin:bin2/GstV4l2Src:v4l2src0:
streaming stopped, reason error (-5)
MediaManager @ /com/vaddio/MediaServer1: Error: gst-stream-error-quark: Internal data stream error. (1): ../../../gstreamer-1.16.0/plugins/elements/gstqueue.c(988): gst_queue_handle_sink_event (): /GstPipeline:server_pipe1/GstBin:bin2/GstQueue:queue0:
streaming stopped, reason error (-5)
```

Pipeline log GST_DEBUG=4 shows:

```
0:03:19.892997563 30958   0x7f7c0044f0 ERROR          v4l2allocator gstv4l2allocator.c:1270:gst_v4l2_allocator_qbuf:<v4l2sink456:pool:sink:allocator> failed queueing buffer 0: Invalid argument
0:03:19.893026363 30958   0x7f7c0044f0 ERROR         v4l2bufferpool gstv4l2bufferpool.c:1180:gst_v4l2_buffer_pool_qbuf:<v4l2sink456:pool:sink> could not queue a buffer 0
0:03:19.893047033 30958   0x7f7c0044f0 ERROR         v4l2bufferpool gstv4l2bufferpool.c:2070:gst_v4l2_buffer_pool_process:<v4l2sink456:pool:sink> failed to queue buffer
0:03:19.893080243 30958   0x7f7c0044f0 INFO                    task gsttask.c:312:gst_task_func:<queue1373:src> Task going to paused
0:03:19.899833123 30958   0x7f5c024de0 INFO                    task gsttask.c:312:gst_task_func:<queue1372:src> Task going to paused
```

Enabled videobuf2 trace options with:

```
echo 1 > /sys/module/videobuf2_common/parameters/debug ; echo 1 >
 /sys/module/videobuf2_dma_sg/parameters/debug ; echo 1 > /sys/module/videobuf2_v4l2/parameters/debug
```

Result:
```
root@vaddio-device-04-91-62-DB-11-1A:~# [  402.450611] videobuf2_common: __prepare_dmabuf: invalid dmabuf length 1843200 for plane 0, minimum length 1610612736
[  402.461173] videobuf2_common: __buf_prepare: buffer preparation failed: -22
[  402.468167] uvc_v4l2_qbuf(): vb2_ioctl_qbuf returned -22 (state=1 halt=0)
```

```
root@vaddio-device-04-91-62-DB-11-1A:~# [ 1720.693355] WARNING: CPU: 1 PID: 935 at drivers/usb/gadget/fun
ction/uvc_queue.c:55 uvc_queue_setup+0x48/0x78
[ 1720.703178] Modules linked in: hbi(O)
[ 1720.706842] CPU: 1 PID: 935 Comm: queue833:src Tainted: G           O      4.19.0-xilinx-v2019.2 #1   
[ 1720.715873] Hardware name: ZynqMP Vaddio Eagle USB (DT)
[ 1720.721084] pstate: 20000005 (nzCv daif -PAN -UAO)
[ 1720.725866] pc : uvc_queue_setup+0x48/0x78
[ 1720.729958] lr : vb2_core_reqbufs+0x10c/0x3f0
[ 1720.734300] sp : ffffff8008ec3ac0
[ 1720.737599] x29: ffffff8008ec3ac0 x28: ffffffc02d177b00
[ 1720.742903] x27: ffffff80089df9e8 x26: ffffff8008ec3b38
[ 1720.748206] x25: 0000000000000004 x24: ffffff8008ec3cf8
[ 1720.753509] x23: 0000000000000000 x22: ffffffc02eb18d50
[ 1720.758813] x21: ffffffc02d177b00 x20: ffffff80089b9588
[ 1720.764117] x19: ffffffc02eb18cf0 x18: 0000000000000000
[ 1720.769420] x17: 0000000000000000 x16: 0000000000000000
[ 1720.774723] x15: 0000000000000000 x14: 0000000000000000
[ 1720.780027] x13: 0000000000000000 x12: 0000000000000000
[ 1720.785330] x11: 0000000000000000 x10: 0000000000000000
[ 1720.790634] x9 : 0000000000000000 x8 : 0000000000000004
[ 1720.795937] x7 : 0000000200000002 x6 : ffffffc02eb18cf0
[ 1720.801241] x5 : ffffff80083d6d38 x4 : ffffffc02eb18d50
[ 1720.806544] x3 : ffffff8008ec3b38 x2 : 00000000003f4800
[ 1720.811848] x1 : 0000000060000000 x0 : 0000000000000000
[ 1720.817152] Call trace:
[ 1720.819585]  uvc_queue_setup+0x48/0x78
[ 1720.823325]  vb2_core_reqbufs+0x10c/0x3f0
[ 1720.827327]  vb2_ioctl_reqbufs+0x6c/0x98
[ 1720.831243]  v4l_reqbufs+0x48/0x58
[ 1720.834635]  __video_do_ioctl+0x23c/0x498
[ 1720.838636]  video_usercopy+0x148/0x560
[ 1720.842463]  video_ioctl2+0x14/0x1c
[ 1720.845945]  v4l2_ioctl+0x3c/0x58
[ 1720.849254]  do_vfs_ioctl+0xbc/0x900
[ 1720.852820]  ksys_ioctl+0x44/0x90
[ 1720.856127]  __arm64_sys_ioctl+0x1c/0x28
[ 1720.860044]  el0_svc_common+0x84/0xd8
[ 1720.863696]  el0_svc_handler+0x68/0x80
[ 1720.867437]  el0_svc+0x8/0xc
[ 1720.870309] ---[ end trace 94b90ca6eb9d03dc ]---
[ 1720.874968] uvc_queue_setup(): 32768x32768 bpp=12 fcc=842094158 imagesize=1610612736
```

Tried `DEBUG_MUTES` and `DEBUG_SPINLOCK`, but no errors showed up.

This reproduces it quickly, both with and without GST_DEBUG=... (the key seems to be strace).
```
GST_DEBUG_FILE=/run/gst.log GST_DEBUG=4,\*v4l2\*:5 strace -fye trace=openat,dup,close,ioctl -o /run/trace.out python -m pyvideo_media_server.server -d 1
```

Figured out the issue: Needed to implement TRY_FMT separately since sometimes gstreamer will perform a TRY_FMT after a SET_FMT. This might be threading-related. Now the driver is always in a good state for STREAMON.

#### Kestrel CMA

Kestrel seems to need quite a bit of CMA. Ran overnight with cma set to 192MB and rtstream completely disabled, and I still get occasional messages:
```
vaddio-kestrel-30-45-11-CC-96-20 login: [ 3243.995321] alloc_contig_range: [b8500, b85fe) PFNs busy
[ 3244.001217] alloc_contig_range: [b8500, b85fe) PFNs busy
[15508.827696] alloc_contig_range: [bc200, bc3c2) PFNs busy
[16838.099138] alloc_contig_range: [b9500, b96c2) PFNs busy
[17335.310568] alloc_contig_range: [b8300, b83fe) PFNs busy
[17335.322325] alloc_contig_range: [b8300, b83fe) PFNs busy
[17335.334034] alloc_contig_range: [b8500, b85fe) PFNs busy
[17335.339948] alloc_contig_range: [b8300, b83fe) PFNs busy
[18083.194018] alloc_contig_range: [b7e00, b7fc2) PFNs busy
[21482.489630] alloc_contig_range: [b8600, b86fe) PFNs busy
[23086.526062] alloc_contig_range: [b8100, b81fe) PFNs busy
[23086.534466] alloc_contig_range: [b8100, b81fe) PFNs busy
[23086.540346] alloc_contig_range: [b8100, b81fe) PFNs busy
[24306.588560] alloc_contig_range: [bc400, bc5c2) PFNs busy
[32921.077966] alloc_contig_range: [b8500, b85fe) PFNs busy
[40920.965033] alloc_contig_range: [b9700, b98c2) PFNs busy
[42853.792633] alloc_contig_range: [b9500, b96c2) PFNs busy
[45613.677349] alloc_contig_range: [b7e00, b7fc2) PFNs busy
[45613.684477] alloc_contig_range: [b7f00, b80c2) PFNs busy
[45613.698149] alloc_contig_range: [b7e00, b7fc2) PFNs busy
[47405.139286] alloc_contig_range: [b8500, b85fe) PFNs busy
[50899.900508] alloc_contig_range: [b7e00, b7fc2) PFNs busy
[51833.153983] alloc_contig_range: [b9300, b94c2) PFNs busy
[54850.638846] alloc_contig_range: [bc200, bc3c2) PFNs busy
[54850.657607] alloc_contig_range: [bc300, bc4c2) PFNs busy
[56463.777073] alloc_contig_range: [b8200, b83c2) PFNs busy
[56463.782829] alloc_contig_range: [b8300, b84c2) PFNs busy
```

## Kestrel HDMI problems

I can force 720p HDMI output with:
```
modetest -s 54:1280x720
```

kmssink seems to need `force-modesetting=true` to switch modes, but then the pipeline does not negotiate.

## Kestrel USB issues

Kestrel still has "clicks" on usb, both playback and record. Tried bringing in SG changes to split bulk transactions, no luck from 16384..16384*16 payload size.

Tried cherry-picking the following commits from upstream:

```
d9feef974e0d usb: dwc3: gadget: Continue to process pending requests
36f05d36b035 usb: dwc3: gadget: Issue END_TRANSFER to retry isoc transfer
d53701067f04 usb: dwc3: gadget: check if dep->frame_number is still valid
25abad6a0584 usb: dwc3: gadget: return errors from __dwc3_gadget_start_isoc()
475d8e0197f1 usb: dwc3: Track DWC_usb31 VERSIONTYPE
d92021f66063 usb: dwc3: Add workaround for isoc start transfer failure
9bc3395c2496 usb: dwc3: gadget: Store resource index of start cmd
8411993e79df usb: dwc3: gadget: Remove unnecessary checks
a7027ca69d82 usb: dwc3: gadget: Give back staled requests
cb11ea56f37a usb: dwc3: gadget: Properly handle ClearFeature(halt)
cf2f8b63f7f1 usb: dwc3: gadget: Remove END_TRANSFER delay
da10bcdd6f70 usb: dwc3: gadget: Delay starting transfer
c58d8bfc77a2 usb: dwc3: gadget: Check END_TRANSFER completion
a3af5e3ad3f1 usb: dwc3: gadget: add dwc3_request status tracking
```

### 9/28/20

- Increased # of requests for audio from 2->4 did not help.
- Running usbtest. Tests 13, 14, and 21 fail both on Eagle and Kestrel.
- Backported changes from vaddio-ti..v4.19.148 appears to resolve hotplug issue where USB feezes. Did not resolve audio distortio

http://www.ti.com/processors/digital-signal-processors/libraries/am57x-video-codecs.html

### 7/14/20

Eagle PR Order:
1. Thud PR
2. VNG-11593: u-boot ext4 metadata/ext4write fix
3. VNG-11116: remove Xilinx bootgen
4. VNG-11081: Zynqmp defconfig
5. VNG-11187: Eagle fitImage support
5a. VNG-11249/VNG-11184: Eagle Machine setup (u-boot)
5b. VNG-11249: Eagle machine setup (vng)
6a. VNG-11503: Zynq US+ DFU (u-boot)
6b. VNG-11503: Zynq US+ DFU (vng)
7. Eagle media


### 11/18/19

Yocto Thud/Warrior upgrade issues remaining:

- [ ] openssl: not found
- [ ] rm: can't remove '/etc/resolv.conf': Read-only file system
- [ ] ln: /etc/resolv.conf: File exists   
- [ ] lircd-0.9.4d[806]: Error: /usr/lib/lirc/plugins/default.so: undefined symbol: major
- [ ] lircd-0.9.4d[806]: Error: /usr/lib/lirc/plugins/default.so: undefined symbol: major

### 3/4/19

- [ ] Imatest on Xylon JPEG encoder
- [ ] Xylon JPEG decoder verification
- [ ] Cast JPEG encoder driver support

### 12/12/18

- [ ] OpenRTOS bringup and investigation for UltraScale R cores

### 11/21/18

UVC-related for next week:
- [ ] Figure out UVC format change in Windows issue
- [ ] Maybe change video->max_payload_size based on image size?
- [ ] Pull in uvc-gadget change for image size
- [ ] Test new FPGA drop from Dennis

### 11/14/18

- [x] Tickets for rest of Prime hardware
- [ ] Macnica repository and zcu106 BSP setup (ongoing)
- [ ] Password document creation for Vaddio accounts

### 11/1/18

Rigel Prime hardware validation.
Prime builds: Need to handle version check to prevent updating backwards to a non-prime supported build

1. FSBL boot console (FPGA-407)
2. Move Red LED (FPGA-407)
3. Camera block outputs (Eliza, VNG-9710)
4. Difference in Rigel/Rigel Prime for BLE presence vs ADV7611 (VNG-9708/VNG-9709)


### 10/18/18

Sumo upgrade TODO:
- [x] remove vaddio-distutils-base completely?
- [x] comment still necessary in u-boot-ti-staging?
- [ ] busybox: remove klogd?
- [x] opkg-utils: Add comment about tar_ignore_error patch
- [x] bump PR_appends on python recipes which removed optimizations
- [x] python-jsonpointer: missed PR bump
- [x] python: panifest -> manifest
- [x] procps: bump PR
- [x] vng-mw-config: bump PR
- [x] vng-util: bump PR

### 10/4/18

Need to research how to do overlays/keying on the UltraScale for the upcong ePTZ product. Looks like the Xilinx Overlay IP core is being obsolete'd, but we could possibly use the [Xilinx Video Mixer](https://www.xilinx.com/products/intellectual-property/ef-di-vid-mix.html).

[Xilinx video example designs](https://www.xilinx.com/support/answers/61625.html)

"The LogiCORE, Video Mixer is now a license free IP and bundled with Vivado." - doesn't seem to be true in 2018.2 - it still wants a license?

Rigel Prime schematic notes:
- [ ] No more ADC_2P5V pin


### 10/3/18
- Boombox has PUDC_B pulled high and CAM_EN pulled low. Should be ok.

How do we know if we need a post-install to reset the camera block?

For Sitara-based products, it depends on any external pull ups/downs as well as the internal state of the Sitara's pins.
For Zynq-based products, it depends on the PUDC_B pin which controls whether the FPGA pins start out as high-Z (PUDC_B pulled high) or pulled up (PUDC_B pin pulled low).

- Romulus and Regula have CAM_RESET pulled up, so they definitely do.
- Remus has its PUDC_B pin tied to a rotary switch, so we can't be sure whether its FPGA IO comes up in High-Z or pulled-up. It should reset it too.
- Risa has PUDC_B pulled down which enables the internal pull-ups. This would fight with the CAM_RESET pin which is pulled down, so we should reset it here, too.
- Kronos has PUDC_B pulled high, CAM_RESET pulled high. It'll need a reset.
- Vulcan: PUDC_B pulled high, CAM_RESET is not pulled. This one probably should have a reset to be safe.
- Rigel has PUDC_B pulled high and CAM_RESET pulled low, so it should reset the block automatically.
- Centaur has PUDC_B pulled high and CAM_RESET pulled high. Needs reset.
- Antares and Cygnus have PUDC_B pulled high and CAM_RESET pulled low. Should be okay without one.
- Shotzy has PUDC_B pulled high and CAM_RESET pulled high. Needs reset.
- Corvus has PUDC_B pulled high and CAM_RESET pulled high. Needs reset.
- Boombox has PUDC_B pulled high and CAM_EN pulled low. Should be ok.
- New Vulcan: PUDC_B pulled high, CAM_RESET is not pulled. This one probably should have a reset to be safe.

The following need to reset the block in post-install:
Romulus, Regula, Remus, Risa, Kronos, Vulcan, Centaur, Shotzy, Corvus, New Vulcan.

The following are fine without one:
Rigel, Antares, Cygnus, Boombox.

### 9/18/18

Boombox 1V noise issue. Investigating DP83867 ethernet phy settings:
- Do not need rxctrl_stap_quirk (RX_CTRL is in mode 3 in hw)
- Impedance control: default is 50ohm. "Mismatch [...] can cause voltage overshoot and undershoot."

### 9/14/18

How to select which rootfs partition to load bitstream from when eeprom is on emio?
Decided to create rootpart_2 or rootpart_3 in boot partition, presence of which indicates which bitstream to load. Web update will need to create this file. Wanted to use "configure-update", but that doesn't handle the first boot after update case. Also, who should place the file on a fresh install?

### 9/12/18

Talking with Steve about Vulcan/New Vulcan shared builds:
- Can merge in New Vulcan shared build support for Vulcan hardware anytime
- Need to be careful about renaming zynq-new-vulcan -> zynq-vulcan. Jeremy is only okay with this if we are ready to commit to having a shared build. So leave this in a branch.
- Try out POC for rename to see if any other issues arise.

### 9/11/18

VNG-9502/VNG-9503 for shared Vulcan/New Vulcan build. Thinking about u-boot boot procedure:

1. Read "prime bit"
2. Load bitstream from rootfsA
3. Read MAC address
4. Read vaddio update status
5. TODO: Load bitstream from other partition?
6. Read dtb
7. Boot

### 9/7/18

Working on VNG-9502 to allow installing multiple dts and bit file on the target.
Need to solve some issues with our dtsi's:

1. Top-level dts files (can be named uniquely)
2. emio/pl dtsi files for fpga (each zynq will have one)
3. vaddio-\*.dtsi files for features (all fpga?) (varies based on machine feature, so far only zynq)

### 9/4/18

Turning on LED's increases noise with both microphone straight through as well as with AEC enabled. Turning off DAC outputs did not appear to affect noise levels.

Trying to pass digital microphone through at 48kHz to TDMA: Seems like this is not possible. From section 2.1.4 of the firmware manual: "This DC-reject filter is always available on the digital mic inputs, and is not programmable."

### 8/31/18

Having issue with timberwolf gaining test tones by 2. Run loopback test:
Loop I2S1L to I2S1L output, same with R:

wr 214 5
wr 216 6

Gains to 0:
wr 23c 0
wr 23e 0

Reset:
wr 6 2

Route USB_L, USB_R (usb playback) to USB_PB_REF_L, USB_PB_R. DSP_SDIA.
TW_MIC_IN_L to USB_L and TW_MIC_R to USB_R (usb record). DSP_SDOA.
devmem 0x40600004 32 0x80400000
devmem 0x40600008 32 0x08040000

Ensure gains are correct:
devmem 0x40600080 32 0x00008000
devmem 0x406000a4 32 0x00008000
devmem 0x406000f8 32 0x00008000
devmem 0x4060011c 32 0x00008000
devmem 0x40600018 32 0x00008000
devmem 0x4060001c 32 0x00008000
devmem 0x40600198 32 0x00008000
devmem 0x4060019c 32 0x00008000

Loopback looks fine. However, test tones from timberwolf are still gained.

Turns out clk edge was backwards on TDMA/TDMB. Clearing xeDR and xeDX (transmit edge) so that the data is transmitted on the falling edge of the clk fixed it. (TDMA PCM configuration 0x264 and TDMB PCM config 0x27c)

wr 264 4040
wr 27c 4040
wr 6 2

##### Rigel Prime schematic comments

- Block diagram still references LVDS for camera block
- Parts of CAM_EN stuff missing on camera block page (pins 27-30 of KEL connector).
- Confirm DIP switch removal (soft DIP's?)
- I2C0_SCL/SDA can be removed

### 8/30/18

- [ ] Talk to Bruce about polarity of Microphones
- [ ] Talk to Glenn about i2s from TW being one bit too large (2x amplitude)

### 8/29/18

Vulcan/New Vulcan common build things to resolve:

1. Include both new vulcan and vulcan bitstreams/dts's in build. 2d
1. Select bitstream/dts in u-boot based on rev bit. 2d
2. web update from old non-ark to ark build
    a. Revert p7m digest to md5, in review.
    b. Add special case where machine == vaddio-vulcan to update to machine == zynq-vulcan. (1d)
3. Handle presence/absence of soft dip's in middleware. 3d (per Eliza)
   a. Web shouldn't need any changes.
4. Support different blocks (if both Sony's, shouldn't be too bad). 2d
5. Detect presence/absence of FPGA genlock capability.
   a. Consensus is to base on New Vulcan rev bit. Middleware will need to determine this. Topic for 9/4 middleware meeting.
   b. Web page needs to handle showing or hiding this as well. Maybe this will be automatic?

Rev bit 4 (MIO 46) will be used to determine Vulcan/New Vulcan. High == Vulcan, Low == New Vulcan.

- [x] Ticket for POC for PSoC touch
- [ ] New Vulcan CPU1 app impact of combining builds
- [ ] New Vulcan/Vulcan Valens ID
- [ ] See who uses clish debug shell
- [ ] Chromium OS requests

### 8/7/18

- [ ] build script updates: ark web update broken
	Reviewers/testers: Joel, Ben, William
- [ ] Talk to Steve S. about flow control on FX3 UART

### 6/6/18

Compressed filesystem things:
- [x] Store devicetree and kernel in boot partition
- [x] Overlayfs for development
- [x] Handle configure initscript
- [x] Get rid of persistent update partition? (used today for updates >50MB) - changed to use for overlay storage
- [x] Use separate dirs for overlay

### 5/31/18

Hobbit IP/USB streaming, enable audio/FX3:
	devmem 0x4060007C 32 0x00000007

### 5/23/18

Plutus gst 1.0:
All cameras at 720p on boot - garbled video. Set one to 1080p and switch to it, issue goes away. (output at 1080p)
Changed output to 720p, all cameras at 360p, changed one to 720p, still garbled.

Headers appear to always be 3 bytes short - 0x41 instead of 0x44, and occasional 0x43's and 0x45's instead of 0x46's and 0x48's.

### 5/17/18

Metropolis u-boot 2018.01: Make sure to turn off CONFIG_DM, otherwise it resets jumping to u-boot.
Ethernet is not using correct MAC?
	Turns out it uses what's in the efuse instead of eeprom.

### 5/14/18

1.4.2 PS-PL interfaces: "Automatic expansion to 64 bits for unaligned 32-bit transfers in 32-bit slave interface aconfiguration mode"

Turned out that loading the bitstream in BOOT.BIN fixed the issue. It sounds like we had issues in the past on Plutus with the AXI interface not working properly with an external bitstream. So we're going back to that for now until we can find the root cause of this issue.

### 5/11/18

a2e on ark:
reg dump - hdr length varies on new build but not on old (always 0x48)
memory fill - shows that memory is not getting written
idr slice header issue?

GST_DEBUG=h264parse:5 /usr/bin/rtsp-server -p 554 -u "/vaddio-conferenceshot-av-stream" "devsrc dev-node=/dev/h264s blocksize=800000 do-timestamp=true ! h264parse ! capsfilter caps=video/x-h264,stream-format=avc,alignment=au ! rtph264pay perfect-rtptime=false pt=96 name=pay0 alsasrc name=audioin provide-clock=true do-timestamp=true slave-method=1 latency-time=21333 buffer-time=1000000 ! capsfilter name=acaps caps=audio/x-raw,format=S16LE,rate=48000,channels=2 ! queue leaky=2 silent=true max-size-buffers=0 max-size-time=37500000 min-threshold-time=0 max-size-bytes=0 ! faac ! audio/mpeg ! rtpmp4apay perfect-rtptime=false pt=97 name=pay1"

### 5/8/18

Add to xlnx-boot-bin.bb:
COMPATIBLE_MACHINE ?= "^$"
COMPATIBLE_MACHINE_zynq = ".*"
COMPATIBLE_MACHINE_zynqmp = ".*"

Fixes:
WARNING: /var/build/poky/meta-vaddio-ark/recipes-bsp/boot-image/xlnx-boot-bin.bb: Unable to get checksum for xlnx-boot-bin SRC_URI entry bootimage.bif: file could not be found
Parsing recipes: 100% |############################################################################| Time: 0:00:49
Parsing of 2355 .bb files complete (0 cached, 2355 parsed). 3189 targets, 537 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies


### 4/26/18

Reproduced issue: streaming from Rigel (input 1), unplugged PCC, when it booted up it connected to the Antares (input 3) and showed a test pattern. Switched to Rigel and it worked fine, but Vulcan (input 2) and Antares just sit on a black screen. For Vulcan, gstreamer crashes, but for Antares it just hangs.

I swapped builds on the Rigel and Vulcan (loaded a Nightly on the Rigel, and new build on the Vulcan), and the PCC was then able to connect to the Antares (immediately once they updated), Rigel, and Vulcan.

Wondering if it is some network caching thing. This seems to do it every time:

1. Stream from a camera, call it 'A', on the PCC.
2. Unplug and re-plug power to PCC.
3. Observe that you will no longer be able to connect to other cameras besides 'A' (likely, but this may take a few attempts).
4. Unplug camera 'A' and observe that you can connect to the other cameras.
5. Once 'A' reboots, observe that you can once again connect to it.

### 4/25/18

Current plutus issues (with 0608e6)
- When switching streams, some inputs will stay black and gstreamer appears to crash. Only rebooting the entire network and all devices seems to fix this.
- PCC occasionally freezes or gets very slow for several minutes and then recovers.

### 4/24/18

[Really detailed h.264 answer post](https://stackoverflow.com/questions/24884827/possible-locations-for-sequence-picture-parameter-sets-for-h-264-stream/24890903#24890903)

New Plutus, new cameras

New Plutus, old cameras

Old Plutus, new cameras
This is interesting. Lots of h264_write()'s compared to how many start frames are found. This indicates we are likely missing frames.
- realized I didn't have the frame offset search fix applied here.
- h264parse is necessary on the server - also, it appears the format of the capsfilter on the server does not matter

TODO: Check shared stream from different clients:
0:00:23.840672801 18814 0xb6619af0 WARN                  devsrc /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0/1.2.4-r0-VNG-0/gstreamer-1.2.4/plugins/elements/gstdevsrc.c:413:gst_dev_src_start:<devsrc1> error: Could not open device node "/dev/h264s" for reading.
0:00:23.841319823 18814 0xb6619af0 WARN                  devsrc /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0/1.2.4-r0-VNG-0/gstreamer-1.2.4/plugins/elements/gstdevsrc.c:413:gst_dev_src_start:<devsrc1> error: system error: Device or resource busy
0:00:23.842994696 18814 0xb6619af0 WARN                  devsrc /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0/1.2.4-r0-VNG-0/gstreamer-1.2.4/plugins/elements/gstdevsrc.c:413:gst_dev_src_start:<devsrc1> error: Could not open device node "/dev/h264s" for reading.
0:00:23.844294656 18814 0xb6619af0 WARN                  devsrc /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0/1.2.4-r0-VNG-0/gstreamer-1.2.4/plugins/elements/gstdevsrc.c:413:gst_dev_src_start:<devsrc1> error: system error: Device or resource busy
0:00:23.844995777 18814 0xb6619af0 WARN                 basesrc /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0/1.2.4-r0-VNG-0/gstreamer-1.2.4/libs/gst/base/gstbasesrc.c:3584:gst_base_src_activate_push:<devsrc1> Failed to start in push mode
0:00:23.845355270 18814 0xb6619af0 WARN                GST_PADS /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0/1.2.4-r0-VNG-0/gstreamer-1.2.4/gst/gstpad.c:994:gst_pad_set_active:<devsrc1:src> Failed to activate pad
0:00:23.845818407 18814 0xb6619af0 WARN               rtspmedia /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/1.2.3-r0/gst-rtsp-server-1.2.3/gst/rtsp-server/rtsp-media.c:2037:start_preroll: failed to preroll pipeline
0:00:23.846196330 18814 0xb6619af0 WARN               rtspmedia /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/1.2.3-r0/gst-rtsp-server-1.2.3/gst/rtsp-server/rtsp-media.c:2109:start_prepare: failed to preroll pipeline
0:00:23.846662641 18814    0xc80c0 WARN               rtspmedia /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/1.2.3-r0/gst-rtsp-server-1.2.3/gst/rtsp-server/rtsp-media.c:2058:wait_preroll: failed to preroll pipeline
0:00:23.846815995 18814    0xc80c0 WARN               rtspmedia /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/1.2.3-r0/gst-rtsp-server-1.2.3/gst/rtsp-server/rtsp-media.c:2264:gst_rtsp_media_prepare: failed to preroll pipeline
0:00:23.864366496 18814    0xc80c0 ERROR             rtspclient /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/1.2.3-r0/gst-rtsp-server-1.2.3/gst/rtsp-server/rtsp-client.c:602:find_media: client 0x1e1c0: can't prepare media
0:00:23.866354539 18814    0xc80c0 ERROR             rtspclient /var/build/build/tmp/work/armv7a-vfp-neon-poky-linux-gnueabi/gstreamer1.0-rtsp-server/1.2.3-r0/gst-rtsp-server-1.2.3/gst/rtsp-server/rtsp-client.c:1807:handle_describe_request: client 0x1e1c0: no media

### 4/23/18

Added a warning that prints when a start code is found on an unaligned (non-4-byte aligned) address. Only a camera with the new gstreamer causes this - the other does not. New gst was on vulcan, old on rigel, have not tried swapping yet to see if it follows the camera.

There does appear to be an issue with teh Vulcan on high quality - swapped the builds, and the slowness appears to follow it.

### 4/17/18

Making rtp parameters match from DESCRIBE request:

gst-rtsp-server:
rtsp-sdp.c
changed "IN" conneciton information to hard-coded IP
removed ts-refclk and mediaclk

gst-plugins-good:
Set is live on devsrc in init: gst_base_src_set_live (GST_BASE_SRC (src), TRUE);
gstrtph264pay: Commented out packetization-mode and profile-level-id

### 4/16/18

gst-launch for testing cameras:
gst-launch-1.0 rtspsrc location="rtsp://10.30.203.66/vaddio-roboshot-hdbt-stream" name=rtspsrc0 port-range="3000-3005" latency=0 rtspsrc0. ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! h264parse ! avdec_h264 ! xvimagesink

Pipeline running in plutus on 1.12 (same results as before)

        ! capsfilter caps="application/x-rtp,media=video"
        ! rtph264depay
        ! capsfilter caps="video/x-h264,stream-format=(string)byte-stream"
        ! filesink location="${video_out_file}" async=false


### 4/10/18

Steps to reproduce potential onelink/rigel/matrix pro audio streaming issue
Set OneLink matrix to connect USB Playback Left -> HDBT Output Left, same w/right
Generate test tone with audacity, using output device OneLink Bridge
Connect Rigel to OneLink, enable HDMI audio soft dip
Try streaming from Rigel, verify you hear test tone
Connect Rigel HDMI to Matrix Pro HDMI PC input
On Matrix Pro matrix page, connect HDMI input -> USB Record
If you let it stream for a while, you should see the matrix pro meters drop out every few seconds for ~0.5s, and you'll hear the audio drop. It'll continue to do this until you either toggle the soft dip on the Rigel, or disable and re-enable the matrix connections on the OneLink or Matrix Pro.

### MW Design notes:

Look into speeding up yocto builds w/shared state or better downloads mirrors.
What about rm_work to speed build times?
Qemu is fast with kvm.
CROPS container for containerized builds?

### 7/21

- [ ] Get bootes polished up and into PR
- [ ] Bootes zcu102 machine support for Tues

Antares/Cygnus FPGA work to be done:

- [ ] Need I2C connected to PS I2C: CY_SDA_MOSI and CY_SCL_MISO.
- [ ] CY_SWDIO_SS0 needs to be connected to PS.
- [ ] Cygnus: Connect LASER_EN to PS.
- [ ] Nice to have: CY_COM_TX and CY_COM_RX routed to a UART on the PS.
- [ ] Clean up Verilog related to IR_SIGNAL since the PS drives it.

Talk to Jeff about:

- [x] PSOC pin 6 and rest of pinout - Jeff investigating
- [x] LASER_EN (who is driving this?) - currently nobody
- [x] IR_SIGNAL commented out - should be removed, driven by PS (Linux)
- [x] Extra ribbon cable for Cygnus

### 7/20

I'm not sure I like how VADDIO_CAMERA_PRODUCTS is defined in all of the
product.py files. It seems that might be a good one to put in machine.py or a
more generic, common spot.

- [ ] Gadget writeable module still needed on Antares/Cygnus?

### 7/19

- [ ] Cygnus build
- [x] Figure out correct machine.py settings for Cygnus (genre, support URL)
  Per Steve, just use sensible defaults or what looks close for now.
- [x] Apply VNG-7214 to Cygnus
- [x] Web frontend issue with Cygnus

### 7/18

- [x] Create <machine>-image and <machine>-debug-image for bootes so Jenkins
  can build it.

  Talked to Eliza about the middleware issues. you can workaround the startup
  problem by manually sending the ready signal to all the services with:

    dbus-send --system /com/vaddio/ProductService/object com.vaddio.ProductService.sync_signal string:"ready"

The HDLink version shows up once the ready signal is sent.

Appears there is till a problem with camera enumeration, though:

```
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 ir-service: _camera_connect_callback; will retry exceptions.TypeError: 'NoneType' object has no attribute '__getitem__'
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 ir-service: Traceback (most recent call last):
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 File "/usr/lib/python2.7/site-packages/vng_mw_ir/ir_mgr.py", line 74, in _camera_connection_callback
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 self._get_capabilities()
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 File "/usr/lib/python2.7/site-packages/vng_mw_ir/ir_mgr.py", line 90, in _get_capabilities
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 _, cam_data = cu.find_in_cache(vid_data, vid_data['input'][0]['device']['camera']['$href'])
Jul 17 23:12:30 vaddio-reveal-hdbt-D8-80-39-9A-F6-B1 TypeError: 'NoneType' object has no attribute '__getitem__'
```

### 7/17

- [x] Investigate which version of dbus_object.py should be used.
- [x] Middleware product startup issue
- [x] HDLink firmware version missing (related to middleware issue?)

### 7/14

Antares:

- [x] Get vng-firmware-archive PR completed for new Antares FPGA/cpu1-app
- [x] Update/compress Antares PR
- [x] Test new changes on Antares
- [x] Test on Kronos to ensure nothing is broken.
- [x] Create JIRA for Antares Motor Updates
- [x] Create JIRA for Cygnus base build

Cygnus:
- [ ] Create Cygnus build based on Antares.

Bootes:
- [x] Get docker image updated
- [x] Update bitbake-mirror

### 6/16

Having issues with odd messages from local Antares build. Lots of:

    /etc/rc5.d/S60vng-mw-network: readonly: line 43: VNG_MACHINE_TYPE: is read only

And nginx startup issues:

    unable to write 'random state'
    writing new private key to '/etc/nginx/local.vaddio.key'
    /etc/nginx/local.vaddio.key: Read-only file system
    3070043344:error:0200101E:system library:fopen:Read-only file system:bss_file.c:398:fopen('/etc/nginx/local.vaddio.key','w')
    3070043344:error:20074002:BIO routines:FILE_CTRL:system lib:bss_file.c:400:
    Starting nginx server: nginx: [emerg] PEM_read_bio_X509_AUX("/etc/nginx/local.vaddio.crt") failed (SSL: error:0906D06C:PEM routines:PEM_read_bio:no start line:Expecting: TRUSTED CERTIFICATE)
    nginx.

Trying an sstate-clean build tonight. Booted fine - tomorrow try a build of
the fw_updater changes.

### 5/30

Read up on PRD's:

- [x] NG Production VIEW
- [x] Romulus (RoboSHOT)
- [x] Remus (RoboSHOT, newer)
- [x] NG Control Interface
