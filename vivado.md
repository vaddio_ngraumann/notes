# Tips and Tricks

#### Hardware manager

Show short names for all debug probes:

```
set my_probes [lindex [get_hw_probes]]
foreach probe $my_probes {
    set_property NAME.SELECT short [get_hw_probes $probe]
}
```
From: https://forums.xilinx.com/t5/Vivado-Debug-and-Power/How-to-set-all-ILA-probe-to-short-name/td-p/885627
