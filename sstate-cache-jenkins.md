### Frequent building due to VAD_SCM_OBJ

Investigated typical increase from build->build due to VAD_SCM_OBJ churn. Built master, removed cache,conf,tmp, committed empty file to change SHA, then built again. Saw 29544 byte increase in size. Approx 150 builds/day across Ark and non-Ark, this is around 120MB/month. Not a big deal.

For the record, tried removing sstate cache from packages using the autofetch by adding this to vaddio-autofetch.bbclass:

"""
python do_sstate_cleanup () {
    sstate_clean_cachefiles(d)
}

addtask sstate_cleanup after do_package_write_ipk do_package_write_deb do_package_write_rpm before do_build
"""

This only dropped usage to 12176 bytes/build.


### bitbake-mirror updating


To verify mirror is functional and only fetch internally, you need this in your local.conf:

"""
SOURCE_MIRROR_URL ?= "http://bitbake-mirror.vaddio.com/yocto/downloads"
INHERIT += "own-mirrors"

BB_FETCH_PREMIRRORONLY = "1"

BB_ALLOWED_NETWORKS = " \
    bitbake-mirror.vaddio.com \
    *.bitbucket.org \
"
"""

To update mirrors, comment out BB_FETCH_PREMIRRORONLY and add BB_GENERATE_MIRROR_TARBALLS = "1"
