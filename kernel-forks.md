### Overview

Kernel git repositories offer several advantages over simply keeping sets of patches around. See this [Proposal A3: Lightweight Kernel Forks](https://vaddio.atlassian.net/wiki/download/attachments/5439518/Proposal%20-%20Lightweight%20Kernel%20Forks.pdf?api=v2) for more information on the how and why around these lightweight forks.

### Viewing the full history of a fork

If you would like the complete history of the kernel you're working with, you will need to add the upstream Xilinx or TI kernel repository/repositories of interest as git remotes and fetch them. Then, you can use `git replace` to stitch together the histories.

##### Cloning and fetching

If you already have a kernel repository cloned from an upstream repository such as one of the official Xilinx or TI repositories or even Linus' maineline, you can simply add our repositories as remotes and fetch them:

```
git remote add bitbucket-xlnx git@bitbucket.org:vaddio/linux-xlnx.git
git remote add bitbucket-ti git@bitbucket.org:vaddio/ti-linux-kernel.git
git fetch bitbucket-xlnx && git fetch bitbucket-ti
```

Otherwise, begin by cloning the fork repository(s) of interest.

Next, add the remotes and fetch. For linux-xlnx:
```
git remote add xlnx https://github.com/Xilinx/linux-xlnx.git
git fetch xlnx
```

For linux-ti-staging:
```
git remote add ti git://git.ti.com/ti-linux-kernel/ti-linux-kernel.git
git fetch ti
```

##### Setting up git replace

We can now add `git replace` directives to stitch the Vaddio and upstream histories together. To do this you must find out the sha's of the base of the Vaddio repository and the first commit:

```
git rev-list --max-parents=0 vaddio_xlnx_rebase_2018.3
b11b85133da3e8e28d3869e2b623dcd5da26c51d

git rev-list -n 1 xlnx_rebase_v4.14_2018.3
eeab73d1207d6fc2082776c954eb19fd7290bfbe
```

The following `git replace` command can then be used to make commit `b11b85133da3e8e28d3869e2b623dcd5da26c51d` appear as if it is `eeab73d1207d6fc2082776c954eb19fd7290bfbe` instead:

```
git replace b11b85133da3e8e28d3869e2b623dcd5da26c51d eeab73d1207d6fc2082776c954eb19fd7290bfbe
```

Now, a `git log vaddio_xlnx_rebase_2018.3` will show the full history rather than just the history of the Vaddio changes.

### Adding a new branch from upstream

Under normal circumstances, once the fork repository has been created and an orphan branch established (see below), we can create a new commit to pull in upstream kernel changes by simply committing the files that changed between the two versions. We also could create another orphan branch (again, see below), but this results in git objects being created for all files rather than just the changes, making the repository grow much more quickly in size. For example, for the 2018.3 to 2019.2 update, this method added 25MB to the git repository size versus 180MB to create an orphan branch for 2019.2.

First clone the fork repository if you haven't done so and add the upstream repository as a remote:
```
git clone git@bitbucket.org:vaddio/linux-xlnx.git
git remote add upstream-xlnx https://github.com/Xilinx/linux-xlnx.git
git fetch upstream-xlnx
```

Next, create the new branch by checking out the import of the previous branch in the history, updating all of the files in the index to the new revision, and committing. For this example, we will assume that the most recent branch in the Xilinx repository is `xlnx_rebase_v4.14_2018.3` and that we would like to create a new branch based on `xlnx_rebase_v4.19_2019.1`. This is most easily done using the `git restore` command introduced in more receet versions of git.
```
git checkout vaddio_xlnx_rebase_v4.14_2018.3-import
git checkout -b vaddio_xlnx_rebase_v4.19_2019.1
git restore --source=xlnx_rebase_v4.19_2019.1
git add .
git commit -q
(Enter commit message following the example shown in `vaddio_xlnx_rebase_v4.14_2018.3-import` or similar which includes the base revision information).
git push -u bitbucket-xlnx vaddio_xlnx_rebase_v4.19_2019.1
```

Next, tag the imported revision with a `-import` suffix so that it's discoverable later, for example for creating newer branches:
```
git tag vaddio_xlnx_rebase_v4.19_2019.1-import
git push bitbucket-xlnx vaddio_xlnx_rebase_v4.19_2019.1-import
```

Now you can rebase the Vaddio custom changes onto the new branch:
```
git checkout vaddio_xlnx_rebase_v4.14_2018.3
git checkout -b VNG-xxxx-xilinx-kernel-update
git rebase --onto vaddio_xlnx_rebase_v4.19_2019.1 xlnx_rebase_v4.14_2018.3
```

Of course, there might be conflicts to resolve here. Once that's done, you can create a PR in bitbucket to update the `vaddio_xlnx_rebase_v4.19_2019.1` with the changes.


### Establishing a new fork repository

Let's assume we'd like to create a new fork repository. This means we must add another orphan branch to the Xilinx repository. In this example. we'll use Xilinx release 2018.3. Ensure you've followed the steps in "Viewing the full history of a fork" above to add the `linux-xlnx` repository as a new remote and fetched it:
```
git remote add bitbucket-xlnx git@bitbucket.org:vaddio/linux-xlnx.git
git remote add upstream-xlnx https://github.com/Xilinx/linux-xlnx.git
git fetch bitbucket-xlnx && git fetch upstream-xlnx
```

Then create the branch and push:
```
git checkout xlnx_rebase_v4.14_2018.3
git checkout --orphan vaddio_xlnx_rebase_v4.14_2018.3
git commit
(enter a useful commit message here about the base revision; for example see the commit message of `git --no-replace-objects show b11b85133da3e8e28d3869e2b623dcd5da26c51d`)
git push -u bitbucket-xlnx vaddio_xlnx_rebase_v4.14_2018.3
```

Tag the imported revision because it'll be useful down the road, and push it:
```
git tag vaddio_xlnx_rebase_v4.14_2018.3-import
git push bitbucket-xlnx vaddio_xlnx_rebase_v4.14_2018.3-import
```
