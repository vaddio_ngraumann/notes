Kestrel review
Block diagram:
- I2C address formats are inconsistent (some are 8-bit which includes R/W LSB, others are 7-bit right-aligned Linux-style). These should be made consistent so we can more easily verify that no devices conflict on the bus. Software strongly prefers 7-bit Linux style when listing i2c addresses for our use. If both are desired, a table format could be used to show both.

LED Driver:
- Is there a reason a different chip was chosen than what was used on Eagle (FAN5702)? These variations add extra work for software, especially when a new driver must be written.

AIC:
- LDO_SELECT needs to be a 1.8V signal for 1.8V IOVDD. This caused us lots of issues on Eagle bringup until we tracked it down.

USB 3.0:
- AC decoupling caps on SS lines (C173, C1678, C208, C204) must be 0.1uF per the USB 3.0 spec. The wrong values on these caused big headaches troubleshooting 3.0 on Eagle.
- USB_VBUS needs to be routed to a gpio input (of course with level shifting etc.) since the internal 2.0 PHY uses this as a presence detect in peripheral mode. Note that we only need VBUS detection, not DRVBUS.

Ethernet:
- Why are we using a different Gigabit PHY than our Zynq products?
