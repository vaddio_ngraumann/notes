```
root@vaddio-huddleshot-54-10-EC-31-37-BD:~# cat /proc/asound/pcm
00-00: logii2s-rx-0 : logii2s-rx-0 PCM : capture 1
00-01: logii2s-rx-1 : logii2s-rx-1 PCM : capture 1
00-02: logii2s-rx-2 : logii2s-rx-2 PCM : capture 1
00-03: logii2s-tx-3 : logii2s-tx-3 PCM : playback 1

root@vaddio-huddleshot-54-10-EC-31-37-BD:~# arecord -l
**** List of CAPTURE Hardware Devices ****
card 0: logii2s [logii2s], device 0: logii2s-rx-0 [logii2s-rx-0 PCM]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 0: logii2s [logii2s], device 1: logii2s-rx-1 [logii2s-rx-1 PCM]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 0: logii2s [logii2s], device 2: logii2s-rx-2 [logii2s-rx-2 PCM]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
  ```
