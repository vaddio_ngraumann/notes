### Xilinx Next-Gen camera meeting 04/16/20

4k60 4:2:2 10-bit using only PS

VCU Bandwidth consumption:

Encode buffer can be enabled - majority of customers do enable it
Essential is an L2 cache.
URAM often is used.

W/o encode buffer:
Approx. 50% (8.9GB/s) worst case
With:
5GB/s

485kB encode buffer

h.264 consumes more memory bandwidth than HEVC
