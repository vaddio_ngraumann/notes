## Antares/Cygnus Notes

6/20: Per Jeff and Glenn, the intention is to have two different FPGA builds,
which are based on the same code just generated with a differnet flag for the
different GPIO mappings.

The aux bus is differentiated between Cygnus and Antares by AUX_PS_1-3, which
are pulled up on the Antares and left floating on Cygnus.

If you get build errors in the FPGA, try going to IP Sources->Block Designs->
Cygnus_System, right click on it, and select "Reset Output Products".

### Single-image solutions

Multi-boot? SD card boot does not support multi-boot.
u-boot can load bitstream, see fpga loadb.

    ext4load mmc 0:2 ${loadbit_addr} ${bootdir}/CYGNUS_TOP.bit
    fpga loadb 0 ${loadbit_addr} ${filesize}

Can read GPIO:

    # Unlock configuration registers:
    mw 0xF8000008 0xDF0D
    # Disable pull-up on MIO11, 12, and/or 13
    mw 0xF800072C 0x00000600
    mw 0xF8000730 0x00000600
    mw 0xF8000734 0x00000600
    # Lock configuration registers, for safety
    mw 0xF8000004 0x767B
    # Read GPIO (may float since no pull up/down currently)
    gpio input 11
    gpio input 12
    gpio input 13

Seems there might be a hardware change needed, since the pins can only be set to
pull-up or floated in the Zynq, and the Cygnus board leaves everything floating.
In testing, this led to unpredictable results. It would be better to have one or
more resistors on the Cygnus daughterboard which would indicate the board type.

## Tasks

- Talk to Jeff about interfacing to motors
- Camera block?
- Daughterboard is a PSOC - mainly GPIO passthrough right now
- Build is based off of Rigel, but motors are likely what was used on Kronos,
    which are SPI-based motors controlled by middleware through /dev/spi.
- PSOC is only controlling the LED for now (pass-through), board also contains
    BLE chip.

### VNG-7422: Remove BLE motor program and update device-fw-updater

Tested with combined image 7422/7427 rev f34b96fb3:

- [x] Antares full SD flash
- [x] Rigel full SD flash
- [x] Rigel web update from latest nightly
- [x] Rigel web update from 1.0.0 release

TODO: Antares middleware does not boot appropriately when /dev/spidev is not
present. I2C version seems to fail a bit more gracefully.

Issue was due to /dev/spiX being completely missing, where i2c bus was still
present. Once SPI devnode is present, it fails much more gracefully.

### VNG-7427: Customize Antares kernel/dts

Antares SPI pins:

| Pin | Net | Pad |
| :-- | :-- | :-- |
| SS | AUX_PL_6 | U17 IO_L16P_T2 |
| CLK | AUX_PL_8 | U17 IO_L16P_T2 |
| MOSI | AUX_PL_10 | W20 IO_L4P_T0 |
| MISO | AUX_PL_12 | U17 IO_L16P_T2 |

Pin mappings on Zynq:
MIO:    0-53
EMIO:   54-?

LED Pins:

| Pin | Net | Net (daugher) | Pad |
| :-- | :-- | :-- | :-- |
| Red | AUX_PL_15 | CY_SPARE1 | IO_L13N_T2_MRCC_33 |
| Green | AUX_PL_11 | CY_SPARE2 | IO_L21P_T3_DQS_33 |
| Blue | AUX_PL_19 | CY_SPARE3 | IO_L14N_T2_SRCC_33 |

Other signals to connect:

AUX_PS_1    11  MOTOR_INT
AUX_PS_2    12  MOTOR_EN_N
AUX_PS_3    13  MOTOR_PP
AUX_PS_4    24  IR_SIGNAL

6/22/17 TODO:

- [x] Transplant and/or restore old system.json settings - Removed old pan/tilt
  motor firmware fields.
- [x] Move special things from Antares service into Antares-specific portion of
  Product Service. Specifically AntaresMgr.
- [x] Align new product refactor with Shotzy.
- [x] Figure out \_system_signal_callback issue - Resolved with ProductMgr
  now being a CameraMgr.
- [ ] product.json, if needed.

#### GPIO DTS Mappings

For flag definitions, see:
meta-vaddio/recipes-kernel/linux/common/src/gpio-common/gpio-common-dt.h
include/linux/of_gpio.h

Common ones:

| Flag | Definition |
| :--- | :--------- |
| 1<<0 | Active low |
| 1<<1 | DIP |
| 1<<2 | Rotary |
| 1<<8 | INIT_ENABLE |

#### EMIO mappings

From Glenn Austin:

Far as the registers to control them and defaults states you will want to talk to Springman.

Here is how they will be mapped to the Aux connector (I believe Jeff needs to push a build for it match this mapping 100%)


        assign AUXPL1 = 1'bz;
        assign AUXPL2 = PAN_LIMIT;  // EMIO GPIO11
        assign AUXPL3 = H_UL;
        assign AUXPL4 = TILT_LIMIT;  //EMIO GPIO12
        assign AUXPL5 = H_LL;
        assign AUXPL6 = SPI_AUX_SS0_N;
        assign AUXPL7 = H_LR;
        assign AUXPL8 = SPI_AUX_SCLK;
        assign AUXPL9 = H_UR;
        assign AUXPL10 = SPI_AUX_MOSI;
        assign AUXPL11 = CY_SPARE2;   //EMIO GPIO9
   //     assign AUXPL12 = SPI_AUX_MISO;
        assign auxpl12_tri = 0;
        assign AUXPL13 = CY_COM_TX;
        assign AUXPL14 = CY_SWDCLK_SCLK;   //EMIO GPIO6
        assign AUXPL15 = CY_SPARE1; // EMIO GPIO8
        assign AUXPL16 = CY_SDA_MOSI;
        assign AUXPL17 = CY_COM_RX;
        assign AUXPL18 = CY_SCL_MISO;
        assign AUXPL19 = CY_SPARE3;   //EMIO GPIO10
        assign AUXPL20 = CY_SWDIO_SS0;
        assign AUXPL21 = 1'bz;
        assign AUXPL22 = CY_XRES_N;  //EMIO GPIO7

Other EMIO Interrupts:

EMIO GPIO17: VS_PRGN_RST
EMIO GPIO18: PI_OE
EMIO GPIO19: IR_DIR
EMIO GPIO21: MDIN_INT
EMIO GPIO22: MDIN_RESET_N
EMIO GPIO23: CAM_RESET_N

6/22/17 Here's how they work:

For EMIO 6, which is CY_SWD_CLK_SCLK from above and AUXPL14:

    IOBUF gpio_0_tri_iobuf_6
        (.I(gpio_0_tri_o_6),
        .IO(CY_SWDCLK_SCLK),
        .O(gpio_0_tri_i_6),
        .T(gpio_0_tri_t_6));

    // .....

    assign AUXPL14 = CY_SWDCLK_SCLK;

#### SPI Debugging with motor jigs

SPI isn't working - gives timeout errors. I probed the board and can see all
the signals, including MISO and MOSI. Linux is sending the data but is not
receiving confirmation that the receive data came back.

The irq in spi-cadence.c is only set up for the Tx empty and mode fault
interrupts. I enabled the "rx not empty" and got tons of spurious interrupts,
so I know that at least the SPI IRQ is set up correctly.

I was placing random dev_dbg's in the code, and when I placed a couple in
`cdns_spi_fill_tx_fifo`, suddenly it started "working", as in, the motor began
twitching on boot. So I'm thinking a race condition due to the delays introduced
by the debug prints. Next I'll try some usleep's.

usleep_range(500, 600) in the first branch producecd the same results.
Also, the camera twiching could be related to bad CPOL/CHPA settings. Just a
guess, though.

##### 7/10/17

I noticed that the mode was set to 0 instead of 3. Changed the mode in the
middleware in motor_trinamic.py, but the choppiness continued. Also tried
decreasing the delay in the driver to 100us with the same result.

Questions/problems:

- Mode should be 3 instead of 1
- MotorSpi.read_register() ignores size argument
- Wrong array length passed to convert_8_bit_array_to_32_bit

Tried several patches from upstream, no dice.

Also tried enabling the tx fifo interrupt before filling it, nope.

##### 7/11/17

tilt-limit issue: Not a h/w problem. I stop in u-boot, it works fine on the
scope. Once the system boots, it only goes between 0-1.45 or 1.45-3.3v depending
on initial state.
Trying not configuring in Linux dts at all.
Tried adding AXI quad spi instead in HDL rather than using the hard SPI0. This
works just like the Linux driver with delays, but the motor is still jumpy.

##### 7/12/17

Talked to Mike about the motor calibration. The motor should always move toward
the sensor until it finds it. The `camera.json.static` file defines the range in
degrees as well as the home position. This needs to be updated. It's also
possible that the camera is trying to move in the wrong direction and will never
reach home.

Known things to fix:

- [ ] Look into not mounting valens chip when spi-xilinx module is loaded.
  Perhaps could use dtsi for this?
- [ ] Run SPI through SPI0 rather than AXI
- [ ] cpu1 app should not be setting the LED gpio's anymore (conflict with tilt)
- [ ] Middleware needs to use SPI mode 3?
- [ ] Camera is very jittery
- [ ] Calibration routine will need to be updated for Antares

Other random things I tried that didn't work:

- Use SPI1 instead of SPI0
- Use chip select 1

Steve suggested ensuring that the data in camera.json.static is correct for
Antares since it was based off Rigel. From what I can tell, it is ID 214 which
already is set to use motor setup "1", the same as Kronos. Ultimately, a new
config will need to be added specifically for Antares.

Use BUILD_SW.tcl for building cpu1-app:

    # On Linux, manually edit scripts/replace_lines.py and replace \\ with /
    # Clean the project
    find . -name .metadata | xargs rm -rf
    rm -rf Cygnus.sdk.BuildServer/
    # Imitate the batch script
    cp Cygnus.runs/impl_1/CYGNUS_TOP.sysdef CYGNUS_TOP.hdf
    # Run the build
    /opt/Xilinx/SDK/2015.4/bin/xsct ./BUILD_SW.tcl

##### 7/13/17

Was having problems with the Valens acting weird with spi-xilinx inserted.
(random ethernet drops, kernel panics related to networking, random reprograms
from the fw updater, etc).
Talked with Steve and we decided to keep trying to get to the bottom of the
issue with SPI0.
With the latest FPGA and cpu 1 app, I couldn't repeat the results I had before
of the CS line being nice and crisp. Instead it looks extremely slow, dropping
over 100us or so. I'm wondering if it's a drive strength issue or something.

Trying a special FPGA build where I can directly control that line via sysfs
(CY_XRES_N/AUXPL22 pin).

Turned out my jumper wire just had broken off.

However, tried the manual CS and it seems to get through the whole transaction
before issuing the timeout. Going to try tying CS high in the bistream and see
if it still times out. Nevermind - still varies transaction length even with
completely manual CS.

Now trying completely taking chip select control away from the kernel: No diff.

At this point I am doubting that the CS is the source of the problem.

##### 7/14/17

Found out that ftrace has a "spi" event class. Enabled it, and now there's an
oops when the transfer times out. If I don't enable the transfer complete event,
works OK.

I had a breakthrough: Replaced writel_relaxed and readl_relaced with their non-
relaxed counterparts. Things started working. Turns out I can replace them just
in fill_tx_fifo and things work as well.

It seems the recommended approach is to use rmb()/wmb() as necessary rather than
converting the relaxed operations to full writel/readl calls.

A couple of good articles on memory barriers in the kernel:

- [Semantics of MMIO mapping attributes across architectures](https://lwn.net/Articles/698014/)
- [Kernel IO Ordering](https://www.kernel.org/doc/Documentation/io_ordering.txt)
- [LDD: Communicating with Hardware](https://static.lwn.net/images/pdf/LDD3/ch09.pdf)

##### 7/21/17

UARTs on Antares and Cygnus:

| Device | Function | Name | Dev node |
| :------| :------- | :--- | :------- |
| PS UART 0 | Unused | N/A | N/A |
| PS UART 1 | Linux | ps7_uart_1 | ttyS0 |
| AXI UART 0 | Core 1 debug | DEBUG_UART | ttyS1 |
| AXI UART 1 | Camera | CAM_UART | ttyS2 |
