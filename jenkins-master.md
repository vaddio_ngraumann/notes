### Jenkins master setup

```
sudo apt-get install openjdk-6-jre-headless daemon apache2 ttf-dejavu-core libfontconfig1
```

Copied over /usr/share/jenkins/jenkins.war, /etc/init.d/jenkins, /etc/default/jenkins, /etc/apache2/*
```
sudo update-rc.d jenkins defaults 20

sudo useradd -u 4000 -U -d /var/lib/jenkins -m -s /bin/bash jenkins
sudo mkdir /var/log/jenkins
sudo chown jenkins:jenkins /var/log/jenkins
sudo mkdir /var/cache/jenkins
sudo chown jenkins:jenkins /var/cache/jenkins

sudo useradd -u 2000 -U -m -s /bin/bash support
sudo usermod -a -G adm,sudo support

sudo useradd -u 3000 -U -m -s /bin/bash builder
```

### NFS setup

copy over /etc/exports
```
sudo apt-get install nfs-kernel-server
```

To copy jobs from other Jenkins:
Create directory in jobs/<job name>
copy build.xml from old Jenkins jobs/<job name> directory to new jobs/<job name> directory.
If desired create file called nextBuildNumber containing the number of the next build. Can be 1 to reset the build numbering.

Set up scratch:
sudo mkdir /media/scratch
sudo rmdir /srv
sudo ln -s /media/scratch /srv
