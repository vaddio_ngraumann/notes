Assignments go output on left, input on right. For example:

assign Reg_Audio_USBRLxEML_Gain = main_reg68;

Means set the gain for USB record channel left output from EzMic left channel input. Use register offset 68.
Values can be converted to dB with 20*log10(val/32768). Often they are set to -6dB (0x4027). 0x8000 corresponds to 0dB.

Use excel spreadsheet to see which bits form connections. Inputs are usually on left, outputs on top, with the particular bit to connect shown in the cell.
