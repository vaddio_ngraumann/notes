### Debugging

Add the following to `OutputBase.enable` in `iobase.py` to dump pipelines on every start/stop of output pipelines:
```
Gst.debug_bin_to_dot_file_with_ts(self.pipeline,
    Gst.DebugGraphDetails.ALL,
    'pipeline_%s' % self.interface)
```

You can remove the `_with_ts` if you don't want to make differently-named pipelines at each timestamp.
