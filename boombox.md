### Audio crosspoints

Route USB playback to IP output for testing:
devmem 0x40600000 32 0x80000000
devmem 0x40600004 32 0
devmem 0x40600008 32 0x804
devmem 0x4060000c 32 0

Optionally, increase gains:
devmem 0x406000B0 32 0x00008000
devmem 0x406000D4 32 0x00008000
