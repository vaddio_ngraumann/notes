# USB Device Firmware Update (DFU)

USB DFU can be used to program a Zynq UltraScale+ device (such as Eagle) or a Sitara AM57xx device (such as Kestrel) regardless of the state of the eMMC; it can be blank, corrupted, or completely valid, since the boot method uses the on-chip ROM to download new firmware over the USB connection.

For a background or overview of USB DFU, see the following A3: (TODO)

### Host PC Setup

You will need [dfu-util](http://dfu-util.sourceforge.net/). For Windows, this tool can be downloaded from SourceForge. At the time of writing, the latest release is version 0.9. Linux users can install it via your package manager, for example on Ubuntu:

```
apt-get update
apt-get install dfu-util
```

The tool is a command-line based tool, so you will want to open a Command Prompt (Windows) or terminal (Linux). Ensure you can run the tool and see the help output:

```
# dfu-util -h
Usage: dfu-util [options] ...
  -h --help			Print this help message
  -V --version			Print the version number
  -v --verbose			Print verbose debug statements
  -l --list			List currently attached DFU capable devices
  -e --detach			Detach currently attached DFU capable devices
  -E --detach-delay seconds	Time to wait before reopening a device after detach
  -d --device <vendor>:<product>[,<vendor_dfu>:<product_dfu>]
				Specify Vendor/Product ID(s) of DFU device
  -p --path <bus-port. ... .port>	Specify path to DFU device
  -c --cfg <config_nr>		Specify the Configuration of DFU device
  -i --intf <intf_nr>		Specify the DFU Interface number
  -S --serial <serial_string>[,<serial_string_dfu>]
				Specify Serial String of DFU device
  -a --alt <alt>		Specify the Altsetting of the DFU Interface
				by name or by number
  -t --transfer-size <size>	Specify the number of bytes per USB Transfer
  -U --upload <file>		Read firmware from device into <file>
  -Z --upload-size <bytes>	Specify the expected upload size in bytes
  -D --download <file>		Write firmware from <file> into device
  -R --reset			Issue USB Reset signalling once we're finished
  -s --dfuse-address <address>	ST DfuSe mode, specify target address for
				raw file download or upload. Not applicable for
				DfuSe file (.dfu) downloads
  -w --wait			Wait for device to appea
```

Windows users will run `dfu-util.exe`, but otherwise the parameters are identical. Note that on Linux you will need to run `dfu-util` as root using `sudo` or set up udev rules to change permissions. udev rules are listed at: TODO

You can run `dfu-util -l` at any time to be presented with a list of all DFU devices currently attached to your PC:

```
dfu-util -l
dfu-util 0.9

Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
Copyright 2010-2019 Tormod Volden and Stefan Schmidt
This program is Free Software and has ABSOLUTELY NO WARRANTY
Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

Found DFU: [03fd:0050] ver=0100, devnum=87, cfg=1, intf=0, path="1-2.3.1", alt=0, name="Xilinx DFU Downloader", serial="2A49876D9CC1AA4"
```

### Acquire the artifacts

If using a Jenkins build, you will need the following build artifacts downloaded to your PC:
- dfu-<machine>.zip (extracted)
- Image file, for example `vaddio-app-debug-image-zynqmp-eagle-eptz.img.gz`, if desired.

### Zynq UltraScale+ DFU Overview

The DFU process happens as follows:

1. Set the boot switch(es) for USB DFU boot mode. On Eagle this is S2.
2. Connect a USB cable between USB0 and the host PC. On Eagle this will be a USB C<->USB A cable (2.0 or 3.0, both will work).
3. Apply power to the board. The device will enumerate as a USB DFU device with name "Xilinx DFU Downloader"
4. Use dfu-util to download the first-stage bootloader for DFU, usually called BOOT-DFU.BIN.
5. The device will re-enumerate with name "Xilinx FSBL USB DFU".
6. Use dfu-util to download the full first-stage bootloader which contains u-boot. This file is usually called BOOT.BIN.
7. u-boot will start up and detect that USB boot mode is enabled. It will enter DFU boot mode and request a boot script which contains u-boot commands for what to do next. Once this happens, the board will enumerate as yet another DFU device with name "boot.scr".
8. Use dfu-util to download the boot script (this will be boot-dfu-usb.scr)
9. The device will re-enumerate a final time. Assuming the script used in step 8 was the MMC gz image script, the DFU device will re-enumerate with name "mmc.img.gz".
10. Use dfu-util to download the gzip'd mmc image which will then be extracted and written by u-boot.
11. Set the boot switch(es) for eMMC boot mode.
12. Cycle power. Note that you must physically remove power and re-apply it, as the Zynq UltraScale+ only samples the boot mode pins on a full power-on-reset (POR), not a soft reboot.

### DFU Download Steps

Run the following commands on the host PC. If you are using a modern `dfu-util`, it will support the `-w` option to allow it to wait for the device to be connected and you can freely add this option if desired to make your life a little easier. If your `dfu-util` does not support this option, you will need to ensure the DFU device has enumerated before running each download step (or simply retry a few times if a step fails due to the device not being found).

##### Download BOOT-DFU.BIN
```
dfu-util -a "Xilinx DFU Downloader" -D BOOT-DFU.BIN -R
```

Console output expected from the device:
```
Xilinx Zynq MP First Stage Boot Loader
Release 2019.2   Jan  9 2020  -  09:03:16
```

##### Download BOOT.BIN
```
dfu-util -a "Xilinx FSBL USB DFU" -D BOOT.BIN -R
```

Console output expected from the device:
```
NOTICE:  ATF running on XCZU7EG/EV/silicon v4/RTL5.1 at 0xfffea000
NOTICE:  BL31: Secure code at 0x0
NOTICE:  BL31: Non secure code at 0x8000000
NOTICE:  BL31: v2.0(release):xilinx-v2019.2
NOTICE:  BL31: Built : 16:12:45, Jan  7 2020
PMUFW:  v1.1

U-Boot 2019.01 (Jan 14 2020 - 21:24:05 +0000)
(...)
Hit any key to stop autoboot:  0
Downloading boot script via DFU
```

##### Download the eMMC programming boot script
```
dfu-util --a "boot.scr" -D boot-dfu-usb.scr -R
```

Console output expected from the device:
```
#DOWNLOAD ... OK
Ctrl+C to exit ...
## Executing script at 20000000
DFU download of MMC image
```

##### Download the eMMC image
```
dfu-util --a "mmc.img.gz" -D vaddio-app-debug-image-zcu104-zynqmp-ark.wic.gz -R
```

Console output expected from the device:
```
########DOWNLOAD ... OK
Ctrl+C to exit ...
Decompressing image to MMC...

461393920/858993459
        uncompressed 461393920 of 858993459
        crcs == 0x33333333/0x0d61f25a
Done. Disengage USB boot mode switch and power cycle device.
```

### References

https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842468/ZynqMp+USB+Stadalone+Driver#ZynqMpUSBStadaloneDriver-USBDFUTesting
https://training.ti.com/sites/default/files/docs/AM57x-DFU-boot-SLIDES.pdf
https://processors.wiki.ti.com/index.php/AMSDK_u-boot_User's_Guide#Updating_an_SD_card_or_eMMC_using_DFU
