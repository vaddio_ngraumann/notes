Do not pull the power while the board is on - it will damage components!
Hold the power switch for 15s which will power off the board, then you can remove power safely.

[TI Getting started link](http://www.ti.com/startyourlinux)

## Booting

I was able to boot the vaddio-minimal-image by creating an SD card with a FAT32 partition and copying the contents from the demo image. Then I created a second rootfs partition and extracted the minimal image there.

## USB

The USB 3.0 ports are all host-only. There is one USB2.0 port that is set up for device mode. To use it, I had to modprobe a couple of modules:
    modprobe -a extcon-palmas dwc3-omap dwc3

Configfs:
    modprobe -a usb-f-uvc usb-f-fs

gst-launch-1.0 -v videotestsrc ! tee ! video/x-raw,format=VYUY,width=640,height=360 ! videoscale ! videoconvert ! v4l2sink device=/dev/video0

usb_f_fs 36864 0 - Live 0xbf06c000
usb_f_uvc 53248 6 - Live 0xbf057000
videobuf2_vmalloc 16384 1 usb_f_uvc, Live 0xbf050000
libcomposite 49152 15 usb_f_fs,usb_f_uvc, Live 0xbf03c000
dwc3 69632 0 - Live 0xbf024000
udc_core 28672 4 usb_f_fs,usb_f_uvc,libcomposite,dwc3, Live 0xbf017000
usb_common 16384 3 libcomposite,dwc3,udc_core, Live 0xbf010000
dwc3_omap 16384 0 - Live 0xbf008000
extcon_palmas 16384 0 - Live 0xbf000000

```
Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(st
ring)VYUY, width=(int)640, height=(int)360, framerate=(fraction)30/1, multiview-mode=(string)mo
no, interlace-mode=(string)progressive, pixel-aspect-ratio=(fracti[   37.414242] Unable to hand
le kernel NULL pointer dereference at virtual address 00000010
[   37.423997] pgd = edfc2300
[   37.426714] [00000010] *pgd=ad8a7003, *pmd=fe0ea003
[   37.431625] Internal error: Oops: a07 [#1] PREEMPT SMP ARM
[   37.437133] Modules linked in: g_webcam usb_f_uvc videobuf2_vmalloc libcomposite dwc3 udc_co
re usb_common dwc3_omap extcon_palmas
[   37.448879] CPU: 0 PID: 487 Comm: videotestsrc0:s Not tainted 4.14.19-g7ca0735de3 #4
[   37.456656] Hardware name: Generic DRA74X (Flattened Device Tree)
[   37.462775] task: edf53100 task.stack: ed826000
[   37.467346] PC is at uvc_video_encode_isoc+0x24/0xe4 [usb_f_uvc]
[   37.473390] LR is at uvcg_video_pump+0x84/0x154 [usb_f_uvc]
[   37.478986] pc : [<bf058c30>]    lr : [<bf058f50>]    psr: a00f0093
[   37.485279] sp : ed827cf0  ip : ed827d18  fp : ed827d14
[   37.490527] r10: 01080020  r9 : 600f0013  r8 : edf69680
[   37.495773] r7 : edf4c800  r6 : 00000000  r5 : edf033f8  r4 : edf033f8
[   37.502326] r3 : 00000002  r2 : edf4c800  r1 : edf033f8  r0 : 00000010
[   37.508881] Flags: NzCv  IRQs off  FIQs on  Mode SVC_32  ISA ARM  Segment user
[   37.516134] Control: 30c5387d  Table: adfc2300  DAC: fffffffd
[   37.521904] Process videotestsrc0:s (pid: 487, stack limit = 0xed826210)
[   37.528633] Stack: (0xed827cf0 to 0xed828000)
[   37.533008] 7ce0:                                     edf033f8 edf696a4 edf0371c edf69680
[   37.541222] 7d00: edf03450 600f0013 ed827d4c ed827d18 bf058f50 bf058c18 edf03460 edf03448
[   37.549436] 7d20: bf026a50 edf033f8 edf03438 00000000 014000c0 bf058acc edf03438 edf03448
[   37.557649] 7d40: ed827d7c ed827d50 bf05918c bf058ed8 00000002 edf03000 00000002 edf03000
[   37.565862] 7d60: c0702dd4 c103bf64 00000000 c103c12c ed827d94 ed827d80 bf0586c4 bf05902c
[   37.574077] 7d80: bf058694 40045612 ed827dac ed827d98 c0702df8 bf0586a0 00000001 40045612
[   37.582289] 7da0: ed827e14 ed827db0 c0707324 c0702de0 00e00000 ffefe7f8 ed827dec bf05d064
[   37.590501] 7dc0: edfd0800 00000001 edfa5480 ed827e28 ffefe7f8 b52ff000 00000001 fe17efdf
[   37.598714] 7de0: ed827e14 c044560f 00000003 40045612 00000001 00000004 ed827e28 000fcd04
[   37.606927] 7e00: 00000000 c070703c ed827ed4 ed827e18 c0706b68 c0707048 ed827e6c 00000000
[   37.615141] 7e20: 00000004 edfa5480 00000002 edf26e00 ed827e84 00002003 00000001 00000000
[   37.623353] 7e40: 00000000 ed827e7c 00000000 ffffffff 00000001 00000001 b5b161d8 00000000
[   37.631566] 7e60: ed827eb4 ed827e70 c02a891c c02a86cc 00000000 ffefe7f8 edf26e38 00000001
[   37.639779] 7e80: ed827e7c b5b16000 edf26e00 000001d8 00000001 00000000 00000081 b5b161d8
[   37.647991] 7ea0: 00000001 b5b161d8 ed827f5c edf03000 edfa5480 40045612 000fcd04 edf03410
[   37.656204] 7ec0: ed826000 00000000 ed827ee4 ed827ed8 c0707038 c0706b10 ed827f0c ed827ee8
[   37.664417] 7ee0: c07016c4 c070702c 000fcd04 ed814850 edfa5480 00000008 000fcd04 ed826000
[   37.672629] 7f00: ed827f7c ed827f10 c034e1a4 c070164c c0359c24 c0289c34 c0282d00 c0ba66d0
[   37.680843] 7f20: ed827f4c b5b16160 00004000 00000008 40045612 000fcd04 ed826000 00000000
[   37.689055] 7f40: ed827f6c ed827f50 c0359d3c c0359b50 b5b16160 edfa5481 edfa5480 00000008
[   37.697269] 7f60: 40045612 000fcd04 ed826000 00000000 ed827fa4 ed827f80 c034e8d4 c034e108
[   37.705482] 7f80: b5b16160 00000000 b685c000 00000036 c0208104 ed826000 00000000 ed827fa8
[   37.713695] 7fa0: c0207f20 c034e8a4 b5b16160 00000000 00000008 40045612 000fcd04 00000000
[   37.721907] 7fc0: b5b16160 00000000 b685c000 00000036 b5b16160 b5b1a8c0 b6967000 b6f3e61c
[   37.730120] 7fe0: b685c21c b64fd4fc b682ddac b6c2384c 600f0010 00000008 00000000 00000000
[   37.738328] Backtrace:
[   37.740813] [<bf058c0c>] (uvc_video_encode_isoc [usb_f_uvc]) from [<bf058f50>] (uvcg_video_p
ump+0x84/0x154 [usb_f_uvc])
[   37.751645]  r9:600f0013 r8:edf03450 r7:edf69680 r6:edf0371c r5:edf696a4 r4:edf033f8
[   37.759441] [<bf058ecc>] (uvcg_video_pump [usb_f_uvc]) from [<bf05918c>] (uvcg_video_enable+
0x16c/0x1a8 [usb_f_uvc])
[   37.770009]  r10:edf03448 r9:edf03438 r8:bf058acc r7:014000c0 r6:00000000 r5:edf03438
[   37.777870]  r4:edf033f8
[   37.780434] [<bf059020>] (uvcg_video_enable [usb_f_uvc]) from [<bf0586c4>] (uvc_v4l2_streamo
n+0x30/0x58 [usb_f_uvc])
[   37.791003]  r10:c103c12c r9:00000000 r8:c103bf64 r7:c0702dd4 r6:edf03000 r5:00000002
[   37.798865]  r4:edf03000 r3:00000002
[   37.802471] [<bf058694>] (uvc_v4l2_streamon [usb_f_uvc]) from [<c0702df8>] (v4l_streamon+0x2
4/0x28)
[   37.811556]  r5:40045612 r4:bf058694
[   37.815152] [<c0702dd4>] (v4l_streamon) from [<c0707324>] (__video_do_ioctl+0x2e8/0x2f0)
[   37.823275]  r5:40045612 r4:00000001
[   37.826869] [<c070703c>] (__video_do_ioctl) from [<c0706b68>] (video_usercopy+0x64/0x51c)
[   37.835083]  r10:c070703c r9:00000000 r8:000fcd04 r7:ed827e28 r6:00000004 r5:00000001
[   37.842946]  r4:40045612
[   37.845492] [<c0706b04>] (video_usercopy) from [<c0707038>] (video_ioctl2+0x18/0x1c)
[   37.853271]  r10:00000000 r9:ed826000 r8:edf03410 r7:000fcd04 r6:40045612 r5:edfa5480
[   37.861132]  r4:edf03000
[   37.863678] [<c0707020>] (video_ioctl2) from [<c07016c4>] (v4l2_ioctl+0x84/0xe0)
[   37.871111] [<c0701640>] (v4l2_ioctl) from [<c034e1a4>] (do_vfs_ioctl+0xa8/0x79c)
[   37.878628]  r9:ed826000 r8:000fcd04 r7:00000008 r6:edfa5480 r5:ed814850 r4:000fcd04
[   37.886409] [<c034e0fc>] (do_vfs_ioctl) from [<c034e8d4>] (SyS_ioctl+0x3c/0x60)
[   37.893752]  r10:00000000 r9:ed826000 r8:000fcd04 r7:40045612 r6:00000008 r5:edfa5480
[   37.901613]  r4:edfa5481
[   37.904163] [<c034e898>] (SyS_ioctl) from [<c0207f20>] (ret_fast_syscall+0x0/0x4c)
[   37.911766]  r9:ed826000 r8:c0208104 r7:00000036 r6:b685c000 r5:00000000 r4:b5b16160
[   37.919544] Code: e591602c e3a03002 e1a07002 e1a05001 (e5c03000)
[   37.925671] ---[ end trace 2a6da758e891f74f ]---
[   37.930307] Kernel panic - not syncing: Fatal exception
[   37.935558] CPU1: stopping
[   37.938282] CPU: 1 PID: 48 Comm: kworker/1:1 Tainted: G      D         4.14.19-g7ca0735de3 #
4
[   37.946843] Hardware name: Generic DRA74X (Flattened Device Tree)
[   37.952967] Workqueue: events bpf_prog_free_deferred
[   37.957954] Backtrace:
[   37.960419] [<c020b848>] (dump_backtrace) from [<c020bb2c>] (show_stack+0x18/0x1c)
[   37.968023]  r7:fa212000 r6:20070193 r5:00000000 r4:c1054d0c
[   37.973713] [<c020bb14>] (show_stack) from [<c091e61c>] (dump_stack+0x90/0xa4)
[   37.980971] [<c091e58c>] (dump_stack) from [<c020edfc>] (handle_IPI+0x1b8/0x1cc)
[   37.988401]  r7:fa212000 r6:00000001 r5:00000000 r4:c0e64adc
[   37.994087] [<c020ec44>] (handle_IPI) from [<c02014a4>] (gic_handle_irq+0x7c/0x80)
[   38.001689]  r6:ee245d68 r5:fa21200c r4:c1005040
[   38.006329] [<c0201428>] (gic_handle_irq) from [<c020c6f8>] (__irq_svc+0x58/0x8c)
[   38.013842] Exception stack(0xee245d68 to 0xee245db0)
[   38.018915] 5d60:                   00000000 00000002 c1005042 00000003 ee245dc0 00000000
[   38.027129] 5d80: c020f148 00000000 c1004cb8 00000001 c1004de4 ee245dfc 00000001 ee245db8
[   38.035341] 5da0: c091e2c0 c02ab630 20070013 ffffffff
[   38.040415]  r9:ee244000 r8:c1004cb8 r7:ee245d9c r6:ffffffff r5:20070013 r4:c02ab630
[   38.048195] [<c02ab4f4>] (smp_call_function_single) from [<c02abbd0>] (smp_call_function_man
y+0x2f8/0x31c)
[   38.057890]  r6:c1004cb8 r5:00000000 r4:00000001
[   38.062528] [<c02ab8d8>] (smp_call_function_many) from [<c02abc3c>] (smp_call_function+0x48/
0x7c)
[   38.071440]  r10:c1002d00 r9:00000000 r8:00000000 r7:04000000 r6:00000000 r5:f1e34000
[   38.079302]  r4:ffffe000
[   38.081849] [<c02abbf4>] (smp_call_function) from [<c020f254>] (broadcast_tlb_a15_erratum+0x
40/0x44)
[   38.091020]  r5:f1e34000 r4:f1e35000
[   38.094614] [<c020f214>] (broadcast_tlb_a15_erratum) from [<c020f694>] (flush_tlb_kernel_ran
ge+0x48/0x80)
[   38.104226] [<c020f64c>] (flush_tlb_kernel_range) from [<c0217aa8>] (change_memory_common+0x
c8/0x130)
[   38.113487] [<c02179e0>] (change_memory_common) from [<c0217b68>] (set_memory_rw+0x28/0x30)
[   38.121876]  r7:eed49c00 r6:eed46b00 r5:04000000 r4:00000000
[   38.127562] [<c0217b40>] (set_memory_rw) from [<c02c6834>] (bpf_prog_free_deferred+0x34/0x7c
)
[   38.136122]  r5:ee9df080 r4:f1e34000
[   38.139718] [<c02c6800>] (bpf_prog_free_deferred) from [<c02439d8>] (process_one_work+0x1dc/
0x418)
[   38.148714]  r5:ee9df080 r4:edfb91c4
[   38.152309] [<c02437fc>] (process_one_work) from [<c0244790>] (worker_thread+0x60/0x5bc)
[   38.160435]  r10:c1002d00 r9:00000008 r8:ffffe000 r7:eed46b18 r6:ee9df098 r5:eed46b00
[   38.168296]  r4:ee9df080
[   38.170845] [<c0244730>] (worker_thread) from [<c0249ac8>] (kthread+0x164/0x16c)
[   38.178274]  r10:ee8b3e90 r9:c0244730 r8:ee9df080 r7:ee244000 r6:00000000 r5:ee9de640
[   38.186136]  r4:ee9de600
[   38.188683] [<c0249964>] (kthread) from [<c0207ff8>] (ret_from_fork+0x14/0x3c)
[   38.195936]  r10:00000000 r9:00000000 r8:00000000 r7:00000000 r6:00000000 r5:c0249964
[   38.203797]  r4:ee9de640
[   38.206349] ---[ end Kernel panic - not syncing: Fatal exception
on)1/1
```

### am572x-idk investigation

USB won't enumerate as a peripheral without vbus detect. I used extcon-usb-gpio to fake a vbus detect and tied it to GPIO2_19, which is on the external header. It also happens to be weakly pulled-up automatically.

modprobe -a dwc3 dwc3-omap extcon-usb-gpio
modprobe g-webcam streaming_maxburst=15
modprobe vivid
uvc-gadget -u /dev/video0 -v /dev/video1 -t 15 -r 3 -n 2 -b &
v4l2-ctl -d /dev/video1 --set-ctrl=horizontal_movement=6

Looks pretty good at 1080p30 on ti2018.00, however 40 is max fps or so and 60 is very choppy. 720p60 is also choppy. This could be a lpm issue like Xilinx recently fixed for us, but unfortunatley their patches don't make a difference, and the latest TI kernel (25eaa57865f5c67e7a55d047e6b7ae9703b55bc8) didn't help either.

### Beaglebone AI

Needed to get updated dts from `/opt/source` directory on the Debian image - version in beaglebone github is outdated!

```
modprobe -a dwc3 dwc3_omap extcon_usb_gpio phy_omap_usb2 usb_f_uvc
modprobe vivid allocators=1,1
```

Board appears to have HW issue where orientation of the USB-C cable dictates whether it enumerates as High-speed or Super-Speed.

##### 10/15/19

DMA SG patches don't work out of the box - creates L3 fault:
```
[   30.627254] omap_l3_noc 44000000.ocp: L3 application error: target 5 mod:1 (u
nclearable)                                                                     
[   30.635406] omap_l3_noc 44000000.ocp: L3 debug error: target 5 mod:1 (unclear
able)
```

I even tried only setting the sg up with the request header and no data, same problem. I am currently thinking there is some issue with the USB DMA that is not allowed to access this portion of system memory. Maybe it needs to bypass the MMU somehow?

##### 10/16/19

Turns out we were missing a couple of changes to the dwc3 driver in the TI kernel from Xilinx which added support for actually using the SG lists:
```
c96e6725db9d6a04ac1bee881e3034b636d9f71
usb: dwc3: gadget: Correct the logic for queuing sgs

a31e63b608ff78c77d8e033347239431d522fe5d
usb: dwc3: gadget: Correct handling of scattergather lists
```

Now streaming works great.

### Vivid framerate

Be sure to set CONFIG_HZ_250 to get 60fps with vivid - the kthread loop can't run accurately enough at HZ_100.

### CMEM vs CMA

See this: https://e2e.ti.com/support/processors/f/791/t/636524
Essentially, CMEM is mainly used these days for TI's OpenCL stuff. CMA can be marked as "Reusable" to allow the regions to be shared when the remoteprocs aren't running. Minimum size for remoteprocs is 8MB. "If OpenCL is not used, the CMEM node in dts can be removed."

### USB Boot

Need a tool called usbboot for first stage (or maybe we can write directly to the USB endpoint?). TI uses a non-standard, vendor-specific USB implementation for the first stage, but you can look for VID:PID 0451:d013. It times out after 3s and proceeds to the next device.

https://training.ti.com/sites/default/files/docs/AM57x-DFU-boot-SLIDES.pdf

### AM5718 dev board (Kestrel)

For gadget testing:
```
modprobe -a phy-omap-usb2 dwc3 dwc3-omap extcon-palmas
```
