% EPTZ Meeting
% Vaddio
% September 15, 2020

# Driver overview

- V4l2 [Selection API](https://www.kernel.org/doc/html/v4.19/media/uapi/v4l/selection-api.html)
- Cropper is a V4L2 sub-device within the capture pipeline
- `media-ctl` and `v4l2-ctl` can be used for prototyping

# Anatomy of media-ctl output

![media-ctl example](images/media-ctl-selection.png)

1. Input video size
2. Crop rectangle location and size
3. Output video size

# Simple Examples

1. Set full input (3840x2160) scaled to 1080p output:

```
media-ctl -v -V '"a0010000.crop":0 [crop:(0,0)/3840x2160]' -d /dev/media3
```

2. Show upper-left 1080p quadrant:

```
media-ctl -v -V '"a0010000.crop":0 [crop:(0,0)/1920x1080]' -d /dev/media3
```

3. Show upper-right 1080p quadrant:

```
media-ctl -v -V '"a0010000.crop":0 [crop:(1920,0)/1920x1080]' -d /dev/media3
```

4. Show middle 960x540 area (4x zoom):

```
media-ctl -v -V '"a0010000.crop":0 [crop:(1440,810)/960x540]' -d /dev/media3
```

# Crop/zoom motion

- V4L2 controls expose motion paramters
- Parameters are "latched" by driver and applied on next SET_SELECTION
- Aspect ratio should match aspect ratio of video

# User Controls

```
root@vaddio-eagle-04-91-62-C8-A4-81:~# v4l2-ctl -d /dev/v4l-subdev9 -l

           auto_pan_zoom_enable 0x009819d0 (bool)   : default=0 value=0
       auto_pan_horizontal_rate 0x009819d1 (int)    : min=0 max=4094 step=1 default=16 value=16
         auto_pan_vertical_rate 0x009819d2 (int)    : min=0 max=4094 step=1 default=9 value=9
      auto_zoom_horizontal_rate 0x009819d3 (int)    : min=0 max=4094 step=1 default=16 value=16
        auto_zoom_vertical_rate 0x009819d4 (int)    : min=0 max=4094 step=1 default=9 value=9
             auto_pan_zoom_done 0x009819d5 (bool)   : default=0 value=1 flags=read-only
```

# Auto Pan/Zoom Examples

- Enable auto pan/zoom:

```
v4l2-ctl -d /dev/v4l-subdev9 -c auto_pan_zoom_enable=1
```

- Change rates:

```
v4l2-ctl -d /dev/v4l-subdev9 -c auto_pan_horizontal_rate=64
v4l2-ctl -d /dev/v4l-subdev9 -c auto_pan_vertical_rate=36
```

# Final notes

- Pan/zoom rates need to be calculated so they finish at the same time
- Driver may be able to calculate this, but does not currently.
- What's missing?
