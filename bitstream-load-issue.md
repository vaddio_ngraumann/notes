### 7/31/18

HP Slave AXI registers appear to have wrong width:
Bad:
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008014              
0x00000F00                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8009014              
0x00000F00                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf800a014              
0x00000F00  


Good:
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008014              
0x00000F01                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8009014              
0x00000F01                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf800a014              
0x00000F01   

If I manually write them to f01 before loading the bitstream, it still looks
bad, however now IP streaming doesn't crash.


Bad (fixed 8014)
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008000              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008004              
0x00000007                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008008              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf800800c              
0x0000002E                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008010              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008014              
0x00000F01                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008018              
0x00000007                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf800801c              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008020              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008024              
0x00000000                             

Good
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008000              
0x00000001                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008004              
0x00000007                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008008              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf800800c              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008010              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008014              
0x00000F01                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008018              
0x00000007                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf800801c              
0x00000000                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008020              
0x00000003                                                                     
root@vaddio-av-bridge-mini-54-10-EC-ED-96-0E:~# devmem 0xf8008024              
0x00000000                                 

u-boot write register to fix:
mw 0xf8008000 0x00000001 1
mw 0xf8009000 0x00000001 1                                               
mw 0xf800a000 0x00000001 1
mw 0xf8008014 0x00000F01 1
mw 0xf8009014 0x00000F01 1
mw 0xf800a014 0x00000F01 1

Then load bitstream in u-boot. This appears to work.

### Bus widths
Hobbit, Boombox, Centaur, Vulcan, Rigel, Antares/Cygnus, Corvus
    32 bit on 0, 1, and 2. 3 is disabled.

Have yet to find one that's different.
