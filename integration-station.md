### USB 2.0 DMA streaming

Found some MUSB patches to apply on [this wiki](http://processors.wiki.ti.com/index.php/Sitara_Linux_MUSB_Issues)

So far, applied 6.1 and 6.2 and still saw choppiness. 6.3 had issues, so skipped for now.
Applied 6.5, still choppy.
6.6 seems not to apply?

##### 9/19/17

Streaming works with two frames per microframe (1<<11), 1024 packet size, with
2048 byte fifos. However is still a bit choppy. Seeing cppi41_channel_abort()
called with DMA busy (both tx and rx). Does not work with three frames per
microframe (2<<11). USB analyzer indicates corrupted packets received.

##### 9/20/17

Got the usb tests working by following these steps:

1. Enable the USB Gadget driver (g_zero) as a module on the target.
2. Load the usbtest module on the host with sudo modprobe usbtest. You may also want to specify alt=1 if you want the isochronous endpoints enabled.
3. Load the usb gadget driver on the target with modprobe g_zero.
4. Connect the target to the host. You should see messages in the syslog about the Linux USB Gadget device appearing.
5. Compile testusb for the host (it's in kernel src/tools/usb).
6. Run a test, such as sudo ./testusb -a -t 6

With isochronous tests, I notice sometimes that running through the USB analyzer caused weird errors, such as the usbtest module not being able to find the endpoints when the isochronous mult was set to 2 (meaning 3 per microframe).

##### 9/22/17

Trying all sorts of permutations of settings today now that I figured out that
I also needed to set dwMaxPayloadTransferSize in Gadget.cpp in gstreamer.

I have not had luck with double-buffering on any packet size, even though the datasheet seems to recommend it for iso transfers.

| Packet Size | DMA | Result |
| :------------- | :------------- | :-------- |
|  512 x1 | N | No work - 0fps |
| 1024 x1 | N | Smooth |
| 1024 x2 | N | Only very top of frame comes in - thinking speed issue |
| 1024 x2 | Y | Choppy but all data. ~30ms delays between actual data xfers. |
| 1024 x3 | N | No work - immediate disconnect |
| 1024 x3 | Y | No work - immediate disconnect |

MUSB_PIO_ONLY=y:
Sep 22 20:26:31 vaddio-avbmp-00-17-EB-82-08-18 [   33.820000] edma3 edma3.0: dma transaction took 86950ns
Sep 22 20:26:31 vaddio-avbmp-00-17-EB-82-08-18 [   33.840000] edma3 edma3.0: dma transaction took 89700ns
Sep 22 20:26:31 vaddio-avbmp-00-17-EB-82-08-18 [   33.870000] edma3 edma3.0: dma transaction took 87850ns
Sep 22 20:26:31 vaddio-avbmp-00-17-EB-82-08-18 [   33.910000] edma3 edma3.0: dma transaction took 90550ns
Sep 22 20:26:31 vaddio-avbmp-00-17-EB-82-08-18 [   33.950000] edma3 edma3.0: dma transaction took 91250ns
Sep 22 20:26:31 vaddio-avbmp-00-17-EB-82-08-18 [   34.020000] edma3 edma3.0: dma transaction took 94750ns

MUSB_PIO_ONLY=n
Sep 22 20:32:34 vaddio-avbmp-00-17-EB-82-08-18 [   36.900000] edma3 edma3.0: dma transaction took 89400ns
Sep 22 20:32:34 vaddio-avbmp-00-17-EB-82-08-18 [   36.930000] edma3 edma3.0: dma transaction took 87750ns
Sep 22 20:32:34 vaddio-avbmp-00-17-EB-82-08-18 [   36.960000] edma3 edma3.0: dma transaction took 85750ns
Sep 22 20:32:34 vaddio-avbmp-00-17-EB-82-08-18 [   37.000000] edma3 edma3.0: dma transaction took 90600ns
Sep 22 20:32:35 vaddio-avbmp-00-17-EB-82-08-18 [   37.040000] edma3 edma3.0: dma transaction took 111450ns

Look into: Set FPS function doesn't seem to work at all.

Functions to trace:

```
edma_start
edma_stop
uvc_\*
\*video\*
\*v4l\*
```

##### 09/25/17

We seem to be allocating more endpoints than we need to in the FIFOs. For example, endpoint 2 (video streaming) only needs a TX fifo.

TXMULT seems to be set incorrectly in musb_gadget.c:948. Appears it should be:
    musb_writew(regs, MUSB_TXMAXP, musb_ep->packet_sz * (musb_ep->hb_mult + 1));

This commit seems suspicious: bb3a2ef2eb8cfaea335dcb3426350df7f3d48069
(usb: musb: set TXMAXP and AUTOSET for full speed bulk in device mode
)
Tried, no luck.

Backports to look at:

commit 1c1a16a300f20cad61127a8ced4289c1bfaa3999
Author: Felipe Balbi <balbi@ti.com>
Date:   Mon Dec 30 12:33:53 2013 -0600

    usb: musb: core: fix TX/RX endpoint order

commit 4858f06e7d92ed2ebdb29ccbc079c127e675a89c
Author: Yauheni Kaliuta <yauheni.kaliuta@nokia.com>
Date:   Wed Jun 8 17:12:02 2011 +0300

    usb: musb: gadget: clear TXPKTRDY flag when set FLUSHFIFO

##### 09/26/17

For some reason, 2048 byte transfers with DMA is pretty smooth with double-buffering on, MUSB_DEBUG enabled, and the debug level set to 5 or higher. So it could be a timing issue or race condition. Needs more investigation.

I really wanted more precision on the timestamps in ftrace and ended up backporting some sched_clock changes so that the omap platform driver actually registers a real sched_clock source. Otherwise, the timestamps were just being calculcated based on the jiffy value which is only accurate to 10Hz.

Via the high-res timestamps, I found that controller is generating interrupts extremely frequently without the debug prints (every ~120-130us) which is pretty close to the microframe interval. I wonder if we're seeing an issue such as the "pre-mature TX complete interrupt" which was addressed in commits a655f481d83d6d37bec0a2ddfdd24c30ff8f541f and 50aea6fca771d6daf3ec24f771da866f7fd836e4.

##### 9/27/17

```
if (!use_dma) {
	if (request->length > fifo_count) {
		DBG(5, "%s TX short-cut writing full request->length %d\n", musb_ep->end_point.name, request->length);

		musb->ops->write_fifo(musb_ep->hw_ep, request->length,
				(u8 *) (request->buf + request->actual));
		request->actual = request->length;
	} else {
		musb->ops->write_fifo(musb_ep->hw_ep, fifo_count,
				(u8 *) (request->buf + request->actual));
		request->actual += fifo_count;
	}
	csr |= MUSB_TXCSR_TXPKTRDY;
	csr &= ~MUSB_TXCSR_P_UNDERRUN;
	musb_writew(epio, MUSB_TXCSR, csr);
}
```

2048 with double buffer: works
4096 with double buffer: no
4096 no double buffer: no

Turns out uvc_video.c is using the maxpacket to directly compute how large of chunks it needs to send. I modified f_uvc to set maxpacket to what it actually should be, sort of like the newer kernels do by calling config_ep_by_speed().


##### 9/29/17

[TI EZ-SDK 512M memory layout](http://processors.wiki.ti.com/index.php/EZSDK_Memory_Map#Changing_Memory_Map_For_512MB_DM814x_Board)

Notes from Darren and Steve:

- Don't need 15 fps in descriptors. Can remove.
- Prefer higher image quality for 720p (looks too soft).
- 1080p is good, but more for a check-box/marketing.
- Uncompressed doesn't need to go any higher in resolution if it affects IP streaming (soft clients usually prefer compressed when they connect).

Dual 1080p (IP/USB) doesn't work - looks like a buffer issue:

```
** (vng-gst-server:6097): CRITICAL **: g_omx_port_allocate_buffers: assertion `port->buffers[i]' failed
```

Need more video memory accoridng to steve. Currently we use 88MB (0x5800000MB) @ 0xAB000000.
I bumped it up to the max, 188M, and things did work. However, we allocate part of that memory to Linux (currently the other 100M). When I go from 720p to 1080p USB streaming, sys_top shows video memory usage going from 65.5MB to 83.1MB, or about a 17.6MB increase. I first tried going from 88MB to 106MB, with no luck. Then I tried 110MB which worked, and finally 108MB which also worked.

##### 10/2/17

Got the FPGA drop from Glenn. Had an issue with "invalid fcc" error showing up in syslog - seemed to be bug where we were disabling the stream by setting "0"s to the values. Not sure why we didn't see that before. Also had to bump video memory to 150MB.

Current bandwidth usage (quality = 90):
1080p: 10.8-11.2 Mbps
720p: 4.9-5.5 Mbps

Quality = 100:

1080p: 24-ish Mbps (?) - choppy
720p: 12-ish Mbps

Quality = 99:
1080p: 13-17 Mbps (choppy)

Quality = 98: - seeing more packet corruption here than I would like
1080p: 13-17 Mbps (25-27 fps)

Quality = 97 - still some corrupt packets
1080p: 11.8-15.2 Mbps (25-27 fps)

Quality = 96 - a few corrupt packets (every 5-15s)
1080p: 10.1-14 Mbps (23-27 fps)

Quality = 95 - very few corrupt packets
1080p: 8.5-10 Mbps (27 fps)
720p: 4-5.2 Mbps   (30 fps)

Quality = 94 - very few corrupt packets
1080p: 8.1-10 Mbps (27-28 fps)

Quality = 92 - very few corrupt packets
1080p: 6.9-9.0 Mbps (27-28 fps)

For reference, uncompressed 424x240: 11-14 Mbps

Overall, there isn't actually a ton of quality difference between even 90 and 100. You really have to stare at the details.

##### 10/5/17

I tested things today on Windows, but there is a problem. Windows shows the device as having an error, code 10, which states that the device is unable to start. I tired uninstalling the deivice within Windows, clearing the LowerFilters as suggested (note that there should only be UpperFilters - don't delete that too! If you do, the value that needs to be restored in UpperFilters is ksthunk).

Things I've tried reverting to make Windows happy
- Back to 1024/1 transaction per microframe
- Revert change to increase TX FIFO to 4096 bytes.
- Completely remove multiple transfers per microframe in PIO patch.
- Revert "Remove 15fps descriptors" change. - This seemed to fix things.

Turns out the descriptor size was wrong (UVC_FRAME_UNCOMPRESSED(n)) where 'n' should match the number of frame intervals you have.

I noticed that the 1020x3 change may not be fully required. On Windows, streaming with 1020x3 bandwidth is usually ~33Mbps but sometimes jumps as high as 56Mbps. And the analyzer shows lots of 'K' errors (classification error).

Updating dwMaxPayloadTransferSize fixed the 'K' errors. Also, after restarting the laptop, I no longer notice the framerate dropping a lot on Windows.

##### 10/6/17

Windows testing:

Uncompressed 424x240: peaks at 47-49Mbps, averages around 44.
Compressed 1080p quality 90: 55-57Mbps, averages around 56.

On Linux with wireshark q90:
Uncompressed averages ~9MByte/s

95 quality causes some choppiness on the network stream.

##### 10/12/17

The print in musb_g_giveback appears to cause enough latency for dma to "work."

Placing delays in musb_g_giveback() all produce consistent results.
Placing them in txstate() is consistent until I placed them only where dma channel_program is called, then the framerate fluctuates more.

I'm hoping to get the DMA working and see if the CPU usage drops any, which could help give us the headroom we need for 1080p bandwidth at higher quality levels.

##### 10/13/17

I've been applying patches from [this repo](https://github.com/aneeshv/linux-bq27xxx/commits/master/drivers/usb/musb) to try to find the issue. So far,

0001-usb-musb-cppi41-txdma-early-completion-fixes-for-sho.patch
0002-usb-musb-ti81xx-pg2.0-transient-babble-workaround-fi.patch
0003-usb-musb-ti81xx-add-api-to-enable-or-disable-sof.patch
0004-usb-musb-cppi41-schedule-isochronous-transfer-using-.patch

The last one, scheduling isochronous transfers using a scheduler, currently isn't working because the SOF interrupt (MUSB_INTR_SOF) is never being called, and I'm not sure why.

##### 10/16/17

I found the newest alpha version of the TI PSP for the 81xx buried at: http://www.ti.com/tool/linuxezsdk-davinci. I decided to try copying all of the usb/musb sources in to the kernel and do a test build. Lo and behold, DMA works out of the box with this! However, the framerate is actually worse than the old code using PIO, even though the CPU usage is lower. Not sure what to make of that yet.

Strangely, it actually behaves better with double-buffering turned off. With it on, the framerate actually gets better with larger resolutions, up until 720p, but without it things are pretty good all the way up until you get to 1080p where it drops quite badly. Note that this was all with the "noise" stress test image. However, reducing the transfer size down to 1024 with 1 per microframe was substantially worse.

##### 10/17/17

Another TI repo with some changes, most of which I've already seen and tried: http://arago-project.org/git/projects/?p=linux-omap3.git;a=shortlog;h=refs/heads/ti81xx-master

[This thread](https://e2e.ti.com/support/dsp/davinci_digital_media_processors/f/716/t/377643) has some useful info about double-buffering on ISO endpoints.

##### 10/18/17

It seems a good majority of the DMA speedup was using exactly 3072 byte transfers to enable RNDIS mode. I reverted the EZSDK patch and performance seemed to stay the same. Some more quantitative benchmarks:

Without DMA fixes, "Glenn video":   one min:  2.02, 0.84, 0.37
                                   five min:  2.14, 1.47, 0.67
With DMA fixes, "Glenn video":      one min:  0.55, 0.29, 0.11
                                   five min:  0.34, 0.39, 0.23

So the CPU utilization did go down dramatically. Trying to figure out what specifically caused that. 

Replaced all of the \__raw_ calls with regular calls: 0.68, 0.38, 0.15
After 5 minutes: 0.39, 0.38, 0.24

_Benchmarking_

All benchmarks done with USB output at 720p for old releases and 1080p for new releases. IP stream at 1080p in all cases.
Measured average CPU with "sleep 300 && uptime"

To run in VLC:
vlc -q v4l2:///dev/video1:chroma=mjpg :v4l2-width=1920 :v4l2-height=1080

_Benchmarks against 1.0.8-RC4_

1080p USB output, 1080p IP stream:
Glenn video 5 minute:  29-30 USB fps, 3.81, 3.11, 2.08
JPEG torture 5 minute: 13-14 USB fps, 3.00, 2.65, 1.63

_Bencharmks of current code with EZSDK changes_

Note that this still has the issue of the extra packet causing corruption sometimes.

720p USB out, 1080p IP stream:
Glenn video 1 minute:  29-30 USB fps, 0.67, 0.37, 0.15
JPEG torture 1 minute:   7-9 USB fps, 2.82, 1.16, 0.45

1080p USB out, 1080p IP stream:
Glenn video 1 minute: 15-16 USB fps, 1.43, 1.58, 0.78
JPEG torture 1 minute:    2 USB fps, 3.43, 1.78, 0.73

##### 10/19/17

_PIO Benchmarks (3060 packet size)_

720p USB output, 1080p IP stream:
Glenn video 5 minutes:  29-30 USB fps, 0.99, 0.81, 0.39
JPEG torture 5 minutes: 25-29 USB fps, 9.01, 6.24, 2.92

1080p USB output, 1080p IP stream:
Glenn video 5 minutes:  21-27 USB fps, 8.47, 10.90, 8.84
JPEG torture 5 minutes: 10-11 USB fps, 21.90, 15.87, 9.04

_PIO Benchmarks (3072 packet size)_

1080p USB output, 1080p IP stream:
Glenn video 5 minutes:  21-27 USB fps, 10.41, 7.58, 3.57
JPEG torture 5 minutes: 10-11 USB fps, 21.82, 15.64, 8.19

I finally tried out audio streaming along with video streaming. I set this up by having my Linux PC output audio over the HDMI port which was connected to the PC input of the AVBMP. Under Audio->Digital, you should see activity on the meters when the audio is going. Then under the Matrix tab, ensure that HDMI Input->USB Record is set to on. Then you can open the video and audio devices in VLC (I had better luck in Windows).

Unfortunately, audio got choppy whenever scenes got busy. This was somewhat alleviated by dropping the MJPG quality to 50, but it still was there to some extent. Adding the IP streaming possibly made it worse, but it was more or less the same.

_UVC Investigation_

Apparently newer versions of guvcview have h.264 support. See these links:
https://superuser.com/questions/549314/v4l2-issues-with-webcam-encoded-to-h-264
https://sourceforge.net/p/linux-uvc/mailman/message/32291864/
https://sourceforge.net/p/guvcview/git-master/ci/b5ad373b378feb0e4c7b4b959f8bb671d342a586/
http://w2017.pl/logitech-vlc-ffmpeg-c922-streaming/
https://sourceforge.net/p/linux-uvc/mailman/linux-uvc-devel/thread/CAOs7PLNXEtPgXi3_qO8nNq%2BN1MgHBmCbQfwUgYbPUUP8%3DWf4qg%40mail.gmail.com/#msg28665660

I cloned the latest guvcview from https://sourceforge.net/p/guvcview/git-master/ci/master/tree/. I checked out v2.0.3 (last version which supports libavcodec 54) I had to install intltool, libv4l-dev, and libusb-1.0-0-dev, libsdl2-dev, and portaudio19-dev.

Install [Ubuntu Multimedia for Trusty](https://launchpad.net/~mc3man/+archive/ubuntu/trusty-media) package source, perform apt-get upgrade and dist-upgrade as specified in the notes.

##### 11/1/17

This patch never went into mainline, but someone implemeneted scatter-gather
support for uvc video [here](https://www.spinics.net/lists/linux-usb/msg84853.html)

It gave me an idea: maybe we could eliminate the memcpy in uvc_video_encode_data by instead adding a new pointer in the usb_request that points to the buffer already copied to by the edma3 driver.

The first approach, offsetting the buffer by 2 so that I can cheat and use the first two bytes for the header, seemed to have problems with subsequent packets' headers overwriting data. Not sure what's wrong there yet.

The second approach I took was to have txstate() and musb_write_fifo() fill in the UVC header by copying data from the regular buffer, then copying the rest of the buffer into the FIFO. I had issues with some packets coming out half-length, but then I saw in the datasheet that you shouldn't mix write sizes when writing to the FIFOs. So, I made everything word-length writes all the time, and that fixed the issue. Interestingly, 1024-byte transfers performed much better (no choppy audio at the expense of some frame-rate) than 2048 and 3072 byte transfers.

##### 11/3/17

The fix I found doesn't have video in Windows, but audio luckily still works well. Scratch that. Turns out with windows in dual-screen mode, ecam always showed up as a black video, even on the last nightly!
Trying the old isochronous scheduler patches. Looks like with 1k transfers I don't need the "fix" I made to set sof_isoc_started back to 0.

With the DMA fix at 2k, things are smooth-ish at 1080p (some chop in motion). 3k appears to make things a bit better at the expense of an occasional corrupt/overly long packet. Still not sure what's up with that yet.

Unfortunately, with 3k we still drop a bunch of frames at 352x240 and 320x240 YUYV. 

##### 11/6/17

Turns out mainline actually has support for the ti81xx.

Set CONFIG_DEBUG_GPIO=n to disable "can't parse ... property of node" warnings.

Some keys to getting some peripherals working:
- GPIO: For some reason the controllers time out early on in the hwmod reset. I patched omap_hwmod.c to not fail out when "softreset failed (waited %d usec)" happens in \_ocp_softreset().
- GPIO banks 3 and 4 do not seem to work since they're shared with 1 and 2. I was unable to add nodes for them in the device tree. I tried having them share hwmod 2 as well as duplicating 2 in the 81xx hwmod definitions file.
- Ethernet: You can disable emac1 in the device tree, but don't mess with anything in the mac node such as slaves, otherwise it'll appear to work but packets won't go in or out.
- USB: The key was building the drivers into the kernel rather than modules. I also turned off unnecessary ones which could've been confusing things in the probe process as well. Basically just ended up with MUSB_HDRC=y, MUSB_GADGET=y, MUSB_DSPS=y, MUSB_AM335X_CHILD=y, USB_TI_CCPI41_DMA=y.

Talked to Steve Z - on DM365 EDMA queue priorty was key to getting video at 30fps and audio clean.

Todo things:
- [ ] multi transfers per microframe with g_zero
- [ ] NULL pointer dereference with tracing on in musb_g_tx
- [ ] Occasional NULL pointer dereference in cppi41_run_queue with DMA

##### 11/8/17

Disable iptables:
```
iptables -F
iptables -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
```

modprobe g_ether dev_addr=d6:3c:fe:f5:bc:db host_addr=72:f8:7a:84:af:09
ifconfig usb0 192.168.100.2 up

iperf with g_ether on 4.9 kernel (100% cpu)
```
nickg@lnx-nickg:~$ iperf -s
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 192.168.100.1 port 5001 connected with 192.168.100.2 port 56912
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0-10.0 sec  58.2 MBytes  48.7 Mbits/sec

nickg@lnx-nickg:~$ iperf -c 192.168.100.2
------------------------------------------------------------
Client connecting to 192.168.100.2, TCP port 5001
TCP window size: 85.0 KByte (default)
------------------------------------------------------------
[  3] local 192.168.100.1 port 49986 connected with 192.168.100.2 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec  43.8 MBytes  36.5 Mbits/sec
```

iperf with g_ether on 2.6.37 kernel, PIO (~25% cpu)
```
nickg@lnx-nickg:~/workspace/vng$ iperf -s
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 192.168.100.1 port 5001 connected with 192.168.100.2 port 48762
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0-10.1 sec   106 MBytes  88.2 Mbits/sec

nickg@lnx-nickg:~/workspace/vng$ iperf -c 192.168.100.2
------------------------------------------------------------
Client connecting to 192.168.100.2, TCP port 5001
TCP window size: 85.0 KByte (default)
------------------------------------------------------------
[  3] local 192.168.100.1 port 51366 connected with 192.168.100.2 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec  60.4 MBytes  50.5 Mbits/sec
```

iperf with g_ether on 2.6.37 kernel, DMA (~85% cpu)
```
nickg@lnx-nickg:~/workspace/vng$ iperf -s
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
[  4] local 192.168.100.1 port 5001 connected with 192.168.100.2 port 50676
[ ID] Interval       Transfer     Bandwidth
[  4]  0.0-10.1 sec  98.8 MBytes  82.2 Mbits/sec

nickg@lnx-nickg:~/workspace/vng$ iperf -c 192.168.100.2
------------------------------------------------------------
Client connecting to 192.168.100.2, TCP port 5001
TCP window size: 85.0 KByte (default)
------------------------------------------------------------
[  3] local 192.168.100.1 port 51496 connected with 192.168.100.2 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec   163 MBytes   137 Mbits/sec

```

Disabling SMP in the kernel config led to some fairly significant usb speedups on the 4.9 kernel (36.5->55.4Mbit/s)

Further, using PIO mode instead of DMA increased the throughput substantially again, to ~91Mbit/s

4.9 device iperf client pc iperf server:
```
PIO
Cpu(s):  4.4%us, 33.8%sy,  0.0%ni, 22.1%id,  0.0%wa,  0.0%hi, 39.7%si,  0.0%st
[  3] 210.0-240.0 sec   326 MBytes  91.3 Mbits/sec
DMA
Cpu(s):  2.9%us, 13.0%sy,  0.0%ni, 66.7%id,  1.4%wa,  0.0%hi, 15.9%si,  0.0%st
[  3] 30.0-60.0 sec   176 MBytes  49.3 Mbits/sec
```

2.6.37 (master) iperf client pc iperf server:
```
PIO
Cpu(s):  1.0%us, 17.6%sy,  0.0%ni, 75.5%id,  1.0%wa,  0.0%hi,  4.9%si,  0.0%st
[  3] 570.0-600.0 sec   328 MBytes  91.6 Mbits/sec
DMA
Cpu(s):  1.0%us, 26.7%sy,  0.0%ni, 69.3%id,  0.0%wa,  1.0%hi,  2.0%si,  0.0%st
[  3]  0.0-30.0 sec   293 MBytes  81.9 Mbits/sec
```

Went back to old 2.6 builds with PIO - seems like adding high resolution timestamps improves performance slightly? Also, going down to 1k transfers vs. 3k drops things quite a bit - only 60-ish Mbit/s max.

##### 11/13/17

Zero-copy implementation with write_fifo commented out (1080p, noise.gif):
Cpu(s): 29.3%us, 26.0%sy,  1.3%ni, 43.1%id,  0.0%wa,  0.0%hi,  0.3%si,  0.0%st
memcpy to usb fifo takes ~43us.
Tried using edma for filling usb fifo. I was having problems with setting asize/bsize correctly (according to the "Peripheral Servicing Example", it appears I need asize=1 and bsize=(len) and csize=1, but things only sort of work if asize=len and bsize=1).
It takes 11us to set up the transfer, and 40-ish for it to complete. This seems unusable.

##### 11/14/17

Going to do MJPEG quality testing today. Want to find out two things:
- Maximum MJPEG quality where sound does not cut out on noise image
- Maximum quality where a typical HD video does not cut out

Also want to run imatest in that range and see where we're at.

These tests check for where you can stream USB with a 440Hz sine test tone with no noticeable or visible choppiness.

| Build              | Max Quality (Noise) | Max Quality (Glenn Video) |
| :----------------- | :------------------ | :------------------------ |
| W/clock 3763530    |         60          |            85             |
| W/o clock 55309418 |        <60          |            70             |

With the sched_clock changes, USB performance does appear quite a bit better.

Q50 noise USB bandwidth: ~85Mbps.

| Quality  | MTF30 |
| :------- | :---- |
|    90    | 0.397 |
|    85    | 0.285 |
|    80    | 0.276 |
|    75    | 0.265 |
|    70    | 0.266 |
|    65    | 0.264 |
|    60    | 0.261 |
|    55    | 0.258 |
|    50    | 0.250 |

Seems like Q80 is an acceptable happy medium where it's highly unlikely we'll ever get audio chop and also keeping the image quality high.

Noise stressing with the final build: with 3k transfers we do get some audio chop with IP and usb streaming. 1k transfers no chop is present at the expense of frame rate. 2k seemed close to 3k in behavior.

##### 11/16/17

Finally getting the last loose ends tied up:
- [x] Dynamic MJPG quality (q80 for 1080p, q90 for others)
- [x] Fix PIO patch to set fifo_count to packetsize * mult
- [x] Bump kernel PR once

Don't use the new Windows Photos app for SFR testing - it's not as clear! Use the Photo Viewer program that's been around forever.

Q70 without double-buffering has no audio choppiness with noise test and IP streaming.

Choppiness seems to stem from double-buffering, so I'm not going to enable it.

Wasn't sure if mjpg quality was actually getting set properly, so I enabled debug prints to verify. It is getting set every resolution change as expected.

##### 11/27/17

Audio lag issue found in review testing -> up to 5s behind video.
VLC in windows is more choppy and has audio choppiness. Part of the issue was it was defaulting to 44100Hz instead of the 48000Hz that the audio is actually coming out at. In Linux, this is detected correctly.

vlc linux:
audio: pulse://alsa_input.usb-Vaddio_AV_Bridge_Matrix_PRO_0017eb820818-48-02-PRO.analog-stereo

video: :v4l2-dev=/dev/video0 :v4l2-vbidev= :v4l2-chroma=MJPG :v4l2-input=0 :v4l2-audio-input=-1 :v4l2-width=1920 :v4l2-height=1080 :v4l2-aspect-ratio=16\:9 :v4l2-fps=30 :v4l2-radio-dev=/dev/radio0 :v4l2-tuner-frequency=-1 :v4l2-tuner-audio-mode=3 :no-v4l2-controls-reset :v4l2-brightness=-1 :v4l2-brightness-auto=-1 :v4l2-contrast=-1 :v4l2-saturation=-1 :v4l2-hue=-1 :v4l2-hue-auto=-1 :v4l2-white-balance-temperature=-1 :v4l2-auto-white-balance=-1 :v4l2-red-balance=-1 :v4l2-blue-balance=-1 :v4l2-gamma=-1 :v4l2-autogain=-1 :v4l2-gain=-1 :v4l2-sharpness=-1 :v4l2-chroma-gain=-1 :v4l2-chroma-gain-auto=-1 :v4l2-power-line-frequency=-1 :v4l2-backlight-compensation=-1 :v4l2-band-stop-filter=-1 :no-v4l2-hflip :no-v4l2-vflip :v4l2-rotate=-1 :v4l2-color-killer=-1 :v4l2-color-effect=-1 :v4l2-audio-volume=-1 :v4l2-audio-balance=-1 :no-v4l2-audio-mute :v4l2-audio-bass=-1 :v4l2-audio-treble=-1 :no-v4l2-audio-loudness :v4l2-set-ctrls= :live-caching=300

VLC opening the audio and video streams separately works fine. Also, Media Player Classic plays both just fine. According to Steve Z., they have seen issues in the past with VLC specifically, and things should be fine as long as you can open both the audio and video streams and play them individually with no issues. He also recommended IP Cam.
