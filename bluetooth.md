Starting my investigation of which kernel bluetooth interface to use when
communicating with the PSoC. It seems we have two main options:

    1. Use the full Bluetooth stack on the PSoC and communicate somehow with
    the kernel. More research is needed to know whether we could somehow
    abstract this away and make it look to userspace like something similar to
    BlueZ's interface.
    2. Use only the lower-level controller interface of the PSoC and keep it
    in "HCI mode". This would keep the host interface in Linux and allow us to
    use the BlueZ daemon for Bluetooth-related things. This does have the
    advantage of allowing us to swap out the PSoC later for something like a
    USB Bluetooth adapter or dedicated Bluetooth hardware. They also call that
    special mode "direct-test mode."

[Linux Without Wires The Basics of Bluetooth](http://opensourceforu.com/2015/06/linux-without-wires-the-basics-of-bluetooth/)
[PSoC Direct Test Mode Project #018](http://www.cypress.com/blog/100-projects-100-days/project-018-direct-test-mode-dtm)

### Vaddio Camera hardware

Eric: Terra, Shotzy, Kronos (only newer things coming)
Jeff: Cygnus, Antares, Vulcan, Rigel

Part number usage:
- 997-7270-002 (BLE board)
    - 998-7270-000 RoboTRAK

- 997-9900-002 (BLE board w/LED)
    - 998-9900-000 RoboSHOT 12
    - 998-9910-000 RoboSHOT 30

##### Shotzy, Terra, Kronos (new version only)

All connections to the PSoC header are connected via PS MIO. This means we must
use one of the hard UARTs. Both are currently used. UART0 is the system console
and does not have two overlapping pins with the header. UART1 is the physical
serial connection on the back of the camera and does have one possibility: pins
16 and 17. This also overlaps with the I2C bootloader pins.

##### Rigel, Antares, Cygnus, Centaur

All CY_* pins are routed to the PL, so we could easily add a UART in the HDL.

##### Romulus, Remus, Regula, Risa, Vulcan, Kronos (current revision in field)

Not possible - only IR and RGB brought out to daughter board.


### Prototyping

TODO:
- [x] FPGA changes for Antares to try Bluez5 with PSoC in HCI mode
- [ ] Shotzy/Kronos/Terra change to disable RS232 and use for PSoC HCI
- [ ] Bit-bang driver for HCI UART
- [ ] Gstreamer 1.0 build and testing

##### Antares

Daugherboard build from Nate:

    RTS P0[7] CY_SWDIO_SS0      (AUXPL20 - UART_BT_HCI_ctsn)
    CTS P0[6] CY_SWDCLK_SCLK    (AUXPL14 - UART_BT_HCI_rtsn)
    RX  P0[4] CY_SDA_MOSI       (AUXPL16 - UART_BT_HCI_txd)
    TX  P0[5] CY_SCL_MISO       (AUXPL18 - UART_BT_HCI_rxd)

##### Shotzy

Unlock GPIO registers. Set MIO pins 36-37 to default reset states.
Then enable UART1 on MIO pins 16-17.

```
mw 0xF8000008 0xDF0D
mw 0xF8000790 0x00001601
mw 0x00000794 0x00001601
mw 0xF8000740 0x000016e0
mw 0xF8000744 0x000016e1
mw 0xF8000004 0x767B
```

Set pins 16-17 to default:
```
mw 0xF8000008 0xDF0D
mw 0xF8000740 0x00001601
mw 0xF8000744 0x00001601
mw 0xF8000004 0x767B
```

New build 8/22 for bitbang gpio: TX P3.5 and RX P3.4 (tx_gpio=28 rx_gpio=26)

### Kernel backports

Steve wanted me to backport a newer bluetooth stack since they are having some
issues with HCI mode not pairing like it should. I used the [kernel backports
project](https://backports.wiki.kernel.org/index.php/Documentation).

The backports recipe has some issues distinguishing between the regular and
cross-compilers. I found that the best thing to do is download backports,
set KLIB_BUILD to the (Yocto) kernel build directory, run "make menuconfig" or
whatever to set up your build, then save off the .config file. I worked around
this in the Yocto recipe by manually setting the CC variable in do_configure
to the host "gcc". This is basically with the Linux kernel build does for its
own menuconfig task.

Building backports was pretty straightforward otherwise. I had some issues
seeing an option for Bluetooth in menuconfig, but it turned out that I was
missing a required prerequisite in the regular kernel config (something like
CONFIG_CRYPTO_ECB or CONFIG_CRYPTO_CMAC). It seems the best thing to do here is
look at the Kconfig which should provide the option, in this case
`net/bluetooth/Kconfig`, and ensure that all of the "depends on" are satisfied,
since backports won't automatically select them for you.

### HCI driver

##### 8/28/17

Spending some time today looking at ways to make the H4 HCI driver more reliable
and less susceptible to errors on the UART line.

Decided to put the work on hold for now since I can't seem to get receive
working 100% reliably. See VNG-7894 for final comments.

##### BLE PRoC Remote

Connected Pioneer board up to the UltraScale with an AXI (FPGA) UART on PMOD connector JA7. Using Rocko with kernel 4.9 and BlueZ 5.46.

    JX2_HP_DP_00_P: D2: rx  PMOD1 -> P3.1 tx
    JX2_HP_DP_00_N: D1: tx  PMOD2 -> P3.0 rx
    JX2_HP_DP_01_P: A3: cts PMOD3 -> P3.2 rts
    JX2_HP_DP_01_N: A2: rts PMOD4 -> P3.3 cts

Be sure to enable CONFIG_UHID for keyboard support.

Once booted:

    hciattach /dev/ttyS0 any 115200 flow
    bluetoothctl
        power on
        scan on
        agent on
        devices
        scan off
        pair <mac>
        trust <mac>
        connect <mac>

If security is off, you may need to connect before pairing.
To unpair, first "disconnect", then "remove <mac>". Just disconnecting is not enough.
Turning scan off prior to or right after connecting seems important - otherwise after connecting it works for maybe 30s and then strangely times out with "HCI command timed out" messages.

To test, use "evtest" and select appropriate input device for remote (something like CY5672 Remote Control RDK, usually will just be '0'). You can also cat the /dev/input/ file directly, or mount debugfs and cat /debug/hid/hidraw0/events.

##### BLE with ZedBoard

Made a bitstream that routes uart0 through the EMIO. Need to load the bitstream
in u-boot with:

ext4load mmc 0:2 ${kernel_load_address} /boot/download.bit
fpga loadb 0 ${kernel_load_address} ${filesize}
boot

Pin assignments:

    tx  PMOD JA1 -> P3.0 rx
    rx  PMOD JA2 -> P3.1 tx
    rts PMOD JA3 -> P3.3 cts
    cts PMOD JA4 -> P3.2 rts

##### BLE on Boombox Hardware

P3.1 (tx):  AIB_SDIO (10)   -> CY_BLE_UART_RX
P3.0 (rx):  AIB_SDI   (9)   -> CY_BLE_UART_TX
P3.2 (rts): AIB_LRCLK (8)   -> CY_BLE_UART_CTS
P3.3 (cts): AIB_SCLK  (7)   -> CY_BLE_UART_RTS
GND: 6, 4

hciattach /dev/ttyS5 any 115200 flow

##### Troubleshooting bluetooth issues

Older u-boots:
setenv mmcargs $mmcargs console=ttyPS1,115200
ext4load mmc 0:2 ${loadaddr} /boot/download.bit
fpga loadb 0 ${loadaddr} ${filesize}
boot

On newer u-boots:
ext4load mmc 0:2 ${kernel_load_address} /boot/download.bit
fpga loadb 0 ${kernel_load_address} ${filesize}
boot

hciattach /dev/ttyPS0 any 115200 flow
hciconfig hci0 up

Commits for bluetooth:
git cherry-pick -x 98f7be9965 a85092a05f

- On zedboard with zedboard machine, VNG-9107-boombox-ble branch:
Uart working fine with FTDI. hciconfig hci0 up gives:
    Can't init device hci0: Invalid argument (22)

- Zedboard, zynq-boombox machine, boombox-bluetooth branch:
Seems to work, occasional messages:
hci0 advertising data length corrected

- Zedboard, zedboard-zynq7-ark machine, changes cherry-picked onto master merge base of boombox-bluetooth branch (0a6ed88196d9c5496f4a9c4a01b0c27213774acc)

setenv devicetree_image /boot/zynq-zed-ark.dtb
boot

With additional commit 16e8d66f9 for console fix, same as boombox machine.

- 4.14 commit based on 907fec52e53a25501e0f02aa9bca6ca2032b6887
Fail

Bisect log points to 4.14/rocko update:

```
git bisect start
# bad: [907fec52e53a25501e0f02aa9bca6ca2032b6887] Merged in vaddio/vaddio_bhanson-vng/VNG-8791-Metropolis-Screens (pull request #5250)
git bisect bad 907fec52e53a25501e0f02aa9bca6ca2032b6887
# good: [0a6ed88196d9c5496f4a9c4a01b0c27213774acc] Merged in vaddio/vaddio_asheriff-vng/VNG-8891-main-nav-tooltips (pull request #5160)
git bisect good 0a6ed88196d9c5496f4a9c4a01b0c27213774acc
# good: [92eee6d52d8facc5cf8881a6765f73c49c967024] Merged in vaddio/vaddio_jquant-vng/VNG-9002-beta-level-public-rest-api (pull request #5187)
git bisect good 92eee6d52d8facc5cf8881a6765f73c49c967024
# good: [45ab7d29a0420581ca5a64dc703aa4c1f7dcfd92] Merged in vaddio/vaddio_mcouture-vng/VNG-9111 (pull request #5226)
git bisect good 45ab7d29a0420581ca5a64dc703aa4c1f7dcfd92
# bad: [935bd0027a6e18edd473c7bb26769c118fb93678] Merged in vaddio/vaddio_ngraumann-vng/VNG-8813-ark-rocko-2.4.2 (pull request #5224)
git bisect bad 935bd0027a6e18edd473c7bb26769c118fb93678
# good: [3c01f4b4289713f78fd4444da290fc6eee7551cb] Merged in vaddio/vaddio_jklemme-vng/VNG-7859-keyer-href-cleans (pull request #5241)
git bisect good 3c01f4b4289713f78fd4444da290fc6eee7551cb
# good: [09ed0c9ea609abf835fb74cd19b4a3c97a99ae60] Merged in vaddio/vaddio_ngenetzky-vng/VNG-9082-lift-metropolis (pull request #5208)
git bisect good 09ed0c9ea609abf835fb74cd19b4a3c97a99ae60
# good: [3421f39a1f7b93ccd2975a30735c49b9e25dddb5] VNG-8813: linux-ti-staging: Disable some configs turned on by upgrade.
git bisect good 3421f39a1f7b93ccd2975a30735c49b9e25dddb5
# bad: [94681047ba73cd6ef55ac83251377a3487a73430] VNG-8813: linux: Always apply patches for features.
git bisect bad 94681047ba73cd6ef55ac83251377a3487a73430
```
good (4.9):
hci0:   Type: Primary  Bus: UART                                                     
        BD Address: 80:49:29:19:95:80  ACL MTU: 27:4  SCO MTU: 0:0                   
        DOWN RUNNING                                                                 
        RX bytes:198 acl:0 sco:0 events:13 errors:0                                  
        TX bytes:76 acl:0 sco:0 commands:13 errors:0           

bad (4.14):
hci0:   Type: Primary  Bus: UART                                                     
        BD Address: 80:49:29:19:95:80  ACL MTU: 27:4  SCO MTU: 0:0                   
        DOWN                                                                         
        RX bytes:1008 acl:0 sco:0 events:54 errors:0                                 
        TX bytes:312 acl:0 sco:0 commands:54 errors:0                      

Some commands seem not supported, see 6.27 of bluetooth core specification.

Reverted:
de2ba3039cfb61334b2523677cc032422873ff93
27bbca44026d81968b002d73edf6976d49edd005

Now things work.

- Trying to update PSoC project to 4.2 with new middlware. Open HCI project in PSoC creator 4.2. Right click on project (on left side) and select "Update Components". Bluetooth will update from 3.30 to 3.52.
