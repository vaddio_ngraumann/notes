### GEM FIFO Interface

We have done some additional research into the PL-PS FIFO interface for the GEMs.

We found that the FIFO interface is trivial to enable: you first need to enable it in the PS configuration in Vivado. On runtime, the fifo is then enabled by setting a bit in the external_fifo_interface GEM register: https://www.xilinx.com/html_docs/registers/ug1087/ug1087-zynq-ultrascale-registers.html

Our main question is whether this FIFO interface will easily adapt to Macnica's packet processing cores. In the Zynq UltraScale+ TRM (UG1085)  GEM chapter around page 1005 they outline the signals which are similar to the packet interface in the latest drop we've received from you: tx_r_sop, tx_r_eop, tx_r_data[7:0], tx_r_valid, etc. and similar for the receive interface.

Our current plan is to create a design with the FIFO interface enabled and a small amount of logic to read data out of the Rx fifo interface and display it in Chip Scope. We will use Xilinx's the baremetal IP example and comment out the DMA setup portions so that packets get routed out the FIFO interface. Once we confirm the format of the data, which should be standard ethernet frames, we can then work with you to determine what will need to be done to integrate that with the existing packet filter logic in the PL.

##### Loopback test

```
while true ; do echo hello | sudo socat - UDP-DATAGRAM:10.30.203.255:8367,broadcast ; sleep 1 ; done
```

Wireshark filter: `udp.dstport == 8367`

Not sure why `start_tx_pclk` (also called `transmit_go` ) does not need to be set to transmit on FIFO interface. Perhaps this is only necessary when using the DMA?

### Drop 01/20/2020
Software questions:
1. u-boot_sources directory checked in to the amsterdam directory: What are they for? Are they needed to build the MPA1000 design?
2. What is the purpose of the PetaLinux project in the peta_183 directory? It is not mentioned in release_note.txt.
3. What is the purpose of serialinterfaceapp? It is present only in the encoder. This appears to process AT commands for the MPA1000; is it necessary for our design to control the Macnica FPGA IP cores or can Vaddio do without it?
4. Can Macnica provide a brief summary of each application and kernel module necessary to run the encoder and decoder? Most of the README files for the Petalinux recipes are generic and do not provide any insight.
5. It seems the main timing-critical software component is the kernel thread that updates the codestream buffer pointers on the receive device. Are there any other timing-critical areas we should be aware of?
6. What changes would be needed to the existing design(s) to support a bidirectional video flow?
7. What are the DDR Memory bandwidth requirements for transmitter and receiver?
8. Was there a reason to use a custom GPIO module instead of the Zynq's built-in EMIO?
9. Was there a reason for implementing a custom read32/write32 ioctl interface instead of having the proavdrv provide a Linux UIO device and performing reads and writes directly via `mmap`?
10. Vaddio tasks on software side?

RTL questions:
1. RCLP clock gen integration with GMII seems very tightly coupled to soft ethernet GMII interface. How might we integrate this with PS MAC?
2. Can Macnica provide a system register definition document?

Future optimizations:
1. Use EMIO from Zynq and remove custom GPIO modules.
2. Move any remaining tight loops to kernel threads instead of userspace?
3. Removal of Xilinx HDMI core items.
