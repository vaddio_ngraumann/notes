# Eagle DDR usage

### Idle
              total        used        free      shared  buff/cache   available
Mem:            996         140         784           0          71         823


### USB Streaming

##### 4k30 (NV12)
CMA: 83.5
```
              total        used        free      shared  buff/cache   available
Mem:            996         228         696           0          71         736
```

##### 1080p60
CMA: 24.2
```
              total        used        free      shared  buff/cache   available
Mem:            996         167         756           0          71         796
```

##### 720p60
CMA: 29.0
```
              total        used        free      shared  buff/cache   available
Mem:            996         171         752           0          71         792
```

### MJPEG Streaming
Note: Numbers also include USB streaming above since it must be running in current builds. MJPEG streams are always 1080p60.

##### USB at 4k30 (NV12)
CMA: 89.5
```
              total        used        free      shared  buff/cache   available
Mem:            996         235         689           0          71         729
```

##### USB at 1080p60
CMA: 30.2
```
              total        used        free      shared  buff/cache   available
Mem:            996         175         749           0          71         789
```

##### USB at 720p60
CMA: 35.0
```
              total        used        free      shared  buff/cache   available
Mem:            996         181         743           0          71         783
```

### Camera block input

1080p60 in, out to HDMI 720p60 (scaler is inline, not m2m):
CMA: 20.2
```
              total        used        free      shared  buff/cache   available
Mem:            996         162         784           0          50         802
```

### HDMI In+Out, Camera block, MJPEG, AI input
Capturing frames with yavta.

##### USB at 4k30 (NV12)
CMA: 116.3

##### USB at 4k15 (YUY2)
CMA: 132.1
