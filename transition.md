GStreamer
- [x] GSTshark for latency and queuelevel

Video for Linux
- [x] V4l2 overview

USB Streaming
- [x] Kernel side: UVC/UAC/HID
- [x] Userspace
- [x] DMABUF optimizations
- [x] Configfs
- [x] halting UVC
- [x] USB compoent verifier

Cast JPEG
- [x] jpegtool
- [x] Architecture
- [x] H.264 driver in the future

UltraScale architecture
- [x] Measuring memory bandwidth (PR onto master)
- [s] Golden/Black Eagle updates for DisplayPort
- [x] Eagle kernel interfaces
- [x] Setting up JTAG (how I debugged DDR issue)
- [x] How to set up tftp server to tftp stuff in u-boot
- [x] Petalinux, dev kits
- [x] EV part and DDR

Multi-scaler
- [x] Fix incorrect boolean case on disable-passthrough
- [x] Fix GStreamer buffer issue
- [x] VNG PR, linux-xlnx PR
- [x] FPGA build integration

Yocto upgrades
MIO vs EMIO
