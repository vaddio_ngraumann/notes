### Factory mode

Hold down the factory button and hit reset. Wait until the LEDs blink amber.

Useful commands:
```
getnetitinfo
Detected Switch chipset : MARVEL 88E6320                                           
Switch total port number : 7                                                       

        Switch port 0 :         Link Down,      Speed : 0,      Half Duplex        
        Switch port 1 :         Link Down,      Speed : 0,      Half Duplex        
        Switch port 2 :         Link Up,        Speed : 1000,   Full Duplex        
        Switch port 3 :         Link Down,      Speed : 0,      Half Duplex        
        Switch port 4 :         Link Up,        Speed : 1000,   Full Duplex        
        Switch port 5 :         Link Up,        Speed : 100,    Full Duplex        
        Switch port 6 :         Link Up,        Speed : 1000,   Full Duplex`
```

`getip` and `getmac` are also useful and do what they imply.

### IP Core discussion
Part which sits in the ARM
Q: Is this baremetal or Linux-based?

Less than 64 channels does not need SRAM on PL side.

MAC only RGMII on PL today. Will check w/audio engineers about using PS for small channel counts.

### DEP
- Hardware clock synchronization is recommended for endpoints
- Recommend hardware clock sync, but can do software samplerate conversion

Q: Container: Memory usage etc -  8376kB (i.MX8)

Latencies:
- Require OS tuning
- Performance dependencies (Dante provides guidance, left to customer)

Beyond 1.0:
- Samplerate pull up/down
- PTP timestamping when hardware available

Linux requirements:
- Kernel 4.18 or newer
- Cgroups, namespaces
- Aim to use standard desktop Linux wiht preemptible kernel
- Not considering for bare metal or other RTOS

Hardware:
- Si5351 VXSO
- Feedback: Edge counter (specific to i.mx8) - for generic ARM will use GPIO feedback path

- Will add sub-core alloation ability for using less than an entire core

Q: Preempt-RT no longer required? (2.7) - no, just regular CONFIG_PREEMPT.
Q: Different build for each channel config, or runtime configurable?

- Need switch to support daisy chaining and redundancy

- Optimizing out Si5351 not part of the plan right now.
