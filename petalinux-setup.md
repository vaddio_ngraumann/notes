# PetaLinux ZCU106 Setup

### Getting Started

1. Download petalinux version 2018.2 from the [Xilinx Embedded Design Tools][] page. The download is called "PetaLinux 2018.2 Installer (TAR/GZIP - 6.15 GB)".
1. Create an installation directory that's writable by your user (unless you want to run the installer as root). For example:
```
sudo mkdir /opt/petalinux-v2018.2
sudo chown myuser:mygroup /opt/petalinux-v2018.2
```
1. Launch the petalinux installer. Open a terminal and run the following commands, replacing Downloads with the directory where you downloaded the installers.
```
$ cd Downloads
$ chmod +x petalinux-v2018.2-final-installer.run
$ ./petalinux-v2018.2-final-installer.run /opt/petalinux-v2018.2
```
1. You will be prompted to read some license agreements. For each, read it if you desire, then press 'q' to exit the viewer and 'y' to agree.
1. Wait until the installation completes.

### Creating a PetaLinux project from a BSP


1. Download the BSP for the zcu106 board: "ZCU106 BSP (BSP - 1.24 GB)" from the [Xilinx Embedded Design Tools][] page.
1. Create the BSP project by running the following commands:
```
source /opt/petalinux-v2018.2/settings.sh
petalinux-create -t project -s ~/Downloads/installs/xilinx-zcu106-v2018.2-final.bsp
```
1. The project will be created in a directory called xilinx-zcu106-v2018.2.

### Building the PetaLinux project

1. You must either create a PetaLinux project by following [Creating a PetaLinux project from a BSP](#creating-a-petalinux-project-from-a-bsp) above, or by cloning an existing PetaLinux project repository such as a shared repository provided by Vaddio.
1. Run the following commands to perform a build. Note that you only need to source the settings.sh script once prior to running any petalinux configuration or build commands.
```
cd project-location
source /opt/petalinux-v2018.2/settings.sh
petalinux-build
```
1. Wait until the build completes, which will take a few minutes.
1. Generate the `BOOT.BIN` output file:
```
cd images/linux
petalinux-package --boot --fsbl zynqmp_fsbl.elf --fpga system.bit --pmufw pmufw.elf --u-boot
```
1. Follow the steps in the "Configuring SD Card ext filesystem Boot" section of Chapter 6 of the [PetaLinux Reference][] guide to prepare an SD card and configure your PetaLinux project for SD card boot with an ext rootfs. Note that the `petalinux-config` steps have already been done, you only need to prepare the SD card itself. These steps outline creating a FAT32 and ext4 partition on the SD card, copying BOOT.BIN and image.ub to the FAT32 partition, and unpacking the rootfs.tar.gz archive into the ext4 partition.
1. Once you've created the SD card, insert it into the zcu106 board. Connect the board to your PC via micro USB cable connection to J83 on the board. Open a serial terminal on the first of the four UARTs that appear (usually /dev/ttyUSBx) at 115200 baud.
1. Power up the zcu106 board. You should see boot messages on the UART connection, finally ending at a Linux login prompt. The default username and password is "root".

### Customize the reference design in Vivado

Ultimately you may wish to create a Vivado project from scratch, but for getting started we can use the pre-configured reference design.

1. Ensure you have Vivado 2018.2 installed including support for the Zynq UltraScale+ MPSoC.
1. The BSP project created in the [Creating a PetaLinux project from a BSP](#creating-a-petalinux-project-from-a-bsp) section will contain a Vivado hardware project in the `hardware/` directory. Open that project in Vivado.
1. Make any changes in Vivado and re-generate the bitstream. Then, export the hardware via File->Export->Export Hardware.
1. Import the updated hardware configuration back to the petalinux project. For more information, See "Importing Hardware Configuration" in Chapter 4 of the [PetaLinux Reference][].
```
petalinux-config --get-hw-description=hardware/xilinx-zcu106-2018.2/
```
You may need to remove any old .hdf files such as the example hdf if `petalinux-config` complains about multiple hdf files in the directory.

### Adding packages to the root filesystem

You can interactively customize which packages are included in the rootfs with the following command:
```
petalinux-config -c rootfs
```

### Adding development tools to the root filesystem

If you'd like to be able to develop on-target, you can add these tools using the same interactive `petalinux-config -c rootfs` command. Some examples include `packagegroup-core-buildessential` (which includes gcc), `binutils`, `make` and `gdb`. These can be found in the following menus:
```
Filesystem Packages -> misc -> packagegroup-core-buildessential
Filesystem Packages -> devel -> binutils
Filesystem Packages -> devel -> make
Filesystem Packages -> misc -> gdb
```

Also note that you can use the `/` key to search within the config interface for a specific package.
Be sure to save your changes, and then run `petalinux-build` to generate a new root filesystem.

### Tips and Tricks

- PetaLinux generates a .gitignore file which ignores any automatically generated files. You can use `git clean -ffdx` to clean any untracked or generated files. Be sure to verify you haven't added any new files which you need to commit first, especially in `project-spec`!

##### Building kernel outside of petalinux

There might be a better way to do this, just writing it down for the future..
Usual kernel build process:

```
export PATH=/opt/petalinux-v2018.3/tools/linux-i386/aarch64-linux-gnu/bin:$PATH
make ARCH=arm64 O=build/ xilinx_zynqmp_defconfig
make ARCH=arm64 O=build/ CROSS_COMPILE=aarch64-linux-gnu- -j8
```

Make u-boot image, use fit image template from this page:
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842374/U-Boot+Images
Only change was I changed compression to gzip and pointed it at Image.gz (in build/arch/arm64/boot)
mkimage -f image.its image.ub

Install modules into rootfs as usual:
```
sudo make ARCH=arm64 O=build/ INSTALL_MOD_PATH=/media/nickg/root/ modules_install
```

If you have custom dts stuff in pl.dtsi you will need to include that in the dts for your board. Some PL clock references needed to be changed from `&clkc 71` to `&zynqmp_clk PL0_REF`

For some reason, kernel command line is not getting setup properly. Have to
manually set it in u-boot:
```
setenv bootargs earlycon console=ttyPS0,115200 clk_ignore_unused root=/dev/mmcblk0p2 rw rootwait
```

Matchbox/other graphical stuff was causing issues. Removed the following files to get a command prompt:
```
/etc/rc5.d/S09xserver-nodm
/etc/rc5.d/S99tcf-agent
```

### References

[Xilinx Embedded Design Tools]: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html
[PetaLinux Reference]: https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_2/ug1144-petalinux-tools-reference-guide.pdf
[Xilnix ZCU106 Board User Guide]: https://www.xilinx.com/support/documentation/boards_and_kits/zcu106/ug1244-zcu106-eval-bd.pdf
