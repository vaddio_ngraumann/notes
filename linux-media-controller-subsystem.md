## Linux Media Controller Subsystem ##

- `/dev/mediaX` for pipelines
- `/dev/videoX` for top-level V4L2 devices
- `/dev/v4l2-subdevX` for V4L2 sub-devices (think nodes)

- Each device (entity) can have source and sink pads

---

## Tools ###

Controls on subdevices set up with ioctl's
Tools exist to modify these devices:
- media-ctl
- v4l2-ctl
- yavta

---
## Simple Example ##

Pipeline from test pattern generator into framebuffer write:
```
media-ctl -p
(...)
Device topology
- entity 1: vcap_tpg output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "80030000.axi_tpg":0 [ENABLED]

- entity 5: 80030000.axi_tpg (1 pad, 1 link)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
		[fmt:UYVY/1920x1080 field:none]
		-> "vcap_tpg output 0":0 [ENABLED]
```

---

## HDMI -> USB Example ##

```
media-ctl -p
(...)
Device topology
- entity 1: video_cap output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "80100000.v_proc_ss":1 [ENABLED]

- entity 5: 80000000.v_hdmi_rx_ss (1 pad, 1 link)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
        [fmt:RBG888_1X24/1920x1080 field:none]
        [dv.caps:BT.656/1120 min:0x0@25000000 
        max:4096x2160@297000000 stds:CEA-861,DMT,CVT,GTF
        caps:progressive,reduced-blanking,custom]
        [dv.detect:BT.656/1120 1920x1080p60 (2200x1125)
        stds:CEA-861 flags:CE-video]
		-> "80040000.v_tpg":0 [ENABLED]
```

---

```
- entity 7: 80040000.v_tpg (2 pads, 2 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev1
	pad0: Sink
		[fmt:RBG888_1X24/1920x1080 field:none]
		<- "80000000.v_hdmi_rx_ss":0 [ENABLED]
	pad1: Source
		[fmt:RBG888_1X24/1920x1080 field:none]
		-> "80100000.v_proc_ss":0 [ENABLED]

- entity 10: 80100000.v_proc_ss (2 pads, 2 links)
             type V4L2 subdev subtype Unknown flags 0
             device node name /dev/v4l-subdev2
	pad0: Sink
		[fmt:RBG888_1X24/1920x1080 field:none]
		<- "80040000.v_tpg":1 [ENABLED]
	pad1: Source
		[fmt:UYVY8_1X16/1920x1080 field:none]
		-> "video_cap output 0":0 [ENABLED]
```

---

## Topology ##

Media device topology is defined in the dts:
```
&v_hdmi_rx_ss {
    /* ... */
    ports {
        #address-cells = <1>;
        #size-cells = <0>;
        port@0 {
            reg = <0>;

            hdmi_rx: endpoint {
                remote-endpoint = <&tpg_in>;
            };
        };
    };
};

```

---

```
&v_proc_ss_0 {
    /* ... */
    ports {
        port@0 {
            /* Sink Pad */
            reg = <0>;
            xlnx,video-format = <XVIP_VF_RBG>;
            xlnx,video-width = <8>;

            scaler_in: endpoint {
                remote-endpoint = <&tpg_out>;
            };
        };

        port@1 {
            /* Sink Pad */
            reg = <1>;
            xlnx,video-format = <XVIP_VF_YUV_422>;
            xlnx,video-width = <8>;

            scaler_out: endpoint {
                remote-endpoint = <&vcap_in>;
            };
        };
    };
```

---

## Configuration ##

Configure formats for uvc streaming:
```
yavta --no-query /dev/v4l-subdev1 -w '0x009f0903 0'
media-ctl -v -V '"80040000.v_tpg":0\
	[fmt:RBG888_1X24 1920x1080 field:none]'
media-ctl -v -V '"80100000.v_proc_ss":0\
	[fmt:RBG888_1X24 1920x1080 field:none]'
```

---

## Use ##

- `/dev/videoX` is a standard V4L2 source
- Capture frames with yavta (for testing):
```
yavta -c10 -f RGB24 -s 1920x1080 --skip 7 -F /dev/video0
```
- Capture with gstreamer:
```
gst-launch-1.0 v4l2src device=/dev/video0 ! \
video/x-raw,format=RGB,width=1920,height=1080,\ 
framerate=60/1 ! multifilesink location="/run/frame%d"
```

---

## Summary ##

Advantages:
- Easy to define media topology.
- Standard set of ioctls to configure formats, etc.

Disadvantages:
- Can be hard to troubleshoot.
- Not the best format auto-negotiation (seems to vary based on driver).

---

## More information ##
- https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841873/Linux+Drivers
- https://linuxtv.org/downloads/v4l-dvb-apis/kapi/mc-core.html
- https://lwn.net/Articles/415714/
