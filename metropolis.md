### Device-tree setup

Note: on the chip they are mmc0-mmc2 but the dts uses mmc1-mmc3.

Having some issues moving the mmc0 (sdcard) card detect and wp pins.
Seems mmc1_pins does not need the wp pin (0x1A0).
u-boot was configuring the pin in actual card detect mode rather than as a
gpio. It seems u-boot does not need that pin at all.
