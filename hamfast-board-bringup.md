### ADV7611

Can read ID registers for both 7611's:
```
i2cget -y 2 0x4c 0xea
0x20
i2cget -y 2 0x4c 0xeb
0x51

i2cget -y 3 0x4c 0xea
0x20
i2cget -y 3 0x4c 0xeb
0x51
```

### ADV7511

```
i2cget -y 4 0x39 0xf5
0x75
i2cget -y 4 0x39 0xf6
0x11
```

Input style is Style 3, 16-bit (Table 17), Input ID=1 (separate syncs)


One-time setup:

```
v4l2-ctl -d /dev/video2 -c transmit_mode=1
media-ctl -v -V '"adv7611 2-004c":1[fmt:YUYV 1920x1080]'
media-ctl -v -V '"adv7611 3-004c":1[fmt:YUYV 1920x1080]'
media-ctl -v -V '"adv7511 4-0039":0[fmt:UYVY 1920x1080]'
python streamon.py
```

Every input hotplug:
```
v4l2-ctl --set-dv-bt-timings index=11 -d /dev/v4l-subdev1
v4l2-ctl --set-dv-bt-timings index=11 -d /dev/v4l-subdev2
```

Every output hotplug:
```
v4l2-ctl --set-dv-bt-timings index=12 -d /dev/v4l-subdev0
```

##### Hotplug/rolling video issue on boot

If video is garbled: ADV7511 register 0x15 and 0x16 are not being restored correctly: Should be set to 0x22 and 0xbd respectively.

Tried removing threaded IRQ handler completely which didn't help. Seems this is not an isr race condition as I initially thought (since the driver only had cursory v4l2 interrupt_service_routine support).

Seems setting the power down bit and clearing it gets it unstuck a bit - then I have to streamon and streamoff once in order to get video to start showing up.

```
v4l2-dbg -d /dev/v4l-subdev2 -s 0x41 0x60
v4l2-dbg -d /dev/v4l-subdev2 -s 0x41 0x40
```

Two issues:
1. On boot sometimes the output is orange and/or scrolling
2. After hotplug, doing a streamoff followed by format setup and streamon results in no output.

Output chip appears to be cause of difference in both cases. These two registers need to be reset to fix it:

```
v4l2-dbg -d /dev/v4l-subdev2 -s 0x15 0x22
v4l2-dbg -d /dev/v4l-subdev2 -s 0x16 0xbd
```

##### Video input/output TODO's

- Audio support/detection/configuration
- Format communication with Roger
- EDID passthrough
- HDCP

##### Driver TODO's

- [x] Why is adv7511 register 0x16 now showing up as 0x43 instead of 0x22?? (nevermind, subdev number changed)
- [x] Move adv7511 devicetree to feature dtsi
- [x] Move adv7611 devicetree to feature dtsi
- [x] Move vad-fpga-vid devicetree to feature dtsi
- [ ] Driver cleanup/renames/etc

##### vad-fpga-vid driver examples

Enumerate inputs: `v4l2-ctl -n`
```
from v4l2 import *
from fcntl import ioctl
from select import select
v = open('/dev/video0', 'rw')
inp = v4l2_input()
inp.index = 0
ioctl(f, VIDIOC_ENUMINPUT, inp)
inp.name, inp.type, inp.status
inp.index = 1
ioctl(f, VIDIOC_ENUMINPUT, inp)
(...)
inp.index = 2
ioctl(f, VIDIOC_ENUMINPUT, inp)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IOError: [Errno 22] Invalid argument
```

Similar for outputs, `v4l2-ctl -N`, use `v4l2_output()` instead

Find associated media device:
`ls /sys/class/video4linux/video0/device/mediaX`

##### EDID

```
v4l2-ctl -d /dev/v4l-subdev2 --get-edid > /tmp/edid
v4l2-ctl -d /dev/v4l-subdev0 --set-edid=pad=0,file=/tmp/edid
```

##### Input hotplug format change sequence

1. Application waits on source change event of adv7611 subdev
2. Application receives notification of hotplug
3. Application queries new detected timings
4. Application sets new timings on adv7611 subdev
5. Application sets timings on vad-fpga-vid device to change FPGA input format

Note: No need to update adv7611 format - width and height come from the timings! Everything else stays the same.

Set timings based on detected input:
```
from fcntl import ioctl
timings = v4l2_dv_timings()
inf = open('/dev/v4l-subdev1', 'rw')
fpga = open('/dev/video0', 'rw')
ioctl(inf, VIDIOC_G_DV_TIMINGS, timings)
ioctl(fpga, VIDIOC_S_DV_TIMINGS, timings)
timings.bt.width, timings.bt.height, timings.bt.pixelclock                  
(1920L, 1080L, 148500000L)
```

##### Output format change sequence

1. Application sets output format timings of adv7511 device
2. Application sets FPGA output format via vad-fpga-vid device

### Microblaze

https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842424/Execute+Microblaze+Application+from+PS+DDR
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841793/Utilizing+PS+memory+to+execute+Microblaze+application+on+Zynq+Ultrascale
https://www.xilinx.com/support/answers/69133.html

We needed the Microblaze to boot from DDR since the BRAM's aren't accessible to Linux.
First, configure the microblaze IP core to boot from the desire start address in DDR (in this case, 0x3f100000). Click Advanced, then Reset.
Then place all code and data, including Vectors, at the desired start address in DDR by generating a new Linker script.
Finally, if you want to debug in Eclipse, you need to create two run configurations. The first should program the bistream and A9 (if desired) as well as run the ps7_* stuff.
The second should only load the microblaze binary and nothing else. Also uncheck "Stop at main"

Loading with remoteproc:
```
echo -n /media/firmware/device-fw/ > /sys/module/firmware_class/parameters/path
modprobe mb_remoteproc firmware=HamFast.elf
```

Fix framerate issue on HDMI output:
```
devmem 0x800a00a8 32 0x081c1800
```
