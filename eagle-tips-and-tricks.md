### Enabling JTAG Access

To use JTAG while Linux is running, CPU idle must be disabled, otherwise the system will hang. The easiest way to do this is to change in at a u-boot prompt. Note that this must be done on every boot:

```
setenv bootargs $bootargs cpuidle.off=1
boot
```

Keep in mind that the CPU will run hotter since idling all cores will be
disabled. For this reason, it is not recommended to make this change in a
persistent manner (such as disabling CONFIG_CPU_IDLE or modifying the bootargs
in boot.scr).

### Custom FPGA bitstreams

#### Using

Custom bitstreams can be used on debug builds by copying the bitstream to a file
called `/boot/bitstream.bit` on the device. Note that by default, this file
won't exist. You can verify the bitstream is being used by looking at the serial
console output right after u-boot initiates boot of the device, where you will
see the date and time information of the bitstream printed. For example:

```
Hit any key to stop autoboot:  0
switch to partitions #0, OK
mmc0(part 0) is current device
Scanning mmc 0:1...
Found U-Boot script /boot.scr
960 bytes read in 7 ms (133.8 KiB/s)
## Executing script at 00000000
Detected external /boot/bitstream.bit
6708711 bytes read in 446 ms (14.3 MiB/s)
  design filename = "EAGLE_TOP;COMPRESS=TRUE;UserID=0XFFFFFFFF;Version=2019.2_AR73100"    
  part number = "xczu4eg-sfvc784-1-e"
  date = "2020/09/16"
  time = "15:21:06"
  bytes in bitstream = 6708576
zynqmp_align_dma_buffer: Align buffer at 0000000018000087 to 0000000017ffffc0(swap 0)
```

#### Removal

You might find yourself in a situation where the kernel hangs on boot due to a
change in the bitstream which requires a dts update. This is usually one of the
following:

- AXI Peripheral address change
- PL interrupt change
- PL reset/enable EMIO number change

If you find yourself in this situation, run the following at a u-boot prompt to
erase the bitstream and then boot the board:

```
ext4write mmc 0:$rootpart $kernel_addr_r /boot/bitstream.bit 0
boot
```

Technically, this overwrites the bitstream file with a 0-length file since
u-boot does not have an `ext4rm` function.
