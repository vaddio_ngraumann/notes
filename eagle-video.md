### USB Streaming

Needed to revert "Revert usb: dwc3: gadget: skip Set/Clear Halt when invalid", otherwise could only stream from 3.0 stream once without restarting uvc-gadget. 2.0 stream worked every time though.

1080p60 looks great. 4k can only manage 18fps. Maybe this is due to rework? Should run IBERT to check noise levels.

Simultaneous USB and HDMI with one scaler (same output resolution):
```
gst-launch-1.0 v4l2src do-timestamp=true device=/dev/video1 io-mode=4 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=60/1 ! queue ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=YUY2,width=1280,height=720 ! tee name=t ! queue ! v4l2sink device=/dev/video5 io-mode=5 t. ! queue ! v4l2sink device=/dev/video0 io-mode=5
```

Scaler on one "leg" of the tee locks up after several seconds, unsure why yet:
```
gst-launch-1.0 v4l2src do-timestamp=true device=/dev/video1 io-mode=4 ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=60/1 ! tee name=t ! queue ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=YUY2,width=1280,height=720 ! queue ! v4l2sink device=/dev/video0 io-mode=5 t. ! video/x-raw,width=1920,height=1080 ! queue ! v4l2sink device=/dev/video5 io-mode=5
```
