Boombox Rev A Board
--------------------------------------------------------------------------------
Uarts:
    PS Uart1:   pass, but shows random characters when power is not applied (Jeff notified)
    PL Camera:      pass (video working)
    PL Timberwolf:  pass (connected with MiniCom and interacted with TW console)
    PL FX3:         pass (streaming working)
    PL RS485:       pass (Joel's ezmic test script)
    PL Debug:       pass (manual connection to PC through FTDI cable)


LEDs:
    Ethernet: Green and orange activity lights  pass
    RGB LED:  On aux connector (pins verified)

Power:

Ethernet:
    Waiting on FPGA build for clock fix: http://jenkins.vaddio.com/jenkins/view/FPGA/job/Win10-FPGA-Xilinx/79/console

IR: N/A

SD Card: Yes

FPGA VERSION:
    Address: 0x43c00014
    [20:16] Board Revision
    [15:8] FPGA Major Version
    [7:0] FPGA Minor Version
        devmem 0x43c00014 32
        0x00000001

GPIOs:
    MIO:
        10: REV_BIT0    pass (bits are inverted)
        11: REV_BIT1    pass (bits are inverted)
        12: REV_BIT2    pass (bits are inverted)
        13: REV_BIT3    pass (bits are inverted)
        30: PS_SPARE_1  pass
        33: POE_T2P -   always reads as high even when powered with 15W cable (phone); Jeff looking into this.
        35: UI_BUTTON   pass
        39: LED_RED_N   pass
        46: LED_GREEN_N pass
        47: PS_SPARE_2  pass
        49: LED_BLUE_N  pass

    EMIO:
        54:   CODEC_RESET_N     pass
        55:   DSP_RESET_N       pass
        56:   USBC_EN_MUX_N     pass - when disabled, device still enumerates as high-speed
        57:   CAM_EN            pass
        61:   USB_RESET_N       pass
        62:   USB_INT_N
        63:   USB_CTL[0]
        64:   USB_CTL[10]
        65:   USB_CTL[2]
        66:   USB_CTL[3]
        67:   USB_CTL[4]
        68:   USB_CTL[5]
        69:   USB_CTL[7]
        70:   AMP_12V_EN
        71:   AMP_RST_N         pass
        72:   AMP_FAULT_N
        73:   AMP_PDN_N
        78:   CODEC_AUDIO_SW
        87:   METER_STROBE
        88:   USB_CTL[6]        pass (streaming works)
        89:   USB_CTL[10]

Temperature Sensor: Yes (watch -n0.5 "cat /sys/bus/iio/devices/iio\:device0/in_temp0_raw")

AIC:
    Detected via i2cdetect
    Fails to load about half the time - Resolved, result of serial flash emulator driver confusing the PL i2c controllers. Loading this before the FX3 is a workaround for now.

    Using Joel firmware 9.98 to generate 200Hz half max on the left, verified 200Hz sine wave on LOL pin. Also verified i2s input to FPGA.
    Using Joel Ezmic with sine wav firmware, verified input through AIC to fpga.

Timberwolf:
    Product code indicates boot mode
    # echo 0 > /sys/devices/soc0/timberwolf-reset/value
    # hbi_test -d 0 -r 0x022 2
    Read 2 bytes
    RD: addr 0x0022 = 0x022a

    Firmware/cfg loading seems to work:
    cat Microsemi_ZLS38063.0_E1.0.0_firmware.bin > /proc/hbi/dev_00/load_fw
    cat Boombox-ZLS38063-config_0.01.bin > /proc/hbi/dev_00/cfgrec
    cat /proc/hbi/dev_00/start_fw
    hbi_test -d 0 -r 0x022 2
    Read 2 bytes
    RD: addr 0x0022 = 0x94af

    Verifying interrupt pin:
    Ensure the interrupt pin is asserted (low on boot).
    Connect to timberwolf serial console with `minicom /dev/ttyS1`.
    Read the interrupt indication register register (0x0000) and ensure its value is 0x0001 which indicates a reset event. The register is self-clearing and will read as 0 a second time. It also will cause the interrupt pin to de-assert.
        .> rd 0000
         0000: 0001
        .> rd 0000
         0000: 0000
    Check the level of the interrupt pin and ensure it is now de-asserted (high).

    Audio in to fpga:
    TW> wr 0202 1C0C

    Loopback testing:
    Need special config from Steve Z. to route I2S input (I2S_SDIA) out both I2S (I2S_SDOA and I2S_SDOB) in TW crosspoints.

    Set gains:
devmem 0x406000F0 32 0x00008000
devmem 0x40600114 32 0x00008000
devmem 0x40600078 32 0x00008000
devmem 0x40600080 32 0x00008000
devmem 0x406000A4 32 0x00008000
devmem 0x4060009C 32 0x00008000

    Connect crosspoints:
devmem 0x40600000 32 0x80000000
devmem 0x40600004 32 0x20100000
devmem 0x40600008 32 0x08040000
devmem 0x4060000C 32 0x00000000

cat /media/firmware/device-fw/Microsemi_ZLS38063.0_E1.0.0_firmware.bin > /proc/hbi/dev_00/load_fw
cat /tmp/Boombox-ZLS38063_i2s_port2_loop3.cr2 > /proc/hbi/dev_00/cfgrec
cat /proc/hbi/dev_00/start_fw


Amp (TAS575):
    Enable fully USB bypass to route usb playback to line out:
devmem 0x406001A0 32 0x00000007
devmem 0x40600000 32 0x80002000
    Generate a test tone and play in audacity.

    de-assert powerdown pin, enable +12V, and de-assert reset
cd /sys/devices/soc0
echo 0 > amp-power-down/value
echo 1 > amp-12v-enable/value
echo 0 > amp-reset/value

    You can run these commands, or use the tas575m.py script:
    # Enable internal oscillator
    i2cset -y 1 0x2a 0x1b 0x00
    # Unmute and set volume *really low*
    i2cset -y 1 0x2a 0x07 0x6001 w
    i2cset -y 1 0x2a 0x08 0x6001 w
    i2cset -y 1 0x2a 0x09 0x6001 w
    # Set i2s word length to 16-bits:
    i2cset -y 1 0x2a 0x04 0x03
    # Set channels 1 and 2 to BD mode:
    i2cset -y 1 0x2a 0x20 0x00 0x89 0x00 0x00 i
    # Exit shutdown
    i2cset -y 1 0x2a 0x05 0x00

    * Circuit needs to be in BD mode

I2C:
    PS i2c0:
    root@zynq-boombox:~# i2cdetect -y -r 0
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
    10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    50: 50 51 52 53 54 55 56 57 -- -- -- -- -- -- -- --
    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70: -- -- -- -- -- -- -- --

    EEPROM works in u-boot with updateget/updateset

    PS i2c1 (emio):
        I2C bus is missing pullups. Glenn will enable internal ones in the FPGA. Jeff will add to next rev.
    root@vaddio-generic-54-10-EC-31-37-BD:~# i2cdetect -y -r 1
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
    10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    20: -- -- -- -- -- -- -- -- -- -- 2a -- -- -- -- --
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70: -- -- -- -- -- -- -- --

    PL i2c0 (part of serial flash emul)
    Disabled sf emul in device tree to probe usb mux:
    root@vaddio-generic-54-10-EC-31-37-BD:~# i2cdetect -y -r 3
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
    10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40: -- -- -- -- -- -- -- 47 -- -- -- -- -- -- -- --
    50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70: -- -- -- -- -- -- -- --

    PL i2c1:
    root@zynq-boombox:~# i2cdetect -y -r 2
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
    10: -- -- -- -- -- -- -- -- 18 -- -- -- -- -- -- --
    20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70: -- -- -- -- -- -- -- --

Motors: N/A

Camera:

MicPods:
    Verified with Joel's script:
    root@vaddio-generic-54-10-EC-31-37-BD:~# python easy_mic_inquiry.py
    Looking for devs...
    Found:
    /dev/ttyS0
    /dev/ttyS1
    /dev/ttyS2
    /dev/ttyS3
    /dev/ttyS4
    ...
    ['connect=3,EasyMic Table,1.11']
    .....
    Port /dev/ttyS0 doesn't easy mic
    Port /dev/ttyS1 doesn't easy mic
    Port /dev/ttyS2 doesn't easy mic
    Connected at /dev/ttyS3
    Port /dev/ttyS4 doesn't easy mic

    Verified full loopback from the PC using "Golden EZMic" loopback firmware:
    1. Connect USB record L and R to AIC speaker L and R in the Crosspoint and set the gain to 0dB.
    2. Connect EZMic in L and R to USB playback L and R in the Crosspoint and set the gain to 0dB.
    3. Generate a test tone in audacity.
    4. Set audacity playback and record devices to the boombox's USB devices.
    5. Click "record" and you should see the test tone returned. Unplug the ezmic and verify that the tone is no longer returned.

    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600130 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600134 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600154 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600150 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600088 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x4060008C 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x4060006C 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600068 32 0x00008000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600000 32 0x80000000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600004 32 0x02010000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x40600008 32 0x00000000
    root@vaddio-boombox-54-10-EC-31-37-BD:~# devmem 0x4060000C 32 0x00000804

Switches: N/A

Memory:
    All weekend:
    memtester 350M (passed 18 loops, then failed due to OOM)

    memtester 264M
    Ran 54 loops overnight 06/11
    Ran 46 loops overnight 06/12
    Ran 58 loops overnight 06/13
    Ran 204 loops over weekend 06/15

Ethernet Performance:
    Boombox as server:
        root@zynq-boombox:~# iperf3 -s
        warning: this system does not seem to support IPv6 - trying IPv4
        -----------------------------------------------------------
        Server listening on 5201
        -----------------------------------------------------------
        Accepted connection from 10.30.203.254, port 37142
        [  5] local 10.30.203.191 port 5201 connected to 10.30.203.254 port 37144
        [   25.089018] random: crng init done
        [ ID] Interval           Transfer     Bitrate
        [  5]   0.00-1.00   sec  77.8 MBytes   652 Mbits/sec
        [  5]   1.00-2.00   sec  86.3 MBytes   725 Mbits/sec
        [  5]   2.00-3.00   sec  86.5 MBytes   725 Mbits/sec
        [  5]   3.00-4.00   sec  85.6 MBytes   718 Mbits/sec
        [  5]   4.00-5.00   sec  86.5 MBytes   725 Mbits/sec
        [  5]   5.00-6.00   sec  85.8 MBytes   720 Mbits/sec
        [  5]   6.00-7.00   sec  86.5 MBytes   725 Mbits/sec
        [  5]   7.00-8.00   sec  85.7 MBytes   719 Mbits/sec
        [  5]   8.00-9.00   sec  86.5 MBytes   725 Mbits/sec
        [  5]   9.00-10.00  sec  85.7 MBytes   720 Mbits/sec
        [  5]  10.00-10.04  sec  3.18 MBytes   715 Mbits/sec
        - - - - - - - - - - - - - - - - - - - - - - - - -
        [ ID] Interval           Transfer     Bitrate
        [  5]   0.00-10.04  sec   856 MBytes   716 Mbits/sec                  receiver

    Boombox as client:
        root@zynq-boombox:~# iperf3 -c 10.30.203.254
        Connecting to host 10.30.203.254, port 5201
        [  5] local 10.30.203.191 port 56568 connected to 10.30.203.254 port 5201
        [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
        [  5]   0.00-1.01   sec   106 MBytes   882 Mbits/sec    0    208 KBytes
        [  5]   1.01-2.01   sec   105 MBytes   885 Mbits/sec    0    208 KBytes
        [  5]   2.01-3.00   sec   105 MBytes   885 Mbits/sec    0    208 KBytes
        [  5]   3.00-4.01   sec   106 MBytes   885 Mbits/sec    0    219 KBytes
        [  5]   4.01-5.00   sec   105 MBytes   885 Mbits/sec    0    219 KBytes
        [  5]   5.00-6.01   sec   106 MBytes   885 Mbits/sec    0    219 KBytes
        [  5]   6.01-7.01   sec   105 MBytes   884 Mbits/sec    0    219 KBytes
        [  5]   7.01-8.00   sec   105 MBytes   885 Mbits/sec    0    219 KBytes
        [  5]   8.00-9.01   sec   106 MBytes   885 Mbits/sec    0    219 KBytes
        [  5]   9.01-10.00  sec   105 MBytes   883 Mbits/sec    0    550 KBytes
        - - - - - - - - - - - - - - - - - - - - - - - - -
        [ ID] Interval           Transfer     Bitrate         Retr
        [  5]   0.00-10.00  sec  1.03 GBytes   884 Mbits/sec    0             sender
        [  5]   0.00-10.00  sec  1.03 GBytes   884 Mbits/sec                  receiver

USB:
    For Jeff: USB mux i2c conflicts with Fx3 i2c (serial flash emulator)

    R157 should be 900k +/- 1%?
    USB mux verification. No cable:
        root@vaddio-generic-54-10-EC-31-37-BD:~# i2cget -y 3 0x47 0x08
        0x00
        root@vaddio-generic-54-10-EC-31-37-BD:~# i2cget -y 3 0x47 0x09
        0x20
            CC1 (default), not attached

        Cable inserted orientation 1:
        root@vaddio-generic-54-10-EC-31-37-BD:~# i2cget -y 3 0x47 0x08
        0x30
            HIGH current mode detect
        root@vaddio-generic-54-10-EC-31-37-BD:~# i2cget -y 3 0x47 0x09
        0xb0
            Attached SNK (UFP), CC1, INT for CSR change

        Cable inserted orientation 2:
        root@vaddio-generic-54-10-EC-31-37-BD:~# i2cget -y 3 0x47 0x08
        0x30
            HIGH current mode detect
        root@vaddio-generic-54-10-EC-31-37-BD:~# i2cget -y 3 0x47 0x09
        0x90
            Attached SNK (UFP), CC2, INT for CSR change

        No change connecting cable to USB2.0 port or 3.0.

    Enumerates as 3.0 when mux is enabled
    Works in 2.0 mode when mux is disabled.
    usbc interrupt pin is de-asserted on boot, asserts once usb-c cable is inserted.

    Need stream, video, and usbctl service to test usb streaming. Enable test bits in Reg_Audio_FX3_Test to get stream to start:
        devmem 0x406001A0 32 0x00000007

    FX3 test patterns:
        The test patters are controlled by bits [1:0] of control_reg2 of the usb_outgen module.

        2'b00 = no test pattern
        2'b10 - test pattern 1
        2'b11 - test pattern 2
        address: 0x43c2004c
        Color bar pattern:
        devmem 0x43c2004c 32 0x80000083

    Audio bypass usb->usb:
    devmem 0x40600000 32 0x80001000

    IP Clipping near max above 7500 gain

Quad SPI:  
    Flash chip is different than schematic - is actually an AT25SF041
    Needed to add jedec ID to drivers/mtd/spi-nor/spi-nor.c:
    { "at25sf041",  INFO(0x1f8401, 0, 64 * 1024,   8, SECT_4K) },

    root@vaddio-boombox-54-10-EC-31-37-BD:~# echo "hello world" > /dev/mtd0
    root@vaddio-boombox-54-10-EC-31-37-BD:~# hexdump -C /dev/mtd0
    00000000  68 65 6c 6c 6f 20 77 6f  72 6c 64 0a ff ff ff ff  |hello world.....|
    00000010  ff ff ff ff ff ff ff ff  ff ff ff ff ff ff ff ff  |................|
    *
    00080000

    In u-boot (also needed JEDEC ID patch):
    Zynq> sf probe
    SF: Bank Address is not set
    SF: enter 3B address mode failed
    SF: Detected at24sf041 with page size 256 Bytes, erase size 4 KiB, total 512 KiB
    Zynq> sf read $loadaddr 0 16
    device 0 offset 0x0, size 0x16
    SF: 22 bytes @ 0x0 Read: OK
    Zynq> md.b $loadaddr 16     
    03000000: 68 65 6c 6c 6f 20 77 6f 72 6c 64 0a ff ff ff ff    hello world.....
    03000010: ff ff ff ff ff ff                                  ......
