### Kestrel

Using gst-interpipe on Kestrel. `vpe` really does not like copied buffers. Seems `make_writable` causes this, so using `passthrough-ts` prevents that. However, passthrough-ts prevents videorate from doing a good job scaling up frame rate.

```
0:00:01.710138248 17464   0x551ec0 DEBUG        GST_PERFORMANCE gstminiobject.c:317:gst_mini_object_make_writable: copy GstBuffer miniobject 0x5bc648 -> 0x5a5bd0
```

Caused an assertion failure in the vpe because it could not import the DMABUF. Seems only `passthrough-ts` does not copy the buffers.

Single interpipes:

```
gst-launch-1.0 v4l2src device=/dev/video3 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! interpipesink name=vcap sync=false async=false interpipesrc listen-to="vcap" stream-sync=passthrough-ts ! queue ! kmssink sync=false async=false qos=false interpipesrc listen-to="vcap" stream-sync=passthrough-ts ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! vpe num-input-buffers=8 device=/dev/video2 ! video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! ducatih264enc inter-interval=1 intra-interval=4 ! h264parse ! video/x-h264, mapping=/stream, alignment=au, framerate=30/1, parsed=true, stream-format=avc ! queue ! rtspsink name=sink service=554 mtu=1400 sync=false async=false qos=false
```

```
gst-launch-1.0 v4l2src device=/dev/video3 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! interpipesink name=vcap sync=false async=false interpipesrc do-timestamp=true listen-to="vcap" stream-sync=passthrough-ts ! queue ! videorate ! kmssink sync=false async=false interpipesrc do-timestamp=true listen-to="vcap" stream-sync=passthrough-ts ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! vpe num-input-buffers=8 device=/dev/video2 ! video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! ducatih264enc inter-interval=1 intra-interval=4 ! h264parse ! video/x-h264, mapping=/stream, alignment=au, framerate=30/1, parsed=true, stream-format=avc ! queue ! rtspsink name=sink service=554 mtu=1400 sync=false async=false
```

Create separate pipes with gstd-client:

```
gstd-client
pipeline_create src_vcap v4l2src device=/dev/video3 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! interpipesink name=vcap sync=false async=false
pipeline_create sink_rtstream interpipesrc listen-to="vcap" stream-sync=passthrough-ts format=GST_FORMAT_TIME ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! queue ! vpe num-input-buffers=12 device=/dev/video2 ! video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! queue ! ducatih264enc inter-interval=1 intra-interval=4 ! h264parse ! video/x-h264, mapping=/stream, alignment=au, framerate=30/1, parsed=true, stream-format=avc ! queue ! rtspsink name=sink service=554 mtu=1400 sync=false async=false
pipeline_play src_vcap
pipeline_play sink_rtstream

pipeline_create sink_hdmi interpipesrc listen-to="vcap" stream-sync=passthrough-ts format=GST_FORMAT_TIME ! queue ! kmssink sync=false async=false
pipeline_play sink_hdmi

pipeline_create sink_usb interpipesrc listen-to="vcap" stream-sync=passthrough-ts format=GST_FORMAT_TIME ! queue ! v4l2video2convert capture-io-mode=4 output-io-mode=5 ! queue ! video/x-raw,format=YUY2,width=1280,height=720 ! videorate average-period=500000000 ! video/x-raw,framerate=30/1 ! v4l2sink device=/dev/video5 io-mode=5 sync=false async=false
pipeline_play sink_usb
```

Important things:

Videorate requires `format=GST_FORMAT_TIME`. `average-period` seems to be important

### Eagle-USB

Here we can use `restart-ts` which seems to work best with videorate.

```
gstd-client
pipeline_create src_vcap v4l2src device=/dev/video0 io-mode=4 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2,colorimetry=bt709 ! queue ! interpipesink name=vcap sync=true async=false
pipeline_play src_vcap

pipeline_create sink_hdmi interpipesrc listen-to="vcap" is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue ! video/x-raw,width=1920,height=1080 ! kmssink plane-id=28 async=false sync=true force-modesetting=true modeset-caps=video/x-raw,format=YUY2,width=1280,height=720,framerate=60/1,interlace-mode=progressive connector-properties=s,hdmi-colorspace=1
pipeline_play sink_hdmi

pipeline_create sink_usb interpipesrc listen-to="vcap" is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue ! v4l2video2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=YUY2,width=1280,height=720 ! queue ! v4l2sink device=/dev/video5 io-mode=5 async=false sync=true
pipeline_play sink_usb

pipeline_create asrc_mic alsasrc device="hw:0,4" ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! queue max-size-time=10000000 ! audiochannelmix name=channelmix left-to-left=1.0 right-to-right=1.0 ! interpipesink name=mic_cap sync=true async=false
pipeline_play asrc_mic

pipeline_create asrc_usb alsasrc device="hw:1,0" ! audio/x-raw,format=S16LE,rate=48000,channels=2 ! queue max-size-time=10000000 ! audiochannelmix name=channelmix left-to-left=1.0 right-to-right=1.0 ! interpipesink name=usb_acap sync=true async=false
pipeline_play asrc_usb

pipeline_create sink_rtstream interpipesrc listen-to="vcap" is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue ! videorate skip-to-first=true max-rate=30 drop-only=true ! video/x-raw,framerate=30/1 ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12,width=1920,height=1080 ! x264enc speed-preset=ultrafast pass=quant quantizer=25 key-int-max=8 ! video/x-h264, mapping=/stream, profile=baseline ! rtspsink name=sink service=554 mtu=1400 sync=true async=false interpipesrc listen-to="mic_cap" is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue ! audiomixer name=mix ! faac ! audio/mpeg,mpegversion=4,rate=48000,channels=2,mapping=/stream ! queue ! sink. interpipesrc listen-to="usb_acap" is-live=true stream-sync=restart-ts format=GST_FORMAT_TIME ! queue ! mix.
pipeline_play sink_rtstream
```
