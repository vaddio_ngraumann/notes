## Useful audio stuff

#### Generate a constant in Audacity

First come up with a bit pattern:
0b0110011001000001 (26177)
Divide by 32768 to get an amplitude:
26177/32768 = 0.70123291015625
Generate a square wave with that amplitude at a very low (<1Hz) frequency
Select the top part of the wave (flat), copy it, paste it into a new track, contiuing to paste until the constant is as long as you need.
