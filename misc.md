## Vaddio

Buildy: http://lnx-terryc:4568/#buildy
Devices list: http://lnx-bjornh/devices/
Schematics: [/media/engrstorage/Common Design/Protel Deisgn](file:///media/engrstorage/Common\ Design/Protel\ Deisgn)

### Jenkins

I added another meta- repository (meta-gplv2 in this case) to bitbucket and
updated the fetch script, but Jenkins was failing to clone it because it
couldn't write to an NFS share. It turns out that clones of the repositories
are stored on a shared NFS drive to reduce bandwidth. I had to perform the
following to clone a new repository to the shared area:

1. ssh support@jenkins.vaddio.com
2. cd /srv/export/yocto
3. sudo mkdir 'repo name'
4. sudo chown support:support 'repo name'
5. git clone git://repo-path.git 'repo-name'
6. sudo chown -R jenkins:jenkins 'repo-name'

## Linux Stuff

### Yocto

Custom local.conf entries:

    # Enable buildhistory
    INHERIT += "buildhistory"
    BUILDHISTORY_COMMIT = "0"
    BUILDHISTORY_FEATURES = "image package"

##### Vaddio-specific stuff

Vaddio build setup to pull from your fork:

    export VAD_SCM_REPO=git@bitbucket.org/vaddio/vaddio_username-vng.git
    export VAD_SCM_OBJ=<Sha 1>
    export BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE VAD_SCM_REPO VAD_SCM_OBJ"

And always set MACHINE:

    export MACHINE=vaddio-<machine>

##### Camera stuff

Codec control mode:
Vaddio camera pretends to be a different Sony camera model to support some CODEC's.
Several bytes in the visca response change:

```
python
>>> import serial
>>> s = serial.Serial('/dev/ttyUSB2', baudrate=9600, timeout=1)
>>> s.write(bytearray([0x81, 0x09, 0x00, 0x02, 0xff]))
5
>>> print [hex(ord(c)) for c in s.readall()]
['0x90', '0x50', '0x0', '0x1', '0x5', '0x4', '0xe', '0xe', '0x2', '0xff']
# Enabled Codec Control
>>> s.write(bytearray([0x81, 0x09, 0x00, 0x02, 0xff]))
5
>>> print [hex(ord(c)) for c in s.readall()]
['0x90', '0x50', '0x0', '0x10', '0x5', '0x8', '0xe', '0xe', '0x2', '0xff']
```

### Video
Query all webcam formats:

    v4l2-ctl --list-formats-ext

### ftrace config

```
CONFIG_TRACEPOINTS=y
# CONFIG_NET_DROP_MONITOR is not set
CONFIG_FRAME_POINTER=y
CONFIG_NOP_TRACER=y
CONFIG_TRACER_MAX_TRACE=y
CONFIG_TRACE_CLOCK=y
CONFIG_RING_BUFFER=y
CONFIG_EVENT_TRACING=y
CONFIG_CONTEXT_SWITCH_TRACER=y
CONFIG_RING_BUFFER_ALLOW_SWAP=y
CONFIG_TRACING=y
CONFIG_GENERIC_TRACER=y
CONFIG_FTRACE=y
CONFIG_FUNCTION_TRACER=y
CONFIG_FUNCTION_GRAPH_TRACER=y
CONFIG_IRQSOFF_TRACER=y
CONFIG_PREEMPT_TRACER=y
CONFIG_SCHED_TRACER=y
CONFIG_FTRACE_SYSCALLS=y
CONFIG_TRACER_SNAPSHOT=y
CONFIG_TRACER_SNAPSHOT_PER_CPU_SWAP=y
CONFIG_BRANCH_PROFILE_NONE=y
# CONFIG_PROFILE_ANNOTATED_BRANCHES is not set
# CONFIG_PROFILE_ALL_BRANCHES is not set
# CONFIG_STACK_TRACER is not set
# CONFIG_BLK_DEV_IO_TRACE is not set
# CONFIG_PROBE_EVENTS is not set
CONFIG_DYNAMIC_FTRACE=y
# CONFIG_FUNCTION_PROFILER is not set
CONFIG_FTRACE_MCOUNT_RECORD=y
# CONFIG_FTRACE_STARTUP_TEST is not set
# CONFIG_RING_BUFFER_BENCHMARK is not set
# CONFIG_RING_BUFFER_STARTUP_TEST is not set
CONFIG_OLD_MCOUNT=y
CONFIG_BINARY_PRINTF=y
```

### On-target gdb

Build gdb with bitbake gdb
Install gdb and optionally gdb-doc
Install libc6, libc6-dbg, libthread-db from the same build to ensure compatibility.

### Building new VLC on 14.04

For more detail, see [this page](https://ubuntuforums.org/showthread.php?t=2141949).

Install dependencies:

    sudo apt-get install lua5.2 liblua5.2-dev libmad0-dev libavcodec-dev libavformat-dev libswscale-dev liba52-dev libxcb-shm0-dev libxcb-composite0-dev libxcb-xv0-dev libxml2-dev libgcrypt20-dev libx264-dev

Download vlc sources and extract.
Download live555 ([here](http://www.live555.com/liveMedia/)) and extract. Build it:

    cd live
    ./genMakefiles linux-64bit
    make -j8
    sudo make Install

This will install headers in /usr/local/include/liveMedia. vlc's configure
expects them in /usr/include, so we need to modify the configure script. Modify
vlc-<version>/configure:

1. Search for liveMedia
2. In each occurrence, change any reference to /usr or ${CONTRIB_DIR}/include
   to /usr/local and ${CONTRIB_DIR}/local/include, respectively. Also update
   the default value of LIVE555_PREFIX to /usr/local.

Configure and build vlc:

    ./configure --prefix=/usr/local --enable-live555
    make -j8

You can now run vlc with `./vlc`.

### bmaptool benchmarks

Using dd:
sh -c   0.02s user 1.13s system 1% cpu 1:44.66 total

bmaptool copy --nobmap:
bmaptool: info: copying time: 1m 41.2s, copying speed 9.4 MiB/sec

sudo bmaptool copy --nobmap:
bmaptool: info: copying time: 1m 41.5s, copying speed 9.4 MiB/sec

bmaptool create -o image.bmap image.img
bmaptool copy:
copying time: 1m 3.1s, copying speed 8.5 MiB/sec

bmaptool create -o image.bmap image.img
sudo bmaptool copy:
bmaptool: info: copying time: 1m 13.3s, copying speed 7.3 MiB/sec

To install newer version than what's on 14.04 repo (for Yocto)
wget http://download.tizen.org/tools/pre-release/Ubuntu_14.04/all/bmap-tools_3.3_all.deb
sudo apt install python-lzma python-gpgme pigz lzop python-support
sudo dpkg -i bmap-tools_3.3_all.deb

### On-target opkg database

I use this in conjuction with NFS to let me install any package. I couldn't figure out a good way to map the pacakge directory via NFS, so I just copied the whole thing to /home/root on the NFS filesystem. Then, add the following lines to yo:wur /etc/opkg/opkg.conf file:

    src/gz local-aarch64 file:///home/root/ipk/aarch64
    src/gz local-all file:///home/root/ipk/all
    src/gz local-ultrzed file:///home/root/ipk/ultrazed_eg_zynqmp

Obviously, arch names may need to be adjusted depending on your target. Then run opkg update. Now, you can opkg install any package from those directories, and the dependencies will be figured out for you.

You can serve from the deploy/ipk directory with: `python -m http.server 8000`

Then add ipk directories to opkg.conf:

```
src/gz armv7at2hf-neon http://10.0.0.6:8000/armv7at2hf-neon
src/gz all http://10.0.0.6:8000/all
src/gz am571x-kestrel http://10.0.0.6:8000/am571x_kestrel
```

For eagle:

```
src/gz aarch64 http://10.0.0.6:8000/aarch64
src/gz all http://10.0.0.6:8000/all
src/gz zynqmp-eagle-usb http://10.0.0.6:8000/zynqmp_eagle_usb
```

To update the index files, run `bitbake package-index`.

### Netconsole

Need to build with CONFIG_NETCONSOLE=m, then you can run (for exmaple):
modprobe netconsole 'netconsole=@/eth0,6666@10.30.203.254/68:05:ca:37:55:e9'

## Build issues

1. Random gcc failure fixed by revision:

    c360dcbda8358286306fbee1073ae00fc7854bbe

2. vng-updater.bb:
arm-poky-linux-gnueabi-libtool: link: arm-poky-linux-gnueabi-g++ -march=armv7-a -mthumb-interwork -mfloat-abi=softfp -mfpu=neon --sysroot=/home/nickg/workspace/vng/poky/build2/tmp/sysroots/vaddio-kronos -O2 -pipe -g -feliminate-unused-debug-types -fvisibility-inlines-hidden -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed -o update-eeprom update-eeprom.o Eeprom.o  -lvaddioutils -lrt -lpthread
| /home/nickg/workspace/vng/poky/build2/tmp/sysroots/vaddio-kronos/usr/lib/libvaddioutils.so: undefined reference to `std::__throw_out_of_range_fmt(char const*, ...)'

### Reproduction

To reproduce vng-updater failure, run:

1. For vaddio-plutus:

    bitbake libvaddioutils vng-updater -c cleansstate
    bitbake libvaddioutils vng-updater

2. For vaddio-shotzy:

    bitbake libvaddioutils vng-updater

### Fix
Cherry-picked these changes:

    444ae98131f7f9e962d102f408eb867ab53a452a
    a1abb63f0b7a629a5d8a0033d639078bc0a6d15c

Had to clean vng-web, vng-clish (Plutus), logrotate (Kronos)

## Vivado

Register for Xilinx account
Download web installer - 2015.2 for Antares/Cygnus Software dev (2015.4 for HW)
Install Design Edition, check box for SDK:

![xilinx-install.png](images/xilinx-install.png)

Once installed, set license path in /etc/profile:

export XILINXD_LICENSE_FILE=27010@license

Logout/login, then run Vivado. You must synthesize and implement the Design
(as far as I know). Then, go to File->Export->Export Hardware, then finally
File->Launch SDK (keep the defaults).

##### Device Tree Generation

Use SDK. Be sure to open Vivado first - it seems sometimes things don't generate consistently when Vivado is closed (for example, clock-frequency shows up as a string instead).

## emmc boot

[Writing a system image using u-boot and TFTP](https://boundarydevices.com/writing-system-image-emmc-using-u-boot-network/)

## Orion

There are two SPI flashes: one stores the FPGA image in a 2MB partition, and the fx3 image on the other partition. The second is a smaller 2MB spi flash which stores the valens image. The FPGA appears to switch between the two using the program_fpga and fpga_cfg_program lines. When both are '1', the mtd device spi points to the flash for the fpga/fx3. When '0', it points to the valens. So, when you load the m25p80 module, which device you talk to depends on the gpio state. Also, the 2MB first partition is key, because on the smaller flash the second partition is automatically disabled since it's past the end of the flash.

## Decrypting web updates
openssl smime -verify -CAfile ~/workspace/vng/meta-vaddio-ark/recipes-vaddio/pki/files/CA/Vaddio-Engineering-CA/Vaddio-Engineering-CA.crt -inform DER -in vaddio-app-debug-image-zynq-boombox-update.p7m -out output.dat

openssl enc -d -aes-256-cbc -md sha256 -pass file:/home/nickg/workspace/vng/meta-vaddio-ark/recipes-vaddio/pki/files/key/update.key -in output.dat -out decrypted.tar.gz

tar xf decrypted.tar.gz

## iperf benchmarking

Be sure to set CPU affinity to a core other than 0. Otherwise the network IRQ and iperf will fight for cpu resulting in lower throughput.

## strace

Strace can show a summary of usage with `strace -c -p <pid> -f`.

## perf

You can build perf on Ark by adding this line to your local.conf:
`WHITELIST_GPL-3.0 = "binutils"`
Add perf to IMAGE_INSTALL or whatever.

Run `perf top` for realtime tracing. Run `perf record <cmd>` to record a command, and `perf report` to view the report.

## git lfs

```
for b in `git branch ` ; do echo $b ; [ "$b" != "master" ] && git branch -D $b ; done

for d in `find ../vng-firmware-archive -mindepth 1 -maxdepth 1 -type d | sort` ; do echo ${d} ; b=`basename ${d}` ; [ "${b}" = ".git" ] && continue ; git checkout master ; git checkout -B `basename ${b}` ; cp -r ${d} . ; git add . ; git commit -m "Import from vng-firmware-archive revision d6fc2f5b3e3294054310462730514458b9692c96" ; done
```
