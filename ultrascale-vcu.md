## References

Some answer records that are good resources:

[2018.1 - Zynq UltraScale+ MPSoC - Video Codec Unit (VCU) - What video formats are supported in GStreamer?](https://www.xilinx.com/support/answers/70645.html)

PetaLinux
[How to format SD card for SD boot](http://www.wiki.xilinx.com/How+to+format+SD+card+for+SD+boot)
    TLDR: 1GB bootable FAT32 partition, rest ext4. Put BOOT.BIN and image.ub in first part.

Build full boot.bin image for ultrascale with petalinux:
    petalinux-build
    petalinux-package --boot --fpga --u-boot --pmufw --force

## gstreamer pipelines

BBB on moitor
gst-launch-1.0 filesrc location="bbb_sunflower_2160p_30fps_normal_avc.mp4" ! qtdemux name=demux demux.video_0 ! h264parse ! omxh264dec ! videocrop left=960 right=-1 top=540 bottom=-1 ! video/x-raw,width=1920,height=1080 ! queue max-size-bytes=0 ! kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=1

Setup ncessary for h.264/265 stuff:
devmem 0xFD3A0008 w 0x3
devmem 0xFD3A001C w 0x3
devmem 0xFD3B0008 w 0x3
devmem 0xFD3B001C w 0x3
devmem 0xFD3A0004 w 0xF
devmem 0xFD3A0018 w 0xF
devmem 0xFD3B0004 w 0xF
devmem 0xFD3B0018 w 0xF

ip-mode=2 control-rate=2 target-bitrate=20000 stride=256 sliceHeight=64 ! h265parse ! queue ! rtph265pay ! udpsink host=10.30.203.254 port=50000 buffer-size=20000000 async=false max-lateness=-1 qos-dscp=60

Show USB3 camera on monitor:
gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,width=1920,height=1080,framerate=60/1 ! queue max-size-bytes=0 ! kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=1

gst-launch-1.0 filesrc location="bbb_sunflower_2160p_30fps_normal_avc.mp4" ! qtdemux ! h264parse ! omxh264dec ip-mode=1 op-mode=1 ! omxh265enc ip-mode=2 control-rate=2 target-bitrate=20000 stride=256 sliceHeight=64 ! h265parse ! queue ! rtph265pay ! udpsink host=10.30.203.254 port=50000 buffer-size=20000000 async=false max-lateness=-1 qos-dscp=60

BBB IP stream:
gst-launch-1.0 filesrc location=./bbb_sunflower_2160p_30fps_normal_avc.mp4 ! qtdemux name=demux demux.video_0 ! h264parse ! omxh264dec ! queue ! omxh264enc gop-length=240 periodicity-idr=240 control-rate=low-latency target-bitrate=5000 cpb-size=1000 gop-mode=low-delay-p ! queue ! rtph264pay ! udpsink host=10.30.203.254 port=50000 max-lateness=-1 qos-dscp=60 async=false max-bitrate=500000000

IP stream camera to PC (udpsink)
gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=\(string\)YUY2 ! videoconvert n-threads=4 ! video/x-raw,format=\(string\)NV12 ! queue max-size-bytes=0 ! omxh264enc gop-length=240 periodicity-idr=240 control-rate=low-latency target-bitrate=5000 cpb-size=1000 gop-mode=low-delay-p ! queue ! rtph264pay ! udpsink host=10.30.203.254 port=50000 max-lateness=-1 qos-dscp=60 async=false max-bitrate=500000000

Decode on PC:
gst-launch-1.0 udpsrc port=50000 caps="application/x-rtp, media=video, clock-rate=90000, payload=96, encoding-name=H265" ! rtpjitterbuffer latency=1000 ! rtph264depay ! h264parse ! avdec_h264 ! xvimagesink

gst-launch-1.0 -v v4l2src io-mode=4 device=/dev/video0 ! video/x-raw,format=NV12,width=1920,height=1080framerate=30/1 ! kmssink driver-name=xilinx_drm_mixer

RTSP server BBB:
rtsp-server -p 554 -u "/stream" "filesrc location=./bbb_sunflower_2160p_30fps_normal_avc.mp4 ! qtdemux name=demux demux.video_0 ! h264parse ! omxh264dec ! queue ! omxh264enc gop-length=240 periodicity-idr=240 control-rate=low-latency target-bitrate=5000 cpb-size=1000 gop-mode=low-delay-p ! queue ! rtph264pay perfect-rtptime=false pt=96 name=pay0"

RTSP camera h.264:
rtsp-server -p 554 -u "/stream" "v4l2src device=/dev/video0 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=\(string\)YUY2 ! videoconvert n-threads=4 ! video/x-raw,format=\(string\)NV12 ! queue max-size-bytes=0 ! omxh264enc gop-length=240 periodicity-idr=240 control-rate=low-latency target-bitrate=5000 cpb-size=1000 gop-mode=low-delay-p ! h264parse ! queue ! rtph264pay perfect-rtptime=false pt=96 name=pay0"

RTSP camera h.265:
rtsp-server -p 554 -u "/stream" "v4l2src device=/dev/video0 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=\(string\)YUY2 ! videoconvert n-threads=4 ! video/x-raw,format=\(string\)NV12 ! queue max-size-bytes=0 ! omxh265enc gop-length=240 periodicity-idr=240 control-rate=low-latency target-bitrate=5000 cpb-size=1000 gop-mode=low-delay-p ! h265parse ! queue ! rtph265pay perfect-rtptime=false pt=96 name=pay0"

Stream vaddio camera and decode with hard h.264 (requires a2e update for main profile). Also unsure why latency must be >= 1000 otherwise lots of frames drop.
gst-launch-1.0 -v rtspsrc location="rtsp://10.30.203.66/vaddio-roboshot-hdbt-stream" name=rtspsrc0 port-range="3000-3005" latency=1000 ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! capsfilter caps=video/x-h264,stream-format=byte-stream,alignment=au ! h264parse ! omxh264dec ! queue max-size-bytes=0 ! fpsdisplaysink text-overlay=false video-sink="kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=1"

Stream vaddio camera and decode with soft h.264:
gst-launch-1.0 -v rtspsrc location="rtsp://10.30.203.81/vaddio-roboshot-uhd-stream" name=rtspsrc0 port-range="3000-3005" latency=0 ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! h264parse ! avdec_h264 max-threads=4 ! videobox autocrop=true ! "video/x-raw, width=1920, height=1080" ! queue max-size-bytes=0 ! fpsdisplaysink text-overlay=false video-sink="kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=1"
Result: 5-10fps, ~30-70%cpu (mostly one core)

Decoding from 8148 (Matrix Pro) requires adding some latency to the rtspsrc to avoid dropping all the frames due to frame too late message. I found >200ms to be a good number. Also the reduced-latency seems important (taken from Xilinx's streaming example).
gst-launch-1.0 rtspsrc location="rtsp://10.30.203.78/vaddio-av-bridge-stream" name=rtspsrc0 port-range="3000-3005" latency=200 ! rtpjitterbuffer ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! h264parse ! omxh264dec latency-mode="reduced-latency" ! queue max-size-bytes=0 ! fpsdisplaysink text-overlay=false video-sink="kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=1"

Transcode bbb with baseline:
gst-launch-1.0 filesrc location="bbb_sunflower_2160p_30fps_normal_avc.mp4" ! qtdemux name=demux demux.video_0 ! h264parse ! omxh264dec ! omxh264enc ! video/x-h264 ! filesink location="bbb_sunflower_2160p_30fps_normal_avc_baseline.mp4"

## Petalinux

In order to build gstreamer1.0-libav in petalinux, I had to modify the following file: /opt/petalinux-v2018.1-final/components/yocto/source/aarch64/layers/meta-petalinux/conf/distro/include/petalinux-features.conf

and add the following line to LICENSE_FLAGS_WHITELIST:
    commercial_${MLPREFIX}gstreamer1.0-libav

## EV Part notes

h.264 encoder expects NV12 or NV16 (planar). You have to convert YUY2 (packed) using videoconvert which is slow-ish, even with n-threads=4. Ideally we would use an IP in the FPGA for this.


### a2e deconding of EV

"scaling matrix is present and not handled": Seems to be alleviated by setting scaling-list=0

On EV side:
/usr/bin/gst-launch-1.0 rtspsrc location="rtsp://10.30.203.82/stream" name=rtspsrc0 port-range="3000-3005" latency=0 rtspsrc0. ! capsfilter caps="application/x-rtp,media=video" ! rtph264depay ! capsfilter caps=video/x-h264,stream-format=byte-stream,alignment=nal ! h264parse ! capsfilter caps=video/x-h264,stream-format=byte-stream,alignment=nal ! filesink location="/dev/h264s" buffer-mode=2 blocksize=800000 async=false

On Plutus side:
rtsp-server -p 554 -u "/stream" "v4l2src device=/dev/video0 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! videoconvert n-threads=4 ! video/x-raw,format=NV12 ! queue max-size-bytes=0 ! omxh264enc gop-length=60 periodicity-idr=60 control-rate=disable target-bitrate=5000 cpb-size=10000 gop-mode=low-delay-p scaling-list=0 entropy-mode=0 num-slices=1 ! video/x-h264,profile=main,level=(string)4 ! h264parse ! queue max-size-bytes=0 ! rtph264pay perfect-rtptime=true pt=96 name=pay0"

### rtspsink

```
gst-launch-1.0 rtspsink service=554 name=sink v4l2src device=/dev/video0 ! video/x-raw,width=1920,height=1080,framerate=30/1,format=YUY2 ! videoconvert n-threads=4 ! video/x-raw,format=NV12 ! queue max-size-bytes=0 ! omxh264enc gop-length=240 periodicity-idr=240 control-rate=low-latency target-bitrate=5000 cpb-size=1000 gop-mode=low-delay-p ! queue ! video/xh264,mapping=/stream ! sink. audiotestsrc ! audio/x-raw,rate=48000 ! faac ! audio/mpeg,mapping=/stream ! sink.
```
gst-launch-1.0 cpu usage: 58% (~70 % on 2 cores, ~60 on one, ~40 on one). Keep in mind this is using videoconvert in software and uvc streaming.

Rtsp server:

rtsp-server cpu usage: 58%
