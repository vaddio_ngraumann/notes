# Eagle review rev 0

### Zynq
- Boot mode pins x (add JTAG per emmc discussion)
- eMMC boot
- Test LEDS x
- GEM1
    - Pins x
- I2C busses 0, 1
    - Pullups   x
    - addresses x
- UARTs
- Security?
- PL initialization/strapping (High-Z vs pullup, etc)
- Rev bits

Notes:
- PS_ERROR_STATUS test point too?
- Any PMU stuff we need?
- Cortex-R stuff?
- Sysmon can be addressed as I2C slave??

### eMMC
- Pins
    - Card detect/DS?

### Networking
- PS GEM1
- DP83867 phy setup
    - Clock output appears enabled due to default straps
- External TSU clock?

### I2C Busses
- Pullups on each
- Addresses
- PS/PL

##### Bus 0
PMIC:           0x30 (can be 0x30-0x3f with variable Raddr)
EEPROM:         0x50-0x57
Security Chip:  0xc0

##### Bus 1
FAN5702:        0x36
J7:             Any

##### Bus 2
TC358840_HDMI:  0x0f (depends on strap INT pin)
AIC:            0x18
TC358840_CAM:   0x1f (depends on strap INT pin)
USB Mux:        0x47 or 0x67 (depends on ADDR pin) - recommend bus 0
ADV7513:        0x72 or 0x7A (depends on strap PD/AD pin)

##### Motors
Pan:    0x50
Tilt:   0x52

### HDMI Inputs
- Compare w/reference
- Ensure routed: I2C, I2S, interrupt

### HDMI Output
- ADV7513 differences vs ADV7511 (same Linux driver)
- Compare w/Hamfast
- I2C, I2S

Notes:
    - PD pin needs external pullup/pulldown to select I2C address
    - Only two I2S are routed (versus 4 on the in)

### USB
- Full review (new)
- 2.0 and 3.0
- DFU capability
    - 2.0 only

Notes:
    - Pullup/down on ULPI reset? (required per datasheet, maybe we can use internal zynq pullup/down)
    - USB0_ID: Needs pullup (is OD)
    - USB0: CPEN and USBC_VBUS?
    - USB0 and USB1: VBUS needs external resistor per USB3320 datasheet Table 5-7
    - USBC_EN_MUX_N: Should we pull this up/down externally?
    - USB1: Think about: How easily could we make this a host port or Type-C port instead on future populations
HD3SS3220:
    - USB_SDA and USB_SCL should go on a PS I2C bus
    - CURRENT_MODE should have a pull for DFP mode per datasheet
    - Should we connect VCONN_FAULT_N for host mode feedback?

### Timberwolf
- Compare with Boombox

### Daughterboard
- Make sure it has everything we may need (UART for BLE?)
- 8x pins going to PL plus i2c on PS, 3.3V, should be fine

### LEDs
- S3 connected to FAN5702: confused about how this works
- I2C for this could probably be on i2c0 if desired (maybe even better, would eliminate switch)

### EEPROM
- Address is wrong, technically 0x50-0x57 7-bit LSB

# Review for Rev A

### PCAL6408A i2c gpio expander

Reset should be pulled through pullup resistor, not tied directly, per datasheet.
- Jeff took are of 12/6/19

### I2c Map:

Bus 0:
GPIO Expander U4: 0x20
PMIC U21: 0x30
EEPROM U18: 0x50-0x57
Crypto Chip U12: 0xc0 (programmable)

Bus 1:
LED driver U31: 0x36
Note: Daughter board could have i2c devices

Bus 2:
Camera HDMI Rx U29: 0x0f
AIC Codec U2: 0x18
HDMI Rx U11: 0x1f
USB Mux U5: 0x47
HDMI Tx U16: 0x72

# Review for rev D

R166 name is confusing in changelog - makes it seem like one DDR termination resistor got added back in.
Same with R204 and C239, maybe add descriptions for removed parts.

LVDS_CAM_RESET: Can this ever be 5V logic like the COM port? May need level shifting. (only 323 can be 5V, this is good).

I2S: Mentioned added terminations, but I only see series terminations on the HDMI input I2S bus and SDOA/B on the Timberwolf. 33ohm terminations on the ADV7513 were removed?
