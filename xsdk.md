# Xilinx SDK Setup

So far for Antares etc. in 2015.2. Copied from Ian's xsdk.docx

### Instructions

Bring new FPGA stuff into build:
- get the following files: PROJECT_TOP.hdf, PROJECT_TOP.bit, Project.elf
- the \_hw file needs to be updated with the new .hdf file (right click "Change
  Hardware Platform Specification")
- right click \_fsbl_bsp and "Regenerate BSP Sources"
- right click \_fsbl "Build Configurations" and clean all & build all
- copy `project_fsbl.elf` to `meta-vaddio/recipes-bsp/u-boot/u-boot-xlnx/vaddio-project/fsbl.elf`
- rename `project_fsbl.elf` to `fsbl.elf`

Put locally built uboot/fsbl onto HW:
- Run local u-boot-xlnx bitbake build
- copy the actual elf file from here-ish (`/home/ian/yocto/poky/build/tmp/work/vaddio_antares-poky-linux-gnueabi/u-boot-xlnx/v2014.01-xilinx+gitAUTOINC+VAD-r0-VNG-115-6027-5/deploy-u-boot-xlnx`) to here-ish (`/home/ian/workspace/antares`)
- Open XSDK
- GOTO: Xilinx Tools -> Create Zynq Boot Image
- Import the existing output.bif
- Update the u-boot.elf
- Create Image
- Copy the newly created BOOT.bin over to the "boot" partition of the SD card

To Create a New XSDK Project:
- Create \_hw project with the .hdf file
- Create \_fsbl_bsp with the \_hw project
- Add xilffs and xilrsa support libraries to the BSP
- Create the \_fsbl by making a new application project with the \_hw and
  \_fsbl_bsp files under the Zynq FSBL template
- Xilinx Tools > Repositories > New Local Repository > /home/ian/git/dts_generator
- Create \_dts make new BSP project, select OS "devicetree"

### XSDK 2015 Necessary BSP Modification

Modify the BSP:

- system.mss source tab: change xilffs from 3.2 to 2.2.
- Regenerate BSP Sources.

Creating the Zynq Boot Image:

![creating-zynq-boot-image.png](images/creating-zynq-boot-image.png)

##### Nick's Note
It turns out this issue is caused by the FatFS bug which is resolved in
[this](https://github.com/Xilinx/embeddedsw/commit/759f8fa904ec6d3818d829ee20ccc9e8c9cf9f92) GitHub commit, or in versions 0.10c and later of FatFS.

### Generating .bin automatically

Add this to the Post-build step command in the Build Steps tab:

    arm-xilinx-eabi-objcopy -O binary ${BuildArtifactFileName} ${BuildArtifactFileBaseName}.bin
