### 8/8/18

Maybe we can use dfu mode with a separate configuration to load the firmware? Use dfu-util.

### Portage

The ChromiumOS build uses Gentoo's build system called portage. You can find their "layer" at: https://chromium-review.googlesource.com/admin/repos/chromiumos/overlays/portage
Which is used by: https://chromium-review.googlesource.com/admin/repos/chromiumos/overlays

We'd need to add an ebuild which has "RDEPEND=app-mobilephone/dfu-util"

### Getting started

Note: Simple Chrome is more intended for people who want to build Chrome for ChromiumOS. However, it gave a good starting point for building Chromium OS to try out the VM. If you want to actually develop Chromium OS, you should follow the [Developer Guide](https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Install-depot_tools).

We want to Set up KVM with [Chrome OS VM for Chromium Developers](https://chromium.googlesource.com/chromiumos/docs/+/master/cros_vm.md)

Clone `depot_tools` and add to your PATH as shown in [depot_tools installation](http://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up).

Be sure to follow each prerequisite first.
The Getting Started portion of Simple Chrome is important! (particularly, the .gclient update)
I ran build/install-build-deps.sh with the --no-arm flag.

Build a simple chrome image:
export BOARD=amd64-generic
cros chrome-sdk --board=$BOARD --log-level=info --nogoma
gn gen out_$SDK_BOARD/Release
autoninja -C out_${SDK_BOARD}/Release chrome chrome_sandbox nacl_helper

Building this does not seem to have worked all the way?

Did a full Chromium OS build following instructions on the [Developer Guide](https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md#Install-depot_tools).


One change from the guide, you need to tell image_to_vm.sh that you want to use the test image:
./image_to_vm.sh --board=${BOARD} --test_image


### Camera troubleshooting

Video4linux device shows up. Can capture frames with yavta.
Syslog seems OK:
2018-09-06T10:21:44.452640-05:00 INFO cros_camera_service[3496]: StartOnThread(): SuperHAL started with 1 modules and 0 built-in cameras
2018-09-06T10:21:44.453301-05:00 INFO cros_camera_service[3496]: RegisterCameraHal(): Registered camera HAL
2018-09-06T10:21:44.457702-05:00 INFO cros_camera_service[3496]: OnDeviceAdded(): New usb camera device at /dev/video0 vid: 046d pid: 0994
...
2018-09-06T10:28:19.261924-05:00 INFO cros_camera_service[3496]: OnQueryVersionOnThread(): Bridge ready (version=1)
2018-09-06T10:28:19.262520-05:00 INFO cros_camera_service[3496]: GetCameraInfo(): camera_id = 0, facing = 2

Build camera tool package:
emerge-${BOARD} cros-camera-tool
cros deploy localhost:9222 cros-camera-tool
cros-camera-tool --foreground modules list

This tool is looking for /dev/media* devices, but we only have /dev/video* devices. On my Ubuntu box, /dev/media0 indicates it's using the 'media' kernel module. The chrome os build does not have this module present nor /sys/modules/media, so it probably needs to be enabled in the kernel config.

Need to build a new kernel with CONFIG_MEDIA_CONTROLLER=m so we can load the 'media' module.

[Chromium Kernel FAQ](https://chromium.googlesource.com/chromiumos/docs/+/master/kernel_faq.md)

Find where kernel lives:
cros_workon --board=$BOARD --all info | grep kernel

Build kernel:
FEATURES="noclean" cros_workon_make --board=${BOARD} --install chromeos-kernel-4_4

Enable CONFIG_MEDIA_CONTROLLER. Ended up adding it to list for vivid (really hacky, I know) in ~/trunk/src/third_party/chromiumos-overlay/eclass/cros-kernel2.eclass

Build kernel again.

Update kernel on target:
~/trunk/src/scripts/update_kernel.sh --remote localhost --ssh_port 9222

(this may not be needed):
Missing libcamera-v4l2-device hal. Need to build and install:
emerge-${BOARD} media-libs/cros-camera-libcamera_v4l2_device
cros deploy localhost:9222 media-libs/cros-camera-libcamera_v4l2_device

Need to add camera info to /etc/camera/camera_characteristics.conf:
```
camera0.lens_facing=0
camera0.sensor_orientation=0
camera0.module0.usb_vid_pid=046d:0994
camera0.module0.lens_info_available_focal_lengths=1.64
camera0.module0.lens_info_minimum_focus_distance=0.22
camera0.module0.lens_info_optimal_focus_distance=0.5
camera0.module0.lens_info_available_apertures=2.0
```

### Useful resources

[Chrome OS Design Documents](https://www.chromium.org/chromium-os/chromiumos-design-docs)
