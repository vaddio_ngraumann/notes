### jsmpeg

From PC:
```
gst-launch-1.0 v4l2src device=/dev/video2 ! video/x-raw,width=640,height=360,framerate=15/1 ! queue ! videoconvert ! queue ! avenc_mpeg1video compliance=-1 bitrate=1000000 ! mpegtsmux ! queue ! curlhttpsink location=http://127.0.0.1:8081/secret
```

From Cerberus:
```
gst-launch-1.0 v4l2src device=/dev/video0 ! queue ! videorate ! image/jpeg,framerate=1/1 ! queue ! jpegparse ! jpegdec ! queue ! videoscale ! video/x-raw,width=320,height=240 ! queue ! videoconvert ! videorate ! video/x-raw,framerate=30/1 ! avenc_mpeg1video bitrate=1000000 ! mpegtsmux ! queue ! curlhttpsink location=http://127.0.0.1:8081/secret
```

```
gst-launch-1.0 v4l2src device=/dev/video0 ! queue ! videorate ! image/jpeg,framerate=1/1 ! queue ! jpegparse ! jpegdec ! queue ! videoscale ! video/x-raw,width=320,height=180 ! queue ! videoconvert ! queue ! videorate ! video/x-raw,framerate=15/1 ! avenc_mpeg1video bitrate=100000 strict=-1 ! queue ! mpegtsmux ! queue ! curlhttpsink location=http://127.0.0.1:8081/secret
```

```
gst-launch-1.0 videotestsrc ! video/x-raw,width=640,height=360,framerate=30/1 ! avenc_mpeg1video bitrate=1000000 strict=-1 ! queue ! mpegtsmux ! queue ! curlhttpsink location=http://127.0.0.1:8081/secret
```
