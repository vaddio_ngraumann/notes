### Vitis AI 04/16/20

Unified Runtime APIs
- Helps in the early phase when youa re not sure which platform you want to use
- Not as efficient

Deep Learning Processing Unit (DPU) ?

"AI Model Zoo"

Vivado vs Vitis flow?

ADAS: 4 channels: 25fps

Q: Possible to use Vitis AI with PetaLinux project?
A: Yes, follow TRD using Vivado flow. Integrate w/ Vitis AI.

Q: Zynq-7000 support?
A: Technically  yes, but some pre/post processing happens on the A cores which the Zynq is a little weak for.
