### UltraScale dev kit

Problem:
Initially the board did not boot, fan did not run, and power good LED did not
light up.

Solution:
Need to remove JP1 on SOM card itself, see: http://zedboard.org/content/ultrazed-som-power-regulator-shunt
I installed the JP1 on the carrier board, and it didn't seem to make a difference
one way or the other.

To boot, need to comment out two lines in psu_init.c:

    //mask_poll(SERDES_L1_PLL_STATUS_READ_1_OFFSET,0x00000010U);
    //mask_poll(SERDES_L3_PLL_STATUS_READ_1_OFFSET,0x00000010U);

And rename psu_init.tcl to something like psu_init_new.tcl, then comment:

    #mask_poll 0XFD4063E4 0x00000010
    #mask_poll 0XFD40E3E4 0x00000010

Finally point the run configuration at psu_init_new.tcl.

(see http://zedboard.org/content/aes-zu3eges-1-sk-g-sktutorial20164-mask-poll-failed-address-0xfd4023e4-mask-0x00000010)

I was reading through these notes and realized I haven't had to do this in a
while. Perhaps it was addressed in Vivado 2017.2 or something? I also did find
later on that there are some clock issues with the Avnet IOCC, specifically,
you need to remove JP1 on the carrier card, and ensure that J1 and J2 both have
jumpers on pin 1-2.

[Thread about Yocto support for UltraZed-EG board](http://zedboard.org/content/yocto-machine-conf-ultrazed-eg)

[Good article about UltraZed development](https://www.invincealabs.com/blog/2017/03/ubuntu-ultrazed/)

[Forum post about it including GitHub link](http://ultrazed.org/content/ubuntu-server-ultrazed)

[Mentor Graphics has someone working on UltraZed-EG IOCC support](https://github.com/MentorEmbedded/mpsoc-linux-xlnx/pull/1/commits/5ce6f21e45d6784f9d1704e61fa81ad731117f08)

[UltraZed Chronicles (Blog)](http://www.ultrazedchronicles.com/)

### Xilinx Linux/u-boot

- [Xilinx Github](https://github.com/Xilinx)
- [Xilinx Linux Info](http://www.wiki.xilinx.com/Linux)
- [PetaLinux](http://www.wiki.xilinx.com/PetaLinux)

### Vivado

Make sure you select the "Engineering Sample" device installation option!

The chip on the SOM is the ES1 silicon. You need to add a license in order for
Vivado to see it. Follow the steps on the card included with the dev kit:

- Go to xilinx.com/getlicense and enter the voucher code to generate a node-
  locked license.
- Run /opt/Xilinx/Vivado/2016.4/bin/vlm to generate the node info required.
- Create a text file called init.tcl and enter the following line:

    enable_beta_device*

- Save and close the file.
- Place it in /opt/Xilinx/Vivado/2016.x/scripts
- As of 2017.3, it appears you need to use Vivado_init.tcl in the same directory instead, otherwise some submodules will fail.

Host info: 2c4d54ef12f8

Had to set up ipvlan so that eth0 is seen by Vivado. Could've also used macvlan,
but I wanted eth0 to have the same MAC as wlan0 for simplicity.

Also need to install digilent jtag drivers:
/opt/Xilinx/SDK/2016.4/data/xicom/cable_drivers/lin64/install_script/install_drivers$ sudo ./install_digilent.sh

I had an issue with the PLL settings not updating when I changed it in the
ZynqMP IP configuration. All of that information should be contained in the hdf
(hardware definition) file according to sources online. I ended up deleting the
whole .sdk directory, re-exporting the hardware defintion, and recreating the
fsbl and pmufw projects in Vivado, after which the change finally showed up.

Looks like right-clicking on the \_bsp projects for the fsbl and pmufw and
selecting "Re-generate BSP Sources" solves the issue of things not updating
properly when you re-export the hardware definition. For some reason, the SDK
does not warn when the hardware definition changes even though it should
according to the preferences (Xilinx SDK->Hardware Specification).

**Important** If there is ever weirdness in Vivado, make sure to go to the
IP Sources window, right-click on the top-level design, and select "Reset Output
Products." This is especially true if you change any of the IP settings,
including settings for the PS block.

Sometimes, if you change a setting in the MIO/EMIO devices setup, it can cause
the SDK's settings to get messed up. I changed stuff with the UART on accident
and couldn't figure out why there wasn't any console output on the PMU or FSBL.
Turns out the UART had gotten switched in the project settings. These settings
should look like the following:

![ps_uart_bsp](images/ps_uart_bsp.png)

### PetaLinux

From the Xilinx Wiki:

"PetaLinux Tools do not deliver or create a "Linux Distribution", but they
allow you to work easily with available software which is independently
available from the Xilinx GIT or open source communities."

Fix warning about xterm:

    sudo mkdir -p /usr/share/terminfo/
    sudo cp /lib/terminfo/x/xterm /usr/share/terminfo/x

##### 6/27/17

Trying to figure out which PetaLinux build to use. The demo image says
"Based Upon the UZ3EG 2016.2 PetaLinux BSP from Avnet." I downloaded the latest
ZED BSP from Xilinx (2017.1), but that turned out to be a Zynq one. There is a
ZCU701 BSP, but that's for a Xilinx dev kit (I think) rather than the Avnet board.

2016.2 boots fine with the prebuilt image:

    petalinux-boot --jtag --prebuilt 3

Also built the image (except for PL) and booted then too:

    petalinux-build
    petalinux-boot --jtag --prebuilt 1
    petalinux-boot --jtag --kernel

But appears to be yocto-less? - Answer: 2016.3 and prior were not Yocto-based.

2017.1: Don't install as root or the SDK will complain.
Needed to install: zlib1g:i386 xvfb

Readme contained on demo SD card mentions Avnet software repo:

    https://github.com/Avnet/software

Need to set boot pins to 0101 (OFF ON OFF ON) in order from 1-4 to boot from SD.

### Yocto

- [Build and deploy Yocto for the Zynq Ultrascale+ MPSoC ZCU102](https://www.starwaredesign.com/index.php/articles-and-talks/87-build-and-deploy-yocto-linux-on-the-xilinx-zynq-ultrascale-mpsoc-zcu102)

##### 6/28/17

I followed the UltraZed tutorial using Vivado 2017.2 (once I fixed the issue of
the board not showing up, which was due to not installing the ES chip support).
That generated the PMU FW, FSBL, and ARM trusted firmware.

I then followed the Build and Deploy Yocto link above, which got me past the
FSBL and PMU firmware and into u-boot, but using the u-boot I built from meta-
xilinx just hung on I2C:. The u-boot included with the demo worked fine. I then
tweaked u-boot and found some necessary changes: disable all I2C in the config.
Then, copy in pcw.dtsi from a different Xilinx chip, and system-top.dts from the Avent repository (petalinux/configs/device-tree), which I simply placed on top of
arch/arm/dts/zynqmp-zcu102.dts for now. I also removed all of the I2c references
inside of include/configs/xilinx_zynqmp_zcu102.h.

I was able to boot the kernel by naming it Image and the dts system.dts and placing
them in the boot partition. I also had to change the sdroot since linux sees the
SD card as mmcblk1 while u-boot sees it as 0:
    setenv sdroot0 'setenv bootargs $bootargs root=/dev/mmcblk1p2 rw rootwait'

But, the rootfs fails to mount:

    [    3.512539] VFS: Cannot open root device "mmcblk1p2" or unknown-block(179,34): error -30
    [    3.520577] Please append a correct "root=" boot option; here are the available partitions:
    (...)
    [    3.623841] b300         7438336 mmcblk0 [    3.627655]  driver: mmcblk
    [    3.630442]   b301          250040 mmcblk0p1 00000000-01[    3.635554]
    [    3.637036] b318            4096 mmcblk0rpmb [    3.641196]  (driver?)
    [    3.643542] b310           16384 mmcblk0boot1 [    3.647794]  (driver?)
    [    3.650144] b308           16384 mmcblk0boot0 [    3.654390]  (driver?)
    [    3.656740] b320         3904512 mmcblk1 [    3.660553]  driver: mmcblk
    [    3.663333]   b321           65536 mmcblk1p1 600ea220-01[    3.668453]
    [    3.669929]   b322         3837952 mmcblk1p2 600ea220-02[    3.675048]
    [    3.676532] Kernel panic - not syncing: VFS: Unable to mount root fs on unknown-block(179,34)

I did notice that the kernel thinks the card is read-only:

    [    3.327303] mmc1: mmc card read-only status: 1
    [    3.335199] mmc1: new high speed SDHC card at address b368
    [    3.335412] mmcblk1: mmc1:b368 USD   3.72 GiB (ro)
    [    3.336227]  mmcblk1: p1 p2

##### 6/29/17

I realized this morning that I was always loading the bitstream, so I tried
removing it from the bif. Unfortunately, same ro card issue.

Discovered an Avnet workaround in psu_init.c of the FSBL. Did not seem to fix ro
issue.

New everything: fail
New u-boot, old kernel:
Old u-boot, new kernel: can't boot due to image type issue

[Xilinx: Build device tree blob](http://www.wiki.xilinx.com/Build+Device+Tree+Blob)

Generated device tree:
zynqmp-clk-ccf.dtsi, zynqmp-clk.dtsi, zynqmp.dtsi: No changes.

Generated pl.dtsi, pcw.dtsi, system.dts. The device tree generation had an issue
with system.mss having a syntax error. I had to remove the whole BEGIN_DRIVER...
END_DRIVER block around line 336 which had to do with dp phy0 and phy1. Will
need to investigate if we end up needing dp (DisplayPort?)

Found a WP option in the Zynq Ultrascale+ block settings of Vivado which
connects to MIO44. This was disabled. The fsbl needs to be rebuilt to pick up
the change. It appears this change does not add anything special to the dts.

![sd_wp](images/sd_wp.png)

I rebuilt the fsbl, and it works! The card no longer shows up as write-
protected, and the rootfs successfully mounts.

##### 6/30/17

Next I need to look at cleaning up u-boot and getting an actual manine in place.
Also need to figure out why it's not seeing the first mmc device (MNAND?) so
that the proper sdroot is set up and passed to the kernel.

Do we still need the Avnet MMC/SD patch in psu_init.c?

For Monday: I created a bootes fpga repo and re-did the project. Need to test.
Monday: Items tested good.

##### 7/3/17

Going to try to figure out why ethernet isn't working.
Also need to finish work on layer/containerization.

DisplayPort peripherial in system.mss is still causing issues. I disabled it
and SATA in the Zynq IP configuration of Vivado.

Building bootes:

    export METAXILINX_REV=refs/remotes/origin/ultrazed-eg
    ./tools/build-scripts/vng-containerized-build -m ultrazed-eg-zynqmp -f core-image-minimal

With build scripts as of end of day (f74000a), can build:

    ./tools/build-scripts/vng-containerized-build -m vaddio-bootes -f core-image-minimal
    ./tools/build-scripts/vng-create-sdcard-image -m vaddio-bootes -f core-image-minimal -b . -d ~/workspace/artifacts/ -z

To boot, need to override where u-boot is looking for Image and dtb:

    setenv sdboot 'mmc dev $sdbootdev && mmcinfo && run uenvboot || run sdroot$sdbootdev; load mmc $sdbootdev:2 $fdt_addr /boot/zynqmp-ultrazed-eg.dtb && load mmc $sdbootdev:2 $kernel_addr /boot/Image && booti $kernel_addr - $fdt_addr'; boot   

TODO for finalized build:

    - [x] Get xilinx-tools-native uploaded to Vaddio bitbake mirror
    - [x] Update bitbake mirror with everything else from downloads directory.
    - [x] Get Docker Hub mirror updated with 16.04 image
    - [x] Change u-boot env to look for kernel/dtb on partition 2.
    - [x] u-boot mtest failing only with new u-boot build

##### 7/5/17

I decided to fix the warning about being assigned a random MAC, since the
carrier card does have an EEPROM explicitly for that. I2C1 addresses:

| Address | Chip |
| :------ | :--- |
| 0x30 | TCA9534 (I2C GPIO expander |
| 0x50 | EEPROM on SOM? |
| 0x70 | TCA9543 (I2C mux)|

The mux is connected to the I2C bus on the SOM, which has:

| Address | Chip |
| :------ | :--- |
| 0x51 | MAC EEPROM |

Don't need to concern ourselves with the I/O expander. Do want the 9543 which
is a 2-channel I2C mux. One bus goes to the carrier card, and the other to the
PMBUS. We want the EEPROM on the carrier card for the MAC address.

Long story short, the example from Avnet for the EEPROM was totally wrong. It
didn't even work on the dev board. The issues that existed were:

- No support for the I2C PCA9543 mux in u-boot. The example was treating it like
  a pCA9544, which isn't totally right, since the control register bits are
  slightly different. It needed to look like:

    case I2C_MUX_PCA9543_ID:
        if (channel > 1)
            return -1;
        buf = (uint8_t)(0x01 << (channel & 0x01));
        break;

- I2C mux and EEPROM needed to be added to the dts, like in the example.
  However, the address for the EEPROM needed to be 0x51, and compatible with
  "at,24c02" to get the correct size. This only has a page size of 8 instead of
  16, but I'm not too worried since not many (if any) writes will ever happen.
- I2C bus needed to be laid out in the config.h file, as well as pointing the
  eeprom at the correct bus and address.
- For a long time, I thought that the drivers/i2c/muxes/pca854x.c file was the
  key, since it didn't seem to be included, but it turns out that the I2C core
  driver is able to handle the muxes without using that file at all.

u-boot mtest: Figured out the test was failing because the scratchpad was placed
in OCM which u-boot cannot read nor write to with my current build. It appears
that is controlled by the FSBL and/or the ARM TrustZone firmware. When I moved
the scratchpad to (CONFIG_SYS_TEXT_BASE-sizeof(vu_long)), things work fine.

##### 7/6/17

Played a bit with qemu. Needed to have everything from the zcu102 machine for
QEMU support in the ultrazed-eg machine. Then, needed to add cpio to
IMAGE_FSTYPES (set in vaddio-common.inc), since runqemu doesn't like using a
tar.gz image. Finally, needed to run "containerize" and copy
/usr/local/bin/bitbake to /usr/local/bin/runqemu to get the container script.

Then, I could run `runqemu vaddio-bootes`. Stdio doesn't seem to work properly.

Interesting conversation about u-boot on the zynqmp: [u-boot on the zynqmp](https://www.mail-archive.com/meta-xilinx@yoctoproject.org/msg01246.html)


##### 7/24/2017

[QEMU on the Xilinx wiki](http://www.wiki.xilinx.com/QEMU+Yocto+Flow)

Trying again with QEMU. Having better luck using the Xilinx fork of u-boot set
via local.conf:

    XILINX_QEMUBOOT = "1"
    XILINX_QEMUBOOT_MULTI = "0"

Had to make sure that the vaddio machines are close to zcu102, ie, have
"qemu-system-xilinx" in the MACHINE_FEATURES.

Now things get to to the arm trustzone firmware, but for some reason u-boot
isn't starting:

```
NOTICE:  BL31: v1.3(release):0d9d51a
NOTICE:  BL31: Built : 19:56:26, Jul 24 2017
INFO:    ARM GICv2 driver initialized
INFO:    BL31: Initializing runtime services
INFO:    BL31: Preparing for EL3 exit to normal world
INFO:    Entry point address = 0x8000000
INFO:    SPSR = 0x3c9
VERBOSE: Argument #0 = 0x0
VERBOSE: Argument #1 = 0x0
VERBOSE: Argument #2 = 0x0
VERBOSE: Argument #3 = 0x0
VERBOSE: Argument #4 = 0x0
VERBOSE: Argument #5 = 0x0
VERBOSE: Argument #6 = 0x0
VERBOSE: Argument #7 = 0x0
```

Qemu claims to have added full support in 2.7 for the zynqmp, so I'm not sure if
moving from 2.7 to 2.8 (included in Pyro) would help anything.

##### 7/25/2017

- [x] Figure out image/machine naming scheme.
- [x] Separate bitstream into own recipe?
- [ ] Define custom u-boot environment?

Recipe naming option 1:

| Machine | Debug Image | Production Image |
| :------ | :---------- | :--------------- |
| zcu102-zynqmp | vaddio-bootes-debug-image | vaddio-bootes-image |
| ultrazed-eg-zynqmp | vaddio-bootes-debug-image | vaddio-bootes-image |
| vaddio-bootes | vaddio-bootes-debug-image | vaddio-bootes-image |

| Machine | Debug Image | Production Image |
| :------ | :---------- | :--------------- |
| vaddio-bootes-zcu102 | vaddio-bootes-debug-image | vaddio-bootes-image |
| vaddio-bootes-ultrazed-eg | vaddio-bootes-debug-image | vaddio-bootes-image |
| vaddio-bootes | vaddio-bootes-debug-image | vaddio-bootes-image |

Load bitstream in u-boot:

    ext2load mmc 1:2 $fdt_high /boot/download.bit
    fpga loadb 0 $fdt_high $filesize

##### 7/26/17

Needed to program MAC on zcu 102 board (find it on the label) in u-boot:

    ZynqMP> mm.b 0
    00000000: 00 ? 00
    00000001: a0 ? 0a
    00000002: 35 ? 35
    00000003: 02 ? 03
    00000004: 00 ? 5b
    00000005: 00 ? b6
    00000006: 00 ? q
    i2c dev 5
    i2c write 0 54 20 6
    i2c md 54 20

Should we look at settings maxcpus=2 for the Avnet board so the SW dev is
similar to production hardware?

Verified DisplayPort output works on ZCU102 board (shows system console).
Not working on UltraZed-EG board though. Turns out there's no display port in
the device tree (I remember taking out since Vivado was choking when trying to
generate a device tree with DisplayPort).
Enabled in devicetree, but now I am having a problem with multiple devices using vpll:

```
[    2.925305] Two devices are using vpll which is forbidden
[    2.930685] ------------[ cut here ]------------
[    2.935242] WARNING: CPU: 2 PID: 1 at /var/build/build/tmp/work-shared/ultrazed-eg-zynqmp/kernel-source/drivers/clk/zynqmp/pll.c:190 zynqmp_pll_set_rate+0x200/0x220
[    2.949913] Modules linked in:
[    2.952931]
[    2.954412] CPU: 2 PID: 1 Comm: swapper/0 Not tainted 4.9.0-xilinx-v2017.2 #1
[    2.961529] Hardware name: Avnet UltraZed-EG (DT)
[    2.966215] task: ffffffc075866c00 task.stack: ffffffc075868000
[    2.972120] PC is at zynqmp_pll_set_rate+0x200/0x220
[    2.977067] LR is at zynqmp_pll_set_rate+0x200/0x220
[    2.982014] pc : [<ffffff80084569f0>] lr : [<ffffff80084569f0>] pstate: 60000045
[    2.989397] sp : ffffffc07586bb20
[    2.992688] x29: ffffffc07586bb20 x28: 0000000000000000
[    2.997981] x27: ffffff8008c2ba20 x26: ffffff8008c1eb08
[    3.003276] x25: 0000000001588800 x24: 0000000000000000
[    3.008570] x23: ffffffc075be3010 x22: ffffffc075a9c100
[    3.013865] x21: 0000000001fca055 x20: 0000000001fca055
[    3.019160] x19: 000000005ad20000 x18: 0000000000000010
[    3.024455] x17: 0000000000000000 x16: 0000000000000000
[    3.029750] x15: 0000000000000006 x14: ffffff8088cf58d7
[    3.035045] x13: ffffff8008cf58e5 x12: 00000000000000fe
[    3.040340] x11: 0000000000000006 x10: 00000000000000ff
[    3.045635] x9 : 000000000000003d x8 : 6564646962726f66
[    3.050929] x7 : 2073692068636968 x6 : ffffff8008cf5914
[    3.056224] x5 : ffffff800843d460 x4 : 0000000000000000
[    3.061519] x3 : 0000000000000000 x2 : 00000000000005aa
[    3.066814] x1 : ffffff8008c7a0c8 x0 : 000000000000002d
[    3.072108]
[    3.073589] ---[ end trace 159d60ac1f70d43f ]---
[    3.078188] Call trace:
[    3.080619] Exception stack(0xffffffc07586b950 to 0xffffffc07586ba80)
[    3.087045] b940:                                   000000005ad20000 0000008000000000
[    3.094863] b960: ffffffc07586bb20 ffffff80084569f0 ffffff8008cf7cf8 000000000000002d
[    3.102675] b980: ffffffc07586b9a0 ffffff80080d853c ffffff8008cf5368 ffffff8008aa3530
[    3.110487] b9a0: ffffffc07586ba40 ffffff80080d8818 000000005ad20000 0000000001fca055
[    3.118299] b9c0: 0000000001fca055 ffffffc075a9c100 ffffffc075be3010 0000000000000000
[    3.126111] b9e0: 0000000001588800 ffffff8008c1eb08 000000000000002d ffffff8008c7a0c8
[    3.133923] ba00: 00000000000005aa 0000000000000000 0000000000000000 ffffff800843d460
[    3.141736] ba20: ffffff8008cf5914 2073692068636968 6564646962726f66 000000000000003d
[    3.149548] ba40: 00000000000000ff 0000000000000006 00000000000000fe ffffff8008cf58e5
[    3.157360] ba60: ffffff8088cf58d7 0000000000000006 0000000000000000 0000000000000000
[    3.165173] [<ffffff80084569f0>] zynqmp_pll_set_rate+0x200/0x220
[    3.171156] [<ffffff8008451700>] clk_change_rate+0x1e0/0x238
[    3.176797] [<ffffff80084517b8>] clk_core_set_rate_nolock+0x60/0xc8
[    3.183046] [<ffffff8008451848>] clk_set_rate+0x28/0x58
[    3.188257] [<ffffff80086f6d88>] xilinx_dp_codec_probe+0xd8/0x1e8
[    3.194333] [<ffffff80084ebb68>] platform_drv_probe+0x58/0xc0
[    3.200060] [<ffffff80084ea014>] driver_probe_device+0x1fc/0x2a8
[    3.206048] [<ffffff80084ea16c>] __driver_attach+0xac/0xb0
[    3.211516] [<ffffff80084e8060>] bus_for_each_dev+0x60/0xa0
[    3.217072] [<ffffff80084e9800>] driver_attach+0x20/0x28
[    3.222366] [<ffffff80084e9348>] bus_add_driver+0x110/0x230
[    3.227923] [<ffffff80084ea940>] driver_register+0x60/0xf8
[    3.233391] [<ffffff80084eba9c>] __platform_driver_register+0x44/0x50
[    3.239817] [<ffffff8008c0f334>] xilinx_dp_codec_driver_init+0x18/0x20
[    3.246326] [<ffffff80080830b8>] do_one_initcall+0x38/0x128
[    3.251880] [<ffffff8008be0c98>] kernel_init_freeable+0x140/0x1e0
[    3.257957] [<ffffff8008907228>] kernel_init+0x10/0x100
[    3.263163] [<ffffff8008082e80>] ret_from_fork+0x10/0x50
[    3.269089] Write failed to divider address:fd1a00c0
```

Tomorrow: There are 4 devices using the vpll clock tree, which triggers the
WARN. Look at the zcu102 image and see what its clock tree looks like.

##### 7/27/17

Can't figure out why the clock tree is different between the two boards. On the
zcu102, nobody uses the vpll except for dp_video. On the UltraZed, currently,
topsw_main_mux as well as other dp_ items are using it.

Interestingly, [this forum post](http://ultrazed.org/content/driving-display-port)
indicates that the UltraZed board uses a differnet clocking scheme to drive
the pixel clock for DisplayPort.

I was reading "UltraZed and Zynq UltraScale+ MPSoC Clocking" on the UltraZed
Chronicles blog, and I discovered that you can change the output clocks in
Vivado in the "Output" tab. This must be where the clock config is getting set
up (likely by the FSBL), and Linux is just using that.

Lots of changes to device-tree-xlnx repository since I last looked, specifically
for zynqmp. Also, the DisplayPort syntax issue with device-tree bsp projects in
Vivado's SDK can be worked around by making the phys PARAMTER setting be all on
one line instead of two in system.mss.

##### 7/28/17

I was able to build the Ubuntu image from the invincealabs.com link. Everything
boots, but DisplayPort does not on that image either. Also tried using PetaLinux
2017.2 and Vivado 2017.2 with no noticeable difference.

Success with DisplayPort! [This thread](http://zedboard.org/content/dma-kernel-panics)
had someone say they got it working in 2017.1 using the settings from the zcu102
board set in Vivado. I set up everything and noticed that none of the clocks
changed in Linux. So, I deleted the bootes.sdk directory to create a new
project, after which DisplayPort started working!

To change the resolution of the framebuffer console, use the video= kernel
command line option, for example `video=1920x1080@60`.

### Touchscreen

There is a driver for the FT5x06 in the kernel, but the chip we are using is the
FT5216. I found a reference for the FT5216 that has a register list and compared
it to a register map for the FT5x06. Almost all of the registers are the same,
except the FT5216 has 0x85, ID_G_THDIFF, defined. I think the Linux driver should
work with the slightly different chip.

The GitHub repo Steve sent me seems to confirm this - its driver is named ft5x06
but yet it claims to support many of the FT chips including FT5x16.

##### 8/3/17

The driver built in to the kernel (edt-ft5x06) worked fine. I had to configure
the I2C bus to only talk at 100kHz, otherwise I had issues with the chip
randomly refusing to communicate after several transactions. Here is the pin
mapping I used:

| LCD Header Pin | PS PMOD Pin | Function | MIO |
| :------------- | :---------- | :------- | :-- |
|  1 | 11 | VSS |  - |
|  2 | 12 | VDD |  - |
|  3 |  4 | SCL | 38 |
|  4 | NC |  -  |  - |
|  5 |  9 | SDA | 39 |
|  6 | NC |  -  |  - |
|  7 |  7 | RST | 36 |
|  8 |  - | WKE |  - |
|  9 |  8 | INT | 40 |
| 10 |  - | VSS |  - |

I determined that the wake input (WKE) was not needed for successful operation.
I also built tslib and used ts_test/ts_print to view the touch points. I did
build a newer version of tslib (1.11) than what was included with Morty (1.1) to
test out multi-touch using the \_mt variants of the same apps.

I placed these changes in a branch called bootes-touchscreen.

### NFS Boot on UltraZed-EG board

First-time setup:
[NFS Server](https://help.ubuntu.com/community/SettingUpNFSHowTo)
[TFTP Server](https://askubuntu.com/questions/201505/how-do-i-install-and-run-a-tftp-server)

For always nfs boot:
    setenv bootcmd 'run nfsboot'

Regular nfs boot:
    setenv nfsroot 'setenv bootargs $bootargs root=/dev/nfs nfsroot=$serverip:/srv/nfs/ultrazed,tcp ip=$ipaddr:$serverip:$serverip:255.255.255.0:ultrazed-eg-zynqmp:eth0:off rw'
    setenv loadbit_net 'dhcp $bit_addr download.bit && fpga loadb 0 $bit_addr $filesize'
    setenv nfsboot 'run loadbit_net && tftpboot $kernel_addr Image && tftpboot $fdt_addr Image-zynqmp-ultrazed-eg.dtb && run nfsroot && booti $kernel_addr - $fdt_addr'
    saveenv

### MTD Device access (SPI flash)

No luck yet, so far, Need to set CONFIG_MTD=y and CONFIG_MTD_OF_PARTS=y.
The PetaLinux 2016.2 image detects the device correctly. Kernel version issue?

Added a device tree node:

```
&qspi {
	is-dual = <1>;
	num-cs = <1>;
	status = "okay";
	flash@0 {
		compatible = "n25q256a";
		reg = <0x0>;
		spi-tx-bus-width = <1>;
		spi-rx-bus-width = <4>;
		spi-max-frequency = <50000000>;
		#address-cells = <1>;
		#size-cells = <1>;

		partitions {
			compatible = "fixed-partitions";
			#address-cells = <1>;
			#size-cells = <1>;

			partition@0 {
				label = "u-boot";
				reg = <0x00000000 0x800000>;
			};
		};
	};
};
```

### Booting with the new ark layer

For some reason boot gets stuck after:

```
[    2.833812] usbcore: registered new interface driver usbhid
[    2.839311] usbhid: USB HID core driver
```

Some items we can remove due to Yocto improvements:
- Can remove update-modules (unnecessary in 1.4 and newer, see RM 6.3.9).

TODO for tues:
- [ ] Ultrazed image mounts as rw only on very first boot
- [x] Messages about /run/lock read-only filesystem
- [x] No kernel in sitara build?

gcc-cross-initial-arm rebuild?
busybox -c cleansstate

??
ENABLE_ROOTFS_FSCK=no

chown: /run/lock: Read-only file system <- fixed with volatiles/fstab fixes
chmod: /run/lock: Read-only file system
chown: unknown user/group admin:admin <- maybe don't worry about these for now, since they are only on minimal
chown: unknown user/group admin:admin
rm: can't remove '/var/lib/urandom/random-seed': Read-only file system <- fixed by writeable mount issue

lrwxrwxrwx    1 root     root            18 Nov 21  2017 lock -> /var/volatile/lock
lrwxrwxrwx    1 root     root            17 Nov 21  2017 log -> /var/volatile/log
lrwxrwxrwx    1 root     root            17 Nov 21  2017 run -> /var/volatile/run
drwxr-xr-x    2 root     root          2048 Nov 20  2017 spool
lrwxrwxrwx    1 root     root            17 Nov 21  2017 tmp -> /var/volatile/tmp
^^^ Need to mount /run as tmpfs too

Root still mounts as rw first time??
/etc/rcS.d/S21configure appears to be the culprit

/var/lib/dbus/machine-id.MyITvP1G: Read-only file system

ip=dhcp seems to work for nfs:

setenv nfsroot 'setenv bootargs $bootargs root=/dev/nfs nfsroot=$serverip:/srv/nfs/ultrazed,tcp ip=dhcp rw'

Fallback:

setenv nfsroot 'setenv bootargs $bootargs root=/dev/nfs nfsroot=$serverip:/srv/nfs/ultrazed,tcp ip=$ipaddr:$serverip:$serverip:255.255.255.0:ultrazed-eg-zynqmp:eth0:off rw'

### core-image with nfs

It looks like you can boot the core-image over NFS by simply making the rootfs
read-write in fstab, /etc/defaults/rcS, and the u-boot bootargs if needed. Note
that this doesn't use the "real" /media/vng-* partitions, rather it just writes
there directly. But the web server does start. I also did have to disable the
iptables initscript, otherwise it hangs on boot when enabling the VNG firewall.

### SD Boot issues

Avnet has a patch to fix behavior with the SD card:
http://zedboard.org/content/ultrazed-eg-petalinux-sdio-patch

Seems this line in the dts &sdhci1 is required for the old boards:
    no-1-8-v; /\* for 1.0 silicon \*/

Without the bitstream, things don't seem to boot all the way anymore. A couple different issues depending on which device tree you have:
- If you have no pl.dtsi, things will freeze when trying to probe the FPGA device with the fpga_manager.
- If you do have a device tree, they freeze much earlier, right after the bootconsole is disabled and before the delay loop is calibrated.

Kernel seems to deploy wrong "Image" sometimes and requires a cleansstate.
You can tell when this happens because the kernel will be ~3MB instead of ~13MB. When the wrong Image is deployed, the system hangs after "Starting Kernel..."

### USB testing

The kernel has a documentation page specifically about the Design-Ware controller used by the UltraScale: https://01.org/linuxgraphics/gfx-docs/drm/driver-api/usb/dwc3.html

USB was enumerating as high-speed instead of super-speed. To fix, I followed the steps in "UltraZed_Open_Source_Linux_USB_Performance_Test_2016_2_01-Tutorial.pdf", namely:

1. Set carrier card jumpers JP1 to off, and J2 and J3 to setting 2-3.
2. Run the i2cget and i2cset commands in Appendix 1. It also appears that their bus is incorrect, it should be bus 1:
    i2cget -y 1 0x6a 0x00 b
    i2cset -y 1 0x6a 0x20 0x00 b
    i2cset -y 1 0x6a 0x21 0x81 b
    i2cset -y 1 0x6a 0x22 0x03 b
    i2cset -y 1 0x6a 0x23 0xd8 b
    i2cset -y 1 0x6a 0x24 0x9d b
    i2cset -y 1 0x6a 0x25 0x8c b
    i2cset -y 1 0x6a 0x26 0x00 b
    i2cset -y 1 0x6a 0x27 0x00 b
    i2cset -y 1 0x6a 0x28 0x00 b
    i2cset -y 1 0x6a 0x29 0x00 b
    i2cset -y 1 0x6a 0x2a 0x04 b
    i2cset -y 1 0x6a 0x2b 0x00 b
    i2cset -y 1 0x6a 0x2c 0x00 b
    i2cset -y 1 0x6a 0x2d 0x01 b
    i2cset -y 1 0x6a 0x2e 0x90 b
    i2cset -y 1 0x6a 0x2f 0x00 b
3. Reboot to re-initialize the USB stack.

Here's a u-boot command to run instead:
    setenv usb3_en 'i2c bus 1; i2c md 0x6a 0x00 1; i2c mw 0x6a 0x20 0x00; i2c mw 0x6a 0x21 0x81; i2c mw 0x6a 0x22 0x03; i2c mw 0x6a 0x23 0xd8; i2c mw 0x6a 0x24 0x9d; i2c mw 0x6a 0x25 0x8c; i2c mw 0x6a 0x26 0x00; i2c mw 0x6a 0x27 0x00; i2c mw 0x6a 0x28 0x00; i2c mw 0x6a 0x29 0x00; i2c mw 0x6a 0x2a 0x04; i2c mw 0x6a 0x2b 0x00; i2c mw 0x6a 0x2c 0x00; i2c mw 0x6a 0x2d 0x01; i2c mw 0x6a 0x2e 0x90; i2c mw 0x6a 0x2f 0x00'

setenv sdboot 'run usb3_en ; mmc dev $sdbootdev && mmcinfo && run uenvboot || run sdroot$sdbootdev; run loadbit_sd; load mmc $sdbootdev:$bootpart $fdt_addr /boot/zynqmp-ultrazed-eg.dtb && load mmc $sdbootdev:$bootpart $kernel_addr /boot/Image && booti $kernel_addr - $fdt_addr'

### UVC Webcam gadget testing

[Xilinx Linux drivers](http://www.wiki.xilinx.com/Linux+Drivers)

[This link](https://wiki.tizen.org/USB/Linux_USB_Layers/Configfs_Composite_Gadget/Usage_eq._to_g_webcam.ko) descripts how to set one up with configfs. Note, I needed the following patch to have the streaming_* items available: [usb: gadget: uvc: Missing files for configfs interface](https://www.spinics.net/lists/linux-usb/msg154404.html).

UltraScale TRD tutorials: http://www.wiki.xilinx.com/Zynq+UltraScale+MPSoC+Base+TRD+2017.2

The gpio cells when linking gpio to gpio chips for Xilinx's axi gpio (xlnx,xps-gpio) are a bit confusing. It isn't chip, number, flags like you'd think, but rather chip, number, channel; where channel is 0 or 1 depending on which of the two channels of the GPIO controller it's on. This bit me because my GPIO wasn't being found since I had channel set to 1 instead of 0.

IRQ: GICPn [x] -> irq= 32*n+x

[    3.210769] xilinx-video amba_pl@0:vcap_tpg: /amba_pl@0/vcap_tpg/ports/port@0 initialization failed
[    3.219754] xilinx-video amba_pl@0:vcap_tpg: DMA initialization failed

commit 16bb05d98c904a4f6c5ce7e2d992299f794acbf2
Author: Roger Quadros <rogerq@ti.com>
Date:   Wed Mar 8 16:05:44 2017 +0200

    usb: gadget: f_uvc: Sanity check wMaxPacketSize for SuperSpeed

commit 09424c50b7dff40cb30011c09114404a4656e023 <-- applied, this fixed usb 3.0
Author: Roger Quadros <rogerq@ti.com>
Date:   Wed Mar 8 16:05:43 2017 +0200

    usb: gadget: f_uvc: Fix SuperSpeed companion descriptor's wBytesPerInterval


commit c5628a2af83aeda6cc02b6c64acb91f249727b1c <-- analagous to a Xilinx hack to fix poor usb streaming performance
Author: Mathias Nyman <mathias.nyman@linux.intel.com>
Date:   Thu Jun 15 11:55:42 2017 +0300

    xhci: remove endpoint ring cache

gst-launch-1.0 videotestsrc ! 'video/x-raw,format=YUY2,width=320,height=240,framerate=30/1' ! v4l2sink device=/dev/video0

gst-launch-1.0 videotestsrc ! 'video/x-raw,format=YUY2,width=320,height=240,framerate=30/1' ! videoconvert ! videorate ! v4l2sink device=/dev/video0

### Xilinx video processing (VIP) drivers

Adding console=ttyPS0 to the bootargs appears to solve hte issue where things hang after "disabling bootconsole"

Finally got the /dev/media0 device node to show up. Several issues I had to work through:
- "compatible" nodes listed on the wiki were different than the drivers needed
- Reset gpios needed to be active-low, also, using AXI GPIO appeared to be a bad idea (no active low support, accidentally specified channel 1 instead). Using EMIO worked much better (first EMIO gpio is 78).
- Needed to specify a clock for the axi_tpg_0 device. In the HDL block diagram I have it set to use the PL0 clock, also known as fclk0. Apparently this needs to be enabled in the device tree so that the xilinx_fclk driver sees it and keeps it on?

```
Media controller API version 0.1.0

Media device information
------------------------
driver          xilinx-video
model           Xilinx Video Composite Device
serial          
bus info        
hw revision     0x0
driver version  0.0.0

Device topology
- entity 1: vcap_tpg output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "80030000.axi_tpg":0 [ENABLED]

- entity 5: 80030000.axi_tpg (1 pad, 1 link)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
		[fmt:UYVY/0x0 field:none]
		-> "vcap_tpg output 0":0 [ENABLED]
```

Can use media-ctl to set up pipeline:

First set up link (if needed):
    media-ctl -r -l '"80030000.axi_tpg":0 -> "vcap_tpg output 0":0 [1]' -v

Set the tpg format:
    media-ctl -v -V '"80030000.axi_tpg":0[fmt:YUYV 640x360]'

Set the v4l2 format (optional, it seems):
    v4l2-ctl -d /dev/video0 --set-fmt-video=width=640,height=360,pixelformat='YUYV'

Finally, use yavta to capture some frames:
    yavta -c10 -f YUYV -s 640x360 --skip 7 -F /dev/video0

Or stream to uvc gadget:
    modprobe g-webcam
    uvc-gadget -v /dev/video0 -u /dev/video1 -n 6

I used rawpixels.net to verify, with the following parameters:
- Predefined format: YUY2
- Pixel format: YUV, Ignore alpha, uncheck alpha first
- Pixel plane: Packed YUV

setenv bootargs $bootargs xilinx_video.is_mplane=N

You can query all of the different controls with:
    yavta --no-query /dev/v4l-subdev0 -l

Example, turn on moving box test:
    yavta --no-query /dev/v4l-subdev0 -w '0x0098c912 1'

[RidgeRun article on Xilinx VIPP with examples](https://developer.ridgerun.com/wiki/index.php?title=Zynq_Ultrascale%2B_Capture_settings_and_Gstreamer_pipelines)
[This thread](https://stackoverflow.com/questions/20664967/media-ctl-commands-for-caspa-mt9v032-omap-isp-gumstix) was very helpful in providing some examples.

Frames appear to be coming in way too fast. Increasing number of buffers allocated by uvc-gadget to 32 seems to sort of help this problem.
Enabled WRITE_FIFO_DELAY on the axi_data_fifo block. This appears to have helped with the latency problem, but frames are still coming in ridiculously fast.

Master branch of linux-xlnx (b450e900fdb473a53613ad014f31eedbc80b1c90) appears to have USB issues - can't find USB3 phy.

You do need the Xilinx video timing controller in order to set the hblank/vblank settings on the tpg and adjust the framerate that way.

Vivid: Can adjust the framerate with this command:
    v4l2-ctl -d /dev/videoX --set-parm=30

The Xilinx TPG can do 4k easily - I had to increase the PL clock frequency to 300MHz and rebuild the bitstream+FSBL. Then we get approx 33fps generating 4k TPG's and capturing with yavta.

### UVC debug with PetaLinux and ZCU-102 board

Install petalinux by running the installer, then create a project with:
    source settings.sh
    petalinux-create -t project -s /storage/installs/xilinx-zcu102-zu9-es2-rev1.0-v2017.4-final.bsp -n uvc-test-project

Run `petalinux-config` and set Image Packaging Configuration->Root filesystgem type to "SD Card"
Also go under Yocto Settings and set up Parallel thread execution for faster builds.

Set usb to peripheral mode by editing project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi and adding this to the end of the file (after }; ):
    &dwc3_0 {
        status = "okay";
        dr_mode = "peripheral";
    };

To fix kernel config issues with 14.04 (which are addressed in upstream poky), edit components/yocto/source/{aarch64,arm}/layers/core/meta/lib/oe/termina.py and replace `sh_cmd = "oe-gnome-terminal-phonehome " + pidfile + " " + sh_cmd` with `sh_cmd = bb.utils.which(os.getenv('PATH'), "oe-gnome-terminal-phonehome") + " " + pidfile + " " + sh_cmd`

Turn on necessary usb gadget options in `petalinux-config -c kernel`:
    Device Drivers->USB support
        DWC3 Mode Selection=Gadget Only
        USB Gadget support
            USB Functions configurable through configfs
                Function filesystem=y
                USB Webcam function=y
        USB Gadget Drivers
            Gadget Filesystem=m
            Function filesystem=m
            USB Webcam gadget=m

Build everything with `petalinux-build`

Create BOOT.BIN:
    petalinux-package --boot --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf

Create a boot partition, at least 100M, on the sdcard, and copy BOOT.BIN,  image.ub, and system.dtb to it.
Create a second partition on the rest of the sd card and extract rootfs.tar.gz to

Add uvc-gadget.bb to meta-user/recipes/devtools/uvc-gadget
Add this to build/conf/local.conf (editing the image.bbappend did not work)
    IMAGE_INSTALL_append = " uvc-gadget"

For zcu106 board, you need to also include the bitstream in boot.bin otherwise hangs after "mali: loading out-of-tree module taints kernel.":
    petalinux-package --boot --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --fpga images/linux/system.bit --force

### Porting vng-gst-server

modprobe g-webcam

/opt/vaddio/bin/vng-gst-server -p "videotestsrc ! video/x-raw-yuv,format=(fourcc)YUY2,width=640,height=360,framerate=30/1 ! queue name=usbvideoq" -m 0x02 -w 640 -t 360 -a


/opt/vaddio/bin/vng-gst-server -p "videotestsrc ! video/x-raw-yuv,format=(fourcc)YUY2,width=640,height=360,framerate=30/1 ! videoconvert ! videoscale ! queue name=usbvideoq" -m 0x02 -w 640 -t 360 -a


GST_DEBUG=2,videotestsrc:6

modprobe -a g-webcam vivid
/opt/vaddio/bin/vng-gst-server -p "v4l2src device=/dev/video1 ! video/x-raw-yuv,format=(fourcc)YUY2,width=(int)640,height=(int)360,framerate=(fraction)30/1 ! queue name=usbvideoq" -m 0x02 -w 640 -t 360 -a

For 1280x720, it appears you need to specify the caps for vivid too:
/opt/vaddio/bin/vng-gst-server -p "v4l2src device=/dev/video1 ! video/x-raw-yuv,format=(fourcc)YUY2,width=(int)1280,height=(int)720,framerate=fraction)30/1 ! queue name=usbvideoq" -m 0x02 -w 1280 -t 720 -a

/opt/vaddio/bin/vng-gst-server -p "v4l2src device=/dev/video1 ! video/x-raw-yuv,format=(fourcc)YUY2,width=(int)1920,height=(int)1080,framerate=fraction)30/1 ! queue name=usbvideoq" -m 0x02 -w 1920 -t 1080 -a

streaming_maxburst=15 allows 30fps - for about 2s, then it crashes.

It seems vivid (or someone) doesn't like changing resolution once it's been set. That's what appears to cause the "internal data flow error" that gstreamer will crash with. I end up having to hard code one resolution everywhere.

Things do look better if I increase the maxburst and/or maxpacket. Thinking maybe I'll try bulk next (but first hopefully get something more stable with isoc going).

FYI: vivid allows you to use an hdmi source which does not have the fps limitations of the webcam source. Just use "modprobe vivid input_types=0xe7" which sets input0's type to 0x3 (hdmi). However, it appears it always wants to jump to 60fps.

- [ ] Streaming only seems to work with uvc trace enabled at 0xffff.
- [ ] Reliability issues with connect/disconnect
- [ ] Unable to select between different resolutions (have to hard-code)
- [ ] Could always test with uvc-gadget if you can get it working
- [ ] v4l2sink?

v4l2-ctl -d /dev/video1 --list-frameintervals=width=1920,height=1080,pixelformat=YUYV
Expect:
    ioctl: VIDIOC_ENUM_FRAMEINTERVALS
    	Interval: Discrete 1.000s (1.000 fps)
    	Interval: Discrete 0.500s (2.000 fps)
    	Interval: Discrete 0.250s (4.000 fps)
    	Interval: Discrete 0.200s (5.000 fps)
    	Interval: Discrete 0.100s (10.000 fps)
    	Interval: Discrete 0.067s (15.000 fps)
    	Interval: Discrete 0.040s (25.000 fps)
    	Interval: Discrete 0.033s (30.000 fps)
    	Interval: Discrete 0.020s (50.000 fps)
    	Interval: Discrete 0.017s (60.000 fps)

Some inspiration from bulk support came from Bhupesh Sharma's unmerged bulk patchset back in 2013 [here](https://www.spinics.net/lists/linux-usb/msg85446.html)

Things worked much better with a few changes:
- Used zcu102 board which has more stable usb phy - ultrazed showed phy errors on the beagle when it disconnected
- Built usb stack as modules
- Multiple patches to make vivid happy as a webcam at any resolution
- Used uvc-gadget instead of gstreamer.
- maxburst=15
- Iso worked fine until 1080p 30, then it was only achieving ~15.7 fps. Bulk mode worked up to 1080p 60.

To load everything usb-related you just need to load:
modprobe xhci-plat-hcd

modprobe g-webcam streaming_maxburst=15
modprobe vivid
uvc-gadget -u /dev/video0 -v /dev/video1 -t 15 -r 2 -n 2 -b

zcu102 can't find dtb, wrong name:
setenv sdboot 'mmc dev $sdbootdev && mmcinfo && run uenvboot || run sdroot$sdbootdev; run loadbit_sd; load mmc $sdbootdev:$rootpart $fdt_addr /boot/devicetree.dtb && load mmc $sdbootdev:$rootpart $kernel_addr /boot/Image && booti $kernel_addr - $fdt_addr'
saveenv && boot

ultrazed, same thing:
setenv sdboot 'setenv rootpart 2 ; mmc dev $sdbootdev && mmcinfo && run uenvboot || run sdroot$sdbootdev; run loadbit_sd; load mmc $sdbootdev:$rootpart $fdt_addr /boot/devicetree.dtb && load mmc $sdbootdev:$rootpart $kernel_addr /boot/Image && booti $kernel_addr - $fdt_addr'

Moving test pattern:
    v4l2-ctl -d /dev/video1 --set-ctrl=horizontal_movement=4

Change the test pattern with:
    v4l2-ctl -d /dev/video1 --set-ctrl=test_pattern=x
    where 0<=x<=21

Useful ones are:
    0-3: various color patterns
    10: Squares
    20: Gradient (seems to have performance issues)

##### 2/26/18

It appears that building the usb 3.0 stuff as modules probably didn't improve reliability - that was all moving to the zcu102 board.

set-parm does not work with xilinx tpg for some reason!

##### 3/7/18

Windows seems to not like arbitrary dwMaxPayloadTransferSize's. Specifically, before I was just using the size of the frame (for bulk transfers), but the fx3 code uses 0x4000 (16k). Manually setting it (in both places in uvc_gadget as well as the max_payload_size in uvc_video.c:uvcg_video_enable) resulted in Windows behaving fine. Through trial and error I doubled it all the way up to larger than the 4k frame with 4k resolution (0x100000) and Windows was OK with it all the time. 0xfd0000 and 0xfe0000 worked, but 0xfd1000 0xfd2000 did not.

##### 12/21/18

Working on getting stream to restart on Windows. Seems like Windows puts the device in a U1/U2 state when switching and it can't get out of that state. Windows is sending HALT repeatedly on the endpoint, so maybe the device is sending video data before Windows is ready?

##### 1/2/19

Back to the drawing board. Using isochronous streaming with latest uvc-gadget upstream (aa82df887ab995473cd83c89777cdf4bc4685dd0). Works at 360p and 720p (both streaming_maxburst=0 and 15). Seems to have some reliability issues when the USB cable is removed. Working okay in Windows both with and without the "revert u1/u2" patch provided by Xilinx, but will need to validate smoothness once vivid and/or Xilinx tpg source is implemented. Currently only pattern fill in uvc-gadget is supported.

##### 1/3/19

Realized I can also set maxpacket (to get mult) in addition to bursting on the isochronous endpoint. However, I found that maxburst=16 and maxpacket=3072 doesn't work. However, maxburst=16 and maxpacket=2048 (for 2048*16=32768 bytes/fame bandwidth) does and gets pretty close performance to what I saw with bulk before (maxes out around 1080p/63fps). This works on Linux, but Windows doesn't seem to like it (stops streaming once resolution changes - maybe receiving frames too fast?).

The uvc-gadget application in "standalone mode" generates frames as fast as it can - I think Windows doesn't appreciate this.

##### 1/4/19

Tried uvc-gadget (master instead of lpinchart) which works better. Set streaming_maxpacket=2048, streaming_maxburst=15, then run:

```
./uvc-setup.sh
modprobe vivid
uvc-gadget -u /dev/video1 -v /dev/video2 -t 15 -r 3 -n 4 -m 1
```

Seems there is still flakiness on Windows - it works pretty well going through the Beagle, but not standalone. I wonder if the issue is related to the zcu104 board still operating sort of in host mode?

End of day update: Turns out that running through a hub improves things a lot - similar to running through the beagle. Possibly due to electrical isolation/change in capacitance or something else? Should try on a zcu106 board at some point since that has real jumpers for USB device mode (whereas the zcu104 doesn't officially support device mode).

Breakthrough from Dennis - new build has Yavta capturing frames from Xilinx IP's.

For both RGB24 and YUYV in yavta:
4k: 33.6fps average
1080p: 125fps average

##### 1/7/19

Since Dennis got the tpg pipeline working, I'm trying to stream it through uvc-gadget. For some reason now the gadget defaults to USERPTR IO which the pipeline doesn't support, so you must specify MMAP on the command line to uvc-gadget with the `-o 0` flag.

xilinx_video's is_mplane paramter must be N! Add this to bootargs:
xilinx_video.is_mplane=N

Streaming is still a little brittle. Doesn't seem to work through hub on Linux, but works through front USB port.

Seems Windows (on Dennis' PC in particular) doesn't like frames coming in faster than the fps rate selected by eCAM. Should look at adding simple frame limiting to uvc-gadget.

##### 1/8/19

Frame rate limiting working by delaying up to the frame interval in between processing each frame.

Trying to get hdmi cores to probe - both the video phy controller and hdmi controller require multiple clocks to be set up. In particular the video phy uses the 8t49n287 clock generator for the dru-clk, but that generator was missing key components in its dts entries (compatible was wrong, status = okay was missing, and clocks/clock-names  needed to be defined to tell it it is being sourced from a 25MHz crystal).

HDMI cores probed but not receiving frames. However `media-ctl -p` shows input resolution changing. Dennis is making a new build.

Tried zcu106 board to see if enumeration problems were electrical (zcu104 doesn't allow disconnecting vbus and doesn't have the shielded directly grounded)- things work much better in Linux. However in Windows it still only enumerates through a hub, except that it's more reliable now. Windows enumerates it as a high-speed instead of super-speed device when plugging directly in - will need to resolve this next.

##### 1/9/19

Per /sys/kernel/debug/fe200000.dwc3/link_state, the controller is going into "Compliance" mode when plugging directly into my Windows PC instead of the hub. Tried setting CONFIG_USB_DWC3_DUAL_ROLE=y instead of DWC3_GADGET=y, but that didn't help.

##### 1/11/19

Pulled in changes to xilinx-dma from 2018.3. Now gstreamer is happy with the caps negitation on the output and I can stream software-encoded h.264 (after running media-ctl).

```
gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,format=RGB,width=1920,height=1080,framerate=60/1 ! multifilesink location="/run/frame%d"

rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 ! video/x-raw,format=RGB,width=1920,height=1080,framerate=5/1 ! videoconvert ! x264enc threads=4 speed-preset=ultrafast tune=4 ! video/x-h264,profile=baseline ! h264parse ! queue ! rtph264pay name=pay0 pt=96 )"
```

Dennis added VPSS Scaler with colorspace conversion support. Can't use Full-featured VPSS since the driver only supports scaler mode and CSC mode.

Need to be very specific when setting up media pipelines. Note difference between RBG888_1X24 which the pipeline usually uses and RGB888_1X24 which the scaler will happily accept but won't link. Also need to set field for the scaler otherwise the fields won't match:
```
media-ctl -v -V '"80100000.v_proc_ss":0[fmt:RBG888_1X24 1920x1080 field:none]'
```

Along with:
```
media-ctl -v -V '"80040000.v_tpg":0[fmt:RBG888_1X24 1920x1080 field:none]'
```

YUYV capture is less intuitive:
```
media-ctl -v -V '"80100000.v_proc_ss":1[fmt:UYVY8_1X16 1920x1080 field:none]'
yavta -c10 -f YUYV -s 1920x1080 --skip 7 -F /dev/video0
```

Full pipeline working with uvc-gadget (note that the first line is now built-in to uvc-gadget since it must be set every time the format changes)
```
yavta --no-query /dev/v4l-subdev1 -w '0x009f0903 0'
media-ctl -v -V '"80040000.v_tpg":0[fmt:RBG888_1X24 1920x1080 field:none]'
media-ctl -v -V '"80100000.v_proc_ss":0[fmt:RBG888_1X24 1920x1080 field:none]'
/etc/init.d/uvc-gadget.sh start
```

Final pipeline looks like this:
```
root@vaddio-generic-00-0A-35-04-D2-73:~# media-ctl -p
Media controller API version 4.14.0

Media device information
------------------------
driver          xilinx-video
model           Xilinx Video Composite Device
serial          
bus info        
hw revision     0x0
driver version  4.14.0

Device topology
- entity 1: video_cap output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "80100000.v_proc_ss":1 [ENABLED]

- entity 5: 80000000.v_hdmi_rx_ss (1 pad, 1 link)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
		[fmt:RBG888_1X24/1920x1080 field:none]
		[dv.caps:BT.656/1120 min:0x0@25000000 max:4096x2160@297000000 stds:CEA-861,DMT,CVT,GTF caps:progressive,reduced-blanking,custom]
		[dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861 flags:CE-video]
		-> "80040000.v_tpg":0 [ENABLED]

- entity 7: 80040000.v_tpg (2 pads, 2 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev1
	pad0: Sink
		[fmt:RBG888_1X24/1920x1080 field:none]
		<- "80000000.v_hdmi_rx_ss":0 [ENABLED]
	pad1: Source
		[fmt:RBG888_1X24/1920x1080 field:none]
		-> "80100000.v_proc_ss":0 [ENABLED]

- entity 10: 80100000.v_proc_ss (2 pads, 2 links)
             type V4L2 subdev subtype Unknown flags 0
             device node name /dev/v4l-subdev2
	pad0: Sink
		[fmt:RBG888_1X24/1920x1080 field:none]
		<- "80040000.v_tpg":1 [ENABLED]
	pad1: Source
		[fmt:UYVY8_1X16/1920x1080 field:none]
		-> "video_cap output 0":0 [ENABLED]
```

##### 1/14/19

List of current USB streaming TODO's:
- [ ] Enumeration issue straight to PC (SR#10456166)
- [ ] Occasional kernel panic when changing resolutions (dwc3 fe200000.dwc3: ep2in is already disabled)
- [ ] dwc3 fe200000.dwc3: ep2in: ran out of requests
- [ ] Occasional lockup/timeout error causing uvc-gadget to fail (dwc3 fe200000.dwc3: timed out waiting for SETUP phase, UVC disconnect failed with -110)

##### 1/17/19

I think the source of the SS USB issues are due to the A->A cables having two types of configuration: "extension" or "crossover". It appears the C2G cable I have is extension, where the Tx pairs are connected and the Rx pairs are connected. To correctly operate in device mode, we need a crossover style where the Tx and Rx are swapped as they would be in an A->B cable. Amazon makes it hard to find, but Digikey ahs the pinouts for their cables availabe. It seems most of theirs are crossover, see 380-1416-ND, Q543-ND. and TL1384-ND for example. I proved this out by ohming out my cable and then trying it through a 2.0 adapter. It enumerates and streams fine over 2.0.

I did notice that running in 2.0 mode there are many artifacts whenever there is motion, even with mult set to 2 (3072 packet size). This also happens in 3.0 mode with the burst and mult settings set the same. Gets much worse at 320x180 resolution. Seems like something is running too fast?

##### 1/18/19

Playing with gstreamer today. I modified the uvc-gadget app based off our gadget.cpp. Thought is to have one application handle uvc events and the other handle uvc streaming.

Learned a couple of things:
- Drivers calls uvc_function_activate/deactivate for each open and close. This must be addressed for multiple apps to share the device.
- gstreamer calls the STREAMON/STREAMOFF ioctls, so all you have to do in the control app is start gstreamer when you get the streamon event.

Gstreamer is a little finnicky - seems to not want to stop streaming in time when the format changes which causes a kernel panic due to uvc_v4l2_qbuf being called when the USB stream is off.

io-mode=2 on v4l2src and io-mode=3 on v4l2sink helps immensely with performance (basically mimicking the io modes of uvc-gadget)

##### 1/21/19

Turns out you need do_timestamp=true on the gstreamer pipeline for DMABUF to work (io-mode=4 on src and io-mode=5 on sink). This also gets rid of the artifacting issue - it turns out this happens even with gstreamer if you're using MMAP/USERPTR (2/3).

Got the new USB 3.0 A<->A crossover cables in. They solve the enumeration issue but seem to struggle with isochronous transfer still. Bulk is working great. The testusb app seems to think there's an issue anytime mult > 0 or maxburst > 0

Target (for example):
```
modprobe g-zero isoc_interval=1 isoc_mult=0 isoc_maxburst=15
```
Host:
```
sudo modprobe usbtest alt=1
sudo ./testusb -D /dev/bus/usb/002/107 -t16
unknown speed	/dev/bus/usb/002/107	0
/dev/bus/usb/002/107 test 16 --> 5 (Input/output error)
```

##### 1/22/19

Mode switching still having some reliability problems evey with gstreamer. Looks like DMABUF may make things worse (kernel panics and lockups) so I'm back to MMAP/USERPTR IO for now. Also, cpu usage is much higher for the dwc3 interrupt with DMABUF/DMABUF_IMPORT. Looks like the memcpy out of the framebuffer is *much* slower:
```
 2) ! 120.081 us  |            uvc_video_encode_bulk [usb_f_uvc]();
```

Switching resolutions to 720p doesn't work at all (unless I remove the 360p descriptor). Yavta captures frames roughly the size of 640x360, but incomplete, so that makes me think there's some sort of buffering issue.

```
Warning: bytes used 458750 != image size 1843200 for plane 0
0 (0) [-] none 0 458750 B 517495.059959 517495.225362 0.414 fps ts mono/SoE
Warning: bytes used 458593 != image size 1843200 for plane 0
1 (1) [-] none 2 458593 B 517495.225291 517495.379405 6.048 fps ts mono/SoE
Warning: bytes used 458691 != image size 1843200 for plane 0
2 (2) [-] none 3 458691 B 517495.391170 517495.545526 6.028 fps ts mono/SoE
Warning: bytes used 458691 != image size 1843200 for plane 0
3 (3) [-] none 5 458691 B 517496.060978 517496.215109 1.493 fps ts mono/SoE
Warning: bytes used 458750 != image size 1843200 for plane 0
4 (4) [-] none 7 458750 B 517496.415563 517496.580960 2.820 fps ts mono/SoE
Warning: bytes used 458591 != image size 1843200 for plane 0
5 (5) [-] none 9 458591 B 517496.580875 517496.735045 6.049 fps ts mono/SoE
Warning: bytes used 458692 != image size 1843200 for plane 0
6 (6) [-] none 10 458692 B 517496.746807 517496.901000 6.027 fps ts mono/SoE
Warning: bytes used 458694 != image size 1843200 for plane 0
7 (7) [-] none 12 458694 B 517497.416651 517497.570948 1.493 fps ts mono/SoE
Warning: bytes used 917341 != image size 1843200 for plane 0
8 (0) [-] none 14 917341 B 517497.754661 517498.086627 2.958 fps ts mono/SoE
Warning: bytes used 917384 != image size 1843200 for plane 0
9 (1) [-] none 16 917384 B 517498.086581 517498.412779 3.013 fps ts mono/SoE
Captured 10 frames in 5.765905 seconds (1.734333 fps, 954607.625790 B/s).
8 buffers released.
```

However, uvc_video, uvc_queue, all seem to indicate correct length of bytes_used and len:
[  737.611960] uvc_queue_setup: imagesize: 1843200
(...)
[  706.633713] uvcg_video_complete: queueing buf: 1843200, 1843200

I tried disabling/enabling the usb endpoint in open/close and/or streamon/streamoff, but so far that's caused more problems and hasn't really fixed anything.

DMA:
Seems like there is a synchronization issue between diferent IO methods if DMA is involved. io-mode=4 on v4l2src and io-mode=3 on v4l2sink seems particularly bad.
gstreamer always performs a buffer copy unless the sink is dmabuf or userptr!

I thought maybe increasing number of buffers would help. Can specify min-buffers on caps filter to set up src, but sink proved difficult - kept wanting to allocate two. Finally had to increase GST_V4L2_MIN_BUFFERS and rebuild gstreamer (also tweak some logic in gstv4l2bufferpool.c: gst_v4l2_buffer_pool_set_config) so that the driver minimum doesn't override the constant. This made things choppier and with a lower framerate.

##### 1/23/19

Key to stopping stream with bulk endpoint is handling the "clear feature" request (per fx3 uvc.c source code!). Turns out dwc3 driver was not delegating this request back to the composite driver, so there was no way for it to know (the endpoint just happened to be halted, but that's it).

##### 1/24/19

Success with stream switching. Had to port over an additional change from uvc-gadget that proved critical:

In uvc_events_init()
```
if (dev->bulk)
 /* FIXME Crude hack, must be negotiated with the driver. */
    dev->probe.dwMaxPayloadTransferSize =
        dev->commit.dwMaxPayloadTransferSize =
            round_down(payload_size, MAX_PAYLOAD_MULT);
```

It seems also sending the last (which is the biggest) descriptor works, too:
```
uvc_fill_streaming_control(dev, &dev->probe, 0, 0);
uvc_fill_streaming_control(dev, &dev->commit, 0, 0);
```

Not totally sure why this makes a difference. Maybe the host requests differnet amounts of data?

For testing, you may want to install newer guvcview than ubuntu 14.04 comes with:

```
sudo add-apt-repository ppa:pj-assis/ppa
sudo apt-get update
sudo apt-get install guvcview
```

Can vew fps of pipeline with fpsdisplaysink even when you need to pass paramters to the video sink::
```
fpsdisplaysink text-overlay=false video-sink="v4l2sink device=/dev/video0"

```
fps was a little low/bouncy until I added a queue in between the source and sink.

##### 1/25/19

Still had an outstanding issue where unplugging the USB cable while streaming didn't stop streaming. Made a build for the zcu106 board, and there it does work properly (likely due to having the circuit properly set up for device mode). Jumpers: J110 (OFF),

##### 1/28/19

Working on enumerating with fewer resolutions in 2.0 mode vs. 3.0 mode. Needed to create separate headers and uncompressed formats in the configfs gadget and add a separate resolution/format list in the uvc-gadget application. Working with reading the link speed via sysfs, except the initial "FillStreamingControl" call seems to be key - Windows isn't happy with the value returned by one for the other, for example if I first plugin as SS and stream, then later unplug and plug via a 2.0 cord, it'll enumerate with the 2.0 descriptors but streaming will be black. Restarting uvc-gadget always fixes the problem.

Other TODO still to work through is the corruption of frames at low resolutions.

##### 1/30/19

Finally, still need to consider what other performance improvements can be made to reach 4k30 streaming resolution. One option would be to eliminate the memcpy in usb_video_encode, such as suggested by these patches: https://www.spinics.net/lists/linux-usb/msg85469.html

Check out `gadget->sg_supported` which is true for dwc3 and is used by f_tcm.c. With this, wouldn't even need a kernel module parameter.

##### 1/31/19

Normal build without sg mode, io-mode=2 and 3 on src and sink respectively:
1080p: irq/34-dwc3: 32%, gst-launch: 4%, load average 0.22 (60fps)
2160p: irq/34-dwc3: 60%, gst-launch: 6%, load average: 0.41 (25fps)
io-mode=4 and 5 do not reach 60fps on 1080p due to 100% cpu for irq-dwc3.


Build with sg mode, io-mode=2 and 3:
Lots todo here yet.

SG mode is proving difficult to avoid complete corruption of the data (header is fine, ironically). Did notice, however, that we can get by with the following settings for both Windows and linux:
ctrl->dwMaxPayloadTransferSize = ctrl->dwMaxVideoFrameSize;
in uv_video_alloc_requests: req_size = max_t(unsigned int, video->imagesize, ...)
In uvc_video_encode_bulk, need to add 2 to every read of video->max_payload_size to account for header.
video->max_payload_size = video->imagesize.

Didn't work when I first applied the change.. must be something else going on. Messing with various permutations of +2 on max_payload_size makes the 180p freak out more (neon colors and such, but no corruption). Could be on to something about the corruption issue.

##### 2/1/19

Streaming can work with Windows as long as
ctrl->dwMaxPayloadTransferSize = ctrl->dwMaxVideoFrameSize
uvc->max_payload_size = video->imagesize;
req_size=video->imagesize

Note that the buffer is kmalloc'd in uvc_video_alloc_requests, so 4k won't work with this by default since its frames are >4MB. I got around this by setting req_size=min(video->imagesize, KMALLOC_MAX_SIZE). Note that dwMaxPayloadTransferSize must be set to corresopnd in the uvc-gadget app:
ctrl->dwMaxPayloadTransferSize = min(ctrl->dwMaxVideoFrameSize, 4194304);


Probably should call vb2_wait_for_all_buffers() before vb2_streamoff()

Still trying to add scatter/gather support from the above patches. I thought maybe I needed to set up the queue memory ops to dma_contig or dma_sg instead of vmalloc, but after getting that "working" it doesn't look any better - just garbage noise for the data. The headers are fine.

##### 2/4/19

Can we even do 4k/30 if no frame data is copied?
    Commented out memcpy in uvc_video_encode_data(). Framerate is exactly the same! (26.81fps)
    Then made max payload transfer size = frame size. Still no improvement (though CPU usage is lower - note potential improvement here). Usage went from ~31% to ~19% with this change at 1080p/60.

Wondering if 4k performance could be improved with one contiguous buffer instead of two buffers split (kmalloc only allows 4MB allocations). Allocated new buffer with dma_alloc_coherent instead of kmalloc. Accessed data in encode() via pointer returned (virtual memory), but set usb controller req->buf to dma_addr_t which is the physical memory address. Note that I made the parent the usb device controller for the appropriate dma mask, which is pretty convoluted:
```
video->req_buffer[i] = dma_alloc_coherent(
    uvc_dev->func.config->cdev->gadget->dev.parent,
    req_size, &video->dma_handle[i], GFP_KERNEL);
```

Had to make req->context a buffer containing both video * and the request number to look up the req_buffer entry in encode().

##### 2/5/19

| Source/Sink Mode | 2     | 3     | 4     | 5    |
| :- | :------------- | :---- | :---- | :---- |
|  2 | No artifacting. High cpu. | Low cpu. Artifacting at low res. | Fail (error) | Fail (unknown) |
|  3 | Fail (contiguous mapping is too small) | Fail (unknown) | Fail (unknown) | Fail (unknown) |
|  4 | No artifacting. High cpu. | Low cpu. Artfacting at low res, frames appear to drop even at full fps at all res. | Fail (?) | No artifacting. High cpu. |
|  5 | Fail | Fail | Fail | Fail |

I thought maybe the vmalloc of the buffer could be causing issues. However, I've since found counter-examples of doing sg dma from a vmalloc buffer such as this one: https://elixir.bootlin.com/linux/v4.7/source/drivers/spi/spi.c#L708. So I think I might just have to set up the scatterlist a bit differently so it follows pages.

##### 8/14/19

Tried PetaLinux 2019.1 kernel. No change in USB3 performance (4k does ~27fps). io-mode=2 to io-mode=3 still results in tearing for very small image sizes.

### Misc USB3.0 notes

[Cypress app not on implementing an image sensor with UVC on the fx3](http://www.cypress.com/file/123506/download)

From that app note, this is why we used bulk endpoints:
Note: The maximum  bandwidth  achievable  using the ISOC-IN  endpoint  under  UVC  for  USB  3.0in  Windows 7/8 machineis 24 Mbps because the UVC driver limits the BURST to 1 for the ISOC endpoint. But with the Windows 10 machine,  BURST  can  be  set  to  15  to  achieve  maximum  ISOC  bandwidth. There  is  no  such  limitation  for  BULK endpoint. Therefore, the ISOC-IN Endpoint is not used in thisproject. Refer to this forum postfor more information

Article about high-bandwidth isochronous transfers and calculating MaxPacketSize, mult, burst, etc: [link](https://docs.microsoft.com/en-us/windows-hardware/drivers/usbcon/transfer-data-to-isochronous-endpoints)

Another set of uvc-gadget fixes:
https://www.spinics.net/lists/linux-usb/msg99220.html

This patch set adds uvc function handling to the kernel instead:
https://www.spinics.net/lists/linux-usb/msg93556.html

### Call with Xilinx 2/21/18

They are having trouble getting it to stream in VLC on Windows 10. Next steps:

- Send rootfs/boot.bin over to akshay via encrypted site
- Try to reproduce vlc issue on windows 10
- If issue is reproduceable - capture log on usb analyzer

### New kernel versions

4.9.19 contains companion descriptor fix
4.9.82 contains lots of v4l2 fixes
4.9.88 (upcoming) appears to have configfs fix

### Streaming time log

1. 3h ??min (VLC, gradient pattern, fast)
2. 41min (VLC, with gradient pattern, fast)
```
root@zcu102-zynqmp-ark:~# fg
uvc-gadget -u /dev/video0 -v /dev/video1 -t 15 -r 2 -n 2 -b
[ 2858.445859] g_webcam gadget: uvc_function_disable
UVC: Possible USB shutdown requested from Host, seen during VIDIO[ 2858.457703] g_webcam gadget: super-speed config #1: Video
[ 2858.465006] g_webcam gadget: uvc_function_set_alt(0, 0)
[ 2858.471762] g_webcam gadget: reset UVC Control
[ 2858.477711] g_webcam gadget: uvc_function_set_alt(1, 0)
[ 2858.484417] g_webcam gadget: reset UVC_DQBUF
UVC: Possible USB shutdown requested from Host, seen during VIDIOC_DQBUF  control request (req 86 cs 04)
control request (req 81 cs 02)
control request (req 86 cs 09)
control request (req 81 cs 02)
control request (req 86 cs 02)
control request (req 82 cs 02)
control request (req 83 cs 02)
control request (req 84 cs 02)
control request (req 87 cs 02)
UVC: Stopping video stream.
```
3. 45-ish min (eCam, squares pattern, fast)
4. 39m (VLC, squares pattern, fast)
5. 13m (VLC, moving colors pattern, fast)
6. 1h 57m (VLC, moving colors, slow)

### Petalinux 2018.1 testing

Had some issues getting the zcu102 to boot with an Ark build - turns out you need to match the pmufw, u-boot, and kernel version, otherwise you'll get a kernel panic about pmu (system) version mismatch.

Things to figure out still:

- [ ] Switching resolutions on Windows doesn't work - you have to restart uvc-gadget every time
- [ ] Changing brightness control quickly in guvcview causes a kernel panic
- [ ] NV12 still not working on Windows

### ZCU104 continued work

Windows 10 doesn't like it if wBytesPerInterval is nonzero. It just said there was an invalid configuration descriptor. I found this by using USB3HWVerifierAnalyzer.exe.

See https://blogs.msdn.microsoft.com/usbcoreblog/2012/07/12/common-issues-in-usb-3-0-devices/ for more help on debugging descriptor issues.


### DFU boot

First image must contain FSBL and PMUFW only. It must be <= 256kB for OCM. In the FSBL build, FSBL_USB_EXCLUDE_VAL must be set to 0 in xfsbl_config.h. Then, build the first image with bootgen:
```
image :
{
    [bootloader, destination_cpu=a5x-0] fsbl.elf
    [pmufw_image] pmufw.elf
}
```
Adding `[boot_device] usb` didn't seem to matter.

The second image can be built/used as normal:
```
image :
{
    [bootloader, destination_cpu=a5x-0] fsbl.elf
    [pmufw_image] pmufw.elf
    [destination_cpu=a5x-0, exception_level=el-3, trustzone] bl31.elf
    [destination_cpu=a5x-0, exception_level=el-2] u-boot.elf
}
```

Load it with dfu-util (you may want to use -d to specify the device):
```
sudo dfu-util -d 03fd:0050 -D BOOT-FIRST.BIN
sudo dfu-util -d 03fd:0050 -D BOOT.BIN
```

See:
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842019/FSBL#FSBL-IcouldseethatUSBbootmodesupportisaddedinFSBLbutisdisabledbydefault.Why
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842468/ZynqMp+USB+Stadalone+Driver#ZynqMpUSBStadaloneDriver-USBDFUTesting
https://events.static.linuxfound.org/sites/events/files/slides/USB%20Gadget%20Configfs%20API_0.pdf
https://training.ti.com/sites/default/files/docs/AM57x-DFU-boot-SLIDES.pdf
https://processors.wiki.ti.com/index.php/AMSDK_u-boot_User's_Guide#Updating_an_SD_card_or_eMMC_using_DFU

##### DFU downloading to ram in u-boot

Tested with 2019.2 u-boot:
```
setenv dfu_alt_info rootimg ram 0x80000 0x3b600000
dfu 0 ram 0
```
On host:
```
sudo dfu-util -D vaddio-app-debug-image-zcu104-zynqmp-ark.wic.gz
```
(...)
66775028 bytes

Then write to mmc in u-boot (need to update file size, not sure how to get this from u-boot?):
```
gzwrite mmc 0 0x80000 0x3fae7e4
```
Could write directly to mmc as well if uncompressed using `mmc write`.

Turns out you can just give gzwrite an arbitrarily large size (such as the full size of the RAM area), and it'll stop once the CRC is no longer valid for the image.

Use `serial#` env variable to control serial number showed via USB DFU interface. Maybe we should set this to ethaddr for Manuf?

USB DFU boot script (inspired by xilinx_versal.h):
```
setenv bootcmd_dfu_usb 'echo Downloading boot script via DFU; setenv dfu_alt_info boot.scr ram $scriptaddr $script_size_f; dfu 0 ram 0 && source $scriptaddr'
setenv boot_targets dfu_usb
boot
```

Boot script contents:
```
echo DFU download of MMC image
setenv dfu_alt_info mmc.img.gz ram 0x80000 0x3b600000
if dfu 0 ram 0 ; then
    echo Decompressing image to MMC...
    gzwrite mmc 0 0x80000 0x3b600000
    echo Done. Disengage USB boot mode switch and power cycle device.
    sleep 1000000
fi
```

Generate image file with:
```
mkimage -A arm -T script -C none -n "DFU Boot Script" -d dfu-image.cmd dfu-image.scr
```

##### Read or write of emmc directly with DFU Download/Upload

Size is maximum size to read and is in sectors (512 bytes).
```
setenv dfu_mmc_size 0x100000
setenv dfu_alt_info mmc.img raw 0 $dfu_mmc_size
echo Ready for DFU access to MMC image, maximum $dfu_mmc_size bytes
dfu 0 mmc 0
echo Done.
```

This method bypasses the gunzip process, and uses less RAM, but takes much longer.
Be sure to set mmc size to size of image or larger.

##### Build DFU Util on Windows

Follow MSYS2 build instructions in dfu-util/www/build.html
After installing MSYS2 but before starting build, install additional packages with pacman:
```
automake-wrapper
libtool
pkg-config
```

Must distribute libpthread-1.dll from MinGW bin directory with binary.

##### Verified/Signed boot of boot scripts

Boot scripts can be in FIT image format which allows signature verification if desired.

Generate wrapper with image.its as shown:
```
/dts-v1/;

/ {
    description = "U-Boot boot script image";
    #address-cells = <1>;

    images {
        default = "script@0";
        script@0 {
            description = "MMC fitImage boot";
            data = /incbin/("./boot.cmd.sd.zynqmp");
            type = "script";
            arch = "arm64";
            compression = "none";
            hash@1 {
                algo = "sha1";
            };
        };
    };
};
```

mkimage -f image.its script.scr

Simply load with `source`. Note the `default` property is very important, otherwise source will complain about the FIT subimage unit name being missing and you'll need to specify it with a colon after the address.

For secure boot, see the following in u-boot repository:
doc/uImage.FIT/signature.txt
doc/uImage.FIT/verified-boot.txt
doc/uImage.FIT/beaglebone_vboot.txt

##### Using FSBL for SuperSpeed validation for board test

Need to remove HIGHSPEED manual setting from XFsbl_UsbInit() call in xfsbl_usb.c.
Also need to handle `SET_ISOCH_DELAY` (0x31) request in XFsbl_StdDevReq() in same file.
Otherwise enumeration will fail in Windows and you will see a STALL error on the FSBL prompt.

Need to set secondary boot mode on initial BOOT.BIN image if we would like it to fall back to USB boot when booting from MMC:
```
[boot_device] usb
```

Note: Need to update `bootgen` utility version since the old version currently used on Ark does not handle secondary boot devices correctly.

##### Write fitImage over DFU

```
setenv dfu_alt_info fitImage ram $kernel_addr_r 0x8000000 ; dfu 0 ram 0
ext4write mmc 0:2 $kernel_addr_r /boot/fitImage $filesize
```

### Power Management

Page talks about CPU frequency and idle, suspend to ram, wakeup, etc:
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842232/Zynq+UltraScale+MPSoC+Power+Management+-+Linux+Kernel

### Video pipeline work with Dennis

Run the following commands to set up and capture frames in RGB mode:
```
media-ctl -v -V '"80040000.v_tpg":0[fmt:RGB888_1X24 1920x1080]'
yavta -c10 -f RGB24 -s 1920x1080 --skip 7 -F /dev/video0
```

### Toshiba TC358840

A driver exists but is not yet mainlined. See hverkuil/media-tree/tc358840-2
See also: https://developer.ridgerun.com/wiki/index.php?title=Toshiba_TC358840_Linux_driver_for_Jetson_TX1_and_TX2
The driver in the kernel sources for the Jetson board has been modified by Nvidia and is differnet from both of Hans Verkuil's versions.
https://devtalk.nvidia.com/default/topic/1005674/regarding-drivers-for-toshiba-tc358840/

This page might have a more up-to-date version of the driver:
https://blog.zhaw.ch/high-performance/2017/10/24/open-source-drivers-for-hdmi2csi-module-updated-to-support-tx1-and-tx2/

https://lwn.net/Articles/706370/

##### Prototyping

zcu104 board. Use J160 (PMOD I2C), SCL pins 5/6, SDA pins 7/8. GND: 9/10. VCC: 11/12
Will operate on I2C1. Avoid addresses 0x20, 0x74, and 0x7C.

On TC358840 dev kit:
- Ensure jumpers are on pins 1-2 and pins 3-4 of JP607 and JP608 (i2c jumper matrix).
- Set JP613 pins 1-2 for I2C level shifting to VCC_EXT

Connect GND, SCL, SDA of J604 (pins 2, 3, 4, respectively)

/dev/i2c0 is actually I2C1 (first bus is disabled). Chip shows up at address 0x0f by default due to strapping low of INT pin:

```
root@vaddio-generic-00-0A-35-04-D2-73:~# i2cdetect -y -r 0          
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f     
00:          -- -- -- -- -- -- -- -- -- -- -- -- 0f     
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --     
20: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- --     
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --     
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --     
50: -- -- -- -- 54 55 56 57 -- -- -- -- -- -- -- --     
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --     
70: -- -- -- -- 74 -- -- --
```

TC358840 can convert between RGB and YCbCr.

Things to do:
DDC I2C (I2C over HDMI?)

Switched to using Hans' driver that's (almost) mainline-ready instead of Nvidia.
This driver uses polling for most format detection (see big comments in driver).
HPD doesn't work until EDID is loaded. Just using Helios' edid:
```
v4l2-ctl -d /dev/v4l-subdev1 --set-edid pad=0,file=edid-helios.hex
```

1080p60 timings:
```
v4l2-ctl --set-dv-bt-timings index=12 -d /dev/v4l-subdev1
```

```
cd /tmp ; yavta -c10 -f YUYV -s 1920x1080 /dev/video0
```

MIPI pin setup
Clock: pin L20 LPC_LA02_P/N (FMC CN2 6/5)
Data0: pin J16 LPC_LA07_P/N (FMC CN2 10/9)
Data1: pin K17 LPC_LA05_P/N (FMC CN4 6/5)
Data2: pin L15 LPC_LA10_P/N (FMC CN4 12/11)
Data3: pin K19 LPC_LA03_P/N (FMC CN3 8/7)
INT:   pin F8  LPC_LA32_P   (FMC CN2 26)

Needed to override Vadj for the FMC breakout in the FSBL (see XFsbl_CalVadj function). There is no eeprom specifying the voltage so it always returned 0.0V, resulting in the default of 1.2V. Manually modified to return 1.8V so that the level shifters are set up appropriately.

MIPI CSI1 lane setup (4k only)
Clock: pin F17 LPC_LA00_CC_P/N  (FMC CN3 6/5)
Data0: pin G15 LPC_LA13_P/N     (FMC CN4 14/13)
Data1: pin H19 LPC_LA06_P/N     (FMC CN4 8/7)
Data2: pin H18 LPC_LA01_CC_P/N  (FMC CN4 4/3)
Data3: pin G18 LPC_LA12_P/N     (FMC CN3 12/11)

INT pin proved difficult - it is a 1.8V IO which is relatively hard to find on the zcu104. Ended up routing through one of the FMC pins since I am using 1.8V IO there.

LI-IMX274MIPI-FMC camera testing
Clock: pin F17 LA00_P/N_CC (FMC J4G G6/7)
Data0: pin L15 LA10_P/N (FMC C14/C15)
Data1: pin H18 LA01_P/N (FMC D8/9)
Data2: pin E18 LA08_P/N (FMC G12/13)
Data3: pin J16 LA07_P/N (FMC H13/14)

Trouble getting vpss-scaler to probe. Turns out when the gpio probe is deferred the DRM driver loads in its place. Worked aroun this by changing the compatible string in the Xilinx DRM scaler driver `drivers/gpu/drm/xlnx/xlnx_scaler.c`

Finally captured some frames from the MIPI camera:
```
v4l2-ctl -d /dev/v4l-subdev1 -c mipi_csi2_rx_subsystem_active=4
media-ctl -v -V '"IMX274":0 [fmt:SRGGB8/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0010000.v_demosaic":0 [fmt:SRGGB8_1X8/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0010000.v_demosaic":1 [fmt:RGB888_1X24/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0080000.v_proc_ss":1 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
yavta -c10 -f YUYV -s 1920x1080 -F'/run/image-#.raw' /dev/video0
```

For Monday:

Used spreadsheet to generate values for 1080p60. Filled those in in the dts. Set link rate to 912Mb/s (456MHz hclk). Now I do not get any errors and see frame number increasing on the MIPI core. Framebuffer write isn't doing anything, though.

```
root@vaddio-generic-00-0A-35-04-D2-73:~# v4l2-ctl --log-status -d /dev/v4l-subdev0

Status Log:                                   

[  629.302172] a0040000.mipi_csi2_rx_subsystem: =================  START STATUS  ==============     
[  629.302180] a0040000.mipi_csi2_rx_subsystem: Frame Received events: 344     
[  629.302183] a0040000.mipi_csi2_rx_subsystem: ***** Core Status *****        
[  629.302186] a0040000.mipi_csi2_rx_subsystem: Short Packet FIFO Full = false
[  629.302188] a0040000.mipi_csi2_rx_subsystem: Short Packet FIFO Not Empty = false        
[  629.302190] a0040000.mipi_csi2_rx_subsystem: Stream line buffer full = false
[  629.302193] a0040000.mipi_csi2_rx_subsystem: Soft reset/Core disable in progress = false
[  629.302195] a0040000.mipi_csi2_rx_subsystem: ******** Clock Lane Info *********         
[  629.302197] a0040000.mipi_csi2_rx_subsystem: Clock Lane in Stop State = false           
[  629.302199] a0040000.mipi_csi2_rx_subsystem: ******** Data Lane Info *********          
[  629.302201] a0040000.mipi_csi2_rx_subsystem: Lane SoT Error       SoT Sync Error  Stop te        
[  629.302204] a0040000.mipi_csi2_rx_subsystem: 0    false           false           false
[  629.302207] a0040000.mipi_csi2_rx_subsystem: 1    false           false           false
[  629.302210] a0040000.mipi_csi2_rx_subsystem: 2    false           false           false
[  629.302213] a0040000.mipi_csi2_rx_subsystem: 3    false           false           false
[  629.302215] a0040000.mipi_csi2_rx_subsystem: ********** Virtual Channel Info *********           
[  629.302217] a0040000.mipi_csi2_rx_subsystem: VC   Line Count      Byte Count      Data e         
[  629.302221] a0040000.mipi_csi2_rx_subsystem: 0    44798           3840            422_8bit       
[  629.302225] a0040000.mipi_csi2_rx_subsystem: 1    0               0               (null)
[  629.302228] a0040000.mipi_csi2_rx_subsystem: 2    0               0               (null)
[  629.302231] a0040000.mipi_csi2_rx_subsystem: 3    0               0               (null)
[  629.302234] a0040000.mipi_csi2_rx_subsystem: 4    0               0               (null)
[  629.302238] a0040000.mipi_csi2_rx_subsystem: 5    0               0               (null)
[  629.302241] a0040000.mipi_csi2_rx_subsystem: 6    0               0               (null)
[  629.302244] a0040000.mipi_csi2_rx_subsystem: 7    0               0               (null)
[  629.302247] a0040000.mipi_csi2_rx_subsystem: 8    0               0               (null)
[  629.302250] a0040000.mipi_csi2_rx_subsystem: 9    0               0               (null)
[  629.302253] a0040000.mipi_csi2_rx_subsystem: 10   0               0               (null)
[  629.302257] a0040000.mipi_csi2_rx_subsystem: 11   0               0               (null)
[  629.302260] a0040000.mipi_csi2_rx_subsystem: 12   0               0               (null)
[  629.302263] a0040000.mipi_csi2_rx_subsystem: 13   0               0               (null)
[  629.302266] a0040000.mipi_csi2_rx_subsystem: 14   0               0               (null)
[  629.302269] a0040000.mipi_csi2_rx_subsystem: 15   0               0               (null)
[  629.302272] a0040000.mipi_csi2_rx_subsystem: ==================  END STATUS  ===============
   ```

Monday: Try RGB8 instead of YUV 4:2:2 to see if axi data width converter was keeping framebuffer write from running.

RGB8 in tc358840 -> RBG8 in MIPI rx css causing issues. Overrode format check in `xcsi2rxss_set_format` so I could set different formats on the input and output of the MIPI rx ss.

` xilinx-csi2rxss a0040000.mipi_csi2_rx_subsystem: Stream Line Buffer Full!``
csi2-rx interrupt seems to hog the cpu it's on when the line buffer is full. For now, disallow it from running on cpu0 so serial/network stay responsive:
```
echo e > /proc/irq/31/smp_affinity
```

All commands to capture RGB24 frames from MIPI chip @1080p60:
```
modprobe tc358840
v4l2-ctl -d /dev/v4l-subdev2 --set-edid pad=0,file=edid-helios.hex
v4l2-ctl -d /dev/v4l-subdev0 -c mipi_csi2_rx_subsystem_active=4
v4l2-ctl --set-dv-bt-timings index=12 -d /dev/v4l-subdev2
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":0 [fmt:RBG888_1X24/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":1 [fmt:RGB888_1X24/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0080000.v_proc_ss":1 [fmt:RBG888_1X24/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"tc358840 0-000f":0 [fmt:RGB888_1X24/1920x1080 field:none colorspace:srgb]'
yavta -c10 -f RGB24 -s 1920x1080 -F'/run/image-#.raw' /dev/video0
```

RGB 1080p pipeline looks as follows:
```
Media controller API version 4.14.0

Media device information
------------------------
driver          xilinx-video
model           Xilinx Video Composite Device
serial          
bus info        
hw revision     0x0
driver version  4.14.0

Device topology
- entity 1: vcap_mipi output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "a0080000.v_proc_ss":1 [ENABLED]

- entity 5: a0040000.mipi_csi2_rx_subsystem (2 pads, 2 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
		[fmt:RBG888_1X24/1920x1080 field:none colorspace:srgb]
		-> "a0080000.v_proc_ss":0 [ENABLED]
	pad1: Sink
		[fmt:RGB888_1X24/1920x1080 field:none colorspace:srgb xfer:srgb quantization:full-range]
		<- "tc358840 0-000f":0 [ENABLED]

- entity 8: a0080000.v_proc_ss (2 pads, 2 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev1
	pad0: Sink
		[fmt:RBG888_1X24/1920x1080 field:none colorspace:srgb]
		<- "a0040000.mipi_csi2_rx_subsystem":0 [ENABLED]
	pad1: Source
		[fmt:RBG888_1X24/1920x1080 field:none colorspace:srgb]
		-> "vcap_mipi output 0":0 [ENABLED]

- entity 11: tc358840 0-000f (2 pads, 1 link)
             type V4L2 subdev subtype Unknown flags 0
             device node name /dev/v4l-subdev2
	pad0: Source
		[fmt:RGB888_1X24/1920x1080 field:none colorspace:srgb xfer:srgb quantization:full-range]
		[dv.caps:BT.656/1120 min:160x120@25000000 max:1920x1200@165000000 stds:CEA-861,DMT,CVT,GTF caps:progressive,reduced-blanking,custom]
		[dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds: flags:]
		[dv.current:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861,DMT flags:can-reduce-fps,CE-video,has-cea861-vic]
		-> "a0040000.mipi_csi2_rx_subsystem":1 [ENABLED]
	pad1: Source
		[dv.caps:BT.656/1120 min:160x120@25000000 max:1920x1200@165000000 stds:CEA-861,DMT,CVT,GTF caps:progressive,reduced-blanking,custom]
		[dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds: flags:]
		[dv.current:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861,DMT flags:can-reduce-fps,CE-video,has-cea861-vic]
```

For YUYV, had to implement AXI4-Stream subset converter between MIPI core and v_proc_ss. ensure the converter is set up to pass TREADY, TLAST, and TUSER (bit 0 only), otherwise the DMA will not run!

```
modprobe tc358840
v4l2-ctl -d /dev/v4l-subdev2 --set-edid pad=0,type=hdmi
v4l2-ctl -d /dev/v4l-subdev0 -c mipi_csi2_rx_subsystem_active=4
v4l2-ctl --set-dv-bt-timings index=12 -d /dev/v4l-subdev2
media-ctl -v -V '"a0080000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
yavta -c10 -f YUYV -s 1920x1080 -F'/run/image-#.raw' /dev/video0
```

With tc358840 built in to kernel:
```
v4l2-ctl -d /dev/v4l-subdev0 --set-edid pad=0,type=hdmi
v4l2-ctl -d /dev/v4l-subdev1 -c mipi_csi2_rx_subsystem_active=4
v4l2-ctl --set-dv-bt-timings index=12 -d /dev/v4l-subdev0
media-ctl -v -V '"a0080000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
yavta -c10 -f YUYV -s 1920x1080 -F'/run/image-#.raw' /dev/video0
```

YUYV 4:2:2 1080p pipeline:
```
Media controller API version 4.14.0

Media device information
------------------------
driver          xilinx-video
model           Xilinx Video Composite Device
serial          
bus info        
hw revision     0x0
driver version  4.14.0

Device topology
- entity 1: vcap_mipi output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "a0080000.v_proc_ss":1 [ENABLED]

- entity 5: a0040000.mipi_csi2_rx_subsystem (2 pads, 2 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
		[fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]
		-> "a0080000.v_proc_ss":0 [ENABLED]
	pad1: Sink
		[fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]
		<- "tc358840 0-000f":0 [ENABLED]

- entity 8: a0080000.v_proc_ss (2 pads, 2 links)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev1
	pad0: Sink
		[fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]
		<- "a0040000.mipi_csi2_rx_subsystem":0 [ENABLED]
	pad1: Source
		[fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]
		-> "vcap_mipi output 0":0 [ENABLED]

- entity 11: tc358840 0-000f (2 pads, 1 link)
             type V4L2 subdev subtype Unknown flags 0
             device node name /dev/v4l-subdev2
	pad0: Source
		[fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb xfer:srgb ycbcr:601 quantization:lim-range]
		[dv.caps:BT.656/1120 min:160x120@25000000 max:1920x1200@165000000 stds:CEA-861,DMT,CVT,GTF caps:progressive,reduced-blanking,custom]
		[dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds: flags:]
		[dv.current:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861,DMT flags:can-reduce-fps,CE-video,has-cea861-vic]
		-> "a0040000.mipi_csi2_rx_subsystem":1 [ENABLED]
	pad1: Source
		[dv.caps:BT.656/1120 min:160x120@25000000 max:1920x1200@165000000 stds:CEA-861,DMT,CVT,GTF caps:progressive,reduced-blanking,custom]
		[dv.detect:BT.656/1120 1920x1080p60 (2200x1125) stds: flags:]
		[dv.current:BT.656/1120 1920x1080p60 (2200x1125) stds:CEA-861,DMT flags:can-reduce-fps,CE-video,has-cea861-vic]
```

I was having to manually place the entries for the scaler and vidcap interface in the dtsi until I changed the scaler from "bilinear" to "bicubic".

Current challenges:

1. If we don't use Xilinx's video pipeline, a custom bridge driver such as what we tried to do for Hamfast will be necessary.
2. 4k input requires two MIPI cores, each which receives half of the frame. The output from these cores must be combined.
3. For the 4k case, this complicates the media graph structure. The tc358840 does have two output ports, but these must be connected to both MIPI cores which then must be connected to either a custom video driver or a modified xilinx driver that is capable of accepting two port inputs.
4. Devicetree generation currently breaks in the SDK if two MIPI cores are connected through custom IP to one Xilinx core.

`v4l2-ctl` has the ability to generate and set EDIDs, for example a 4k EDID: `--set-edid pad=0,type=hdmi-4k-300mhz`

Full 4k input setup:
```
v4l2-ctl -d /dev/v4l-subdev3 --set-edid pad=0,type=hdmi-4k-300mhz
v4l2-ctl --set-dv-bt-timings index=89 -d /dev/v4l-subdev3
v4l2-ctl -d /dev/v4l-subdev0 -c mipi_csi2_rx_subsystem_active=4
v4l2-ctl -d /dev/v4l-subdev1 -c mipi_csi2_rx_subsystem_active=4
media-ctl -v -V '"a0080000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0080000.v_proc_ss":2 [fmt:UYVY8_1X16/1920x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0080000.v_proc_ss":1 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/1920x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/1920x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0010000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/1920x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0010000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/1920x2160 field:none colorspace:srgb]'
```

### MIPI FPGA Design

With frame_stream.v, we can reduce the line buffers of the MIPI cores all the way down to 128 lines (can go up to 512 and stay within 1 BRAM per core, so may want to keep that as a minimum). Just added a FIFO after the second core which is the one that needs to be held off. This fifo only needs to be 1024 entries long (it fills up to 958 entries while waiting for the other core).

What about changing resolutions?
It seems the Toshiba chip mirrors the video across both MIPI outputs, because at lower resolutions things just seem to work seamlessly. Still need to make sure to set MIPI active lanes to 4 for each chip.

```
gst-launch-1.0 -v v4l2src device=/dev/video0 do-timestamp=true io-mode=5 ! video/x-raw,width=3840,height=2160,format=YUY2 ! queue ! v4l2video1convert capture-io-mode=4 output-io-mode=4 ! video/x-raw,width=1920,height=1080, format=YUY2 ! queue ! fpsdisplaysink video-sink="multifilesink location=/run/image-%06d.raw" text-overlay=false
```

```
exec gst-launch-1.0 \
    v4l2src \
        device=/dev/video0 \
        do-timestamp=true \
        io-mode=5 ! \
    video/x-raw,format=YUY2,width=3840,height=2160,framerate=${fps}/1 ! \
    queue ! \
    v4l2video1convert capture-io-mode=4 output-io-mode=4 ! \
    video/x-raw,format=YUY2,width=${width},height=${height},framerate=${fps}/1 ! \
    queue ! \
    videorate drop-only=true ! \
    queue ! \
    v4l2sink \
        device=/dev/video2 \
        io-mode=5
```

MIPI input still seems a little flaky - it will run from somewhere between 20 seconds and several minutes before hanging (usually 10000 frames is enough to see it). Confirmed this isn't related to USB or the m2m scaler by running vivid -> m2m scaler -> USB for a *long* time.

Ran all night using vivid streaming out USB, so I am almost positive the hangups are due to the MIPI core input which is a little hoky with jumper wires.

##### USB Streaming with SG

Finally got SG working. Needed to use dma-sg and dmabuf with io-mode=5 for dmabuf import. Another key was using the correct device; the parent of the gadget device is the USB host controller that can handle DMA mappings:
```
struct usb_composite_dev *cdev = uvc->func.config->cdev;
struct device *parent = cdev->gadget->dev.parent;
```

Use `vb2_dma_sg_plane_desc(vb, 0);` to get the sg table of the data.

It also turns out you don't need to use multiples of 64k or whatever for Windows to work, and this caused an issue with 4k because I was rounding the packets down. Instead, just set the max paylaod to be the actual size of the data plus the header size and all is well.

##### USB Streaming setup with m2m

setup-4k.sh:
```
#!/bin/sh

v4l2-ctl -d /dev/v4l-subdev0 --set-edid pad=0,type=hdmi-4k-300mhz
v4l2-ctl -d /dev/v4l-subdev0 --set-dv-bt-timings index=89
v4l2-ctl -d /dev/v4l-subdev1 -c mipi_csi2_rx_subsystem_active=4
v4l2-ctl -d /dev/v4l-subdev2 -c mipi_csi2_rx_subsystem_active=4
media-ctl -v -V '"a0080000.v_proc_ss":1 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0060000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0060000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]'
```

setup-1080p.sh:
```
#!/bin/sh

v4l2-ctl -d /dev/v4l-subdev0 --set-edid pad=0,type=hdmi-4k-300mhz
v4l2-ctl -d /dev/v4l-subdev0 --set-dv-bt-timings index=12
v4l2-ctl -d /dev/v4l-subdev1 -c mipi_csi2_rx_subsystem_active=4
v4l2-ctl -d /dev/v4l-subdev2 -c mipi_csi2_rx_subsystem_active=4
media-ctl -v -V '"a0080000.v_proc_ss":1 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0060000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0060000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":1 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
media-ctl -v -V '"a0040000.mipi_csi2_rx_subsystem":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]'
```

Be sure to update inwidth and inheight in `/usr/bin/uvc-stream` with input resolution.

##### USB streaming 2x streams


Using Eagle hardware, tee'd off pipeline after v4l2convert. Needed to increase min_buffers in v4l2src plugin to eliminate tearing (min_buffers + 4 for our pool and +2 for import). Unfortunately maxing out at 1080p 52-ish fps. 720p can hit 60. Will need to see if there is another delay in the gstreamer pipeline, or if that's just what the limit is. CMA usage is just under 60MB (including 720p output to HDMI).

Memory B/W:
Write: 241 MB/s
Read: 1089MB/s

```
exec gst-launch-1.0 \
    v4l2src \
        do-timestamp=true \
        device=/dev/video1 \
        io-mode=4 ! \
    video/x-raw,format=YUY2,width=${inwidth},height=${inheight},framerate=${infps}/1 ! \
    tee name=t ! \
    queue ! \
    videorate ! \
    video/x-raw,framerate=${fps}/1 ! \
    ${VIDEOCONV} \
    queue ! \
    tee name=usbt ! \
    queue ! \
    v4l2sink \
        device=/dev/video5 \
        io-mode=5 \
    usbt. ! \
    queue ! \
    v4l2sink device=/dev/video7 io-mode=5 \
    t. ! \
    queue ! \
    video/x-raw,width=1920,height=1080 ! \
    v4l2sink \
        device=/dev/video0 \
        io-mode=5
```

##### APM for performance monitoring

Built-in to PS, see TRM Figure 15-1.

Need to add portion to dts for DDR monitoring, see wiki article: https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842046/APM

Xilinx has sample application in kernel samples/ directory for reading the APM.
It does not reset performance counters by default! I had to add a call to resetmetriccounter() before starting monitoring. Also added some usleep(10)'s after the register read and write in that function otherwise it didn't seem t stick. Not sure why that is?

Video streaming bandwidth is fairly close to calculated with the exception of USB that appears to use around 2x the video bandwidth. Wondering if this is simply related to memory activity induced by all of the usb requests.

##### VPSS scaler

4k with 4:2:2 and 4:2:0 at 2 pixels per clock uses:
- Bilinear: 16 BRAM
- Bicubic: 22 BRAM
- Polyphase 12-tap: 50 BRAM

Disabling 4:2:0 support does drop BRAMs (50 to 40 in polyphase case).

No CSC, 4:4:4 and 4:2:2 only at 2 pixels/clock:
- Bilinear: 8 BRAM
- Bicubic: 14 BRAM
- Polyphase 12-tap: 40 BRAM

No CSC, 4:4:4 and 4:2:2 only at 1 pixels/clock:
- Bilinear: ? BRAM
- Bicubic: 15 BRAM
- Polyphase 12-tap: ? BRAM

But FB Read/Write DMA use 3 BRAM instead of 4, and routing was faster.

##### DRM/KMS stuff

For gstreamer-plugins-bad:
`PACKAGECONFIG_append = " kms"`
Requires libdrm and libkms.

Install `libdrm-tests` for `modetest`.

KMS with displayport:

Set up alpha value first:
```
modetest -M xlnx -w 35:alpha:128
```

Primary plane (only RGBA/BGRA):
```
gst-launch-1.0 videotestsrc ! video/x-raw,format=RGBA,width=1920,height=1080 ! kmssink driver-name=xlnx plane-id=35 force-modesetting=true
```

Overlay plane (supports YUY2 etc):
```
gst-launch-1.0 v4l2src device=/dev/video0 do-timestamp=true io-mode=5 ! video/x-raw,format=YUY2,width=3840,height=2160,framerate=30/1 ! queue ! v4l2video2convert capture-io-mode=4 output-io-mode=4 ! video/x-raw,format=YUY2,width=1920,height=1080 ! queue ! videorate ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=60/1 ! kmssink driver-name=xlnx plane-id=34
```

Seems to only work if `force-modesetting` is turned on for the primary plane but *not* the overlay plane.
Killing the stream to the primary plane kills the overlay stream as well.

Can stream to both KMS and USB simultaneously:
```
gst-launch-1.0 v4l2src device=/dev/video0 do-timestamp=true io-mode=5 ! video/x-raw,format=YUY2,width=3840,height=2160,framerate=30/1 ! queue ! v4l2video2convert capture-io-mode=4 output-io-mode=4 ! video/x-raw,format=YUY2,width=1920,height=1080 ! queue ! videorate ! video/x-raw,format=YUY2,width=1920,height=1080,framerate=60/1 ! tee name=videot ! queue ! kmssink driver-name=xlnx plane-id=34 videot. ! queue ! v4l2sink device=/dev/video3 io-mode=5
```

Todo: What does `fullscreen-overlay` do that xilinx added to `kmssink` ?

### AI


```
media-ctl -v -V '"a0080000.v_proc_ss":0 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb]' -d /dev/media2
media-ctl -v -V '"a0080000.v_proc_ss":1 [fmt:RBG888_1X24/500x280 field:none colorspace:srgb]' -d /dev/media2

python3 people_counter.py --prototxt mobilenet_ssd/MobileNetSSD_deploy.prototxt --model mobilenet_ssd/MobileNetSSD_deploy.caffemodel --input "v4l2src device=/dev/video0 do-timestamp=true ! video/x-raw,format=YUY2,width=3840,height=2160,framerate=30/1 ! v4l2video1convert ! video/x-raw,format=BGR,width=500,height=280 ! videorate ! video/x-raw,framerate=10/1 ! appsink" -s 10
```

### Video cropping

media-ctl -v -V '"a0001000.crop":0 [fmt:UYVY8_1X16/3840x2160 field:none colorspace:srgb crop:(1920,540)/1920x1080]'

### Memory Bandwidth

1GB, 1066MHz, 32-bit (dev kit):
```
tinymembench v0.4 (simple benchmark for memory throughput and latency)

==========================================================================
== Memory bandwidth tests                                               ==
==                                                                      ==
== Note 1: 1MB = 1000000 bytes                                          ==
== Note 2: Results for 'copy' tests show how many bytes can be          ==
==         copied per second (adding together read and writen           ==
==         bytes would have provided twice higher numbers)              ==
== Note 3: 2-pass copy means that we are using a small temporary buffer ==
==         to first fetch data into it, and only then write it to the   ==
==         destination (source -> L1 cache, L1 cache -> destination)    ==
== Note 4: If sample standard deviation exceeds 0.1%, it is shown in    ==
==         brackets                                                     ==
==========================================================================

 C copy backwards                                     :   1440.7 MB/s (1.2%)
 C copy backwards (32 byte blocks)                    :   1459.2 MB/s (1.6%)             
 C copy backwards (64 byte blocks)                    :   1438.7 MB/s (0.9%)             
 C copy                                               :   1470.0 MB/s (0.2%)             
 C copy prefetched (32 bytes step)                    :   1173.7 MB/s (0.5%)             
 C copy prefetched (64 bytes step)                    :   1255.7 MB/s (0.3%)             
 C 2-pass copy                                        :   1488.0 MB/s (0.2%)             
 C 2-pass copy prefetched (32 bytes step)             :   1106.0 MB/s (0.2%)             
 C 2-pass copy prefetched (64 bytes step)             :   1034.4 MB/s (0.2%)             
 C fill                                               :   3899.1 MB/s (0.7%)             
 C fill (shuffle within 16 byte blocks)               :   3900.2 MB/s (0.7%)             
 C fill (shuffle within 32 byte blocks)               :   3896.0 MB/s (0.7%)             
 C fill (shuffle within 64 byte blocks)               :   3887.8 MB/s (0.6%)             
 ---                                                                                     
 standard memcpy                                      :   1467.7 MB/s (0.4%)             
 standard memset                                      :   3901.4 MB/s (0.6%)             
 ---                                                                                     
 NEON LDP/STP copy                                    :   1514.9 MB/s (0.4%)             
 NEON LD1/ST1 copy                                    :   1529.3 MB/s (0.2%)             
 NEON STP fill                                        :   3897.4 MB/s (0.8%)             
 NEON STNP fill                                       :   1305.1 MB/s (1.8%)             
 ARM LDP/STP copy                                     :   1520.6 MB/s (0.4%)             
 ARM STP fill                                         :   3887.0 MB/s (0.5%)             
 ARM STNP fill                                        :   1323.3 MB/s (1.9%)             

==========================================================================               
== Memory latency test                                                  ==               
==                                                                      ==               
== Average time is measured for random memory accesses in the buffers   ==               
== of different sizes. The larger is the buffer, the more significant   ==               
== are relative contributions of TLB, L1/L2 cache misses and SDRAM      ==               
== accesses. For extremely large buffer sizes we are expecting to see   ==               
== page table walk with several requests to SDRAM for almost every      ==               
== memory access (though 64MiB is not nearly large enough to experience ==               
== this effect to its fullest).                                         ==               
==                                                                      ==               
== Note 1: All the numbers are representing extra time, which needs to  ==               
==         be added to L1 cache latency. The cycle timings for L1 cache ==               
==         latency can be usually found in the processor documentation. ==               
== Note 2: Dual random read means that we are simultaneously performing ==               
==         two independent memory accesses at a time. In the case if    ==               
==         the memory subsystem can't handle multiple outstanding       ==               
==         requests, dual random read has the same timings as two       ==               
==         single reads performed one after another.                    ==               
==========================================================================               

block size : single random read / dual random read, [MADV_NOHUGEPAGE]                    
      1024 :    0.0 ns          /     0.0 ns                                             
      2048 :    0.0 ns          /     0.0 ns                                             
      4096 :    0.0 ns          /     0.0 ns                                             
      8192 :    0.0 ns          /     0.0 ns                                             
     16384 :    0.0 ns          /     0.0 ns                                             
     32768 :    0.0 ns          /     0.0 ns                                             
     65536 :    5.8 ns          /     9.6 ns                                             
    131072 :    8.9 ns          /    13.3 ns                                             
    262144 :   10.4 ns          /    14.8 ns                                             
    524288 :   11.2 ns          /    15.5 ns                                             
   1048576 :   12.5 ns          /    17.1 ns                                             
   2097152 :   77.4 ns          /   115.4 ns                                             
   4194304 :  116.8 ns          /   154.4 ns                                             
   8388608 :  137.0 ns          /   169.6 ns                                             
  16777216 :  148.4 ns          /   178.7 ns                                             
  33554432 :  154.8 ns          /   184.5 ns                                             
  67108864 :  158.4 ns          /   187.9 ns                                             

block size : single random read / dual random read, [MADV_HUGEPAGE]                      
      1024 :    0.0 ns          /     0.0 ns                                             
      2048 :    0.0 ns          /     0.0 ns                                             
      4096 :    0.0 ns          /     0.0 ns                                             
      8192 :    0.0 ns          /     0.0 ns                                             
     16384 :    0.0 ns          /     0.0 ns                                             
     32768 :    0.0 ns          /     0.0 ns                                             
     65536 :    5.8 ns          /     9.6 ns                                             
    131072 :    8.9 ns          /    13.3 ns                                             
    262144 :   10.4 ns          /    14.9 ns                                             
    524288 :   11.2 ns          /    15.5 ns                                             
   1048576 :   12.5 ns          /    17.2 ns                                             
   2097152 :   76.8 ns          /   114.8 ns                                             
   4194304 :  109.8 ns          /   143.3 ns                                             
   8388608 :  125.9 ns          /   151.9 ns                                             
  16777216 :  134.2 ns          /   155.1 ns                                             
  33554432 :  138.1 ns          /   156.3 ns                                             
  67108864 :  139.9 ns          /   156.8 ns
```

### 2019.2 upgrade

m2m has changed in 4.19 and Xilinx has not yet updated their drivers to use the new registration mechanism. Must comment out `vdev->vfl_dir == VFL_DIR_M2M` check in `drivers/media/v4l2-core/v4l2-dev.c` to work around.

gstreamer 1.16.0 now names the first video convert found `v4l2convert`. The device can beoverridden by the `device` property.

##### Audio

Audio sounds good with 4.19 kernel (had chops with 4.14). Turn on F_UAC1 and CONFIGFS_F_UAC1, add this to configfs setup:
```
mkdir functions/uac1.usb0             
ln -s functions/uac1.usb0 configs/c.1
```

Also need kernel patch to work with usb 3.0.
Issue: Discontinuities and/or complete audio stream death when changing resolution/frame rate.

##### Audio Mixing

Tried on Eagle:
```
gst-launch-1.0 alsasrc device="hw:0,0" ! queue ! audiomixer name=mix ! queue ! alsasink device="hw:1,0" alsasrc device="hw:0,2" ! queue ! mix. alsasrc device="hw:0,4" ! queue ! mix. alsasrc device="hw:0,5" ! queue ! mix.
```
~8% cpu usage.

Added volume and level to each and did not seem to make much CPU impact (maybe 1% more):
```
gst-launch-1.0 alsasrc device="hw:0,0" ! level ! volume volume=0.8 ! audiomixer name=mix ! queue ! volume volume=0.9 ! alsasink device="hw:1,0" latency-time=50000 alsasrc device="hw:0,2" ! level ! volume volume=0.7 ! mix. alsasrc device="hw:0,4" ! level ! volume volume=0.45 ! mix. alsasrc device="hw:0,5" ! level ! volume volume=0.3 ! mix.
```

##### Audio latency

On Eagle: Recommend ~50ms latency addition to audio to match video. Example:
```
gst-launch-1.0 alsasrcvdevice="hw:0,0" ! queue ! audiomixer name=mix ! queue ! alsasink ^Catency-time=50000 alsasrc device="hw:0,2" ! queue ! mroot@vaddio-generic-7E-DB-DB-9D-80-E1:~# gst-launch-1.0 alsasrc device="hw:0,0" ! queue ! audiomixer name=mix ! queue ! alsasink device="hw:1,0" latency-time=50000 alsasrc device="hw:0,2" ! queue ! mix. alsasrc device="hw:0,4" ! queue ! mix. alsasrc device="hw:0,5" ! queue ! mix.
```

##### NV12 Streaming

Make sure 4:2:0 is enabled in VPSS and framebuffer write (FB write calls this)

Xilinx framebuffer driver keeps trying to use NV12M which is a non-contiguous format. I had to comment that format out of the `xilinx_frmbuf_formats` array in xilinx_frmbuf.c. Should figure out why this isn't working.

UVC just needs PIX_FMT_NV12 added to the `uvc_formats` in uvc_v4l2.c. Rest of UVC stuff can be set up by configfs by writing to NV12 to `guidFormat` (first 4 bytes only, I used dd) and 12 to `bBitsPerPixel` in configfs.

Ironically Windows is fine with everything, after restarting uvc-gadget once. Not sure what's up with that yet. Linux works in guvcview but shows RGB8. Turns out this is an emulated format from libv4l. Running with `qv4l2 -R` for "raw mode" bypasses this. Why does libv4l think this format needs to be emulated? Otherwise works fine in Teams and Zoom.

VLC can't handle it on Windows and Linux it seems.

- [ ] Figure out NV12/NV12M in xilinx_frmbuf.c
- [ ] libv4l emulation (is this actually a problem?)
- [ ] VLC crashes?

##### Eagle AI Pipeline

AI Pipeline:
```
v4l2-ctl -d /dev/v4l-subdev4 --set-dv-bt-timings index=7
v4l2-ctl -d /dev/v4l-subdev4 -c transmit_mode=1
media-ctl -v -V '"a0280000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"a0280000.v_proc_ss":1 [fmt:RBG888_1X24/640x480 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"adv7511 2-0039":0 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media2
media-ctl -v -V '"a0200000.v_proc_ss":0 [fmt:RBG888_1X24/640x480 field:none colorspace:srgb]' -d /dev/media2
media-ctl -v -V '"a0200000.v_proc_ss":1 [fmt:UYVY8_1X16/1280x720 field:none colorspace:srgb]' -d /dev/media2
gst-launch-1.0 v4l2src device=/dev/video3 do-timestamp=1 io-mode=4 ! video/x-raw,format=BGR,width=640,height=360,framerate=60/1 ! queue ! v4l2sink device=/dev/video1 io-mode=5

python3 people_counter.py --prototxt mobilenet_ssd/MobileNetSSD_deploy.prototxt --model mobilenet_ssd/MobileNetSSD_deploy.caffemodel --input "v4l2src device=/dev/video3 do-timestamp=true io-mode=4 ! video/x-raw,format=BGR,width=640,height=480 ! appsink max-buffers=1 drop=true" -s 1 -c .6 -o "appsrc is-live=true ! video/x-raw,format=BGR ! videoconvert ! video/x-raw,width=640,height=480,format=BGR ! v4l2sink device=/dev/video1 sync=false"
```

Gstreamer playthrough HDMI:
```
gst-launch-1.0 v4l2src device=/dev/video3 do-timestamp=true io-mode=4 ! video/x-raw,format=BGR,width=640,height=360 ! v4l2sink device=/dev/video1
```

### SMMU/IOMMU CCI

This page is helpful: https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842098/Zynq+UltraScale+MPSoC+Cache+Coherency#ZynqUltraScaleMPSoCCacheCoherency-2.1CacheCoherentInterconnect(CCI)

To turn on SMMU for USB etc:
1. Change smmu dts node status to "okay"
2. Add regs.init file to bootimage creation with .set. 0xFF41A040 = 0x3;
3. New FSBL build from Vivado build with PS->Advanced->CCI Enablement->Check: USB0, USB1, SD0, GEM
4. Add dma-coherent to dwc3_0 and dwc3_1.

Does seem to increase CPU usage substantially, however memory bandwidth is lower (1080p dropped from 850MB/s to 650MB/s).

##### USB UAC Debug

- Seems like extra complete request is coming in
- Has issue with only capture (on device, host playback) enabled - with device playback enabled things are all good

### Secure boot

https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842432/Loading+authenticated+and+or+encrypted+image+partitions+from+u-boot
https://www.timesys.com/security/secure-boot-encrypted-data-storage/

### GPT Boot

You can boot from a GPT partition table using a "Hybrid MBR":
https://www.rodsbooks.com/gdisk/hybrid.html
https://wiki.gentoo.org/wiki/Hybrid_partition_table
https://barower.github.io/embedded/2019/08/10/Booting_ZynqMP_With_GPT.html (manual steps, just use gdisk)

Basically, use gdisk to create a hybrid MBR in the "r, h" menu. The boot partition must be first in the list, so answer "n" when asked if the EE partition should be first.

Command to run automatically with gdisk (sgdisk does not have all options needed):
```
gdisk /dev/sdf <<EOF
r
h
1
n
0c
y
y
0a
w
y
y
EOF
```

Add the following to the wks file to generate a gpt image:
`bootloader --ptable gpt`

When transitioning vng, may want to create temporary partition to take place of extended partition so that the numbers don't change.

### H.264 software encoding

- x264enc appears to perform better than open264enc.
- `speed-preset` is *very* important - needs to be `ultrafast` or `superfast` for 1080p30.
- Denoise might help a little?
- Encoder takes 5-10s to start up, but once it is running latency is pretty good.
- Using xilinx m2m for colorspace conversion - could also use framebuffer write.
- Didn't notice a big difference between constrained-baseline and baseline, but main and high are definitely off the table (too much CPU). Baseline might actually have slightly lower CPU by 10-15%.

```
/etc/init.d/pyvideo-media-server stop
/etc/init.d/pyvideo-rtstream-server stop
/etc/init.d/iptables stop
media-ctl -v -V '"a0040000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]' -d /dev/media4
media-ctl -v -V '"a0040000.v_proc_ss":1 [fmt:VYYUYY8_1X24/1920x1080 field:none colorspace:srgb]' -d /dev/media4
rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 io-mode=4 do-timestamp=true ! video/x-raw,width=1920,height=1080,format=YUY2,framerate=60/1 ! videorate drop-only=true ! video/x-raw,framerate=30/1 ! queue leaky=2 ! v4l2convert output-io-mode=5 capture-io-mode=4 ! video/x-raw,format=NV12,width=1920,height=1080 ! queue ! x264enc speed-preset=ultrafast pass=quant quantizer=25 ! video/x-h264,profile=baseline ! queue ! rtph264pay perfect-rtptime=false pt=96 name=pay0 )"
```

Or with separate v4l2 endpoint:
```
/etc/init.d/iptables stop
media-ctl -v -V '"a0280000.v_proc_ss":0 [fmt:UYVY8_1X16/1920x1080 field:none colorspace:srgb]' -d /dev/media1
media-ctl -v -V '"a0280000.v_proc_ss":1 [fmt:VYYUYY8_1X24/1920x1080 field:none colorspace:srgb]' -d /dev/media1
rtsp-server -p 554 -u /stream "( v4l2src device=/dev/video0 io-mode=4 do-timestamp=true ! video/x-raw,width=1920,height=1080,format=NV12,framerate=60/1 ! queue leaky=2 ! videorate drop-only=true ! video/x-raw,framerate=30/1 ! queue ! x264enc speed-preset=ultrafast pass=quant quantizer=25 ! video/x-h264,profile=baseline ! queue ! rtph264pay perfect-rtptime=false pt=96 name=pay0 )"
```

### Very important notes

If the PS side locks up when connecting a debugger to the PL, try adding 'cpuidle.off=1' to the bootargs per [AR69143](https://www.xilinx.com/support/answers/69143.html).
```
setenv bootargs $bootargs cpuidle.off=1
boot
```

### References

[When should I use the VDMA and when should I use the Frame Buffer?](https://www.xilinx.com/support/answers/72543.html)
[How do you determine which pins correspond to a devicetree group on the ZynqMP?](https://forums.xilinx.com/t5/Embedded-Linux/How-do-you-determine-which-pins-correspond-to-a-devicetree-group/td-p/887722)

##### HLS

https://www.instructables.com/id/Vivado-HLS-Video-IP-Block-Synthesis/
http://users.ece.utexas.edu/~gerstl/ee382v_f14/soc/vivado_hls/VivadoHLS_Overview.pdf
