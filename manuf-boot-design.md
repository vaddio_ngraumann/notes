Eagle Manufacturing programming design

At Board-level test (requires USB jig or external USB0 connection):
1. Load 1st stage bootloader (BOOT-DFU.BIN) followed by full FSBL+u-boot (BOOT.BIN) over USB DFU.
2. u-boot loads and requests boot script over USB (could we build this in somehow?).
3. Boot script runs and does the following:
 - Requests minimal bootloader image (FAT partition+BOOT.BIN+boot.scr) over USB and writes to memory
 - Unpacks image to eMMC

Now that the eMMC has a bootloader, the full image can be programmed later. These steps are as follows:
1. u-boot boots and runs the default boot script (boot.scr).
2. Booting to eMMC fails, distro boot back to requesting a boot script over the network (if it fails, it falls back again to USB).
3. Boot script is downloaded and runs, which does the following:
 - Downloads the full gzip'ed image file from a TFTP server
 - Unpacks the full image to eMMC (this takes a few minutes)

