## Initial Build Setup

Search for "Kronos" in meta-vaddio, meta-vaddio-util. Then, duplicate for Shotzy.

global search for apex (remove all references)

Todo:

- [x] Create duplicate of Kronos items for Shotzy
- [x] Replace Kronos references in those files
- [x] Update first stage bootloader binary with Shotzy name (ok just to leave)

Resolve differences in rootfs:
- [x]] Middleware - base on Apex - only minimum set
- [x] /etc/writable-tab.d/gadget-writable.conf (Added by vng-mw-stream)
- [x] /opt/vaddio/var/data/system.json - set up by vng-mw-system
- [x] /var/lib/opkg/info/cpu1-app.list (Only empty dirs, seems benign)
- [x] /var/lib/opkg/info/flash-policy.list (Only empty dirs again)

Build script for artifacts:

    ./tools/build-scripts/vng-create-build-artifacts -w /home/nickg/workspace/vng/poky/ -m vaddio-kronos -d ~/workspace/artifacts -V all --include-firmware

If vng-mw-src causes issues with do_rootfs task "opkg_install_pkg:
Package packagegroup-vaddio-vng-mw-src md5sum mismatch.", run:

    bitbake packagegroup-vaddio-vng-mw-src -c cleanall

Broken after removing middleware and vng-machine-specific.

Missing update to product.py in vng-webapp-utils.

## System Package Tasks

Verification of functionality:

- [x] device-fw-updater: Files present on device, message about fx3 image loaded on boot
- [x] cpu1-app: Verified FPGA console at 9600 baud
- [x] vng-gst-rtsp-server: verified with VLC (needed to require vaddio-gst.inc as well)
- [ ] watchdog: Verified watched process restarts. Does not reset after kill -9?

## BLE chip/I2C1

I tried configuring the MIO pins for I2C1 manually to enable communication with
the BLE chip. I did this in u-boot. Note that the configuration registers need
to be unlocked first (the write to 0xF8000008 does this) and locked afterwards.

PULLUP: 0
IO_Type: 011
SPEED: 0
MIO16 (addr 0xF8000740): 0x00000640
MIO17 (addr 0xF8000744): 0x00000640

```
mw 0xF8000008 0xDF0D
mw 0xF8000740 0x00000640 1
mw 0xF8000744 0x00000640 1
mw 0xF8000004 0x767B

I2C1 with internal pull-up:
mw 0xF8000740 0x00001640 1
mw 0xF8000744 0x00001640 1

Tri-state SWDIO/SWCLK:
mw 0xF8000750 0x00001701
mw 0xF8000758 0x00001701
```
