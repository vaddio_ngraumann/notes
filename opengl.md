

### OpenGL ES with Gstreamer


Set up Mali for headless mode:
`update-alternatives --install /usr/lib/libMali.so.8.0 libmali /usr/lib/headless/libMali.so.8.0 90`

(Or if you want to use fbdev mode):
`update-alternatives --install /usr/lib/libMali.so.8.0 libmali /usr/lib/fbdev/libMali.so.8.0 90`

Various conversion pipelines:

```
gst-launch-1.0 -v v4l2src device=/dev/video0 do-timestamp=true io-mode=4 ! video/x-raw,width=1920,height=1080,format=YUY2,framerate=60/1 ! queue ! glupload ! glcolorconvert ! glcolorscale ! gldownload ! video/x-raw,width=1280,height=720 ! fpsdisplaysink text-overlay=false video-sink=fakesink

gst-launch-1.0 -v gltestsrc ! video/x-raw\(memory:GLMemory\),width=1920,height=1080,framerate=60/1 ! glcolorconvert ! glcolorscale ! gldownload ! video/x-raw,format=RGBA,width=1280,height=720,framerate=60/1 ! fpsdisplaysink text-overlay=false video-sink=fakesink

gst-launch-1.0 -v gltestsrc ! video/x-raw\(memory:GLMemory\),width=1280,height=720,framerate=60/1 ! glcolorconvert ! glcolorscale ! gldownload ! video/x-raw,format=RGBA,width=1920,height=1080,framerate=60/1 ! fpsdisplaysink text-overlay=false video-sink=fakesink

GST_DEBUG="GST_TRACER:7" gst-launch-1.0 -v gltestsrc num-buffers=200 ! video/x-raw\(memory:GLMemory\),width=3840,height=2160,frarate=30/1 ! gleffects_squeeze ! gldownload ! fakesink sync=true
```

##### Important changes

gst-plugins-bad:
commit 420a175b4f2a701f1c945027eb5c7041ab7d040a
Author: Nicolas Dufresne <nicolas.dufresne@collabora.com>
Date:   Fri Nov 13 17:24:30 2015 +0100

    glupload: Add dmabuf upload method.

    This upload method detect and optimize uploads of DMABuf memory. This is
    done by creating and caching EGLImages wrapper around DMABuf. The
    EGLImages are then binded to a texture which get converter using
    standard shader.

    Example pipeline:

    GST_GL_PLATFORM=egl \
    gst-launch-1.0 v4l2src device=/dev/video1 io-mode=4 ! \
                   video/x-raw,format=NV12 ! glimagesink

    https://bugzilla.gnome.org/show_bug.cgi?id=743345

### Dewarping

Be sure to use `GL_LINEAR` so interpolation looks good - with `GL_NEAREST` you get lots of jagged edges.

Very useful articles about dewarping using meshes and the vertex shader for performance:
https://www.imgtec.com/blog/speeding-up-gpu-barrel-distortion-correction-in-mobile-vr/

Does even/odd vertex number seem to make a differnce to performance? Nope.

Why does eglSwapInterval cause such a framerate hit (sometimes 30fps vs 55). I think this might be solved with threads (so we can dequeue bufs and render simultaneously) like gstreamer does with queues.

1080p dewarp looks (subectively) "good enough" with 16/9 horizontal and vertical vertex meshes respectively. Looks really good with 32/18. Higher would continue to give better image quality provided performance is adequate.

##### 4k testing with vivid:

be sure to run `v4l2-ctl --set-parm 60` once streaming has started. All tests run with: `eglfbdev -p -i 0`

4k in, 1080p viewport out
Meshes:
32x18: 45.9 fps
24x14: 47.4 fps
16x9:  48.4 fps

This leads me to believe we are fillrate limited more than mesh-limited at 4k60.
Also, this is still running the GPU at 500MHz.

### Ultrascale+ notes

[2016.4-2017.2 Zynq UltraScale+ MPSoC: Build and Run fbdev based Graphics Applications using PetaLinux](https://www.xilinx.com/support/answers/68821.html)
Seems to come from: https://forums.xilinx.com/t5/Embedded-Linux/OpenGL-on-zynqmp/m-p/765953#M19780
[zerocopy frame-buffer pixels from Mali GPU](https://forums.xilinx.com/t5/Embedded-Linux/zerocopy-frame-buffer-pixels-from-Mali-GPU-space-to-continous/td-p/926674)
https://www.xilinx.com/support/answers/71619.html

http://www.edlangley.co.uk/projects/opengl-streaming-textures/
