# To turn on debugging in the camera service:
/opt/vaddio/bin/dbus-send-helper -o -s CameraService -m vng_debug -p int32 5

# To watch debug messages:
tail -f /var/log/debug &

# To read the resolution register:
dbus-send --system --print-reply --dest=com.vaddio.CameraService /com/vaddio/CameraService/object com.vaddio.CameraService.visca_pass_thru int32:0 array:int32:0x81,9,4,0x24,0x72,0xff

# To set the resolution register (to 13; meanings vary by block):
dbus-send --system --print-reply --dest=com.vaddio.CameraService /com/vaddio/CameraService/object com.vaddio.CameraService.visca_pass_thru int32:0 array:int32:0x81,1,4,0x24,0x72,0x01,0x03,0xff

# To reset the block:
echo 0 >> /sys/class/gpio-vaddio/camera-reset/value           # wait a few seconds! then:
echo 1 >> /sys/class/gpio-vaddio/camera-reset/value
